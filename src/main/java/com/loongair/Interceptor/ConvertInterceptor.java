package com.loongair.Interceptor;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.DispatcherServlet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class ConvertInterceptor implements HandlerInterceptor {

    private String fromAppId;
    private String toAppId;
    private String methodName;
    private String securityKey;
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        fromAppId = httpServletRequest.getHeader("FromAppId");
        toAppId = httpServletRequest.getHeader("ToAppId");
        methodName = httpServletRequest.getHeader("MethodName");
        securityKey = httpServletRequest.getHeader("SecurityKey");
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddmmss");
        String dateStr = formatter.format(new Date());
        String secret = DigestUtils.md5Hex(dateStr+fromAppId+toAppId+methodName+securityKey);
        httpServletResponse.setHeader("Secret",secret);
        httpServletResponse.setHeader("FromAppId",fromAppId);
        httpServletResponse.setHeader("ToAppId",toAppId);
        httpServletResponse.setHeader("MethodName",methodName);
    }
}
