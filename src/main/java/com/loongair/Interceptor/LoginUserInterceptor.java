package com.loongair.Interceptor;

import com.loongair.Exception.NoLoginException;
import com.loongair.Model.base.SessionInfo;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Create By Hong Cui
 * 2017/10/30
 */
public class LoginUserInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        SessionInfo sessionInfo=(SessionInfo)httpServletRequest.getSession().getAttribute("SessionInfo");
        if(sessionInfo==null||sessionInfo.LoginUser==null)
        {
            throw new NoLoginException(1,"未登入");
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
