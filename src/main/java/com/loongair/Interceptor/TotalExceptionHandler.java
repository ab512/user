package com.loongair.Interceptor;

import com.google.gson.Gson;
import com.loongair.Exception.ExceptionBase;
import com.loongair.Exception.NoLoginException;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Create By Hong Cui
 * 2017/10/30
 */
public class TotalExceptionHandler implements HandlerExceptionResolver {
    @Override
    public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {
        Logger.getLogger(TotalExceptionHandler.class).error("TotalException:"+e.getMessage());
        if(e instanceof ExceptionBase)
        {
            try {
                httpServletResponse.getWriter().write(new Gson().toJson(e));
            } catch (IOException e1) {
                e1.printStackTrace();
                Logger.getLogger(TotalExceptionHandler.class).error("TotalException:"+e1.getMessage());
            }
        }
        else {
            try {
                httpServletResponse.getWriter().write(new Gson().toJson(new ExceptionBase(1,"服务器内部错误")));
            } catch (IOException e1) {
                e1.printStackTrace();
                Logger.getLogger(TotalExceptionHandler.class).error("TotalException:"+e1.getMessage());
            }
        }
//        return new ModelAndView("Error");
        return null;
    }
}
