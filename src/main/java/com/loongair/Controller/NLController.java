package com.loongair.Controller;

import com.loongair.Controller.base.BaseController;
import com.loongair.DB.Entity.Lauser;
import com.loongair.Model.GetUserByLoginNameAndPasswordPara;
import com.loongair.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Create By Hong Cui
 * 2017-10-31
 */
@Controller
public class NLController extends BaseController {
    @Autowired
    private UserService userService;
    @RequestMapping(value = "login")
    @ResponseBody
    public Lauser getUserByLoginNameAndPassowrd(@RequestBody GetUserByLoginNameAndPasswordPara para){
        return userService.getUserByLoginNameAndPassword(para);
    }
}
