package com.loongair.Controller;

import com.loongair.Controller.base.BaseController;
import com.loongair.DB.Entity.Orgagent;
import com.loongair.Model.AddAgentPara;
import com.loongair.Model.Enum;
import com.loongair.Model.base.ModelResultBase;
import com.loongair.Business.OrgagentBusiness;
import com.loongair.Util.IdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Controller
@RequestMapping(value = "Orgagent")
public class OrgagentController extends BaseController {

    @Autowired
    private OrgagentBusiness modelBusiness;

    /**
     * 添加代理人机构
     * @param para
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/add")
    public ModelResultBase AddOrgAgent(@RequestBody AddAgentPara para, HttpServletRequest request) {
        modelBusiness.AddOrgAgent(para);
        return new ModelResultBase();
    }
}
