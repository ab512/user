package com.loongair.Controller;

import com.loongair.Controller.base.BaseController;
import com.loongair.Business.BcstaffBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Create By Hong Cui
 * 2017-10-31
 */
@Controller
@RequestMapping(value = "Bcstaff")
public class BcstaffController extends BaseController {
    @Autowired
    private BcstaffBusiness BcstaffBusiness;

}
