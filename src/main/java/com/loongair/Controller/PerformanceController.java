package com.loongair.Controller;

import com.loongair.Controller.base.BaseController;
import com.loongair.Business.PerformanceBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "Performance")
public class PerformanceController extends BaseController {

    @Autowired
    private PerformanceBusiness modelBusiness;
}
