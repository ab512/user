package com.loongair.Controller;

import com.loongair.Controller.base.BaseController;
import com.loongair.Business.OrganizationBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "Organization")
public class OrganizationController extends BaseController {

    @Autowired
    private OrganizationBusiness modelBusiness;
}
