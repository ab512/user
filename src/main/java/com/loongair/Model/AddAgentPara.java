package com.loongair.Model;

import com.loongair.Model.base.ModelParaBase;
import com.loongair.Model.Enum.*;

import javax.validation.constraints.NotNull;

/**
 * Create By Hong Cui
 * 2017-11-13
 */
public class AddAgentPara extends ModelParaBase{
    /// <summary>
    /// OFFICECODE
    /// </summary>
    public String  AC_OfficeCode;

    /// <summary>
    /// 代理人名称
    /// </summary>
    public String AC_AgentCompanyName;

    /// <summary>
    /// IATANO
    /// </summary>
    public String AC_IATANO;
    /// <summary>
    /// 所属营业部
    /// </summary>
    public int AC_BelongDepartment;
    /// <summary>
    /// 用户类型
    /// </summary>
    public int AC_AgentUserType;
    /// <summary>
    /// 是否启用
    /// </summary>
    public int Status;
    /// <summary>
    /// 货币种类
    /// </summary>
    public int AC_MoneyType;
    /// <summary>
    /// 所属国家或地区
    /// </summary>
    public int AC_Country;
    /// <summary>
    /// 所在城市
    /// </summary>
    public String AC_City;
    /// <summary>
    /// 权限
    /// </summary>
    public String AC_Permission;
    /// <summary>
    /// 国际代理费率
    /// </summary>
    public float AC_IAgencyRate;
    /// <summary>
    /// 国内代理费率
    /// </summary>
    public float AC_DAgencyRate;
    /// <summary>
    /// 团队订座编码
    /// </summary>
    public String AC_TeamReservationCode;
    /// <summary>
    /// 联系人
    /// </summary>
    public String AC_Contact;
    /// <summary>
    /// 联系人办公电话
    /// </summary>
    public String AC_Telephone;
    /// <summary>
    /// 联系人手机
    /// </summary>
    public String AC_ContactPhone;
    /// <summary>
    /// 电子邮件
    /// </summary>
    public String AC_Email;
    /// <summary>
    /// 地址
    /// </summary>
    public String AC_Address;
    /// <summary>
    /// 邮编
    /// </summary>
    public String AC_ZipCode;
    /// <summary>
    /// 传真
    /// </summary>
    public String AC_Fax;
}
