package com.loongair.Model.base;

/**
 * Create By Hong Cui
 * 2017/10/30
 */
public class LoginUser extends ModelBase  {
public long UserId;
public long CompanyId ;
public String Name ;
public String LoginName ;
public String WorkNum ;
public String Mobile ;
public String Email ;
public String[] Permissions ;
public String Sign ;
public String BU_WeChatUnionId;
public boolean AccountExit;
}
