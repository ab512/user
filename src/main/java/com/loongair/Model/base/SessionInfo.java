package com.loongair.Model.base;

import java.util.Date;
import java.util.HashMap;

/**
 * Create By Hong Cui
 * 2017/10/30
 */
public class SessionInfo extends HashMap<Object,Object>{
    public String SessionId;
    public LoginUser LoginUser;
    public String IP;
    public Date Deadline;

    public class VerifyCode
    {
        public String Code;
        public Date DeadLineTime;
        public int LeftVerifyCount;
    }
}
