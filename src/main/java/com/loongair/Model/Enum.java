package com.loongair.Model;

/**
 * Create By Hong Cui
 * 2017-11-13
 */
public class Enum {
    public class BelongDepartment {
        public final static int Department1 = 1;
        public final static int Department2 = 2;
    }

    public class AgentUserType {
        public final static int User1 = 1;
        public final static int User2 = 2;
    }

    public class ActiveStatus {
        /// <summary>
        /// 启用
        /// </summary>
        public final static int Enable = 1;
        /// <summary>
        /// 禁用
        /// </summary>
        public final static int Disable = 2;
        /// <summary>
        /// 删除
        /// </summary>
        public final static int Deleted = 3;
    }

    public class MoneyType {
        public final static int Dollar = 1;
        public final static int RMB = 2;
    }

    public class Country {
        ///中国
        public final static int China = 1;
        ///美国
        public final static int USA = 2;
        ///法国
        public final static int France = 3;
        ///德国
        public final static int Deutschland = 4;
        ///加拿大
        public final static int Canada = 5;
        ///欧盟
        public final static int EuropeanUnion = 6;
        ///英联邦
        public final static int UnitedKingdom = 7;
        ///澳大利亚
        public final static int Australia = 8;
        ///日本
        public final static int Japan = 9;
        ///韩国
        public final static int Korea = 10;
    }
}
