package com.loongair.Exception;

/**
 * Create By Hong Cui
 * 2017/10/30
 */
public  class ExceptionBase extends Exception {
    public int ErrorCode;
    public String Result;
    public ExceptionBase(String messeage){
        super(messeage);
        Result=messeage;
    }
    public ExceptionBase(int errorCode,String messeage){
        super(messeage);
        Result=messeage;
        ErrorCode=errorCode;
    }
}
