package com.loongair.Exception;

/**
 * Create By Hong Cui
 * 2017/10/30
 */
public class NoLoginException extends ExceptionBase {

    public NoLoginException(String messeage) {
        super(messeage);
    }

    public NoLoginException(int errorCode, String messeage) {
        super(errorCode, messeage);
    }
}
