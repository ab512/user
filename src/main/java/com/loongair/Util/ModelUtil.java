package com.loongair.Util;

import java.lang.reflect.Field;

/**
 * Create By Hong Cui
 * 2017-11-2
 */
public class ModelUtil {
    public static Object convertModel(Object s, Object t) {
        Field[] sFields = s.getClass().getDeclaredFields();
        Class tClazz = t.getClass();
        for (Field f : sFields) {
            try {
                Field tf=tClazz.getField(f.getName());
                if(tf!=null)
                {
                    tf.set(t,f.get(s));
                }
            } catch (NoSuchFieldException e) {
            } catch (IllegalAccessException e) {
            }
        }
        return t;
    }
}
