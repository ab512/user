package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Presentcard extends BaseEntity {


    private Integer gsCardtype;

    private String gsCustomercode;

    private String gsCardcode;

    private String gsCardholder;

    private String gsDkhrcode;

    private String gsOpetator;



    public Integer getGsCardtype() {
        return gsCardtype;
    }

    public void setGsCardtype(Integer gsCardtype) {
        this.gsCardtype = gsCardtype;
    }

    public String getGsCustomercode() {
        return gsCustomercode;
    }

    public void setGsCustomercode(String gsCustomercode) {
        this.gsCustomercode = gsCustomercode == null ? null : gsCustomercode.trim();
    }

    public String getGsCardcode() {
        return gsCardcode;
    }

    public void setGsCardcode(String gsCardcode) {
        this.gsCardcode = gsCardcode == null ? null : gsCardcode.trim();
    }

    public String getGsCardholder() {
        return gsCardholder;
    }

    public void setGsCardholder(String gsCardholder) {
        this.gsCardholder = gsCardholder == null ? null : gsCardholder.trim();
    }

    public String getGsDkhrcode() {
        return gsDkhrcode;
    }

    public void setGsDkhrcode(String gsDkhrcode) {
        this.gsDkhrcode = gsDkhrcode == null ? null : gsDkhrcode.trim();
    }

    public String getGsOpetator() {
        return gsOpetator;
    }

    public void setGsOpetator(String gsOpetator) {
        this.gsOpetator = gsOpetator == null ? null : gsOpetator.trim();
    }
}