package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Wechatinfo extends BaseEntity {


    private Long wciLausertid;

    private String wciAppid;

    private String wciAccesstoken;

    private Date wciAccesstokenexpires;

    private String wciRefreshtoken;

    private Date wciRefreshtokenexpires;

    private String wciOpenid;

    private String wciScope;

    private String wciUnionid;

    private Integer wciLoginplat;



    public Long getWciLausertid() {
        return wciLausertid;
    }

    public void setWciLausertid(Long wciLausertid) {
        this.wciLausertid = wciLausertid;
    }

    public String getWciAppid() {
        return wciAppid;
    }

    public void setWciAppid(String wciAppid) {
        this.wciAppid = wciAppid == null ? null : wciAppid.trim();
    }

    public String getWciAccesstoken() {
        return wciAccesstoken;
    }

    public void setWciAccesstoken(String wciAccesstoken) {
        this.wciAccesstoken = wciAccesstoken == null ? null : wciAccesstoken.trim();
    }

    public Date getWciAccesstokenexpires() {
        return wciAccesstokenexpires;
    }

    public void setWciAccesstokenexpires(Date wciAccesstokenexpires) {
        this.wciAccesstokenexpires = wciAccesstokenexpires;
    }

    public String getWciRefreshtoken() {
        return wciRefreshtoken;
    }

    public void setWciRefreshtoken(String wciRefreshtoken) {
        this.wciRefreshtoken = wciRefreshtoken == null ? null : wciRefreshtoken.trim();
    }

    public Date getWciRefreshtokenexpires() {
        return wciRefreshtokenexpires;
    }

    public void setWciRefreshtokenexpires(Date wciRefreshtokenexpires) {
        this.wciRefreshtokenexpires = wciRefreshtokenexpires;
    }

    public String getWciOpenid() {
        return wciOpenid;
    }

    public void setWciOpenid(String wciOpenid) {
        this.wciOpenid = wciOpenid == null ? null : wciOpenid.trim();
    }

    public String getWciScope() {
        return wciScope;
    }

    public void setWciScope(String wciScope) {
        this.wciScope = wciScope == null ? null : wciScope.trim();
    }

    public String getWciUnionid() {
        return wciUnionid;
    }

    public void setWciUnionid(String wciUnionid) {
        this.wciUnionid = wciUnionid == null ? null : wciUnionid.trim();
    }

    public Integer getWciLoginplat() {
        return wciLoginplat;
    }

    public void setWciLoginplat(Integer wciLoginplat) {
        this.wciLoginplat = wciLoginplat;
    }
}