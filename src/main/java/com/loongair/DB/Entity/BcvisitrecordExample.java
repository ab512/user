package com.loongair.DB.Entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BcvisitrecordExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BcvisitrecordExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTidIsNull() {
            addCriterion("Tid is null");
            return (Criteria) this;
        }

        public Criteria andTidIsNotNull() {
            addCriterion("Tid is not null");
            return (Criteria) this;
        }

        public Criteria andTidEqualTo(Long value) {
            addCriterion("Tid =", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotEqualTo(Long value) {
            addCriterion("Tid <>", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThan(Long value) {
            addCriterion("Tid >", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThanOrEqualTo(Long value) {
            addCriterion("Tid >=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThan(Long value) {
            addCriterion("Tid <", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThanOrEqualTo(Long value) {
            addCriterion("Tid <=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidIn(List<Long> values) {
            addCriterion("Tid in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotIn(List<Long> values) {
            addCriterion("Tid not in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidBetween(Long value1, Long value2) {
            addCriterion("Tid between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotBetween(Long value1, Long value2) {
            addCriterion("Tid not between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNull() {
            addCriterion("CreateId is null");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNotNull() {
            addCriterion("CreateId is not null");
            return (Criteria) this;
        }

        public Criteria andCreateidEqualTo(Long value) {
            addCriterion("CreateId =", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotEqualTo(Long value) {
            addCriterion("CreateId <>", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThan(Long value) {
            addCriterion("CreateId >", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThanOrEqualTo(Long value) {
            addCriterion("CreateId >=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThan(Long value) {
            addCriterion("CreateId <", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThanOrEqualTo(Long value) {
            addCriterion("CreateId <=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidIn(List<Long> values) {
            addCriterion("CreateId in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotIn(List<Long> values) {
            addCriterion("CreateId not in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidBetween(Long value1, Long value2) {
            addCriterion("CreateId between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotBetween(Long value1, Long value2) {
            addCriterion("CreateId not between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNull() {
            addCriterion("ProxyUserId is null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNotNull() {
            addCriterion("ProxyUserId is not null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridEqualTo(Long value) {
            addCriterion("ProxyUserId =", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotEqualTo(Long value) {
            addCriterion("ProxyUserId <>", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThan(Long value) {
            addCriterion("ProxyUserId >", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId >=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThan(Long value) {
            addCriterion("ProxyUserId <", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId <=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIn(List<Long> values) {
            addCriterion("ProxyUserId in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotIn(List<Long> values) {
            addCriterion("ProxyUserId not in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId not between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNull() {
            addCriterion("LastGuid is null");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNotNull() {
            addCriterion("LastGuid is not null");
            return (Criteria) this;
        }

        public Criteria andLastguidEqualTo(Long value) {
            addCriterion("LastGuid =", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotEqualTo(Long value) {
            addCriterion("LastGuid <>", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThan(Long value) {
            addCriterion("LastGuid >", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThanOrEqualTo(Long value) {
            addCriterion("LastGuid >=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThan(Long value) {
            addCriterion("LastGuid <", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThanOrEqualTo(Long value) {
            addCriterion("LastGuid <=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidIn(List<Long> values) {
            addCriterion("LastGuid in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotIn(List<Long> values) {
            addCriterion("LastGuid not in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidBetween(Long value1, Long value2) {
            addCriterion("LastGuid between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotBetween(Long value1, Long value2) {
            addCriterion("LastGuid not between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andCloneIsNull() {
            addCriterion("Clone is null");
            return (Criteria) this;
        }

        public Criteria andCloneIsNotNull() {
            addCriterion("Clone is not null");
            return (Criteria) this;
        }

        public Criteria andCloneEqualTo(Integer value) {
            addCriterion("Clone =", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotEqualTo(Integer value) {
            addCriterion("Clone <>", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThan(Integer value) {
            addCriterion("Clone >", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThanOrEqualTo(Integer value) {
            addCriterion("Clone >=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThan(Integer value) {
            addCriterion("Clone <", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThanOrEqualTo(Integer value) {
            addCriterion("Clone <=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneIn(List<Integer> values) {
            addCriterion("Clone in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotIn(List<Integer> values) {
            addCriterion("Clone not in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneBetween(Integer value1, Integer value2) {
            addCriterion("Clone between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotBetween(Integer value1, Integer value2) {
            addCriterion("Clone not between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNull() {
            addCriterion("UpdateId is null");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNotNull() {
            addCriterion("UpdateId is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateidEqualTo(Long value) {
            addCriterion("UpdateId =", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotEqualTo(Long value) {
            addCriterion("UpdateId <>", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThan(Long value) {
            addCriterion("UpdateId >", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThanOrEqualTo(Long value) {
            addCriterion("UpdateId >=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThan(Long value) {
            addCriterion("UpdateId <", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThanOrEqualTo(Long value) {
            addCriterion("UpdateId <=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidIn(List<Long> values) {
            addCriterion("UpdateId in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotIn(List<Long> values) {
            addCriterion("UpdateId not in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidBetween(Long value1, Long value2) {
            addCriterion("UpdateId between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotBetween(Long value1, Long value2) {
            addCriterion("UpdateId not between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("UpdateDate is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("UpdateDate is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("UpdateDate =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("UpdateDate <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("UpdateDate >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("UpdateDate >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("UpdateDate <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("UpdateDate <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("UpdateDate in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("UpdateDate not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("UpdateDate between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("UpdateDate not between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("Status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("Status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("Status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("Status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("Status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("Status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("Status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("Status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("Status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("Status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("Status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("Status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andCvSerialnumberIsNull() {
            addCriterion("CV_SerialNumber is null");
            return (Criteria) this;
        }

        public Criteria andCvSerialnumberIsNotNull() {
            addCriterion("CV_SerialNumber is not null");
            return (Criteria) this;
        }

        public Criteria andCvSerialnumberEqualTo(String value) {
            addCriterion("CV_SerialNumber =", value, "cvSerialnumber");
            return (Criteria) this;
        }

        public Criteria andCvSerialnumberNotEqualTo(String value) {
            addCriterion("CV_SerialNumber <>", value, "cvSerialnumber");
            return (Criteria) this;
        }

        public Criteria andCvSerialnumberGreaterThan(String value) {
            addCriterion("CV_SerialNumber >", value, "cvSerialnumber");
            return (Criteria) this;
        }

        public Criteria andCvSerialnumberGreaterThanOrEqualTo(String value) {
            addCriterion("CV_SerialNumber >=", value, "cvSerialnumber");
            return (Criteria) this;
        }

        public Criteria andCvSerialnumberLessThan(String value) {
            addCriterion("CV_SerialNumber <", value, "cvSerialnumber");
            return (Criteria) this;
        }

        public Criteria andCvSerialnumberLessThanOrEqualTo(String value) {
            addCriterion("CV_SerialNumber <=", value, "cvSerialnumber");
            return (Criteria) this;
        }

        public Criteria andCvSerialnumberLike(String value) {
            addCriterion("CV_SerialNumber like", value, "cvSerialnumber");
            return (Criteria) this;
        }

        public Criteria andCvSerialnumberNotLike(String value) {
            addCriterion("CV_SerialNumber not like", value, "cvSerialnumber");
            return (Criteria) this;
        }

        public Criteria andCvSerialnumberIn(List<String> values) {
            addCriterion("CV_SerialNumber in", values, "cvSerialnumber");
            return (Criteria) this;
        }

        public Criteria andCvSerialnumberNotIn(List<String> values) {
            addCriterion("CV_SerialNumber not in", values, "cvSerialnumber");
            return (Criteria) this;
        }

        public Criteria andCvSerialnumberBetween(String value1, String value2) {
            addCriterion("CV_SerialNumber between", value1, value2, "cvSerialnumber");
            return (Criteria) this;
        }

        public Criteria andCvSerialnumberNotBetween(String value1, String value2) {
            addCriterion("CV_SerialNumber not between", value1, value2, "cvSerialnumber");
            return (Criteria) this;
        }

        public Criteria andCvBccodeIsNull() {
            addCriterion("CV_BCCode is null");
            return (Criteria) this;
        }

        public Criteria andCvBccodeIsNotNull() {
            addCriterion("CV_BCCode is not null");
            return (Criteria) this;
        }

        public Criteria andCvBccodeEqualTo(String value) {
            addCriterion("CV_BCCode =", value, "cvBccode");
            return (Criteria) this;
        }

        public Criteria andCvBccodeNotEqualTo(String value) {
            addCriterion("CV_BCCode <>", value, "cvBccode");
            return (Criteria) this;
        }

        public Criteria andCvBccodeGreaterThan(String value) {
            addCriterion("CV_BCCode >", value, "cvBccode");
            return (Criteria) this;
        }

        public Criteria andCvBccodeGreaterThanOrEqualTo(String value) {
            addCriterion("CV_BCCode >=", value, "cvBccode");
            return (Criteria) this;
        }

        public Criteria andCvBccodeLessThan(String value) {
            addCriterion("CV_BCCode <", value, "cvBccode");
            return (Criteria) this;
        }

        public Criteria andCvBccodeLessThanOrEqualTo(String value) {
            addCriterion("CV_BCCode <=", value, "cvBccode");
            return (Criteria) this;
        }

        public Criteria andCvBccodeLike(String value) {
            addCriterion("CV_BCCode like", value, "cvBccode");
            return (Criteria) this;
        }

        public Criteria andCvBccodeNotLike(String value) {
            addCriterion("CV_BCCode not like", value, "cvBccode");
            return (Criteria) this;
        }

        public Criteria andCvBccodeIn(List<String> values) {
            addCriterion("CV_BCCode in", values, "cvBccode");
            return (Criteria) this;
        }

        public Criteria andCvBccodeNotIn(List<String> values) {
            addCriterion("CV_BCCode not in", values, "cvBccode");
            return (Criteria) this;
        }

        public Criteria andCvBccodeBetween(String value1, String value2) {
            addCriterion("CV_BCCode between", value1, value2, "cvBccode");
            return (Criteria) this;
        }

        public Criteria andCvBccodeNotBetween(String value1, String value2) {
            addCriterion("CV_BCCode not between", value1, value2, "cvBccode");
            return (Criteria) this;
        }

        public Criteria andCvBcnameIsNull() {
            addCriterion("CV_BCName is null");
            return (Criteria) this;
        }

        public Criteria andCvBcnameIsNotNull() {
            addCriterion("CV_BCName is not null");
            return (Criteria) this;
        }

        public Criteria andCvBcnameEqualTo(String value) {
            addCriterion("CV_BCName =", value, "cvBcname");
            return (Criteria) this;
        }

        public Criteria andCvBcnameNotEqualTo(String value) {
            addCriterion("CV_BCName <>", value, "cvBcname");
            return (Criteria) this;
        }

        public Criteria andCvBcnameGreaterThan(String value) {
            addCriterion("CV_BCName >", value, "cvBcname");
            return (Criteria) this;
        }

        public Criteria andCvBcnameGreaterThanOrEqualTo(String value) {
            addCriterion("CV_BCName >=", value, "cvBcname");
            return (Criteria) this;
        }

        public Criteria andCvBcnameLessThan(String value) {
            addCriterion("CV_BCName <", value, "cvBcname");
            return (Criteria) this;
        }

        public Criteria andCvBcnameLessThanOrEqualTo(String value) {
            addCriterion("CV_BCName <=", value, "cvBcname");
            return (Criteria) this;
        }

        public Criteria andCvBcnameLike(String value) {
            addCriterion("CV_BCName like", value, "cvBcname");
            return (Criteria) this;
        }

        public Criteria andCvBcnameNotLike(String value) {
            addCriterion("CV_BCName not like", value, "cvBcname");
            return (Criteria) this;
        }

        public Criteria andCvBcnameIn(List<String> values) {
            addCriterion("CV_BCName in", values, "cvBcname");
            return (Criteria) this;
        }

        public Criteria andCvBcnameNotIn(List<String> values) {
            addCriterion("CV_BCName not in", values, "cvBcname");
            return (Criteria) this;
        }

        public Criteria andCvBcnameBetween(String value1, String value2) {
            addCriterion("CV_BCName between", value1, value2, "cvBcname");
            return (Criteria) this;
        }

        public Criteria andCvBcnameNotBetween(String value1, String value2) {
            addCriterion("CV_BCName not between", value1, value2, "cvBcname");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeIsNull() {
            addCriterion("OBC_CustomerType is null");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeIsNotNull() {
            addCriterion("OBC_CustomerType is not null");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeEqualTo(Integer value) {
            addCriterion("OBC_CustomerType =", value, "obcCustomertype");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeNotEqualTo(Integer value) {
            addCriterion("OBC_CustomerType <>", value, "obcCustomertype");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeGreaterThan(Integer value) {
            addCriterion("OBC_CustomerType >", value, "obcCustomertype");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("OBC_CustomerType >=", value, "obcCustomertype");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeLessThan(Integer value) {
            addCriterion("OBC_CustomerType <", value, "obcCustomertype");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeLessThanOrEqualTo(Integer value) {
            addCriterion("OBC_CustomerType <=", value, "obcCustomertype");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeIn(List<Integer> values) {
            addCriterion("OBC_CustomerType in", values, "obcCustomertype");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeNotIn(List<Integer> values) {
            addCriterion("OBC_CustomerType not in", values, "obcCustomertype");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeBetween(Integer value1, Integer value2) {
            addCriterion("OBC_CustomerType between", value1, value2, "obcCustomertype");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeNotBetween(Integer value1, Integer value2) {
            addCriterion("OBC_CustomerType not between", value1, value2, "obcCustomertype");
            return (Criteria) this;
        }

        public Criteria andCvVisittimeIsNull() {
            addCriterion("CV_VisitTime is null");
            return (Criteria) this;
        }

        public Criteria andCvVisittimeIsNotNull() {
            addCriterion("CV_VisitTime is not null");
            return (Criteria) this;
        }

        public Criteria andCvVisittimeEqualTo(Date value) {
            addCriterion("CV_VisitTime =", value, "cvVisittime");
            return (Criteria) this;
        }

        public Criteria andCvVisittimeNotEqualTo(Date value) {
            addCriterion("CV_VisitTime <>", value, "cvVisittime");
            return (Criteria) this;
        }

        public Criteria andCvVisittimeGreaterThan(Date value) {
            addCriterion("CV_VisitTime >", value, "cvVisittime");
            return (Criteria) this;
        }

        public Criteria andCvVisittimeGreaterThanOrEqualTo(Date value) {
            addCriterion("CV_VisitTime >=", value, "cvVisittime");
            return (Criteria) this;
        }

        public Criteria andCvVisittimeLessThan(Date value) {
            addCriterion("CV_VisitTime <", value, "cvVisittime");
            return (Criteria) this;
        }

        public Criteria andCvVisittimeLessThanOrEqualTo(Date value) {
            addCriterion("CV_VisitTime <=", value, "cvVisittime");
            return (Criteria) this;
        }

        public Criteria andCvVisittimeIn(List<Date> values) {
            addCriterion("CV_VisitTime in", values, "cvVisittime");
            return (Criteria) this;
        }

        public Criteria andCvVisittimeNotIn(List<Date> values) {
            addCriterion("CV_VisitTime not in", values, "cvVisittime");
            return (Criteria) this;
        }

        public Criteria andCvVisittimeBetween(Date value1, Date value2) {
            addCriterion("CV_VisitTime between", value1, value2, "cvVisittime");
            return (Criteria) this;
        }

        public Criteria andCvVisittimeNotBetween(Date value1, Date value2) {
            addCriterion("CV_VisitTime not between", value1, value2, "cvVisittime");
            return (Criteria) this;
        }

        public Criteria andCvContactpeopleIsNull() {
            addCriterion("CV_ContactPeople is null");
            return (Criteria) this;
        }

        public Criteria andCvContactpeopleIsNotNull() {
            addCriterion("CV_ContactPeople is not null");
            return (Criteria) this;
        }

        public Criteria andCvContactpeopleEqualTo(String value) {
            addCriterion("CV_ContactPeople =", value, "cvContactpeople");
            return (Criteria) this;
        }

        public Criteria andCvContactpeopleNotEqualTo(String value) {
            addCriterion("CV_ContactPeople <>", value, "cvContactpeople");
            return (Criteria) this;
        }

        public Criteria andCvContactpeopleGreaterThan(String value) {
            addCriterion("CV_ContactPeople >", value, "cvContactpeople");
            return (Criteria) this;
        }

        public Criteria andCvContactpeopleGreaterThanOrEqualTo(String value) {
            addCriterion("CV_ContactPeople >=", value, "cvContactpeople");
            return (Criteria) this;
        }

        public Criteria andCvContactpeopleLessThan(String value) {
            addCriterion("CV_ContactPeople <", value, "cvContactpeople");
            return (Criteria) this;
        }

        public Criteria andCvContactpeopleLessThanOrEqualTo(String value) {
            addCriterion("CV_ContactPeople <=", value, "cvContactpeople");
            return (Criteria) this;
        }

        public Criteria andCvContactpeopleLike(String value) {
            addCriterion("CV_ContactPeople like", value, "cvContactpeople");
            return (Criteria) this;
        }

        public Criteria andCvContactpeopleNotLike(String value) {
            addCriterion("CV_ContactPeople not like", value, "cvContactpeople");
            return (Criteria) this;
        }

        public Criteria andCvContactpeopleIn(List<String> values) {
            addCriterion("CV_ContactPeople in", values, "cvContactpeople");
            return (Criteria) this;
        }

        public Criteria andCvContactpeopleNotIn(List<String> values) {
            addCriterion("CV_ContactPeople not in", values, "cvContactpeople");
            return (Criteria) this;
        }

        public Criteria andCvContactpeopleBetween(String value1, String value2) {
            addCriterion("CV_ContactPeople between", value1, value2, "cvContactpeople");
            return (Criteria) this;
        }

        public Criteria andCvContactpeopleNotBetween(String value1, String value2) {
            addCriterion("CV_ContactPeople not between", value1, value2, "cvContactpeople");
            return (Criteria) this;
        }

        public Criteria andCvPhonenumberIsNull() {
            addCriterion("CV_PhoneNumber is null");
            return (Criteria) this;
        }

        public Criteria andCvPhonenumberIsNotNull() {
            addCriterion("CV_PhoneNumber is not null");
            return (Criteria) this;
        }

        public Criteria andCvPhonenumberEqualTo(String value) {
            addCriterion("CV_PhoneNumber =", value, "cvPhonenumber");
            return (Criteria) this;
        }

        public Criteria andCvPhonenumberNotEqualTo(String value) {
            addCriterion("CV_PhoneNumber <>", value, "cvPhonenumber");
            return (Criteria) this;
        }

        public Criteria andCvPhonenumberGreaterThan(String value) {
            addCriterion("CV_PhoneNumber >", value, "cvPhonenumber");
            return (Criteria) this;
        }

        public Criteria andCvPhonenumberGreaterThanOrEqualTo(String value) {
            addCriterion("CV_PhoneNumber >=", value, "cvPhonenumber");
            return (Criteria) this;
        }

        public Criteria andCvPhonenumberLessThan(String value) {
            addCriterion("CV_PhoneNumber <", value, "cvPhonenumber");
            return (Criteria) this;
        }

        public Criteria andCvPhonenumberLessThanOrEqualTo(String value) {
            addCriterion("CV_PhoneNumber <=", value, "cvPhonenumber");
            return (Criteria) this;
        }

        public Criteria andCvPhonenumberLike(String value) {
            addCriterion("CV_PhoneNumber like", value, "cvPhonenumber");
            return (Criteria) this;
        }

        public Criteria andCvPhonenumberNotLike(String value) {
            addCriterion("CV_PhoneNumber not like", value, "cvPhonenumber");
            return (Criteria) this;
        }

        public Criteria andCvPhonenumberIn(List<String> values) {
            addCriterion("CV_PhoneNumber in", values, "cvPhonenumber");
            return (Criteria) this;
        }

        public Criteria andCvPhonenumberNotIn(List<String> values) {
            addCriterion("CV_PhoneNumber not in", values, "cvPhonenumber");
            return (Criteria) this;
        }

        public Criteria andCvPhonenumberBetween(String value1, String value2) {
            addCriterion("CV_PhoneNumber between", value1, value2, "cvPhonenumber");
            return (Criteria) this;
        }

        public Criteria andCvPhonenumberNotBetween(String value1, String value2) {
            addCriterion("CV_PhoneNumber not between", value1, value2, "cvPhonenumber");
            return (Criteria) this;
        }

        public Criteria andCvVisitrecordIsNull() {
            addCriterion("CV_VisitRecord is null");
            return (Criteria) this;
        }

        public Criteria andCvVisitrecordIsNotNull() {
            addCriterion("CV_VisitRecord is not null");
            return (Criteria) this;
        }

        public Criteria andCvVisitrecordEqualTo(String value) {
            addCriterion("CV_VisitRecord =", value, "cvVisitrecord");
            return (Criteria) this;
        }

        public Criteria andCvVisitrecordNotEqualTo(String value) {
            addCriterion("CV_VisitRecord <>", value, "cvVisitrecord");
            return (Criteria) this;
        }

        public Criteria andCvVisitrecordGreaterThan(String value) {
            addCriterion("CV_VisitRecord >", value, "cvVisitrecord");
            return (Criteria) this;
        }

        public Criteria andCvVisitrecordGreaterThanOrEqualTo(String value) {
            addCriterion("CV_VisitRecord >=", value, "cvVisitrecord");
            return (Criteria) this;
        }

        public Criteria andCvVisitrecordLessThan(String value) {
            addCriterion("CV_VisitRecord <", value, "cvVisitrecord");
            return (Criteria) this;
        }

        public Criteria andCvVisitrecordLessThanOrEqualTo(String value) {
            addCriterion("CV_VisitRecord <=", value, "cvVisitrecord");
            return (Criteria) this;
        }

        public Criteria andCvVisitrecordLike(String value) {
            addCriterion("CV_VisitRecord like", value, "cvVisitrecord");
            return (Criteria) this;
        }

        public Criteria andCvVisitrecordNotLike(String value) {
            addCriterion("CV_VisitRecord not like", value, "cvVisitrecord");
            return (Criteria) this;
        }

        public Criteria andCvVisitrecordIn(List<String> values) {
            addCriterion("CV_VisitRecord in", values, "cvVisitrecord");
            return (Criteria) this;
        }

        public Criteria andCvVisitrecordNotIn(List<String> values) {
            addCriterion("CV_VisitRecord not in", values, "cvVisitrecord");
            return (Criteria) this;
        }

        public Criteria andCvVisitrecordBetween(String value1, String value2) {
            addCriterion("CV_VisitRecord between", value1, value2, "cvVisitrecord");
            return (Criteria) this;
        }

        public Criteria andCvVisitrecordNotBetween(String value1, String value2) {
            addCriterion("CV_VisitRecord not between", value1, value2, "cvVisitrecord");
            return (Criteria) this;
        }

        public Criteria andCvFeedbackproblemIsNull() {
            addCriterion("CV_FeedbackProblem is null");
            return (Criteria) this;
        }

        public Criteria andCvFeedbackproblemIsNotNull() {
            addCriterion("CV_FeedbackProblem is not null");
            return (Criteria) this;
        }

        public Criteria andCvFeedbackproblemEqualTo(String value) {
            addCriterion("CV_FeedbackProblem =", value, "cvFeedbackproblem");
            return (Criteria) this;
        }

        public Criteria andCvFeedbackproblemNotEqualTo(String value) {
            addCriterion("CV_FeedbackProblem <>", value, "cvFeedbackproblem");
            return (Criteria) this;
        }

        public Criteria andCvFeedbackproblemGreaterThan(String value) {
            addCriterion("CV_FeedbackProblem >", value, "cvFeedbackproblem");
            return (Criteria) this;
        }

        public Criteria andCvFeedbackproblemGreaterThanOrEqualTo(String value) {
            addCriterion("CV_FeedbackProblem >=", value, "cvFeedbackproblem");
            return (Criteria) this;
        }

        public Criteria andCvFeedbackproblemLessThan(String value) {
            addCriterion("CV_FeedbackProblem <", value, "cvFeedbackproblem");
            return (Criteria) this;
        }

        public Criteria andCvFeedbackproblemLessThanOrEqualTo(String value) {
            addCriterion("CV_FeedbackProblem <=", value, "cvFeedbackproblem");
            return (Criteria) this;
        }

        public Criteria andCvFeedbackproblemLike(String value) {
            addCriterion("CV_FeedbackProblem like", value, "cvFeedbackproblem");
            return (Criteria) this;
        }

        public Criteria andCvFeedbackproblemNotLike(String value) {
            addCriterion("CV_FeedbackProblem not like", value, "cvFeedbackproblem");
            return (Criteria) this;
        }

        public Criteria andCvFeedbackproblemIn(List<String> values) {
            addCriterion("CV_FeedbackProblem in", values, "cvFeedbackproblem");
            return (Criteria) this;
        }

        public Criteria andCvFeedbackproblemNotIn(List<String> values) {
            addCriterion("CV_FeedbackProblem not in", values, "cvFeedbackproblem");
            return (Criteria) this;
        }

        public Criteria andCvFeedbackproblemBetween(String value1, String value2) {
            addCriterion("CV_FeedbackProblem between", value1, value2, "cvFeedbackproblem");
            return (Criteria) this;
        }

        public Criteria andCvFeedbackproblemNotBetween(String value1, String value2) {
            addCriterion("CV_FeedbackProblem not between", value1, value2, "cvFeedbackproblem");
            return (Criteria) this;
        }

        public Criteria andCvResourceinfoIsNull() {
            addCriterion("CV_ResourceInfo is null");
            return (Criteria) this;
        }

        public Criteria andCvResourceinfoIsNotNull() {
            addCriterion("CV_ResourceInfo is not null");
            return (Criteria) this;
        }

        public Criteria andCvResourceinfoEqualTo(String value) {
            addCriterion("CV_ResourceInfo =", value, "cvResourceinfo");
            return (Criteria) this;
        }

        public Criteria andCvResourceinfoNotEqualTo(String value) {
            addCriterion("CV_ResourceInfo <>", value, "cvResourceinfo");
            return (Criteria) this;
        }

        public Criteria andCvResourceinfoGreaterThan(String value) {
            addCriterion("CV_ResourceInfo >", value, "cvResourceinfo");
            return (Criteria) this;
        }

        public Criteria andCvResourceinfoGreaterThanOrEqualTo(String value) {
            addCriterion("CV_ResourceInfo >=", value, "cvResourceinfo");
            return (Criteria) this;
        }

        public Criteria andCvResourceinfoLessThan(String value) {
            addCriterion("CV_ResourceInfo <", value, "cvResourceinfo");
            return (Criteria) this;
        }

        public Criteria andCvResourceinfoLessThanOrEqualTo(String value) {
            addCriterion("CV_ResourceInfo <=", value, "cvResourceinfo");
            return (Criteria) this;
        }

        public Criteria andCvResourceinfoLike(String value) {
            addCriterion("CV_ResourceInfo like", value, "cvResourceinfo");
            return (Criteria) this;
        }

        public Criteria andCvResourceinfoNotLike(String value) {
            addCriterion("CV_ResourceInfo not like", value, "cvResourceinfo");
            return (Criteria) this;
        }

        public Criteria andCvResourceinfoIn(List<String> values) {
            addCriterion("CV_ResourceInfo in", values, "cvResourceinfo");
            return (Criteria) this;
        }

        public Criteria andCvResourceinfoNotIn(List<String> values) {
            addCriterion("CV_ResourceInfo not in", values, "cvResourceinfo");
            return (Criteria) this;
        }

        public Criteria andCvResourceinfoBetween(String value1, String value2) {
            addCriterion("CV_ResourceInfo between", value1, value2, "cvResourceinfo");
            return (Criteria) this;
        }

        public Criteria andCvResourceinfoNotBetween(String value1, String value2) {
            addCriterion("CV_ResourceInfo not between", value1, value2, "cvResourceinfo");
            return (Criteria) this;
        }

        public Criteria andCvVisitpeopleIsNull() {
            addCriterion("CV_VisitPeople is null");
            return (Criteria) this;
        }

        public Criteria andCvVisitpeopleIsNotNull() {
            addCriterion("CV_VisitPeople is not null");
            return (Criteria) this;
        }

        public Criteria andCvVisitpeopleEqualTo(String value) {
            addCriterion("CV_VisitPeople =", value, "cvVisitpeople");
            return (Criteria) this;
        }

        public Criteria andCvVisitpeopleNotEqualTo(String value) {
            addCriterion("CV_VisitPeople <>", value, "cvVisitpeople");
            return (Criteria) this;
        }

        public Criteria andCvVisitpeopleGreaterThan(String value) {
            addCriterion("CV_VisitPeople >", value, "cvVisitpeople");
            return (Criteria) this;
        }

        public Criteria andCvVisitpeopleGreaterThanOrEqualTo(String value) {
            addCriterion("CV_VisitPeople >=", value, "cvVisitpeople");
            return (Criteria) this;
        }

        public Criteria andCvVisitpeopleLessThan(String value) {
            addCriterion("CV_VisitPeople <", value, "cvVisitpeople");
            return (Criteria) this;
        }

        public Criteria andCvVisitpeopleLessThanOrEqualTo(String value) {
            addCriterion("CV_VisitPeople <=", value, "cvVisitpeople");
            return (Criteria) this;
        }

        public Criteria andCvVisitpeopleLike(String value) {
            addCriterion("CV_VisitPeople like", value, "cvVisitpeople");
            return (Criteria) this;
        }

        public Criteria andCvVisitpeopleNotLike(String value) {
            addCriterion("CV_VisitPeople not like", value, "cvVisitpeople");
            return (Criteria) this;
        }

        public Criteria andCvVisitpeopleIn(List<String> values) {
            addCriterion("CV_VisitPeople in", values, "cvVisitpeople");
            return (Criteria) this;
        }

        public Criteria andCvVisitpeopleNotIn(List<String> values) {
            addCriterion("CV_VisitPeople not in", values, "cvVisitpeople");
            return (Criteria) this;
        }

        public Criteria andCvVisitpeopleBetween(String value1, String value2) {
            addCriterion("CV_VisitPeople between", value1, value2, "cvVisitpeople");
            return (Criteria) this;
        }

        public Criteria andCvVisitpeopleNotBetween(String value1, String value2) {
            addCriterion("CV_VisitPeople not between", value1, value2, "cvVisitpeople");
            return (Criteria) this;
        }

        public Criteria andCvResultIsNull() {
            addCriterion("CV_Result is null");
            return (Criteria) this;
        }

        public Criteria andCvResultIsNotNull() {
            addCriterion("CV_Result is not null");
            return (Criteria) this;
        }

        public Criteria andCvResultEqualTo(Integer value) {
            addCriterion("CV_Result =", value, "cvResult");
            return (Criteria) this;
        }

        public Criteria andCvResultNotEqualTo(Integer value) {
            addCriterion("CV_Result <>", value, "cvResult");
            return (Criteria) this;
        }

        public Criteria andCvResultGreaterThan(Integer value) {
            addCriterion("CV_Result >", value, "cvResult");
            return (Criteria) this;
        }

        public Criteria andCvResultGreaterThanOrEqualTo(Integer value) {
            addCriterion("CV_Result >=", value, "cvResult");
            return (Criteria) this;
        }

        public Criteria andCvResultLessThan(Integer value) {
            addCriterion("CV_Result <", value, "cvResult");
            return (Criteria) this;
        }

        public Criteria andCvResultLessThanOrEqualTo(Integer value) {
            addCriterion("CV_Result <=", value, "cvResult");
            return (Criteria) this;
        }

        public Criteria andCvResultIn(List<Integer> values) {
            addCriterion("CV_Result in", values, "cvResult");
            return (Criteria) this;
        }

        public Criteria andCvResultNotIn(List<Integer> values) {
            addCriterion("CV_Result not in", values, "cvResult");
            return (Criteria) this;
        }

        public Criteria andCvResultBetween(Integer value1, Integer value2) {
            addCriterion("CV_Result between", value1, value2, "cvResult");
            return (Criteria) this;
        }

        public Criteria andCvResultNotBetween(Integer value1, Integer value2) {
            addCriterion("CV_Result not between", value1, value2, "cvResult");
            return (Criteria) this;
        }

        public Criteria andCvAgentcompanynameIsNull() {
            addCriterion("CV_AgentCompanyName is null");
            return (Criteria) this;
        }

        public Criteria andCvAgentcompanynameIsNotNull() {
            addCriterion("CV_AgentCompanyName is not null");
            return (Criteria) this;
        }

        public Criteria andCvAgentcompanynameEqualTo(String value) {
            addCriterion("CV_AgentCompanyName =", value, "cvAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andCvAgentcompanynameNotEqualTo(String value) {
            addCriterion("CV_AgentCompanyName <>", value, "cvAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andCvAgentcompanynameGreaterThan(String value) {
            addCriterion("CV_AgentCompanyName >", value, "cvAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andCvAgentcompanynameGreaterThanOrEqualTo(String value) {
            addCriterion("CV_AgentCompanyName >=", value, "cvAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andCvAgentcompanynameLessThan(String value) {
            addCriterion("CV_AgentCompanyName <", value, "cvAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andCvAgentcompanynameLessThanOrEqualTo(String value) {
            addCriterion("CV_AgentCompanyName <=", value, "cvAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andCvAgentcompanynameLike(String value) {
            addCriterion("CV_AgentCompanyName like", value, "cvAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andCvAgentcompanynameNotLike(String value) {
            addCriterion("CV_AgentCompanyName not like", value, "cvAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andCvAgentcompanynameIn(List<String> values) {
            addCriterion("CV_AgentCompanyName in", values, "cvAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andCvAgentcompanynameNotIn(List<String> values) {
            addCriterion("CV_AgentCompanyName not in", values, "cvAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andCvAgentcompanynameBetween(String value1, String value2) {
            addCriterion("CV_AgentCompanyName between", value1, value2, "cvAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andCvAgentcompanynameNotBetween(String value1, String value2) {
            addCriterion("CV_AgentCompanyName not between", value1, value2, "cvAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andCvOfficecodeIsNull() {
            addCriterion("CV_OfficeCode is null");
            return (Criteria) this;
        }

        public Criteria andCvOfficecodeIsNotNull() {
            addCriterion("CV_OfficeCode is not null");
            return (Criteria) this;
        }

        public Criteria andCvOfficecodeEqualTo(String value) {
            addCriterion("CV_OfficeCode =", value, "cvOfficecode");
            return (Criteria) this;
        }

        public Criteria andCvOfficecodeNotEqualTo(String value) {
            addCriterion("CV_OfficeCode <>", value, "cvOfficecode");
            return (Criteria) this;
        }

        public Criteria andCvOfficecodeGreaterThan(String value) {
            addCriterion("CV_OfficeCode >", value, "cvOfficecode");
            return (Criteria) this;
        }

        public Criteria andCvOfficecodeGreaterThanOrEqualTo(String value) {
            addCriterion("CV_OfficeCode >=", value, "cvOfficecode");
            return (Criteria) this;
        }

        public Criteria andCvOfficecodeLessThan(String value) {
            addCriterion("CV_OfficeCode <", value, "cvOfficecode");
            return (Criteria) this;
        }

        public Criteria andCvOfficecodeLessThanOrEqualTo(String value) {
            addCriterion("CV_OfficeCode <=", value, "cvOfficecode");
            return (Criteria) this;
        }

        public Criteria andCvOfficecodeLike(String value) {
            addCriterion("CV_OfficeCode like", value, "cvOfficecode");
            return (Criteria) this;
        }

        public Criteria andCvOfficecodeNotLike(String value) {
            addCriterion("CV_OfficeCode not like", value, "cvOfficecode");
            return (Criteria) this;
        }

        public Criteria andCvOfficecodeIn(List<String> values) {
            addCriterion("CV_OfficeCode in", values, "cvOfficecode");
            return (Criteria) this;
        }

        public Criteria andCvOfficecodeNotIn(List<String> values) {
            addCriterion("CV_OfficeCode not in", values, "cvOfficecode");
            return (Criteria) this;
        }

        public Criteria andCvOfficecodeBetween(String value1, String value2) {
            addCriterion("CV_OfficeCode between", value1, value2, "cvOfficecode");
            return (Criteria) this;
        }

        public Criteria andCvOfficecodeNotBetween(String value1, String value2) {
            addCriterion("CV_OfficeCode not between", value1, value2, "cvOfficecode");
            return (Criteria) this;
        }

        public Criteria andCvIatanoIsNull() {
            addCriterion("CV_IATANO is null");
            return (Criteria) this;
        }

        public Criteria andCvIatanoIsNotNull() {
            addCriterion("CV_IATANO is not null");
            return (Criteria) this;
        }

        public Criteria andCvIatanoEqualTo(String value) {
            addCriterion("CV_IATANO =", value, "cvIatano");
            return (Criteria) this;
        }

        public Criteria andCvIatanoNotEqualTo(String value) {
            addCriterion("CV_IATANO <>", value, "cvIatano");
            return (Criteria) this;
        }

        public Criteria andCvIatanoGreaterThan(String value) {
            addCriterion("CV_IATANO >", value, "cvIatano");
            return (Criteria) this;
        }

        public Criteria andCvIatanoGreaterThanOrEqualTo(String value) {
            addCriterion("CV_IATANO >=", value, "cvIatano");
            return (Criteria) this;
        }

        public Criteria andCvIatanoLessThan(String value) {
            addCriterion("CV_IATANO <", value, "cvIatano");
            return (Criteria) this;
        }

        public Criteria andCvIatanoLessThanOrEqualTo(String value) {
            addCriterion("CV_IATANO <=", value, "cvIatano");
            return (Criteria) this;
        }

        public Criteria andCvIatanoLike(String value) {
            addCriterion("CV_IATANO like", value, "cvIatano");
            return (Criteria) this;
        }

        public Criteria andCvIatanoNotLike(String value) {
            addCriterion("CV_IATANO not like", value, "cvIatano");
            return (Criteria) this;
        }

        public Criteria andCvIatanoIn(List<String> values) {
            addCriterion("CV_IATANO in", values, "cvIatano");
            return (Criteria) this;
        }

        public Criteria andCvIatanoNotIn(List<String> values) {
            addCriterion("CV_IATANO not in", values, "cvIatano");
            return (Criteria) this;
        }

        public Criteria andCvIatanoBetween(String value1, String value2) {
            addCriterion("CV_IATANO between", value1, value2, "cvIatano");
            return (Criteria) this;
        }

        public Criteria andCvIatanoNotBetween(String value1, String value2) {
            addCriterion("CV_IATANO not between", value1, value2, "cvIatano");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}