package com.loongair.DB.Entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FrequentpassengerExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public FrequentpassengerExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTidIsNull() {
            addCriterion("Tid is null");
            return (Criteria) this;
        }

        public Criteria andTidIsNotNull() {
            addCriterion("Tid is not null");
            return (Criteria) this;
        }

        public Criteria andTidEqualTo(Long value) {
            addCriterion("Tid =", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotEqualTo(Long value) {
            addCriterion("Tid <>", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThan(Long value) {
            addCriterion("Tid >", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThanOrEqualTo(Long value) {
            addCriterion("Tid >=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThan(Long value) {
            addCriterion("Tid <", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThanOrEqualTo(Long value) {
            addCriterion("Tid <=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidIn(List<Long> values) {
            addCriterion("Tid in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotIn(List<Long> values) {
            addCriterion("Tid not in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidBetween(Long value1, Long value2) {
            addCriterion("Tid between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotBetween(Long value1, Long value2) {
            addCriterion("Tid not between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNull() {
            addCriterion("CreateId is null");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNotNull() {
            addCriterion("CreateId is not null");
            return (Criteria) this;
        }

        public Criteria andCreateidEqualTo(Long value) {
            addCriterion("CreateId =", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotEqualTo(Long value) {
            addCriterion("CreateId <>", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThan(Long value) {
            addCriterion("CreateId >", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThanOrEqualTo(Long value) {
            addCriterion("CreateId >=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThan(Long value) {
            addCriterion("CreateId <", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThanOrEqualTo(Long value) {
            addCriterion("CreateId <=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidIn(List<Long> values) {
            addCriterion("CreateId in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotIn(List<Long> values) {
            addCriterion("CreateId not in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidBetween(Long value1, Long value2) {
            addCriterion("CreateId between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotBetween(Long value1, Long value2) {
            addCriterion("CreateId not between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNull() {
            addCriterion("ProxyUserId is null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNotNull() {
            addCriterion("ProxyUserId is not null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridEqualTo(Long value) {
            addCriterion("ProxyUserId =", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotEqualTo(Long value) {
            addCriterion("ProxyUserId <>", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThan(Long value) {
            addCriterion("ProxyUserId >", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId >=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThan(Long value) {
            addCriterion("ProxyUserId <", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId <=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIn(List<Long> values) {
            addCriterion("ProxyUserId in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotIn(List<Long> values) {
            addCriterion("ProxyUserId not in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId not between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNull() {
            addCriterion("LastGuid is null");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNotNull() {
            addCriterion("LastGuid is not null");
            return (Criteria) this;
        }

        public Criteria andLastguidEqualTo(Long value) {
            addCriterion("LastGuid =", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotEqualTo(Long value) {
            addCriterion("LastGuid <>", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThan(Long value) {
            addCriterion("LastGuid >", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThanOrEqualTo(Long value) {
            addCriterion("LastGuid >=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThan(Long value) {
            addCriterion("LastGuid <", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThanOrEqualTo(Long value) {
            addCriterion("LastGuid <=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidIn(List<Long> values) {
            addCriterion("LastGuid in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotIn(List<Long> values) {
            addCriterion("LastGuid not in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidBetween(Long value1, Long value2) {
            addCriterion("LastGuid between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotBetween(Long value1, Long value2) {
            addCriterion("LastGuid not between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andCloneIsNull() {
            addCriterion("Clone is null");
            return (Criteria) this;
        }

        public Criteria andCloneIsNotNull() {
            addCriterion("Clone is not null");
            return (Criteria) this;
        }

        public Criteria andCloneEqualTo(Integer value) {
            addCriterion("Clone =", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotEqualTo(Integer value) {
            addCriterion("Clone <>", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThan(Integer value) {
            addCriterion("Clone >", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThanOrEqualTo(Integer value) {
            addCriterion("Clone >=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThan(Integer value) {
            addCriterion("Clone <", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThanOrEqualTo(Integer value) {
            addCriterion("Clone <=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneIn(List<Integer> values) {
            addCriterion("Clone in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotIn(List<Integer> values) {
            addCriterion("Clone not in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneBetween(Integer value1, Integer value2) {
            addCriterion("Clone between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotBetween(Integer value1, Integer value2) {
            addCriterion("Clone not between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNull() {
            addCriterion("UpdateId is null");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNotNull() {
            addCriterion("UpdateId is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateidEqualTo(Long value) {
            addCriterion("UpdateId =", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotEqualTo(Long value) {
            addCriterion("UpdateId <>", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThan(Long value) {
            addCriterion("UpdateId >", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThanOrEqualTo(Long value) {
            addCriterion("UpdateId >=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThan(Long value) {
            addCriterion("UpdateId <", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThanOrEqualTo(Long value) {
            addCriterion("UpdateId <=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidIn(List<Long> values) {
            addCriterion("UpdateId in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotIn(List<Long> values) {
            addCriterion("UpdateId not in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidBetween(Long value1, Long value2) {
            addCriterion("UpdateId between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotBetween(Long value1, Long value2) {
            addCriterion("UpdateId not between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("UpdateDate is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("UpdateDate is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("UpdateDate =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("UpdateDate <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("UpdateDate >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("UpdateDate >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("UpdateDate <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("UpdateDate <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("UpdateDate in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("UpdateDate not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("UpdateDate between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("UpdateDate not between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("Status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("Status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("Status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("Status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("Status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("Status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("Status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("Status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("Status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("Status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("Status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("Status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andFpLausertidIsNull() {
            addCriterion("FP_LaUserTid is null");
            return (Criteria) this;
        }

        public Criteria andFpLausertidIsNotNull() {
            addCriterion("FP_LaUserTid is not null");
            return (Criteria) this;
        }

        public Criteria andFpLausertidEqualTo(Long value) {
            addCriterion("FP_LaUserTid =", value, "fpLausertid");
            return (Criteria) this;
        }

        public Criteria andFpLausertidNotEqualTo(Long value) {
            addCriterion("FP_LaUserTid <>", value, "fpLausertid");
            return (Criteria) this;
        }

        public Criteria andFpLausertidGreaterThan(Long value) {
            addCriterion("FP_LaUserTid >", value, "fpLausertid");
            return (Criteria) this;
        }

        public Criteria andFpLausertidGreaterThanOrEqualTo(Long value) {
            addCriterion("FP_LaUserTid >=", value, "fpLausertid");
            return (Criteria) this;
        }

        public Criteria andFpLausertidLessThan(Long value) {
            addCriterion("FP_LaUserTid <", value, "fpLausertid");
            return (Criteria) this;
        }

        public Criteria andFpLausertidLessThanOrEqualTo(Long value) {
            addCriterion("FP_LaUserTid <=", value, "fpLausertid");
            return (Criteria) this;
        }

        public Criteria andFpLausertidIn(List<Long> values) {
            addCriterion("FP_LaUserTid in", values, "fpLausertid");
            return (Criteria) this;
        }

        public Criteria andFpLausertidNotIn(List<Long> values) {
            addCriterion("FP_LaUserTid not in", values, "fpLausertid");
            return (Criteria) this;
        }

        public Criteria andFpLausertidBetween(Long value1, Long value2) {
            addCriterion("FP_LaUserTid between", value1, value2, "fpLausertid");
            return (Criteria) this;
        }

        public Criteria andFpLausertidNotBetween(Long value1, Long value2) {
            addCriterion("FP_LaUserTid not between", value1, value2, "fpLausertid");
            return (Criteria) this;
        }

        public Criteria andFpNameIsNull() {
            addCriterion("FP_Name is null");
            return (Criteria) this;
        }

        public Criteria andFpNameIsNotNull() {
            addCriterion("FP_Name is not null");
            return (Criteria) this;
        }

        public Criteria andFpNameEqualTo(String value) {
            addCriterion("FP_Name =", value, "fpName");
            return (Criteria) this;
        }

        public Criteria andFpNameNotEqualTo(String value) {
            addCriterion("FP_Name <>", value, "fpName");
            return (Criteria) this;
        }

        public Criteria andFpNameGreaterThan(String value) {
            addCriterion("FP_Name >", value, "fpName");
            return (Criteria) this;
        }

        public Criteria andFpNameGreaterThanOrEqualTo(String value) {
            addCriterion("FP_Name >=", value, "fpName");
            return (Criteria) this;
        }

        public Criteria andFpNameLessThan(String value) {
            addCriterion("FP_Name <", value, "fpName");
            return (Criteria) this;
        }

        public Criteria andFpNameLessThanOrEqualTo(String value) {
            addCriterion("FP_Name <=", value, "fpName");
            return (Criteria) this;
        }

        public Criteria andFpNameLike(String value) {
            addCriterion("FP_Name like", value, "fpName");
            return (Criteria) this;
        }

        public Criteria andFpNameNotLike(String value) {
            addCriterion("FP_Name not like", value, "fpName");
            return (Criteria) this;
        }

        public Criteria andFpNameIn(List<String> values) {
            addCriterion("FP_Name in", values, "fpName");
            return (Criteria) this;
        }

        public Criteria andFpNameNotIn(List<String> values) {
            addCriterion("FP_Name not in", values, "fpName");
            return (Criteria) this;
        }

        public Criteria andFpNameBetween(String value1, String value2) {
            addCriterion("FP_Name between", value1, value2, "fpName");
            return (Criteria) this;
        }

        public Criteria andFpNameNotBetween(String value1, String value2) {
            addCriterion("FP_Name not between", value1, value2, "fpName");
            return (Criteria) this;
        }

        public Criteria andFpBirthdayIsNull() {
            addCriterion("FP_Birthday is null");
            return (Criteria) this;
        }

        public Criteria andFpBirthdayIsNotNull() {
            addCriterion("FP_Birthday is not null");
            return (Criteria) this;
        }

        public Criteria andFpBirthdayEqualTo(Date value) {
            addCriterion("FP_Birthday =", value, "fpBirthday");
            return (Criteria) this;
        }

        public Criteria andFpBirthdayNotEqualTo(Date value) {
            addCriterion("FP_Birthday <>", value, "fpBirthday");
            return (Criteria) this;
        }

        public Criteria andFpBirthdayGreaterThan(Date value) {
            addCriterion("FP_Birthday >", value, "fpBirthday");
            return (Criteria) this;
        }

        public Criteria andFpBirthdayGreaterThanOrEqualTo(Date value) {
            addCriterion("FP_Birthday >=", value, "fpBirthday");
            return (Criteria) this;
        }

        public Criteria andFpBirthdayLessThan(Date value) {
            addCriterion("FP_Birthday <", value, "fpBirthday");
            return (Criteria) this;
        }

        public Criteria andFpBirthdayLessThanOrEqualTo(Date value) {
            addCriterion("FP_Birthday <=", value, "fpBirthday");
            return (Criteria) this;
        }

        public Criteria andFpBirthdayIn(List<Date> values) {
            addCriterion("FP_Birthday in", values, "fpBirthday");
            return (Criteria) this;
        }

        public Criteria andFpBirthdayNotIn(List<Date> values) {
            addCriterion("FP_Birthday not in", values, "fpBirthday");
            return (Criteria) this;
        }

        public Criteria andFpBirthdayBetween(Date value1, Date value2) {
            addCriterion("FP_Birthday between", value1, value2, "fpBirthday");
            return (Criteria) this;
        }

        public Criteria andFpBirthdayNotBetween(Date value1, Date value2) {
            addCriterion("FP_Birthday not between", value1, value2, "fpBirthday");
            return (Criteria) this;
        }

        public Criteria andFpFoidtypeIsNull() {
            addCriterion("FP_FoidType is null");
            return (Criteria) this;
        }

        public Criteria andFpFoidtypeIsNotNull() {
            addCriterion("FP_FoidType is not null");
            return (Criteria) this;
        }

        public Criteria andFpFoidtypeEqualTo(Integer value) {
            addCriterion("FP_FoidType =", value, "fpFoidtype");
            return (Criteria) this;
        }

        public Criteria andFpFoidtypeNotEqualTo(Integer value) {
            addCriterion("FP_FoidType <>", value, "fpFoidtype");
            return (Criteria) this;
        }

        public Criteria andFpFoidtypeGreaterThan(Integer value) {
            addCriterion("FP_FoidType >", value, "fpFoidtype");
            return (Criteria) this;
        }

        public Criteria andFpFoidtypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("FP_FoidType >=", value, "fpFoidtype");
            return (Criteria) this;
        }

        public Criteria andFpFoidtypeLessThan(Integer value) {
            addCriterion("FP_FoidType <", value, "fpFoidtype");
            return (Criteria) this;
        }

        public Criteria andFpFoidtypeLessThanOrEqualTo(Integer value) {
            addCriterion("FP_FoidType <=", value, "fpFoidtype");
            return (Criteria) this;
        }

        public Criteria andFpFoidtypeIn(List<Integer> values) {
            addCriterion("FP_FoidType in", values, "fpFoidtype");
            return (Criteria) this;
        }

        public Criteria andFpFoidtypeNotIn(List<Integer> values) {
            addCriterion("FP_FoidType not in", values, "fpFoidtype");
            return (Criteria) this;
        }

        public Criteria andFpFoidtypeBetween(Integer value1, Integer value2) {
            addCriterion("FP_FoidType between", value1, value2, "fpFoidtype");
            return (Criteria) this;
        }

        public Criteria andFpFoidtypeNotBetween(Integer value1, Integer value2) {
            addCriterion("FP_FoidType not between", value1, value2, "fpFoidtype");
            return (Criteria) this;
        }

        public Criteria andFpCustomeraccountidIsNull() {
            addCriterion("FP_CustomerAccountID is null");
            return (Criteria) this;
        }

        public Criteria andFpCustomeraccountidIsNotNull() {
            addCriterion("FP_CustomerAccountID is not null");
            return (Criteria) this;
        }

        public Criteria andFpCustomeraccountidEqualTo(Long value) {
            addCriterion("FP_CustomerAccountID =", value, "fpCustomeraccountid");
            return (Criteria) this;
        }

        public Criteria andFpCustomeraccountidNotEqualTo(Long value) {
            addCriterion("FP_CustomerAccountID <>", value, "fpCustomeraccountid");
            return (Criteria) this;
        }

        public Criteria andFpCustomeraccountidGreaterThan(Long value) {
            addCriterion("FP_CustomerAccountID >", value, "fpCustomeraccountid");
            return (Criteria) this;
        }

        public Criteria andFpCustomeraccountidGreaterThanOrEqualTo(Long value) {
            addCriterion("FP_CustomerAccountID >=", value, "fpCustomeraccountid");
            return (Criteria) this;
        }

        public Criteria andFpCustomeraccountidLessThan(Long value) {
            addCriterion("FP_CustomerAccountID <", value, "fpCustomeraccountid");
            return (Criteria) this;
        }

        public Criteria andFpCustomeraccountidLessThanOrEqualTo(Long value) {
            addCriterion("FP_CustomerAccountID <=", value, "fpCustomeraccountid");
            return (Criteria) this;
        }

        public Criteria andFpCustomeraccountidIn(List<Long> values) {
            addCriterion("FP_CustomerAccountID in", values, "fpCustomeraccountid");
            return (Criteria) this;
        }

        public Criteria andFpCustomeraccountidNotIn(List<Long> values) {
            addCriterion("FP_CustomerAccountID not in", values, "fpCustomeraccountid");
            return (Criteria) this;
        }

        public Criteria andFpCustomeraccountidBetween(Long value1, Long value2) {
            addCriterion("FP_CustomerAccountID between", value1, value2, "fpCustomeraccountid");
            return (Criteria) this;
        }

        public Criteria andFpCustomeraccountidNotBetween(Long value1, Long value2) {
            addCriterion("FP_CustomerAccountID not between", value1, value2, "fpCustomeraccountid");
            return (Criteria) this;
        }

        public Criteria andFpFoidIsNull() {
            addCriterion("FP_Foid is null");
            return (Criteria) this;
        }

        public Criteria andFpFoidIsNotNull() {
            addCriterion("FP_Foid is not null");
            return (Criteria) this;
        }

        public Criteria andFpFoidEqualTo(String value) {
            addCriterion("FP_Foid =", value, "fpFoid");
            return (Criteria) this;
        }

        public Criteria andFpFoidNotEqualTo(String value) {
            addCriterion("FP_Foid <>", value, "fpFoid");
            return (Criteria) this;
        }

        public Criteria andFpFoidGreaterThan(String value) {
            addCriterion("FP_Foid >", value, "fpFoid");
            return (Criteria) this;
        }

        public Criteria andFpFoidGreaterThanOrEqualTo(String value) {
            addCriterion("FP_Foid >=", value, "fpFoid");
            return (Criteria) this;
        }

        public Criteria andFpFoidLessThan(String value) {
            addCriterion("FP_Foid <", value, "fpFoid");
            return (Criteria) this;
        }

        public Criteria andFpFoidLessThanOrEqualTo(String value) {
            addCriterion("FP_Foid <=", value, "fpFoid");
            return (Criteria) this;
        }

        public Criteria andFpFoidLike(String value) {
            addCriterion("FP_Foid like", value, "fpFoid");
            return (Criteria) this;
        }

        public Criteria andFpFoidNotLike(String value) {
            addCriterion("FP_Foid not like", value, "fpFoid");
            return (Criteria) this;
        }

        public Criteria andFpFoidIn(List<String> values) {
            addCriterion("FP_Foid in", values, "fpFoid");
            return (Criteria) this;
        }

        public Criteria andFpFoidNotIn(List<String> values) {
            addCriterion("FP_Foid not in", values, "fpFoid");
            return (Criteria) this;
        }

        public Criteria andFpFoidBetween(String value1, String value2) {
            addCriterion("FP_Foid between", value1, value2, "fpFoid");
            return (Criteria) this;
        }

        public Criteria andFpFoidNotBetween(String value1, String value2) {
            addCriterion("FP_Foid not between", value1, value2, "fpFoid");
            return (Criteria) this;
        }

        public Criteria andFpMobileIsNull() {
            addCriterion("FP_Mobile is null");
            return (Criteria) this;
        }

        public Criteria andFpMobileIsNotNull() {
            addCriterion("FP_Mobile is not null");
            return (Criteria) this;
        }

        public Criteria andFpMobileEqualTo(String value) {
            addCriterion("FP_Mobile =", value, "fpMobile");
            return (Criteria) this;
        }

        public Criteria andFpMobileNotEqualTo(String value) {
            addCriterion("FP_Mobile <>", value, "fpMobile");
            return (Criteria) this;
        }

        public Criteria andFpMobileGreaterThan(String value) {
            addCriterion("FP_Mobile >", value, "fpMobile");
            return (Criteria) this;
        }

        public Criteria andFpMobileGreaterThanOrEqualTo(String value) {
            addCriterion("FP_Mobile >=", value, "fpMobile");
            return (Criteria) this;
        }

        public Criteria andFpMobileLessThan(String value) {
            addCriterion("FP_Mobile <", value, "fpMobile");
            return (Criteria) this;
        }

        public Criteria andFpMobileLessThanOrEqualTo(String value) {
            addCriterion("FP_Mobile <=", value, "fpMobile");
            return (Criteria) this;
        }

        public Criteria andFpMobileLike(String value) {
            addCriterion("FP_Mobile like", value, "fpMobile");
            return (Criteria) this;
        }

        public Criteria andFpMobileNotLike(String value) {
            addCriterion("FP_Mobile not like", value, "fpMobile");
            return (Criteria) this;
        }

        public Criteria andFpMobileIn(List<String> values) {
            addCriterion("FP_Mobile in", values, "fpMobile");
            return (Criteria) this;
        }

        public Criteria andFpMobileNotIn(List<String> values) {
            addCriterion("FP_Mobile not in", values, "fpMobile");
            return (Criteria) this;
        }

        public Criteria andFpMobileBetween(String value1, String value2) {
            addCriterion("FP_Mobile between", value1, value2, "fpMobile");
            return (Criteria) this;
        }

        public Criteria andFpMobileNotBetween(String value1, String value2) {
            addCriterion("FP_Mobile not between", value1, value2, "fpMobile");
            return (Criteria) this;
        }

        public Criteria andFpEmailIsNull() {
            addCriterion("FP_EMail is null");
            return (Criteria) this;
        }

        public Criteria andFpEmailIsNotNull() {
            addCriterion("FP_EMail is not null");
            return (Criteria) this;
        }

        public Criteria andFpEmailEqualTo(String value) {
            addCriterion("FP_EMail =", value, "fpEmail");
            return (Criteria) this;
        }

        public Criteria andFpEmailNotEqualTo(String value) {
            addCriterion("FP_EMail <>", value, "fpEmail");
            return (Criteria) this;
        }

        public Criteria andFpEmailGreaterThan(String value) {
            addCriterion("FP_EMail >", value, "fpEmail");
            return (Criteria) this;
        }

        public Criteria andFpEmailGreaterThanOrEqualTo(String value) {
            addCriterion("FP_EMail >=", value, "fpEmail");
            return (Criteria) this;
        }

        public Criteria andFpEmailLessThan(String value) {
            addCriterion("FP_EMail <", value, "fpEmail");
            return (Criteria) this;
        }

        public Criteria andFpEmailLessThanOrEqualTo(String value) {
            addCriterion("FP_EMail <=", value, "fpEmail");
            return (Criteria) this;
        }

        public Criteria andFpEmailLike(String value) {
            addCriterion("FP_EMail like", value, "fpEmail");
            return (Criteria) this;
        }

        public Criteria andFpEmailNotLike(String value) {
            addCriterion("FP_EMail not like", value, "fpEmail");
            return (Criteria) this;
        }

        public Criteria andFpEmailIn(List<String> values) {
            addCriterion("FP_EMail in", values, "fpEmail");
            return (Criteria) this;
        }

        public Criteria andFpEmailNotIn(List<String> values) {
            addCriterion("FP_EMail not in", values, "fpEmail");
            return (Criteria) this;
        }

        public Criteria andFpEmailBetween(String value1, String value2) {
            addCriterion("FP_EMail between", value1, value2, "fpEmail");
            return (Criteria) this;
        }

        public Criteria andFpEmailNotBetween(String value1, String value2) {
            addCriterion("FP_EMail not between", value1, value2, "fpEmail");
            return (Criteria) this;
        }

        public Criteria andFpTravellertypeIsNull() {
            addCriterion("FP_TravellerType is null");
            return (Criteria) this;
        }

        public Criteria andFpTravellertypeIsNotNull() {
            addCriterion("FP_TravellerType is not null");
            return (Criteria) this;
        }

        public Criteria andFpTravellertypeEqualTo(Integer value) {
            addCriterion("FP_TravellerType =", value, "fpTravellertype");
            return (Criteria) this;
        }

        public Criteria andFpTravellertypeNotEqualTo(Integer value) {
            addCriterion("FP_TravellerType <>", value, "fpTravellertype");
            return (Criteria) this;
        }

        public Criteria andFpTravellertypeGreaterThan(Integer value) {
            addCriterion("FP_TravellerType >", value, "fpTravellertype");
            return (Criteria) this;
        }

        public Criteria andFpTravellertypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("FP_TravellerType >=", value, "fpTravellertype");
            return (Criteria) this;
        }

        public Criteria andFpTravellertypeLessThan(Integer value) {
            addCriterion("FP_TravellerType <", value, "fpTravellertype");
            return (Criteria) this;
        }

        public Criteria andFpTravellertypeLessThanOrEqualTo(Integer value) {
            addCriterion("FP_TravellerType <=", value, "fpTravellertype");
            return (Criteria) this;
        }

        public Criteria andFpTravellertypeIn(List<Integer> values) {
            addCriterion("FP_TravellerType in", values, "fpTravellertype");
            return (Criteria) this;
        }

        public Criteria andFpTravellertypeNotIn(List<Integer> values) {
            addCriterion("FP_TravellerType not in", values, "fpTravellertype");
            return (Criteria) this;
        }

        public Criteria andFpTravellertypeBetween(Integer value1, Integer value2) {
            addCriterion("FP_TravellerType between", value1, value2, "fpTravellertype");
            return (Criteria) this;
        }

        public Criteria andFpTravellertypeNotBetween(Integer value1, Integer value2) {
            addCriterion("FP_TravellerType not between", value1, value2, "fpTravellertype");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}