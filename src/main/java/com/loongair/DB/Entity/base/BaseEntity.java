package com.loongair.DB.Entity.base;

import com.loongair.Model.Enum;
import com.loongair.Util.IdGenerator;

import java.util.Date;

public class BaseEntity {

    public BaseEntity()
    {
        tid = IdGenerator.getInstance().nextId();
        createid = 0L;
        proxyuserid = 0L;
        lastguid = 0L;
        clone = 1;
        updateid = 0L;
//        updatedate = new Date();
        status = Enum.ActiveStatus.Enable;
    }

    private Long tid;

    private Long createid;

    private Long proxyuserid;

    private Long lastguid;

    private Integer clone;

    private Long updateid;

    private Date updatedate;

    private Integer status;

    public Long getTid() {
        return tid;
    }

    public void setTid(Long tid) {
        this.tid = tid;
    }

    public Long getCreateid() {
        return createid;
    }

    public void setCreateid(Long createid) {
        this.createid = createid;
    }

    public Long getProxyuserid() {
        return proxyuserid;
    }

    public void setProxyuserid(Long proxyuserid) {
        this.proxyuserid = proxyuserid;
    }

    public Long getLastguid() {
        return lastguid;
    }

    public void setLastguid(Long lastguid) {
        this.lastguid = lastguid;
    }

    public Integer getClone() {
        return clone;
    }

    public void setClone(Integer clone) {
        this.clone = clone;
    }

    public Long getUpdateid() {
        return updateid;
    }

    public void setUpdateid(Long updateid) {
        this.updateid = updateid;
    }

    public Date getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
