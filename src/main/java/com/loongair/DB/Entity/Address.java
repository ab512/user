package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class Address extends BaseEntity {


    private Integer adProvince;

    private String adCity;

    private String adPostcode;

    private String adCompanyname;

    private String adPosition;

    private Integer adCountry;

    private String adDetail;

    private String adTel;

    private String adJob;



    public Integer getAdProvince() {
        return adProvince;
    }

    public void setAdProvince(Integer adProvince) {
        this.adProvince = adProvince;
    }

    public String getAdCity() {
        return adCity;
    }

    public void setAdCity(String adCity) {
        this.adCity = adCity == null ? null : adCity.trim();
    }

    public String getAdPostcode() {
        return adPostcode;
    }

    public void setAdPostcode(String adPostcode) {
        this.adPostcode = adPostcode == null ? null : adPostcode.trim();
    }

    public String getAdCompanyname() {
        return adCompanyname;
    }

    public void setAdCompanyname(String adCompanyname) {
        this.adCompanyname = adCompanyname == null ? null : adCompanyname.trim();
    }

    public String getAdPosition() {
        return adPosition;
    }

    public void setAdPosition(String adPosition) {
        this.adPosition = adPosition == null ? null : adPosition.trim();
    }

    public Integer getAdCountry() {
        return adCountry;
    }

    public void setAdCountry(Integer adCountry) {
        this.adCountry = adCountry;
    }

    public String getAdDetail() {
        return adDetail;
    }

    public void setAdDetail(String adDetail) {
        this.adDetail = adDetail == null ? null : adDetail.trim();
    }

    public String getAdTel() {
        return adTel;
    }

    public void setAdTel(String adTel) {
        this.adTel = adTel == null ? null : adTel.trim();
    }

    public String getAdJob() {
        return adJob;
    }

    public void setAdJob(String adJob) {
        this.adJob = adJob == null ? null : adJob.trim();
    }
}