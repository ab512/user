package com.loongair.DB.Entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserbigcustomerExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UserbigcustomerExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTidIsNull() {
            addCriterion("Tid is null");
            return (Criteria) this;
        }

        public Criteria andTidIsNotNull() {
            addCriterion("Tid is not null");
            return (Criteria) this;
        }

        public Criteria andTidEqualTo(Long value) {
            addCriterion("Tid =", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotEqualTo(Long value) {
            addCriterion("Tid <>", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThan(Long value) {
            addCriterion("Tid >", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThanOrEqualTo(Long value) {
            addCriterion("Tid >=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThan(Long value) {
            addCriterion("Tid <", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThanOrEqualTo(Long value) {
            addCriterion("Tid <=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidIn(List<Long> values) {
            addCriterion("Tid in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotIn(List<Long> values) {
            addCriterion("Tid not in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidBetween(Long value1, Long value2) {
            addCriterion("Tid between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotBetween(Long value1, Long value2) {
            addCriterion("Tid not between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNull() {
            addCriterion("CreateId is null");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNotNull() {
            addCriterion("CreateId is not null");
            return (Criteria) this;
        }

        public Criteria andCreateidEqualTo(Long value) {
            addCriterion("CreateId =", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotEqualTo(Long value) {
            addCriterion("CreateId <>", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThan(Long value) {
            addCriterion("CreateId >", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThanOrEqualTo(Long value) {
            addCriterion("CreateId >=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThan(Long value) {
            addCriterion("CreateId <", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThanOrEqualTo(Long value) {
            addCriterion("CreateId <=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidIn(List<Long> values) {
            addCriterion("CreateId in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotIn(List<Long> values) {
            addCriterion("CreateId not in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidBetween(Long value1, Long value2) {
            addCriterion("CreateId between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotBetween(Long value1, Long value2) {
            addCriterion("CreateId not between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNull() {
            addCriterion("ProxyUserId is null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNotNull() {
            addCriterion("ProxyUserId is not null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridEqualTo(Long value) {
            addCriterion("ProxyUserId =", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotEqualTo(Long value) {
            addCriterion("ProxyUserId <>", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThan(Long value) {
            addCriterion("ProxyUserId >", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId >=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThan(Long value) {
            addCriterion("ProxyUserId <", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId <=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIn(List<Long> values) {
            addCriterion("ProxyUserId in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotIn(List<Long> values) {
            addCriterion("ProxyUserId not in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId not between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNull() {
            addCriterion("LastGuid is null");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNotNull() {
            addCriterion("LastGuid is not null");
            return (Criteria) this;
        }

        public Criteria andLastguidEqualTo(Long value) {
            addCriterion("LastGuid =", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotEqualTo(Long value) {
            addCriterion("LastGuid <>", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThan(Long value) {
            addCriterion("LastGuid >", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThanOrEqualTo(Long value) {
            addCriterion("LastGuid >=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThan(Long value) {
            addCriterion("LastGuid <", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThanOrEqualTo(Long value) {
            addCriterion("LastGuid <=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidIn(List<Long> values) {
            addCriterion("LastGuid in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotIn(List<Long> values) {
            addCriterion("LastGuid not in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidBetween(Long value1, Long value2) {
            addCriterion("LastGuid between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotBetween(Long value1, Long value2) {
            addCriterion("LastGuid not between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andCloneIsNull() {
            addCriterion("Clone is null");
            return (Criteria) this;
        }

        public Criteria andCloneIsNotNull() {
            addCriterion("Clone is not null");
            return (Criteria) this;
        }

        public Criteria andCloneEqualTo(Integer value) {
            addCriterion("Clone =", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotEqualTo(Integer value) {
            addCriterion("Clone <>", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThan(Integer value) {
            addCriterion("Clone >", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThanOrEqualTo(Integer value) {
            addCriterion("Clone >=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThan(Integer value) {
            addCriterion("Clone <", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThanOrEqualTo(Integer value) {
            addCriterion("Clone <=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneIn(List<Integer> values) {
            addCriterion("Clone in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotIn(List<Integer> values) {
            addCriterion("Clone not in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneBetween(Integer value1, Integer value2) {
            addCriterion("Clone between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotBetween(Integer value1, Integer value2) {
            addCriterion("Clone not between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNull() {
            addCriterion("UpdateId is null");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNotNull() {
            addCriterion("UpdateId is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateidEqualTo(Long value) {
            addCriterion("UpdateId =", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotEqualTo(Long value) {
            addCriterion("UpdateId <>", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThan(Long value) {
            addCriterion("UpdateId >", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThanOrEqualTo(Long value) {
            addCriterion("UpdateId >=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThan(Long value) {
            addCriterion("UpdateId <", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThanOrEqualTo(Long value) {
            addCriterion("UpdateId <=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidIn(List<Long> values) {
            addCriterion("UpdateId in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotIn(List<Long> values) {
            addCriterion("UpdateId not in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidBetween(Long value1, Long value2) {
            addCriterion("UpdateId between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotBetween(Long value1, Long value2) {
            addCriterion("UpdateId not between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("UpdateDate is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("UpdateDate is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("UpdateDate =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("UpdateDate <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("UpdateDate >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("UpdateDate >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("UpdateDate <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("UpdateDate <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("UpdateDate in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("UpdateDate not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("UpdateDate between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("UpdateDate not between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("Status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("Status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("Status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("Status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("Status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("Status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("Status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("Status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("Status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("Status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("Status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("Status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andUbcLausertidIsNull() {
            addCriterion("UBC_LaUserTid is null");
            return (Criteria) this;
        }

        public Criteria andUbcLausertidIsNotNull() {
            addCriterion("UBC_LaUserTid is not null");
            return (Criteria) this;
        }

        public Criteria andUbcLausertidEqualTo(Long value) {
            addCriterion("UBC_LaUserTid =", value, "ubcLausertid");
            return (Criteria) this;
        }

        public Criteria andUbcLausertidNotEqualTo(Long value) {
            addCriterion("UBC_LaUserTid <>", value, "ubcLausertid");
            return (Criteria) this;
        }

        public Criteria andUbcLausertidGreaterThan(Long value) {
            addCriterion("UBC_LaUserTid >", value, "ubcLausertid");
            return (Criteria) this;
        }

        public Criteria andUbcLausertidGreaterThanOrEqualTo(Long value) {
            addCriterion("UBC_LaUserTid >=", value, "ubcLausertid");
            return (Criteria) this;
        }

        public Criteria andUbcLausertidLessThan(Long value) {
            addCriterion("UBC_LaUserTid <", value, "ubcLausertid");
            return (Criteria) this;
        }

        public Criteria andUbcLausertidLessThanOrEqualTo(Long value) {
            addCriterion("UBC_LaUserTid <=", value, "ubcLausertid");
            return (Criteria) this;
        }

        public Criteria andUbcLausertidIn(List<Long> values) {
            addCriterion("UBC_LaUserTid in", values, "ubcLausertid");
            return (Criteria) this;
        }

        public Criteria andUbcLausertidNotIn(List<Long> values) {
            addCriterion("UBC_LaUserTid not in", values, "ubcLausertid");
            return (Criteria) this;
        }

        public Criteria andUbcLausertidBetween(Long value1, Long value2) {
            addCriterion("UBC_LaUserTid between", value1, value2, "ubcLausertid");
            return (Criteria) this;
        }

        public Criteria andUbcLausertidNotBetween(Long value1, Long value2) {
            addCriterion("UBC_LaUserTid not between", value1, value2, "ubcLausertid");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnamecnIsNull() {
            addCriterion("UBC_FirstNameCn is null");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnamecnIsNotNull() {
            addCriterion("UBC_FirstNameCn is not null");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnamecnEqualTo(String value) {
            addCriterion("UBC_FirstNameCn =", value, "ubcFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnamecnNotEqualTo(String value) {
            addCriterion("UBC_FirstNameCn <>", value, "ubcFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnamecnGreaterThan(String value) {
            addCriterion("UBC_FirstNameCn >", value, "ubcFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnamecnGreaterThanOrEqualTo(String value) {
            addCriterion("UBC_FirstNameCn >=", value, "ubcFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnamecnLessThan(String value) {
            addCriterion("UBC_FirstNameCn <", value, "ubcFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnamecnLessThanOrEqualTo(String value) {
            addCriterion("UBC_FirstNameCn <=", value, "ubcFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnamecnLike(String value) {
            addCriterion("UBC_FirstNameCn like", value, "ubcFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnamecnNotLike(String value) {
            addCriterion("UBC_FirstNameCn not like", value, "ubcFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnamecnIn(List<String> values) {
            addCriterion("UBC_FirstNameCn in", values, "ubcFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnamecnNotIn(List<String> values) {
            addCriterion("UBC_FirstNameCn not in", values, "ubcFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnamecnBetween(String value1, String value2) {
            addCriterion("UBC_FirstNameCn between", value1, value2, "ubcFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnamecnNotBetween(String value1, String value2) {
            addCriterion("UBC_FirstNameCn not between", value1, value2, "ubcFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnamecnIsNull() {
            addCriterion("UBC_SecondNameCn is null");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnamecnIsNotNull() {
            addCriterion("UBC_SecondNameCn is not null");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnamecnEqualTo(String value) {
            addCriterion("UBC_SecondNameCn =", value, "ubcSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnamecnNotEqualTo(String value) {
            addCriterion("UBC_SecondNameCn <>", value, "ubcSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnamecnGreaterThan(String value) {
            addCriterion("UBC_SecondNameCn >", value, "ubcSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnamecnGreaterThanOrEqualTo(String value) {
            addCriterion("UBC_SecondNameCn >=", value, "ubcSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnamecnLessThan(String value) {
            addCriterion("UBC_SecondNameCn <", value, "ubcSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnamecnLessThanOrEqualTo(String value) {
            addCriterion("UBC_SecondNameCn <=", value, "ubcSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnamecnLike(String value) {
            addCriterion("UBC_SecondNameCn like", value, "ubcSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnamecnNotLike(String value) {
            addCriterion("UBC_SecondNameCn not like", value, "ubcSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnamecnIn(List<String> values) {
            addCriterion("UBC_SecondNameCn in", values, "ubcSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnamecnNotIn(List<String> values) {
            addCriterion("UBC_SecondNameCn not in", values, "ubcSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnamecnBetween(String value1, String value2) {
            addCriterion("UBC_SecondNameCn between", value1, value2, "ubcSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnamecnNotBetween(String value1, String value2) {
            addCriterion("UBC_SecondNameCn not between", value1, value2, "ubcSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnameenIsNull() {
            addCriterion("UBC_FirstNameEn is null");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnameenIsNotNull() {
            addCriterion("UBC_FirstNameEn is not null");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnameenEqualTo(String value) {
            addCriterion("UBC_FirstNameEn =", value, "ubcFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnameenNotEqualTo(String value) {
            addCriterion("UBC_FirstNameEn <>", value, "ubcFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnameenGreaterThan(String value) {
            addCriterion("UBC_FirstNameEn >", value, "ubcFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnameenGreaterThanOrEqualTo(String value) {
            addCriterion("UBC_FirstNameEn >=", value, "ubcFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnameenLessThan(String value) {
            addCriterion("UBC_FirstNameEn <", value, "ubcFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnameenLessThanOrEqualTo(String value) {
            addCriterion("UBC_FirstNameEn <=", value, "ubcFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnameenLike(String value) {
            addCriterion("UBC_FirstNameEn like", value, "ubcFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnameenNotLike(String value) {
            addCriterion("UBC_FirstNameEn not like", value, "ubcFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnameenIn(List<String> values) {
            addCriterion("UBC_FirstNameEn in", values, "ubcFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnameenNotIn(List<String> values) {
            addCriterion("UBC_FirstNameEn not in", values, "ubcFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnameenBetween(String value1, String value2) {
            addCriterion("UBC_FirstNameEn between", value1, value2, "ubcFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUbcFirstnameenNotBetween(String value1, String value2) {
            addCriterion("UBC_FirstNameEn not between", value1, value2, "ubcFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnameenIsNull() {
            addCriterion("UBC_SecondNameEn is null");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnameenIsNotNull() {
            addCriterion("UBC_SecondNameEn is not null");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnameenEqualTo(String value) {
            addCriterion("UBC_SecondNameEn =", value, "ubcSecondnameen");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnameenNotEqualTo(String value) {
            addCriterion("UBC_SecondNameEn <>", value, "ubcSecondnameen");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnameenGreaterThan(String value) {
            addCriterion("UBC_SecondNameEn >", value, "ubcSecondnameen");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnameenGreaterThanOrEqualTo(String value) {
            addCriterion("UBC_SecondNameEn >=", value, "ubcSecondnameen");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnameenLessThan(String value) {
            addCriterion("UBC_SecondNameEn <", value, "ubcSecondnameen");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnameenLessThanOrEqualTo(String value) {
            addCriterion("UBC_SecondNameEn <=", value, "ubcSecondnameen");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnameenLike(String value) {
            addCriterion("UBC_SecondNameEn like", value, "ubcSecondnameen");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnameenNotLike(String value) {
            addCriterion("UBC_SecondNameEn not like", value, "ubcSecondnameen");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnameenIn(List<String> values) {
            addCriterion("UBC_SecondNameEn in", values, "ubcSecondnameen");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnameenNotIn(List<String> values) {
            addCriterion("UBC_SecondNameEn not in", values, "ubcSecondnameen");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnameenBetween(String value1, String value2) {
            addCriterion("UBC_SecondNameEn between", value1, value2, "ubcSecondnameen");
            return (Criteria) this;
        }

        public Criteria andUbcSecondnameenNotBetween(String value1, String value2) {
            addCriterion("UBC_SecondNameEn not between", value1, value2, "ubcSecondnameen");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}