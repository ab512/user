package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Organization extends BaseEntity {


    private String cName;

    private Integer cIndex;

    private String cTreecode;

    private Long cCompanyid;

    private String oAddress;

    private String oZip;

    private String oLeadername;

    private String oLeaderphone;

    private String oQq;

    private Integer oSort;

    private String oUsersystemid;



    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName == null ? null : cName.trim();
    }

    public Integer getcIndex() {
        return cIndex;
    }

    public void setcIndex(Integer cIndex) {
        this.cIndex = cIndex;
    }

    public String getcTreecode() {
        return cTreecode;
    }

    public void setcTreecode(String cTreecode) {
        this.cTreecode = cTreecode == null ? null : cTreecode.trim();
    }

    public Long getcCompanyid() {
        return cCompanyid;
    }

    public void setcCompanyid(Long cCompanyid) {
        this.cCompanyid = cCompanyid;
    }

    public String getoAddress() {
        return oAddress;
    }

    public void setoAddress(String oAddress) {
        this.oAddress = oAddress == null ? null : oAddress.trim();
    }

    public String getoZip() {
        return oZip;
    }

    public void setoZip(String oZip) {
        this.oZip = oZip == null ? null : oZip.trim();
    }

    public String getoLeadername() {
        return oLeadername;
    }

    public void setoLeadername(String oLeadername) {
        this.oLeadername = oLeadername == null ? null : oLeadername.trim();
    }

    public String getoLeaderphone() {
        return oLeaderphone;
    }

    public void setoLeaderphone(String oLeaderphone) {
        this.oLeaderphone = oLeaderphone == null ? null : oLeaderphone.trim();
    }

    public String getoQq() {
        return oQq;
    }

    public void setoQq(String oQq) {
        this.oQq = oQq == null ? null : oQq.trim();
    }

    public Integer getoSort() {
        return oSort;
    }

    public void setoSort(Integer oSort) {
        this.oSort = oSort;
    }

    public String getoUsersystemid() {
        return oUsersystemid;
    }

    public void setoUsersystemid(String oUsersystemid) {
        this.oUsersystemid = oUsersystemid == null ? null : oUsersystemid.trim();
    }
}