package com.loongair.DB.Entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AgentauditExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public AgentauditExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTidIsNull() {
            addCriterion("Tid is null");
            return (Criteria) this;
        }

        public Criteria andTidIsNotNull() {
            addCriterion("Tid is not null");
            return (Criteria) this;
        }

        public Criteria andTidEqualTo(Long value) {
            addCriterion("Tid =", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotEqualTo(Long value) {
            addCriterion("Tid <>", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThan(Long value) {
            addCriterion("Tid >", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThanOrEqualTo(Long value) {
            addCriterion("Tid >=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThan(Long value) {
            addCriterion("Tid <", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThanOrEqualTo(Long value) {
            addCriterion("Tid <=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidIn(List<Long> values) {
            addCriterion("Tid in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotIn(List<Long> values) {
            addCriterion("Tid not in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidBetween(Long value1, Long value2) {
            addCriterion("Tid between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotBetween(Long value1, Long value2) {
            addCriterion("Tid not between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNull() {
            addCriterion("CreateId is null");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNotNull() {
            addCriterion("CreateId is not null");
            return (Criteria) this;
        }

        public Criteria andCreateidEqualTo(Long value) {
            addCriterion("CreateId =", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotEqualTo(Long value) {
            addCriterion("CreateId <>", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThan(Long value) {
            addCriterion("CreateId >", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThanOrEqualTo(Long value) {
            addCriterion("CreateId >=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThan(Long value) {
            addCriterion("CreateId <", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThanOrEqualTo(Long value) {
            addCriterion("CreateId <=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidIn(List<Long> values) {
            addCriterion("CreateId in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotIn(List<Long> values) {
            addCriterion("CreateId not in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidBetween(Long value1, Long value2) {
            addCriterion("CreateId between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotBetween(Long value1, Long value2) {
            addCriterion("CreateId not between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNull() {
            addCriterion("ProxyUserId is null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNotNull() {
            addCriterion("ProxyUserId is not null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridEqualTo(Long value) {
            addCriterion("ProxyUserId =", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotEqualTo(Long value) {
            addCriterion("ProxyUserId <>", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThan(Long value) {
            addCriterion("ProxyUserId >", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId >=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThan(Long value) {
            addCriterion("ProxyUserId <", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId <=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIn(List<Long> values) {
            addCriterion("ProxyUserId in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotIn(List<Long> values) {
            addCriterion("ProxyUserId not in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId not between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNull() {
            addCriterion("LastGuid is null");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNotNull() {
            addCriterion("LastGuid is not null");
            return (Criteria) this;
        }

        public Criteria andLastguidEqualTo(Long value) {
            addCriterion("LastGuid =", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotEqualTo(Long value) {
            addCriterion("LastGuid <>", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThan(Long value) {
            addCriterion("LastGuid >", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThanOrEqualTo(Long value) {
            addCriterion("LastGuid >=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThan(Long value) {
            addCriterion("LastGuid <", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThanOrEqualTo(Long value) {
            addCriterion("LastGuid <=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidIn(List<Long> values) {
            addCriterion("LastGuid in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotIn(List<Long> values) {
            addCriterion("LastGuid not in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidBetween(Long value1, Long value2) {
            addCriterion("LastGuid between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotBetween(Long value1, Long value2) {
            addCriterion("LastGuid not between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andCloneIsNull() {
            addCriterion("Clone is null");
            return (Criteria) this;
        }

        public Criteria andCloneIsNotNull() {
            addCriterion("Clone is not null");
            return (Criteria) this;
        }

        public Criteria andCloneEqualTo(Integer value) {
            addCriterion("Clone =", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotEqualTo(Integer value) {
            addCriterion("Clone <>", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThan(Integer value) {
            addCriterion("Clone >", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThanOrEqualTo(Integer value) {
            addCriterion("Clone >=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThan(Integer value) {
            addCriterion("Clone <", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThanOrEqualTo(Integer value) {
            addCriterion("Clone <=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneIn(List<Integer> values) {
            addCriterion("Clone in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotIn(List<Integer> values) {
            addCriterion("Clone not in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneBetween(Integer value1, Integer value2) {
            addCriterion("Clone between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotBetween(Integer value1, Integer value2) {
            addCriterion("Clone not between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNull() {
            addCriterion("UpdateId is null");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNotNull() {
            addCriterion("UpdateId is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateidEqualTo(Long value) {
            addCriterion("UpdateId =", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotEqualTo(Long value) {
            addCriterion("UpdateId <>", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThan(Long value) {
            addCriterion("UpdateId >", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThanOrEqualTo(Long value) {
            addCriterion("UpdateId >=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThan(Long value) {
            addCriterion("UpdateId <", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThanOrEqualTo(Long value) {
            addCriterion("UpdateId <=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidIn(List<Long> values) {
            addCriterion("UpdateId in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotIn(List<Long> values) {
            addCriterion("UpdateId not in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidBetween(Long value1, Long value2) {
            addCriterion("UpdateId between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotBetween(Long value1, Long value2) {
            addCriterion("UpdateId not between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("UpdateDate is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("UpdateDate is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("UpdateDate =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("UpdateDate <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("UpdateDate >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("UpdateDate >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("UpdateDate <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("UpdateDate <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("UpdateDate in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("UpdateDate not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("UpdateDate between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("UpdateDate not between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("Status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("Status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("Status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("Status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("Status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("Status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("Status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("Status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("Status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("Status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("Status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("Status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andAgaAgentorgtidIsNull() {
            addCriterion("AGA_AgentOrgTid is null");
            return (Criteria) this;
        }

        public Criteria andAgaAgentorgtidIsNotNull() {
            addCriterion("AGA_AgentOrgTid is not null");
            return (Criteria) this;
        }

        public Criteria andAgaAgentorgtidEqualTo(Long value) {
            addCriterion("AGA_AgentOrgTid =", value, "agaAgentorgtid");
            return (Criteria) this;
        }

        public Criteria andAgaAgentorgtidNotEqualTo(Long value) {
            addCriterion("AGA_AgentOrgTid <>", value, "agaAgentorgtid");
            return (Criteria) this;
        }

        public Criteria andAgaAgentorgtidGreaterThan(Long value) {
            addCriterion("AGA_AgentOrgTid >", value, "agaAgentorgtid");
            return (Criteria) this;
        }

        public Criteria andAgaAgentorgtidGreaterThanOrEqualTo(Long value) {
            addCriterion("AGA_AgentOrgTid >=", value, "agaAgentorgtid");
            return (Criteria) this;
        }

        public Criteria andAgaAgentorgtidLessThan(Long value) {
            addCriterion("AGA_AgentOrgTid <", value, "agaAgentorgtid");
            return (Criteria) this;
        }

        public Criteria andAgaAgentorgtidLessThanOrEqualTo(Long value) {
            addCriterion("AGA_AgentOrgTid <=", value, "agaAgentorgtid");
            return (Criteria) this;
        }

        public Criteria andAgaAgentorgtidIn(List<Long> values) {
            addCriterion("AGA_AgentOrgTid in", values, "agaAgentorgtid");
            return (Criteria) this;
        }

        public Criteria andAgaAgentorgtidNotIn(List<Long> values) {
            addCriterion("AGA_AgentOrgTid not in", values, "agaAgentorgtid");
            return (Criteria) this;
        }

        public Criteria andAgaAgentorgtidBetween(Long value1, Long value2) {
            addCriterion("AGA_AgentOrgTid between", value1, value2, "agaAgentorgtid");
            return (Criteria) this;
        }

        public Criteria andAgaAgentorgtidNotBetween(Long value1, Long value2) {
            addCriterion("AGA_AgentOrgTid not between", value1, value2, "agaAgentorgtid");
            return (Criteria) this;
        }

        public Criteria andAgaIndexIsNull() {
            addCriterion("AGA_Index is null");
            return (Criteria) this;
        }

        public Criteria andAgaIndexIsNotNull() {
            addCriterion("AGA_Index is not null");
            return (Criteria) this;
        }

        public Criteria andAgaIndexEqualTo(Integer value) {
            addCriterion("AGA_Index =", value, "agaIndex");
            return (Criteria) this;
        }

        public Criteria andAgaIndexNotEqualTo(Integer value) {
            addCriterion("AGA_Index <>", value, "agaIndex");
            return (Criteria) this;
        }

        public Criteria andAgaIndexGreaterThan(Integer value) {
            addCriterion("AGA_Index >", value, "agaIndex");
            return (Criteria) this;
        }

        public Criteria andAgaIndexGreaterThanOrEqualTo(Integer value) {
            addCriterion("AGA_Index >=", value, "agaIndex");
            return (Criteria) this;
        }

        public Criteria andAgaIndexLessThan(Integer value) {
            addCriterion("AGA_Index <", value, "agaIndex");
            return (Criteria) this;
        }

        public Criteria andAgaIndexLessThanOrEqualTo(Integer value) {
            addCriterion("AGA_Index <=", value, "agaIndex");
            return (Criteria) this;
        }

        public Criteria andAgaIndexIn(List<Integer> values) {
            addCriterion("AGA_Index in", values, "agaIndex");
            return (Criteria) this;
        }

        public Criteria andAgaIndexNotIn(List<Integer> values) {
            addCriterion("AGA_Index not in", values, "agaIndex");
            return (Criteria) this;
        }

        public Criteria andAgaIndexBetween(Integer value1, Integer value2) {
            addCriterion("AGA_Index between", value1, value2, "agaIndex");
            return (Criteria) this;
        }

        public Criteria andAgaIndexNotBetween(Integer value1, Integer value2) {
            addCriterion("AGA_Index not between", value1, value2, "agaIndex");
            return (Criteria) this;
        }

        public Criteria andAgaAuditortidIsNull() {
            addCriterion("AGA_AuditorTid is null");
            return (Criteria) this;
        }

        public Criteria andAgaAuditortidIsNotNull() {
            addCriterion("AGA_AuditorTid is not null");
            return (Criteria) this;
        }

        public Criteria andAgaAuditortidEqualTo(Long value) {
            addCriterion("AGA_AuditorTid =", value, "agaAuditortid");
            return (Criteria) this;
        }

        public Criteria andAgaAuditortidNotEqualTo(Long value) {
            addCriterion("AGA_AuditorTid <>", value, "agaAuditortid");
            return (Criteria) this;
        }

        public Criteria andAgaAuditortidGreaterThan(Long value) {
            addCriterion("AGA_AuditorTid >", value, "agaAuditortid");
            return (Criteria) this;
        }

        public Criteria andAgaAuditortidGreaterThanOrEqualTo(Long value) {
            addCriterion("AGA_AuditorTid >=", value, "agaAuditortid");
            return (Criteria) this;
        }

        public Criteria andAgaAuditortidLessThan(Long value) {
            addCriterion("AGA_AuditorTid <", value, "agaAuditortid");
            return (Criteria) this;
        }

        public Criteria andAgaAuditortidLessThanOrEqualTo(Long value) {
            addCriterion("AGA_AuditorTid <=", value, "agaAuditortid");
            return (Criteria) this;
        }

        public Criteria andAgaAuditortidIn(List<Long> values) {
            addCriterion("AGA_AuditorTid in", values, "agaAuditortid");
            return (Criteria) this;
        }

        public Criteria andAgaAuditortidNotIn(List<Long> values) {
            addCriterion("AGA_AuditorTid not in", values, "agaAuditortid");
            return (Criteria) this;
        }

        public Criteria andAgaAuditortidBetween(Long value1, Long value2) {
            addCriterion("AGA_AuditorTid between", value1, value2, "agaAuditortid");
            return (Criteria) this;
        }

        public Criteria andAgaAuditortidNotBetween(Long value1, Long value2) {
            addCriterion("AGA_AuditorTid not between", value1, value2, "agaAuditortid");
            return (Criteria) this;
        }

        public Criteria andAgaNoteIsNull() {
            addCriterion("AGA_Note is null");
            return (Criteria) this;
        }

        public Criteria andAgaNoteIsNotNull() {
            addCriterion("AGA_Note is not null");
            return (Criteria) this;
        }

        public Criteria andAgaNoteEqualTo(String value) {
            addCriterion("AGA_Note =", value, "agaNote");
            return (Criteria) this;
        }

        public Criteria andAgaNoteNotEqualTo(String value) {
            addCriterion("AGA_Note <>", value, "agaNote");
            return (Criteria) this;
        }

        public Criteria andAgaNoteGreaterThan(String value) {
            addCriterion("AGA_Note >", value, "agaNote");
            return (Criteria) this;
        }

        public Criteria andAgaNoteGreaterThanOrEqualTo(String value) {
            addCriterion("AGA_Note >=", value, "agaNote");
            return (Criteria) this;
        }

        public Criteria andAgaNoteLessThan(String value) {
            addCriterion("AGA_Note <", value, "agaNote");
            return (Criteria) this;
        }

        public Criteria andAgaNoteLessThanOrEqualTo(String value) {
            addCriterion("AGA_Note <=", value, "agaNote");
            return (Criteria) this;
        }

        public Criteria andAgaNoteLike(String value) {
            addCriterion("AGA_Note like", value, "agaNote");
            return (Criteria) this;
        }

        public Criteria andAgaNoteNotLike(String value) {
            addCriterion("AGA_Note not like", value, "agaNote");
            return (Criteria) this;
        }

        public Criteria andAgaNoteIn(List<String> values) {
            addCriterion("AGA_Note in", values, "agaNote");
            return (Criteria) this;
        }

        public Criteria andAgaNoteNotIn(List<String> values) {
            addCriterion("AGA_Note not in", values, "agaNote");
            return (Criteria) this;
        }

        public Criteria andAgaNoteBetween(String value1, String value2) {
            addCriterion("AGA_Note between", value1, value2, "agaNote");
            return (Criteria) this;
        }

        public Criteria andAgaNoteNotBetween(String value1, String value2) {
            addCriterion("AGA_Note not between", value1, value2, "agaNote");
            return (Criteria) this;
        }

        public Criteria andAgaAuditorIsNull() {
            addCriterion("AGA_Auditor is null");
            return (Criteria) this;
        }

        public Criteria andAgaAuditorIsNotNull() {
            addCriterion("AGA_Auditor is not null");
            return (Criteria) this;
        }

        public Criteria andAgaAuditorEqualTo(String value) {
            addCriterion("AGA_Auditor =", value, "agaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgaAuditorNotEqualTo(String value) {
            addCriterion("AGA_Auditor <>", value, "agaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgaAuditorGreaterThan(String value) {
            addCriterion("AGA_Auditor >", value, "agaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgaAuditorGreaterThanOrEqualTo(String value) {
            addCriterion("AGA_Auditor >=", value, "agaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgaAuditorLessThan(String value) {
            addCriterion("AGA_Auditor <", value, "agaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgaAuditorLessThanOrEqualTo(String value) {
            addCriterion("AGA_Auditor <=", value, "agaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgaAuditorLike(String value) {
            addCriterion("AGA_Auditor like", value, "agaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgaAuditorNotLike(String value) {
            addCriterion("AGA_Auditor not like", value, "agaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgaAuditorIn(List<String> values) {
            addCriterion("AGA_Auditor in", values, "agaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgaAuditorNotIn(List<String> values) {
            addCriterion("AGA_Auditor not in", values, "agaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgaAuditorBetween(String value1, String value2) {
            addCriterion("AGA_Auditor between", value1, value2, "agaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgaAuditorNotBetween(String value1, String value2) {
            addCriterion("AGA_Auditor not between", value1, value2, "agaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgaAuditretIsNull() {
            addCriterion("AGA_AuditRet is null");
            return (Criteria) this;
        }

        public Criteria andAgaAuditretIsNotNull() {
            addCriterion("AGA_AuditRet is not null");
            return (Criteria) this;
        }

        public Criteria andAgaAuditretEqualTo(Integer value) {
            addCriterion("AGA_AuditRet =", value, "agaAuditret");
            return (Criteria) this;
        }

        public Criteria andAgaAuditretNotEqualTo(Integer value) {
            addCriterion("AGA_AuditRet <>", value, "agaAuditret");
            return (Criteria) this;
        }

        public Criteria andAgaAuditretGreaterThan(Integer value) {
            addCriterion("AGA_AuditRet >", value, "agaAuditret");
            return (Criteria) this;
        }

        public Criteria andAgaAuditretGreaterThanOrEqualTo(Integer value) {
            addCriterion("AGA_AuditRet >=", value, "agaAuditret");
            return (Criteria) this;
        }

        public Criteria andAgaAuditretLessThan(Integer value) {
            addCriterion("AGA_AuditRet <", value, "agaAuditret");
            return (Criteria) this;
        }

        public Criteria andAgaAuditretLessThanOrEqualTo(Integer value) {
            addCriterion("AGA_AuditRet <=", value, "agaAuditret");
            return (Criteria) this;
        }

        public Criteria andAgaAuditretIn(List<Integer> values) {
            addCriterion("AGA_AuditRet in", values, "agaAuditret");
            return (Criteria) this;
        }

        public Criteria andAgaAuditretNotIn(List<Integer> values) {
            addCriterion("AGA_AuditRet not in", values, "agaAuditret");
            return (Criteria) this;
        }

        public Criteria andAgaAuditretBetween(Integer value1, Integer value2) {
            addCriterion("AGA_AuditRet between", value1, value2, "agaAuditret");
            return (Criteria) this;
        }

        public Criteria andAgaAuditretNotBetween(Integer value1, Integer value2) {
            addCriterion("AGA_AuditRet not between", value1, value2, "agaAuditret");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}