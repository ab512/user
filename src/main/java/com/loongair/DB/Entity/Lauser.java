package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Lauser extends BaseEntity {


    private String buToken;

    private Long uCompanyid;

    private String uUserorgcode;

    private String uLoginname;

    private String uPassword;

    private String uName;

    private Date uLastlogintime;

    private String uLastloginip;

    private Integer uLogintimes;

    private Date uLoginlocktime;

    private Integer uLoginerrortime;

    private String uPhone;

    private String uQq;

    private String uEmail;

    private String uAddress;

    private String uCity;

    private String uSecurityquestion;

    private String uSecurityanswer;

    private Integer uLoginsecurity;

    private String uTmpguid;

    private Integer uSextype;

    private String uWorkphone;

    private Date uBirthday;

    private String uUserroles;

    private String buContacthope;

    private String buFax;

    private String buPpmeals;

    private String buPpseats;

    private String buPpdiscount;

    private String buPpchannel;

    private String buPppaymentmethod;

    private Long buContactaddresshopeid;

    private Long buCompanyaddressid;

    private Long buHomeaddressid;

    private Integer buNationality;

    private Integer buLanguagehope;

    private String buWechat;



    public String getBuToken() {
        return buToken;
    }

    public void setBuToken(String buToken) {
        this.buToken = buToken == null ? null : buToken.trim();
    }

    public Long getuCompanyid() {
        return uCompanyid;
    }

    public void setuCompanyid(Long uCompanyid) {
        this.uCompanyid = uCompanyid;
    }

    public String getuUserorgcode() {
        return uUserorgcode;
    }

    public void setuUserorgcode(String uUserorgcode) {
        this.uUserorgcode = uUserorgcode == null ? null : uUserorgcode.trim();
    }

    public String getuLoginname() {
        return uLoginname;
    }

    public void setuLoginname(String uLoginname) {
        this.uLoginname = uLoginname == null ? null : uLoginname.trim();
    }

    public String getuPassword() {
        return uPassword;
    }

    public void setuPassword(String uPassword) {
        this.uPassword = uPassword == null ? null : uPassword.trim();
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName == null ? null : uName.trim();
    }

    public Date getuLastlogintime() {
        return uLastlogintime;
    }

    public void setuLastlogintime(Date uLastlogintime) {
        this.uLastlogintime = uLastlogintime;
    }

    public String getuLastloginip() {
        return uLastloginip;
    }

    public void setuLastloginip(String uLastloginip) {
        this.uLastloginip = uLastloginip == null ? null : uLastloginip.trim();
    }

    public Integer getuLogintimes() {
        return uLogintimes;
    }

    public void setuLogintimes(Integer uLogintimes) {
        this.uLogintimes = uLogintimes;
    }

    public Date getuLoginlocktime() {
        return uLoginlocktime;
    }

    public void setuLoginlocktime(Date uLoginlocktime) {
        this.uLoginlocktime = uLoginlocktime;
    }

    public Integer getuLoginerrortime() {
        return uLoginerrortime;
    }

    public void setuLoginerrortime(Integer uLoginerrortime) {
        this.uLoginerrortime = uLoginerrortime;
    }

    public String getuPhone() {
        return uPhone;
    }

    public void setuPhone(String uPhone) {
        this.uPhone = uPhone == null ? null : uPhone.trim();
    }

    public String getuQq() {
        return uQq;
    }

    public void setuQq(String uQq) {
        this.uQq = uQq == null ? null : uQq.trim();
    }

    public String getuEmail() {
        return uEmail;
    }

    public void setuEmail(String uEmail) {
        this.uEmail = uEmail == null ? null : uEmail.trim();
    }

    public String getuAddress() {
        return uAddress;
    }

    public void setuAddress(String uAddress) {
        this.uAddress = uAddress == null ? null : uAddress.trim();
    }

    public String getuCity() {
        return uCity;
    }

    public void setuCity(String uCity) {
        this.uCity = uCity == null ? null : uCity.trim();
    }

    public String getuSecurityquestion() {
        return uSecurityquestion;
    }

    public void setuSecurityquestion(String uSecurityquestion) {
        this.uSecurityquestion = uSecurityquestion == null ? null : uSecurityquestion.trim();
    }

    public String getuSecurityanswer() {
        return uSecurityanswer;
    }

    public void setuSecurityanswer(String uSecurityanswer) {
        this.uSecurityanswer = uSecurityanswer == null ? null : uSecurityanswer.trim();
    }

    public Integer getuLoginsecurity() {
        return uLoginsecurity;
    }

    public void setuLoginsecurity(Integer uLoginsecurity) {
        this.uLoginsecurity = uLoginsecurity;
    }

    public String getuTmpguid() {
        return uTmpguid;
    }

    public void setuTmpguid(String uTmpguid) {
        this.uTmpguid = uTmpguid == null ? null : uTmpguid.trim();
    }

    public Integer getuSextype() {
        return uSextype;
    }

    public void setuSextype(Integer uSextype) {
        this.uSextype = uSextype;
    }

    public String getuWorkphone() {
        return uWorkphone;
    }

    public void setuWorkphone(String uWorkphone) {
        this.uWorkphone = uWorkphone == null ? null : uWorkphone.trim();
    }

    public Date getuBirthday() {
        return uBirthday;
    }

    public void setuBirthday(Date uBirthday) {
        this.uBirthday = uBirthday;
    }

    public String getuUserroles() {
        return uUserroles;
    }

    public void setuUserroles(String uUserroles) {
        this.uUserroles = uUserroles == null ? null : uUserroles.trim();
    }

    public String getBuContacthope() {
        return buContacthope;
    }

    public void setBuContacthope(String buContacthope) {
        this.buContacthope = buContacthope == null ? null : buContacthope.trim();
    }

    public String getBuFax() {
        return buFax;
    }

    public void setBuFax(String buFax) {
        this.buFax = buFax == null ? null : buFax.trim();
    }

    public String getBuPpmeals() {
        return buPpmeals;
    }

    public void setBuPpmeals(String buPpmeals) {
        this.buPpmeals = buPpmeals == null ? null : buPpmeals.trim();
    }

    public String getBuPpseats() {
        return buPpseats;
    }

    public void setBuPpseats(String buPpseats) {
        this.buPpseats = buPpseats == null ? null : buPpseats.trim();
    }

    public String getBuPpdiscount() {
        return buPpdiscount;
    }

    public void setBuPpdiscount(String buPpdiscount) {
        this.buPpdiscount = buPpdiscount == null ? null : buPpdiscount.trim();
    }

    public String getBuPpchannel() {
        return buPpchannel;
    }

    public void setBuPpchannel(String buPpchannel) {
        this.buPpchannel = buPpchannel == null ? null : buPpchannel.trim();
    }

    public String getBuPppaymentmethod() {
        return buPppaymentmethod;
    }

    public void setBuPppaymentmethod(String buPppaymentmethod) {
        this.buPppaymentmethod = buPppaymentmethod == null ? null : buPppaymentmethod.trim();
    }

    public Long getBuContactaddresshopeid() {
        return buContactaddresshopeid;
    }

    public void setBuContactaddresshopeid(Long buContactaddresshopeid) {
        this.buContactaddresshopeid = buContactaddresshopeid;
    }

    public Long getBuCompanyaddressid() {
        return buCompanyaddressid;
    }

    public void setBuCompanyaddressid(Long buCompanyaddressid) {
        this.buCompanyaddressid = buCompanyaddressid;
    }

    public Long getBuHomeaddressid() {
        return buHomeaddressid;
    }

    public void setBuHomeaddressid(Long buHomeaddressid) {
        this.buHomeaddressid = buHomeaddressid;
    }

    public Integer getBuNationality() {
        return buNationality;
    }

    public void setBuNationality(Integer buNationality) {
        this.buNationality = buNationality;
    }

    public Integer getBuLanguagehope() {
        return buLanguagehope;
    }

    public void setBuLanguagehope(Integer buLanguagehope) {
        this.buLanguagehope = buLanguagehope;
    }

    public String getBuWechat() {
        return buWechat;
    }

    public void setBuWechat(String buWechat) {
        this.buWechat = buWechat == null ? null : buWechat.trim();
    }
}