package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Orguserrelation extends BaseEntity {


    private Long ourOrgtid;

    private Long ourUsertid;



    public Long getOurOrgtid() {
        return ourOrgtid;
    }

    public void setOurOrgtid(Long ourOrgtid) {
        this.ourOrgtid = ourOrgtid;
    }

    public Long getOurUsertid() {
        return ourUsertid;
    }

    public void setOurUsertid(Long ourUsertid) {
        this.ourUsertid = ourUsertid;
    }
}