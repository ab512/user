package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Laorg extends BaseEntity {


    private String loName;

    private Integer loPlat;



    public String getLoName() {
        return loName;
    }

    public void setLoName(String loName) {
        this.loName = loName == null ? null : loName.trim();
    }

    public Integer getLoPlat() {
        return loPlat;
    }

    public void setLoPlat(Integer loPlat) {
        this.loPlat = loPlat;
    }
}