package com.loongair.DB.Entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserfrepassengerExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UserfrepassengerExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTidIsNull() {
            addCriterion("Tid is null");
            return (Criteria) this;
        }

        public Criteria andTidIsNotNull() {
            addCriterion("Tid is not null");
            return (Criteria) this;
        }

        public Criteria andTidEqualTo(Long value) {
            addCriterion("Tid =", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotEqualTo(Long value) {
            addCriterion("Tid <>", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThan(Long value) {
            addCriterion("Tid >", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThanOrEqualTo(Long value) {
            addCriterion("Tid >=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThan(Long value) {
            addCriterion("Tid <", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThanOrEqualTo(Long value) {
            addCriterion("Tid <=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidIn(List<Long> values) {
            addCriterion("Tid in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotIn(List<Long> values) {
            addCriterion("Tid not in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidBetween(Long value1, Long value2) {
            addCriterion("Tid between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotBetween(Long value1, Long value2) {
            addCriterion("Tid not between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNull() {
            addCriterion("CreateId is null");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNotNull() {
            addCriterion("CreateId is not null");
            return (Criteria) this;
        }

        public Criteria andCreateidEqualTo(Long value) {
            addCriterion("CreateId =", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotEqualTo(Long value) {
            addCriterion("CreateId <>", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThan(Long value) {
            addCriterion("CreateId >", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThanOrEqualTo(Long value) {
            addCriterion("CreateId >=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThan(Long value) {
            addCriterion("CreateId <", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThanOrEqualTo(Long value) {
            addCriterion("CreateId <=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidIn(List<Long> values) {
            addCriterion("CreateId in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotIn(List<Long> values) {
            addCriterion("CreateId not in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidBetween(Long value1, Long value2) {
            addCriterion("CreateId between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotBetween(Long value1, Long value2) {
            addCriterion("CreateId not between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNull() {
            addCriterion("ProxyUserId is null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNotNull() {
            addCriterion("ProxyUserId is not null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridEqualTo(Long value) {
            addCriterion("ProxyUserId =", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotEqualTo(Long value) {
            addCriterion("ProxyUserId <>", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThan(Long value) {
            addCriterion("ProxyUserId >", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId >=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThan(Long value) {
            addCriterion("ProxyUserId <", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId <=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIn(List<Long> values) {
            addCriterion("ProxyUserId in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotIn(List<Long> values) {
            addCriterion("ProxyUserId not in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId not between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNull() {
            addCriterion("LastGuid is null");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNotNull() {
            addCriterion("LastGuid is not null");
            return (Criteria) this;
        }

        public Criteria andLastguidEqualTo(Long value) {
            addCriterion("LastGuid =", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotEqualTo(Long value) {
            addCriterion("LastGuid <>", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThan(Long value) {
            addCriterion("LastGuid >", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThanOrEqualTo(Long value) {
            addCriterion("LastGuid >=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThan(Long value) {
            addCriterion("LastGuid <", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThanOrEqualTo(Long value) {
            addCriterion("LastGuid <=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidIn(List<Long> values) {
            addCriterion("LastGuid in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotIn(List<Long> values) {
            addCriterion("LastGuid not in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidBetween(Long value1, Long value2) {
            addCriterion("LastGuid between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotBetween(Long value1, Long value2) {
            addCriterion("LastGuid not between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andCloneIsNull() {
            addCriterion("Clone is null");
            return (Criteria) this;
        }

        public Criteria andCloneIsNotNull() {
            addCriterion("Clone is not null");
            return (Criteria) this;
        }

        public Criteria andCloneEqualTo(Integer value) {
            addCriterion("Clone =", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotEqualTo(Integer value) {
            addCriterion("Clone <>", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThan(Integer value) {
            addCriterion("Clone >", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThanOrEqualTo(Integer value) {
            addCriterion("Clone >=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThan(Integer value) {
            addCriterion("Clone <", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThanOrEqualTo(Integer value) {
            addCriterion("Clone <=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneIn(List<Integer> values) {
            addCriterion("Clone in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotIn(List<Integer> values) {
            addCriterion("Clone not in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneBetween(Integer value1, Integer value2) {
            addCriterion("Clone between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotBetween(Integer value1, Integer value2) {
            addCriterion("Clone not between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNull() {
            addCriterion("UpdateId is null");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNotNull() {
            addCriterion("UpdateId is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateidEqualTo(Long value) {
            addCriterion("UpdateId =", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotEqualTo(Long value) {
            addCriterion("UpdateId <>", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThan(Long value) {
            addCriterion("UpdateId >", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThanOrEqualTo(Long value) {
            addCriterion("UpdateId >=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThan(Long value) {
            addCriterion("UpdateId <", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThanOrEqualTo(Long value) {
            addCriterion("UpdateId <=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidIn(List<Long> values) {
            addCriterion("UpdateId in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotIn(List<Long> values) {
            addCriterion("UpdateId not in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidBetween(Long value1, Long value2) {
            addCriterion("UpdateId between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotBetween(Long value1, Long value2) {
            addCriterion("UpdateId not between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("UpdateDate is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("UpdateDate is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("UpdateDate =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("UpdateDate <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("UpdateDate >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("UpdateDate >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("UpdateDate <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("UpdateDate <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("UpdateDate in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("UpdateDate not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("UpdateDate between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("UpdateDate not between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("Status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("Status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("Status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("Status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("Status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("Status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("Status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("Status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("Status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("Status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("Status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("Status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andUfpLausertidIsNull() {
            addCriterion("UFP_LaUserTid is null");
            return (Criteria) this;
        }

        public Criteria andUfpLausertidIsNotNull() {
            addCriterion("UFP_LaUserTid is not null");
            return (Criteria) this;
        }

        public Criteria andUfpLausertidEqualTo(Long value) {
            addCriterion("UFP_LaUserTid =", value, "ufpLausertid");
            return (Criteria) this;
        }

        public Criteria andUfpLausertidNotEqualTo(Long value) {
            addCriterion("UFP_LaUserTid <>", value, "ufpLausertid");
            return (Criteria) this;
        }

        public Criteria andUfpLausertidGreaterThan(Long value) {
            addCriterion("UFP_LaUserTid >", value, "ufpLausertid");
            return (Criteria) this;
        }

        public Criteria andUfpLausertidGreaterThanOrEqualTo(Long value) {
            addCriterion("UFP_LaUserTid >=", value, "ufpLausertid");
            return (Criteria) this;
        }

        public Criteria andUfpLausertidLessThan(Long value) {
            addCriterion("UFP_LaUserTid <", value, "ufpLausertid");
            return (Criteria) this;
        }

        public Criteria andUfpLausertidLessThanOrEqualTo(Long value) {
            addCriterion("UFP_LaUserTid <=", value, "ufpLausertid");
            return (Criteria) this;
        }

        public Criteria andUfpLausertidIn(List<Long> values) {
            addCriterion("UFP_LaUserTid in", values, "ufpLausertid");
            return (Criteria) this;
        }

        public Criteria andUfpLausertidNotIn(List<Long> values) {
            addCriterion("UFP_LaUserTid not in", values, "ufpLausertid");
            return (Criteria) this;
        }

        public Criteria andUfpLausertidBetween(Long value1, Long value2) {
            addCriterion("UFP_LaUserTid between", value1, value2, "ufpLausertid");
            return (Criteria) this;
        }

        public Criteria andUfpLausertidNotBetween(Long value1, Long value2) {
            addCriterion("UFP_LaUserTid not between", value1, value2, "ufpLausertid");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnamecnIsNull() {
            addCriterion("UFP_FirstNameCn is null");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnamecnIsNotNull() {
            addCriterion("UFP_FirstNameCn is not null");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnamecnEqualTo(String value) {
            addCriterion("UFP_FirstNameCn =", value, "ufpFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnamecnNotEqualTo(String value) {
            addCriterion("UFP_FirstNameCn <>", value, "ufpFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnamecnGreaterThan(String value) {
            addCriterion("UFP_FirstNameCn >", value, "ufpFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnamecnGreaterThanOrEqualTo(String value) {
            addCriterion("UFP_FirstNameCn >=", value, "ufpFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnamecnLessThan(String value) {
            addCriterion("UFP_FirstNameCn <", value, "ufpFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnamecnLessThanOrEqualTo(String value) {
            addCriterion("UFP_FirstNameCn <=", value, "ufpFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnamecnLike(String value) {
            addCriterion("UFP_FirstNameCn like", value, "ufpFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnamecnNotLike(String value) {
            addCriterion("UFP_FirstNameCn not like", value, "ufpFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnamecnIn(List<String> values) {
            addCriterion("UFP_FirstNameCn in", values, "ufpFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnamecnNotIn(List<String> values) {
            addCriterion("UFP_FirstNameCn not in", values, "ufpFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnamecnBetween(String value1, String value2) {
            addCriterion("UFP_FirstNameCn between", value1, value2, "ufpFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnamecnNotBetween(String value1, String value2) {
            addCriterion("UFP_FirstNameCn not between", value1, value2, "ufpFirstnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnamecnIsNull() {
            addCriterion("UFP_SecondNameCn is null");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnamecnIsNotNull() {
            addCriterion("UFP_SecondNameCn is not null");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnamecnEqualTo(String value) {
            addCriterion("UFP_SecondNameCn =", value, "ufpSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnamecnNotEqualTo(String value) {
            addCriterion("UFP_SecondNameCn <>", value, "ufpSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnamecnGreaterThan(String value) {
            addCriterion("UFP_SecondNameCn >", value, "ufpSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnamecnGreaterThanOrEqualTo(String value) {
            addCriterion("UFP_SecondNameCn >=", value, "ufpSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnamecnLessThan(String value) {
            addCriterion("UFP_SecondNameCn <", value, "ufpSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnamecnLessThanOrEqualTo(String value) {
            addCriterion("UFP_SecondNameCn <=", value, "ufpSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnamecnLike(String value) {
            addCriterion("UFP_SecondNameCn like", value, "ufpSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnamecnNotLike(String value) {
            addCriterion("UFP_SecondNameCn not like", value, "ufpSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnamecnIn(List<String> values) {
            addCriterion("UFP_SecondNameCn in", values, "ufpSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnamecnNotIn(List<String> values) {
            addCriterion("UFP_SecondNameCn not in", values, "ufpSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnamecnBetween(String value1, String value2) {
            addCriterion("UFP_SecondNameCn between", value1, value2, "ufpSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnamecnNotBetween(String value1, String value2) {
            addCriterion("UFP_SecondNameCn not between", value1, value2, "ufpSecondnamecn");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnameenIsNull() {
            addCriterion("UFP_FirstNameEn is null");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnameenIsNotNull() {
            addCriterion("UFP_FirstNameEn is not null");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnameenEqualTo(String value) {
            addCriterion("UFP_FirstNameEn =", value, "ufpFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnameenNotEqualTo(String value) {
            addCriterion("UFP_FirstNameEn <>", value, "ufpFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnameenGreaterThan(String value) {
            addCriterion("UFP_FirstNameEn >", value, "ufpFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnameenGreaterThanOrEqualTo(String value) {
            addCriterion("UFP_FirstNameEn >=", value, "ufpFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnameenLessThan(String value) {
            addCriterion("UFP_FirstNameEn <", value, "ufpFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnameenLessThanOrEqualTo(String value) {
            addCriterion("UFP_FirstNameEn <=", value, "ufpFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnameenLike(String value) {
            addCriterion("UFP_FirstNameEn like", value, "ufpFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnameenNotLike(String value) {
            addCriterion("UFP_FirstNameEn not like", value, "ufpFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnameenIn(List<String> values) {
            addCriterion("UFP_FirstNameEn in", values, "ufpFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnameenNotIn(List<String> values) {
            addCriterion("UFP_FirstNameEn not in", values, "ufpFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnameenBetween(String value1, String value2) {
            addCriterion("UFP_FirstNameEn between", value1, value2, "ufpFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUfpFirstnameenNotBetween(String value1, String value2) {
            addCriterion("UFP_FirstNameEn not between", value1, value2, "ufpFirstnameen");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnameenIsNull() {
            addCriterion("UFP_SecondNameEn is null");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnameenIsNotNull() {
            addCriterion("UFP_SecondNameEn is not null");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnameenEqualTo(String value) {
            addCriterion("UFP_SecondNameEn =", value, "ufpSecondnameen");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnameenNotEqualTo(String value) {
            addCriterion("UFP_SecondNameEn <>", value, "ufpSecondnameen");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnameenGreaterThan(String value) {
            addCriterion("UFP_SecondNameEn >", value, "ufpSecondnameen");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnameenGreaterThanOrEqualTo(String value) {
            addCriterion("UFP_SecondNameEn >=", value, "ufpSecondnameen");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnameenLessThan(String value) {
            addCriterion("UFP_SecondNameEn <", value, "ufpSecondnameen");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnameenLessThanOrEqualTo(String value) {
            addCriterion("UFP_SecondNameEn <=", value, "ufpSecondnameen");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnameenLike(String value) {
            addCriterion("UFP_SecondNameEn like", value, "ufpSecondnameen");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnameenNotLike(String value) {
            addCriterion("UFP_SecondNameEn not like", value, "ufpSecondnameen");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnameenIn(List<String> values) {
            addCriterion("UFP_SecondNameEn in", values, "ufpSecondnameen");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnameenNotIn(List<String> values) {
            addCriterion("UFP_SecondNameEn not in", values, "ufpSecondnameen");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnameenBetween(String value1, String value2) {
            addCriterion("UFP_SecondNameEn between", value1, value2, "ufpSecondnameen");
            return (Criteria) this;
        }

        public Criteria andUfpSecondnameenNotBetween(String value1, String value2) {
            addCriterion("UFP_SecondNameEn not between", value1, value2, "ufpSecondnameen");
            return (Criteria) this;
        }

        public Criteria andUfpIntegralIsNull() {
            addCriterion("UFP_Integral is null");
            return (Criteria) this;
        }

        public Criteria andUfpIntegralIsNotNull() {
            addCriterion("UFP_Integral is not null");
            return (Criteria) this;
        }

        public Criteria andUfpIntegralEqualTo(Integer value) {
            addCriterion("UFP_Integral =", value, "ufpIntegral");
            return (Criteria) this;
        }

        public Criteria andUfpIntegralNotEqualTo(Integer value) {
            addCriterion("UFP_Integral <>", value, "ufpIntegral");
            return (Criteria) this;
        }

        public Criteria andUfpIntegralGreaterThan(Integer value) {
            addCriterion("UFP_Integral >", value, "ufpIntegral");
            return (Criteria) this;
        }

        public Criteria andUfpIntegralGreaterThanOrEqualTo(Integer value) {
            addCriterion("UFP_Integral >=", value, "ufpIntegral");
            return (Criteria) this;
        }

        public Criteria andUfpIntegralLessThan(Integer value) {
            addCriterion("UFP_Integral <", value, "ufpIntegral");
            return (Criteria) this;
        }

        public Criteria andUfpIntegralLessThanOrEqualTo(Integer value) {
            addCriterion("UFP_Integral <=", value, "ufpIntegral");
            return (Criteria) this;
        }

        public Criteria andUfpIntegralIn(List<Integer> values) {
            addCriterion("UFP_Integral in", values, "ufpIntegral");
            return (Criteria) this;
        }

        public Criteria andUfpIntegralNotIn(List<Integer> values) {
            addCriterion("UFP_Integral not in", values, "ufpIntegral");
            return (Criteria) this;
        }

        public Criteria andUfpIntegralBetween(Integer value1, Integer value2) {
            addCriterion("UFP_Integral between", value1, value2, "ufpIntegral");
            return (Criteria) this;
        }

        public Criteria andUfpIntegralNotBetween(Integer value1, Integer value2) {
            addCriterion("UFP_Integral not between", value1, value2, "ufpIntegral");
            return (Criteria) this;
        }

        public Criteria andUfpVipcardnumIsNull() {
            addCriterion("UFP_VIPCardNum is null");
            return (Criteria) this;
        }

        public Criteria andUfpVipcardnumIsNotNull() {
            addCriterion("UFP_VIPCardNum is not null");
            return (Criteria) this;
        }

        public Criteria andUfpVipcardnumEqualTo(String value) {
            addCriterion("UFP_VIPCardNum =", value, "ufpVipcardnum");
            return (Criteria) this;
        }

        public Criteria andUfpVipcardnumNotEqualTo(String value) {
            addCriterion("UFP_VIPCardNum <>", value, "ufpVipcardnum");
            return (Criteria) this;
        }

        public Criteria andUfpVipcardnumGreaterThan(String value) {
            addCriterion("UFP_VIPCardNum >", value, "ufpVipcardnum");
            return (Criteria) this;
        }

        public Criteria andUfpVipcardnumGreaterThanOrEqualTo(String value) {
            addCriterion("UFP_VIPCardNum >=", value, "ufpVipcardnum");
            return (Criteria) this;
        }

        public Criteria andUfpVipcardnumLessThan(String value) {
            addCriterion("UFP_VIPCardNum <", value, "ufpVipcardnum");
            return (Criteria) this;
        }

        public Criteria andUfpVipcardnumLessThanOrEqualTo(String value) {
            addCriterion("UFP_VIPCardNum <=", value, "ufpVipcardnum");
            return (Criteria) this;
        }

        public Criteria andUfpVipcardnumLike(String value) {
            addCriterion("UFP_VIPCardNum like", value, "ufpVipcardnum");
            return (Criteria) this;
        }

        public Criteria andUfpVipcardnumNotLike(String value) {
            addCriterion("UFP_VIPCardNum not like", value, "ufpVipcardnum");
            return (Criteria) this;
        }

        public Criteria andUfpVipcardnumIn(List<String> values) {
            addCriterion("UFP_VIPCardNum in", values, "ufpVipcardnum");
            return (Criteria) this;
        }

        public Criteria andUfpVipcardnumNotIn(List<String> values) {
            addCriterion("UFP_VIPCardNum not in", values, "ufpVipcardnum");
            return (Criteria) this;
        }

        public Criteria andUfpVipcardnumBetween(String value1, String value2) {
            addCriterion("UFP_VIPCardNum between", value1, value2, "ufpVipcardnum");
            return (Criteria) this;
        }

        public Criteria andUfpVipcardnumNotBetween(String value1, String value2) {
            addCriterion("UFP_VIPCardNum not between", value1, value2, "ufpVipcardnum");
            return (Criteria) this;
        }

        public Criteria andUfpViplevelIsNull() {
            addCriterion("UFP_VIPLevel is null");
            return (Criteria) this;
        }

        public Criteria andUfpViplevelIsNotNull() {
            addCriterion("UFP_VIPLevel is not null");
            return (Criteria) this;
        }

        public Criteria andUfpViplevelEqualTo(Integer value) {
            addCriterion("UFP_VIPLevel =", value, "ufpViplevel");
            return (Criteria) this;
        }

        public Criteria andUfpViplevelNotEqualTo(Integer value) {
            addCriterion("UFP_VIPLevel <>", value, "ufpViplevel");
            return (Criteria) this;
        }

        public Criteria andUfpViplevelGreaterThan(Integer value) {
            addCriterion("UFP_VIPLevel >", value, "ufpViplevel");
            return (Criteria) this;
        }

        public Criteria andUfpViplevelGreaterThanOrEqualTo(Integer value) {
            addCriterion("UFP_VIPLevel >=", value, "ufpViplevel");
            return (Criteria) this;
        }

        public Criteria andUfpViplevelLessThan(Integer value) {
            addCriterion("UFP_VIPLevel <", value, "ufpViplevel");
            return (Criteria) this;
        }

        public Criteria andUfpViplevelLessThanOrEqualTo(Integer value) {
            addCriterion("UFP_VIPLevel <=", value, "ufpViplevel");
            return (Criteria) this;
        }

        public Criteria andUfpViplevelIn(List<Integer> values) {
            addCriterion("UFP_VIPLevel in", values, "ufpViplevel");
            return (Criteria) this;
        }

        public Criteria andUfpViplevelNotIn(List<Integer> values) {
            addCriterion("UFP_VIPLevel not in", values, "ufpViplevel");
            return (Criteria) this;
        }

        public Criteria andUfpViplevelBetween(Integer value1, Integer value2) {
            addCriterion("UFP_VIPLevel between", value1, value2, "ufpViplevel");
            return (Criteria) this;
        }

        public Criteria andUfpViplevelNotBetween(Integer value1, Integer value2) {
            addCriterion("UFP_VIPLevel not between", value1, value2, "ufpViplevel");
            return (Criteria) this;
        }

        public Criteria andUfpLevelupdatetimeIsNull() {
            addCriterion("UFP_LevelUpdateTime is null");
            return (Criteria) this;
        }

        public Criteria andUfpLevelupdatetimeIsNotNull() {
            addCriterion("UFP_LevelUpdateTime is not null");
            return (Criteria) this;
        }

        public Criteria andUfpLevelupdatetimeEqualTo(Date value) {
            addCriterion("UFP_LevelUpdateTime =", value, "ufpLevelupdatetime");
            return (Criteria) this;
        }

        public Criteria andUfpLevelupdatetimeNotEqualTo(Date value) {
            addCriterion("UFP_LevelUpdateTime <>", value, "ufpLevelupdatetime");
            return (Criteria) this;
        }

        public Criteria andUfpLevelupdatetimeGreaterThan(Date value) {
            addCriterion("UFP_LevelUpdateTime >", value, "ufpLevelupdatetime");
            return (Criteria) this;
        }

        public Criteria andUfpLevelupdatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("UFP_LevelUpdateTime >=", value, "ufpLevelupdatetime");
            return (Criteria) this;
        }

        public Criteria andUfpLevelupdatetimeLessThan(Date value) {
            addCriterion("UFP_LevelUpdateTime <", value, "ufpLevelupdatetime");
            return (Criteria) this;
        }

        public Criteria andUfpLevelupdatetimeLessThanOrEqualTo(Date value) {
            addCriterion("UFP_LevelUpdateTime <=", value, "ufpLevelupdatetime");
            return (Criteria) this;
        }

        public Criteria andUfpLevelupdatetimeIn(List<Date> values) {
            addCriterion("UFP_LevelUpdateTime in", values, "ufpLevelupdatetime");
            return (Criteria) this;
        }

        public Criteria andUfpLevelupdatetimeNotIn(List<Date> values) {
            addCriterion("UFP_LevelUpdateTime not in", values, "ufpLevelupdatetime");
            return (Criteria) this;
        }

        public Criteria andUfpLevelupdatetimeBetween(Date value1, Date value2) {
            addCriterion("UFP_LevelUpdateTime between", value1, value2, "ufpLevelupdatetime");
            return (Criteria) this;
        }

        public Criteria andUfpLevelupdatetimeNotBetween(Date value1, Date value2) {
            addCriterion("UFP_LevelUpdateTime not between", value1, value2, "ufpLevelupdatetime");
            return (Criteria) this;
        }

        public Criteria andUfpVipstatusIsNull() {
            addCriterion("UFP_VIPStatus is null");
            return (Criteria) this;
        }

        public Criteria andUfpVipstatusIsNotNull() {
            addCriterion("UFP_VIPStatus is not null");
            return (Criteria) this;
        }

        public Criteria andUfpVipstatusEqualTo(Integer value) {
            addCriterion("UFP_VIPStatus =", value, "ufpVipstatus");
            return (Criteria) this;
        }

        public Criteria andUfpVipstatusNotEqualTo(Integer value) {
            addCriterion("UFP_VIPStatus <>", value, "ufpVipstatus");
            return (Criteria) this;
        }

        public Criteria andUfpVipstatusGreaterThan(Integer value) {
            addCriterion("UFP_VIPStatus >", value, "ufpVipstatus");
            return (Criteria) this;
        }

        public Criteria andUfpVipstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("UFP_VIPStatus >=", value, "ufpVipstatus");
            return (Criteria) this;
        }

        public Criteria andUfpVipstatusLessThan(Integer value) {
            addCriterion("UFP_VIPStatus <", value, "ufpVipstatus");
            return (Criteria) this;
        }

        public Criteria andUfpVipstatusLessThanOrEqualTo(Integer value) {
            addCriterion("UFP_VIPStatus <=", value, "ufpVipstatus");
            return (Criteria) this;
        }

        public Criteria andUfpVipstatusIn(List<Integer> values) {
            addCriterion("UFP_VIPStatus in", values, "ufpVipstatus");
            return (Criteria) this;
        }

        public Criteria andUfpVipstatusNotIn(List<Integer> values) {
            addCriterion("UFP_VIPStatus not in", values, "ufpVipstatus");
            return (Criteria) this;
        }

        public Criteria andUfpVipstatusBetween(Integer value1, Integer value2) {
            addCriterion("UFP_VIPStatus between", value1, value2, "ufpVipstatus");
            return (Criteria) this;
        }

        public Criteria andUfpVipstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("UFP_VIPStatus not between", value1, value2, "ufpVipstatus");
            return (Criteria) this;
        }

        public Criteria andUfpMobile2IsNull() {
            addCriterion("UFP_Mobile2 is null");
            return (Criteria) this;
        }

        public Criteria andUfpMobile2IsNotNull() {
            addCriterion("UFP_Mobile2 is not null");
            return (Criteria) this;
        }

        public Criteria andUfpMobile2EqualTo(String value) {
            addCriterion("UFP_Mobile2 =", value, "ufpMobile2");
            return (Criteria) this;
        }

        public Criteria andUfpMobile2NotEqualTo(String value) {
            addCriterion("UFP_Mobile2 <>", value, "ufpMobile2");
            return (Criteria) this;
        }

        public Criteria andUfpMobile2GreaterThan(String value) {
            addCriterion("UFP_Mobile2 >", value, "ufpMobile2");
            return (Criteria) this;
        }

        public Criteria andUfpMobile2GreaterThanOrEqualTo(String value) {
            addCriterion("UFP_Mobile2 >=", value, "ufpMobile2");
            return (Criteria) this;
        }

        public Criteria andUfpMobile2LessThan(String value) {
            addCriterion("UFP_Mobile2 <", value, "ufpMobile2");
            return (Criteria) this;
        }

        public Criteria andUfpMobile2LessThanOrEqualTo(String value) {
            addCriterion("UFP_Mobile2 <=", value, "ufpMobile2");
            return (Criteria) this;
        }

        public Criteria andUfpMobile2Like(String value) {
            addCriterion("UFP_Mobile2 like", value, "ufpMobile2");
            return (Criteria) this;
        }

        public Criteria andUfpMobile2NotLike(String value) {
            addCriterion("UFP_Mobile2 not like", value, "ufpMobile2");
            return (Criteria) this;
        }

        public Criteria andUfpMobile2In(List<String> values) {
            addCriterion("UFP_Mobile2 in", values, "ufpMobile2");
            return (Criteria) this;
        }

        public Criteria andUfpMobile2NotIn(List<String> values) {
            addCriterion("UFP_Mobile2 not in", values, "ufpMobile2");
            return (Criteria) this;
        }

        public Criteria andUfpMobile2Between(String value1, String value2) {
            addCriterion("UFP_Mobile2 between", value1, value2, "ufpMobile2");
            return (Criteria) this;
        }

        public Criteria andUfpMobile2NotBetween(String value1, String value2) {
            addCriterion("UFP_Mobile2 not between", value1, value2, "ufpMobile2");
            return (Criteria) this;
        }

        public Criteria andUfpMobile3IsNull() {
            addCriterion("UFP_Mobile3 is null");
            return (Criteria) this;
        }

        public Criteria andUfpMobile3IsNotNull() {
            addCriterion("UFP_Mobile3 is not null");
            return (Criteria) this;
        }

        public Criteria andUfpMobile3EqualTo(String value) {
            addCriterion("UFP_Mobile3 =", value, "ufpMobile3");
            return (Criteria) this;
        }

        public Criteria andUfpMobile3NotEqualTo(String value) {
            addCriterion("UFP_Mobile3 <>", value, "ufpMobile3");
            return (Criteria) this;
        }

        public Criteria andUfpMobile3GreaterThan(String value) {
            addCriterion("UFP_Mobile3 >", value, "ufpMobile3");
            return (Criteria) this;
        }

        public Criteria andUfpMobile3GreaterThanOrEqualTo(String value) {
            addCriterion("UFP_Mobile3 >=", value, "ufpMobile3");
            return (Criteria) this;
        }

        public Criteria andUfpMobile3LessThan(String value) {
            addCriterion("UFP_Mobile3 <", value, "ufpMobile3");
            return (Criteria) this;
        }

        public Criteria andUfpMobile3LessThanOrEqualTo(String value) {
            addCriterion("UFP_Mobile3 <=", value, "ufpMobile3");
            return (Criteria) this;
        }

        public Criteria andUfpMobile3Like(String value) {
            addCriterion("UFP_Mobile3 like", value, "ufpMobile3");
            return (Criteria) this;
        }

        public Criteria andUfpMobile3NotLike(String value) {
            addCriterion("UFP_Mobile3 not like", value, "ufpMobile3");
            return (Criteria) this;
        }

        public Criteria andUfpMobile3In(List<String> values) {
            addCriterion("UFP_Mobile3 in", values, "ufpMobile3");
            return (Criteria) this;
        }

        public Criteria andUfpMobile3NotIn(List<String> values) {
            addCriterion("UFP_Mobile3 not in", values, "ufpMobile3");
            return (Criteria) this;
        }

        public Criteria andUfpMobile3Between(String value1, String value2) {
            addCriterion("UFP_Mobile3 between", value1, value2, "ufpMobile3");
            return (Criteria) this;
        }

        public Criteria andUfpMobile3NotBetween(String value1, String value2) {
            addCriterion("UFP_Mobile3 not between", value1, value2, "ufpMobile3");
            return (Criteria) this;
        }

        public Criteria andUfpReferrerIsNull() {
            addCriterion("UFP_Referrer is null");
            return (Criteria) this;
        }

        public Criteria andUfpReferrerIsNotNull() {
            addCriterion("UFP_Referrer is not null");
            return (Criteria) this;
        }

        public Criteria andUfpReferrerEqualTo(String value) {
            addCriterion("UFP_Referrer =", value, "ufpReferrer");
            return (Criteria) this;
        }

        public Criteria andUfpReferrerNotEqualTo(String value) {
            addCriterion("UFP_Referrer <>", value, "ufpReferrer");
            return (Criteria) this;
        }

        public Criteria andUfpReferrerGreaterThan(String value) {
            addCriterion("UFP_Referrer >", value, "ufpReferrer");
            return (Criteria) this;
        }

        public Criteria andUfpReferrerGreaterThanOrEqualTo(String value) {
            addCriterion("UFP_Referrer >=", value, "ufpReferrer");
            return (Criteria) this;
        }

        public Criteria andUfpReferrerLessThan(String value) {
            addCriterion("UFP_Referrer <", value, "ufpReferrer");
            return (Criteria) this;
        }

        public Criteria andUfpReferrerLessThanOrEqualTo(String value) {
            addCriterion("UFP_Referrer <=", value, "ufpReferrer");
            return (Criteria) this;
        }

        public Criteria andUfpReferrerLike(String value) {
            addCriterion("UFP_Referrer like", value, "ufpReferrer");
            return (Criteria) this;
        }

        public Criteria andUfpReferrerNotLike(String value) {
            addCriterion("UFP_Referrer not like", value, "ufpReferrer");
            return (Criteria) this;
        }

        public Criteria andUfpReferrerIn(List<String> values) {
            addCriterion("UFP_Referrer in", values, "ufpReferrer");
            return (Criteria) this;
        }

        public Criteria andUfpReferrerNotIn(List<String> values) {
            addCriterion("UFP_Referrer not in", values, "ufpReferrer");
            return (Criteria) this;
        }

        public Criteria andUfpReferrerBetween(String value1, String value2) {
            addCriterion("UFP_Referrer between", value1, value2, "ufpReferrer");
            return (Criteria) this;
        }

        public Criteria andUfpReferrerNotBetween(String value1, String value2) {
            addCriterion("UFP_Referrer not between", value1, value2, "ufpReferrer");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}