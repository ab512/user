package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Contactsinfo extends BaseEntity {


    private Long ciLausertid;

    private String ciName;

    private String ciMobile;

    private String ciEmail;



    public Long getCiLausertid() {
        return ciLausertid;
    }

    public void setCiLausertid(Long ciLausertid) {
        this.ciLausertid = ciLausertid;
    }

    public String getCiName() {
        return ciName;
    }

    public void setCiName(String ciName) {
        this.ciName = ciName == null ? null : ciName.trim();
    }

    public String getCiMobile() {
        return ciMobile;
    }

    public void setCiMobile(String ciMobile) {
        this.ciMobile = ciMobile == null ? null : ciMobile.trim();
    }

    public String getCiEmail() {
        return ciEmail;
    }

    public void setCiEmail(String ciEmail) {
        this.ciEmail = ciEmail == null ? null : ciEmail.trim();
    }
}