package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Useragent extends BaseEntity {


    private Long uaLausertid;

    private String uaDefaultszm;

    private String uaDeparturecity;

    private String uaDepartureairport;

    

    public Long getUaLausertid() {
        return uaLausertid;
    }

    public void setUaLausertid(Long uaLausertid) {
        this.uaLausertid = uaLausertid;
    }

    public String getUaDefaultszm() {
        return uaDefaultszm;
    }

    public void setUaDefaultszm(String uaDefaultszm) {
        this.uaDefaultszm = uaDefaultszm == null ? null : uaDefaultszm.trim();
    }

    public String getUaDeparturecity() {
        return uaDeparturecity;
    }

    public void setUaDeparturecity(String uaDeparturecity) {
        this.uaDeparturecity = uaDeparturecity == null ? null : uaDeparturecity.trim();
    }

    public String getUaDepartureairport() {
        return uaDepartureairport;
    }

    public void setUaDepartureairport(String uaDepartureairport) {
        this.uaDepartureairport = uaDepartureairport == null ? null : uaDepartureairport.trim();
    }
}