package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Agentaudit extends BaseEntity {


    private Long agaAgentorgtid;

    private Integer agaIndex;

    private Long agaAuditortid;

    private String agaNote;

    private String agaAuditor;

    private Integer agaAuditret;


    public Long getAgaAgentorgtid() {
        return agaAgentorgtid;
    }

    public void setAgaAgentorgtid(Long agaAgentorgtid) {
        this.agaAgentorgtid = agaAgentorgtid;
    }

    public Integer getAgaIndex() {
        return agaIndex;
    }

    public void setAgaIndex(Integer agaIndex) {
        this.agaIndex = agaIndex;
    }

    public Long getAgaAuditortid() {
        return agaAuditortid;
    }

    public void setAgaAuditortid(Long agaAuditortid) {
        this.agaAuditortid = agaAuditortid;
    }

    public String getAgaNote() {
        return agaNote;
    }

    public void setAgaNote(String agaNote) {
        this.agaNote = agaNote == null ? null : agaNote.trim();
    }

    public String getAgaAuditor() {
        return agaAuditor;
    }

    public void setAgaAuditor(String agaAuditor) {
        this.agaAuditor = agaAuditor == null ? null : agaAuditor.trim();
    }

    public Integer getAgaAuditret() {
        return agaAuditret;
    }

    public void setAgaAuditret(Integer agaAuditret) {
        this.agaAuditret = agaAuditret;
    }
}