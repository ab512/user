package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Frequentpassenger extends BaseEntity {


    private Long fpLausertid;

    private String fpName;

    private Date fpBirthday;

    private Integer fpFoidtype;

    private Long fpCustomeraccountid;

    private String fpFoid;

    private String fpMobile;

    private String fpEmail;

    private Integer fpTravellertype;



    public Long getFpLausertid() {
        return fpLausertid;
    }

    public void setFpLausertid(Long fpLausertid) {
        this.fpLausertid = fpLausertid;
    }

    public String getFpName() {
        return fpName;
    }

    public void setFpName(String fpName) {
        this.fpName = fpName == null ? null : fpName.trim();
    }

    public Date getFpBirthday() {
        return fpBirthday;
    }

    public void setFpBirthday(Date fpBirthday) {
        this.fpBirthday = fpBirthday;
    }

    public Integer getFpFoidtype() {
        return fpFoidtype;
    }

    public void setFpFoidtype(Integer fpFoidtype) {
        this.fpFoidtype = fpFoidtype;
    }

    public Long getFpCustomeraccountid() {
        return fpCustomeraccountid;
    }

    public void setFpCustomeraccountid(Long fpCustomeraccountid) {
        this.fpCustomeraccountid = fpCustomeraccountid;
    }

    public String getFpFoid() {
        return fpFoid;
    }

    public void setFpFoid(String fpFoid) {
        this.fpFoid = fpFoid == null ? null : fpFoid.trim();
    }

    public String getFpMobile() {
        return fpMobile;
    }

    public void setFpMobile(String fpMobile) {
        this.fpMobile = fpMobile == null ? null : fpMobile.trim();
    }

    public String getFpEmail() {
        return fpEmail;
    }

    public void setFpEmail(String fpEmail) {
        this.fpEmail = fpEmail == null ? null : fpEmail.trim();
    }

    public Integer getFpTravellertype() {
        return fpTravellertype;
    }

    public void setFpTravellertype(Integer fpTravellertype) {
        this.fpTravellertype = fpTravellertype;
    }
}