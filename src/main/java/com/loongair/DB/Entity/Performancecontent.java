package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Performancecontent extends BaseEntity {


    private Long pcPerformancetid;

    private String pcEffectivecabins;

    private Float pcTicketrate;

    private Date pcSplitdate;

    private Float pcTicketrateafter;



    public Long getPcPerformancetid() {
        return pcPerformancetid;
    }

    public void setPcPerformancetid(Long pcPerformancetid) {
        this.pcPerformancetid = pcPerformancetid;
    }

    public String getPcEffectivecabins() {
        return pcEffectivecabins;
    }

    public void setPcEffectivecabins(String pcEffectivecabins) {
        this.pcEffectivecabins = pcEffectivecabins == null ? null : pcEffectivecabins.trim();
    }

    public Float getPcTicketrate() {
        return pcTicketrate;
    }

    public void setPcTicketrate(Float pcTicketrate) {
        this.pcTicketrate = pcTicketrate;
    }

    public Date getPcSplitdate() {
        return pcSplitdate;
    }

    public void setPcSplitdate(Date pcSplitdate) {
        this.pcSplitdate = pcSplitdate;
    }

    public Float getPcTicketrateafter() {
        return pcTicketrateafter;
    }

    public void setPcTicketrateafter(Float pcTicketrateafter) {
        this.pcTicketrateafter = pcTicketrateafter;
    }
}