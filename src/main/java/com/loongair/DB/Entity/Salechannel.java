package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Salechannel extends BaseEntity {


    private String scChannelname;

    private String scChannelcode;

    private String scChannelkey;

    private String scLimitip;

    private Integer scChanneltype;

    private String scLoginname;

    private Long scOrggroupid;



    public String getScChannelname() {
        return scChannelname;
    }

    public void setScChannelname(String scChannelname) {
        this.scChannelname = scChannelname == null ? null : scChannelname.trim();
    }

    public String getScChannelcode() {
        return scChannelcode;
    }

    public void setScChannelcode(String scChannelcode) {
        this.scChannelcode = scChannelcode == null ? null : scChannelcode.trim();
    }

    public String getScChannelkey() {
        return scChannelkey;
    }

    public void setScChannelkey(String scChannelkey) {
        this.scChannelkey = scChannelkey == null ? null : scChannelkey.trim();
    }

    public String getScLimitip() {
        return scLimitip;
    }

    public void setScLimitip(String scLimitip) {
        this.scLimitip = scLimitip == null ? null : scLimitip.trim();
    }

    public Integer getScChanneltype() {
        return scChanneltype;
    }

    public void setScChanneltype(Integer scChanneltype) {
        this.scChanneltype = scChanneltype;
    }

    public String getScLoginname() {
        return scLoginname;
    }

    public void setScLoginname(String scLoginname) {
        this.scLoginname = scLoginname == null ? null : scLoginname.trim();
    }

    public Long getScOrggroupid() {
        return scOrggroupid;
    }

    public void setScOrggroupid(Long scOrggroupid) {
        this.scOrggroupid = scOrggroupid;
    }
}