package com.loongair.DB.Entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrgbigcustomerExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public OrgbigcustomerExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTidIsNull() {
            addCriterion("Tid is null");
            return (Criteria) this;
        }

        public Criteria andTidIsNotNull() {
            addCriterion("Tid is not null");
            return (Criteria) this;
        }

        public Criteria andTidEqualTo(Long value) {
            addCriterion("Tid =", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotEqualTo(Long value) {
            addCriterion("Tid <>", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThan(Long value) {
            addCriterion("Tid >", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThanOrEqualTo(Long value) {
            addCriterion("Tid >=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThan(Long value) {
            addCriterion("Tid <", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThanOrEqualTo(Long value) {
            addCriterion("Tid <=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidIn(List<Long> values) {
            addCriterion("Tid in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotIn(List<Long> values) {
            addCriterion("Tid not in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidBetween(Long value1, Long value2) {
            addCriterion("Tid between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotBetween(Long value1, Long value2) {
            addCriterion("Tid not between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNull() {
            addCriterion("CreateId is null");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNotNull() {
            addCriterion("CreateId is not null");
            return (Criteria) this;
        }

        public Criteria andCreateidEqualTo(Long value) {
            addCriterion("CreateId =", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotEqualTo(Long value) {
            addCriterion("CreateId <>", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThan(Long value) {
            addCriterion("CreateId >", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThanOrEqualTo(Long value) {
            addCriterion("CreateId >=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThan(Long value) {
            addCriterion("CreateId <", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThanOrEqualTo(Long value) {
            addCriterion("CreateId <=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidIn(List<Long> values) {
            addCriterion("CreateId in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotIn(List<Long> values) {
            addCriterion("CreateId not in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidBetween(Long value1, Long value2) {
            addCriterion("CreateId between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotBetween(Long value1, Long value2) {
            addCriterion("CreateId not between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNull() {
            addCriterion("ProxyUserId is null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNotNull() {
            addCriterion("ProxyUserId is not null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridEqualTo(Long value) {
            addCriterion("ProxyUserId =", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotEqualTo(Long value) {
            addCriterion("ProxyUserId <>", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThan(Long value) {
            addCriterion("ProxyUserId >", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId >=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThan(Long value) {
            addCriterion("ProxyUserId <", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId <=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIn(List<Long> values) {
            addCriterion("ProxyUserId in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotIn(List<Long> values) {
            addCriterion("ProxyUserId not in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId not between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNull() {
            addCriterion("LastGuid is null");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNotNull() {
            addCriterion("LastGuid is not null");
            return (Criteria) this;
        }

        public Criteria andLastguidEqualTo(Long value) {
            addCriterion("LastGuid =", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotEqualTo(Long value) {
            addCriterion("LastGuid <>", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThan(Long value) {
            addCriterion("LastGuid >", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThanOrEqualTo(Long value) {
            addCriterion("LastGuid >=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThan(Long value) {
            addCriterion("LastGuid <", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThanOrEqualTo(Long value) {
            addCriterion("LastGuid <=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidIn(List<Long> values) {
            addCriterion("LastGuid in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotIn(List<Long> values) {
            addCriterion("LastGuid not in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidBetween(Long value1, Long value2) {
            addCriterion("LastGuid between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotBetween(Long value1, Long value2) {
            addCriterion("LastGuid not between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andCloneIsNull() {
            addCriterion("Clone is null");
            return (Criteria) this;
        }

        public Criteria andCloneIsNotNull() {
            addCriterion("Clone is not null");
            return (Criteria) this;
        }

        public Criteria andCloneEqualTo(Integer value) {
            addCriterion("Clone =", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotEqualTo(Integer value) {
            addCriterion("Clone <>", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThan(Integer value) {
            addCriterion("Clone >", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThanOrEqualTo(Integer value) {
            addCriterion("Clone >=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThan(Integer value) {
            addCriterion("Clone <", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThanOrEqualTo(Integer value) {
            addCriterion("Clone <=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneIn(List<Integer> values) {
            addCriterion("Clone in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotIn(List<Integer> values) {
            addCriterion("Clone not in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneBetween(Integer value1, Integer value2) {
            addCriterion("Clone between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotBetween(Integer value1, Integer value2) {
            addCriterion("Clone not between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNull() {
            addCriterion("UpdateId is null");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNotNull() {
            addCriterion("UpdateId is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateidEqualTo(Long value) {
            addCriterion("UpdateId =", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotEqualTo(Long value) {
            addCriterion("UpdateId <>", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThan(Long value) {
            addCriterion("UpdateId >", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThanOrEqualTo(Long value) {
            addCriterion("UpdateId >=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThan(Long value) {
            addCriterion("UpdateId <", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThanOrEqualTo(Long value) {
            addCriterion("UpdateId <=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidIn(List<Long> values) {
            addCriterion("UpdateId in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotIn(List<Long> values) {
            addCriterion("UpdateId not in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidBetween(Long value1, Long value2) {
            addCriterion("UpdateId between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotBetween(Long value1, Long value2) {
            addCriterion("UpdateId not between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("UpdateDate is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("UpdateDate is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("UpdateDate =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("UpdateDate <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("UpdateDate >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("UpdateDate >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("UpdateDate <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("UpdateDate <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("UpdateDate in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("UpdateDate not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("UpdateDate between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("UpdateDate not between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("Status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("Status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("Status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("Status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("Status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("Status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("Status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("Status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("Status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("Status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("Status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("Status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andObcOrgtidIsNull() {
            addCriterion("OBC_OrgTid is null");
            return (Criteria) this;
        }

        public Criteria andObcOrgtidIsNotNull() {
            addCriterion("OBC_OrgTid is not null");
            return (Criteria) this;
        }

        public Criteria andObcOrgtidEqualTo(Long value) {
            addCriterion("OBC_OrgTid =", value, "obcOrgtid");
            return (Criteria) this;
        }

        public Criteria andObcOrgtidNotEqualTo(Long value) {
            addCriterion("OBC_OrgTid <>", value, "obcOrgtid");
            return (Criteria) this;
        }

        public Criteria andObcOrgtidGreaterThan(Long value) {
            addCriterion("OBC_OrgTid >", value, "obcOrgtid");
            return (Criteria) this;
        }

        public Criteria andObcOrgtidGreaterThanOrEqualTo(Long value) {
            addCriterion("OBC_OrgTid >=", value, "obcOrgtid");
            return (Criteria) this;
        }

        public Criteria andObcOrgtidLessThan(Long value) {
            addCriterion("OBC_OrgTid <", value, "obcOrgtid");
            return (Criteria) this;
        }

        public Criteria andObcOrgtidLessThanOrEqualTo(Long value) {
            addCriterion("OBC_OrgTid <=", value, "obcOrgtid");
            return (Criteria) this;
        }

        public Criteria andObcOrgtidIn(List<Long> values) {
            addCriterion("OBC_OrgTid in", values, "obcOrgtid");
            return (Criteria) this;
        }

        public Criteria andObcOrgtidNotIn(List<Long> values) {
            addCriterion("OBC_OrgTid not in", values, "obcOrgtid");
            return (Criteria) this;
        }

        public Criteria andObcOrgtidBetween(Long value1, Long value2) {
            addCriterion("OBC_OrgTid between", value1, value2, "obcOrgtid");
            return (Criteria) this;
        }

        public Criteria andObcOrgtidNotBetween(Long value1, Long value2) {
            addCriterion("OBC_OrgTid not between", value1, value2, "obcOrgtid");
            return (Criteria) this;
        }

        public Criteria andObcAuditstatusIsNull() {
            addCriterion("OBC_AuditStatus is null");
            return (Criteria) this;
        }

        public Criteria andObcAuditstatusIsNotNull() {
            addCriterion("OBC_AuditStatus is not null");
            return (Criteria) this;
        }

        public Criteria andObcAuditstatusEqualTo(Integer value) {
            addCriterion("OBC_AuditStatus =", value, "obcAuditstatus");
            return (Criteria) this;
        }

        public Criteria andObcAuditstatusNotEqualTo(Integer value) {
            addCriterion("OBC_AuditStatus <>", value, "obcAuditstatus");
            return (Criteria) this;
        }

        public Criteria andObcAuditstatusGreaterThan(Integer value) {
            addCriterion("OBC_AuditStatus >", value, "obcAuditstatus");
            return (Criteria) this;
        }

        public Criteria andObcAuditstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("OBC_AuditStatus >=", value, "obcAuditstatus");
            return (Criteria) this;
        }

        public Criteria andObcAuditstatusLessThan(Integer value) {
            addCriterion("OBC_AuditStatus <", value, "obcAuditstatus");
            return (Criteria) this;
        }

        public Criteria andObcAuditstatusLessThanOrEqualTo(Integer value) {
            addCriterion("OBC_AuditStatus <=", value, "obcAuditstatus");
            return (Criteria) this;
        }

        public Criteria andObcAuditstatusIn(List<Integer> values) {
            addCriterion("OBC_AuditStatus in", values, "obcAuditstatus");
            return (Criteria) this;
        }

        public Criteria andObcAuditstatusNotIn(List<Integer> values) {
            addCriterion("OBC_AuditStatus not in", values, "obcAuditstatus");
            return (Criteria) this;
        }

        public Criteria andObcAuditstatusBetween(Integer value1, Integer value2) {
            addCriterion("OBC_AuditStatus between", value1, value2, "obcAuditstatus");
            return (Criteria) this;
        }

        public Criteria andObcAuditstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("OBC_AuditStatus not between", value1, value2, "obcAuditstatus");
            return (Criteria) this;
        }

        public Criteria andObcDkhcodeIsNull() {
            addCriterion("OBC_DKHCode is null");
            return (Criteria) this;
        }

        public Criteria andObcDkhcodeIsNotNull() {
            addCriterion("OBC_DKHCode is not null");
            return (Criteria) this;
        }

        public Criteria andObcDkhcodeEqualTo(String value) {
            addCriterion("OBC_DKHCode =", value, "obcDkhcode");
            return (Criteria) this;
        }

        public Criteria andObcDkhcodeNotEqualTo(String value) {
            addCriterion("OBC_DKHCode <>", value, "obcDkhcode");
            return (Criteria) this;
        }

        public Criteria andObcDkhcodeGreaterThan(String value) {
            addCriterion("OBC_DKHCode >", value, "obcDkhcode");
            return (Criteria) this;
        }

        public Criteria andObcDkhcodeGreaterThanOrEqualTo(String value) {
            addCriterion("OBC_DKHCode >=", value, "obcDkhcode");
            return (Criteria) this;
        }

        public Criteria andObcDkhcodeLessThan(String value) {
            addCriterion("OBC_DKHCode <", value, "obcDkhcode");
            return (Criteria) this;
        }

        public Criteria andObcDkhcodeLessThanOrEqualTo(String value) {
            addCriterion("OBC_DKHCode <=", value, "obcDkhcode");
            return (Criteria) this;
        }

        public Criteria andObcDkhcodeLike(String value) {
            addCriterion("OBC_DKHCode like", value, "obcDkhcode");
            return (Criteria) this;
        }

        public Criteria andObcDkhcodeNotLike(String value) {
            addCriterion("OBC_DKHCode not like", value, "obcDkhcode");
            return (Criteria) this;
        }

        public Criteria andObcDkhcodeIn(List<String> values) {
            addCriterion("OBC_DKHCode in", values, "obcDkhcode");
            return (Criteria) this;
        }

        public Criteria andObcDkhcodeNotIn(List<String> values) {
            addCriterion("OBC_DKHCode not in", values, "obcDkhcode");
            return (Criteria) this;
        }

        public Criteria andObcDkhcodeBetween(String value1, String value2) {
            addCriterion("OBC_DKHCode between", value1, value2, "obcDkhcode");
            return (Criteria) this;
        }

        public Criteria andObcDkhcodeNotBetween(String value1, String value2) {
            addCriterion("OBC_DKHCode not between", value1, value2, "obcDkhcode");
            return (Criteria) this;
        }

        public Criteria andObcDateofagreementIsNull() {
            addCriterion("OBC_DateOfAgreement is null");
            return (Criteria) this;
        }

        public Criteria andObcDateofagreementIsNotNull() {
            addCriterion("OBC_DateOfAgreement is not null");
            return (Criteria) this;
        }

        public Criteria andObcDateofagreementEqualTo(Date value) {
            addCriterion("OBC_DateOfAgreement =", value, "obcDateofagreement");
            return (Criteria) this;
        }

        public Criteria andObcDateofagreementNotEqualTo(Date value) {
            addCriterion("OBC_DateOfAgreement <>", value, "obcDateofagreement");
            return (Criteria) this;
        }

        public Criteria andObcDateofagreementGreaterThan(Date value) {
            addCriterion("OBC_DateOfAgreement >", value, "obcDateofagreement");
            return (Criteria) this;
        }

        public Criteria andObcDateofagreementGreaterThanOrEqualTo(Date value) {
            addCriterion("OBC_DateOfAgreement >=", value, "obcDateofagreement");
            return (Criteria) this;
        }

        public Criteria andObcDateofagreementLessThan(Date value) {
            addCriterion("OBC_DateOfAgreement <", value, "obcDateofagreement");
            return (Criteria) this;
        }

        public Criteria andObcDateofagreementLessThanOrEqualTo(Date value) {
            addCriterion("OBC_DateOfAgreement <=", value, "obcDateofagreement");
            return (Criteria) this;
        }

        public Criteria andObcDateofagreementIn(List<Date> values) {
            addCriterion("OBC_DateOfAgreement in", values, "obcDateofagreement");
            return (Criteria) this;
        }

        public Criteria andObcDateofagreementNotIn(List<Date> values) {
            addCriterion("OBC_DateOfAgreement not in", values, "obcDateofagreement");
            return (Criteria) this;
        }

        public Criteria andObcDateofagreementBetween(Date value1, Date value2) {
            addCriterion("OBC_DateOfAgreement between", value1, value2, "obcDateofagreement");
            return (Criteria) this;
        }

        public Criteria andObcDateofagreementNotBetween(Date value1, Date value2) {
            addCriterion("OBC_DateOfAgreement not between", value1, value2, "obcDateofagreement");
            return (Criteria) this;
        }

        public Criteria andObcValidperiodendIsNull() {
            addCriterion("OBC_ValidPeriodEnd is null");
            return (Criteria) this;
        }

        public Criteria andObcValidperiodendIsNotNull() {
            addCriterion("OBC_ValidPeriodEnd is not null");
            return (Criteria) this;
        }

        public Criteria andObcValidperiodendEqualTo(Date value) {
            addCriterion("OBC_ValidPeriodEnd =", value, "obcValidperiodend");
            return (Criteria) this;
        }

        public Criteria andObcValidperiodendNotEqualTo(Date value) {
            addCriterion("OBC_ValidPeriodEnd <>", value, "obcValidperiodend");
            return (Criteria) this;
        }

        public Criteria andObcValidperiodendGreaterThan(Date value) {
            addCriterion("OBC_ValidPeriodEnd >", value, "obcValidperiodend");
            return (Criteria) this;
        }

        public Criteria andObcValidperiodendGreaterThanOrEqualTo(Date value) {
            addCriterion("OBC_ValidPeriodEnd >=", value, "obcValidperiodend");
            return (Criteria) this;
        }

        public Criteria andObcValidperiodendLessThan(Date value) {
            addCriterion("OBC_ValidPeriodEnd <", value, "obcValidperiodend");
            return (Criteria) this;
        }

        public Criteria andObcValidperiodendLessThanOrEqualTo(Date value) {
            addCriterion("OBC_ValidPeriodEnd <=", value, "obcValidperiodend");
            return (Criteria) this;
        }

        public Criteria andObcValidperiodendIn(List<Date> values) {
            addCriterion("OBC_ValidPeriodEnd in", values, "obcValidperiodend");
            return (Criteria) this;
        }

        public Criteria andObcValidperiodendNotIn(List<Date> values) {
            addCriterion("OBC_ValidPeriodEnd not in", values, "obcValidperiodend");
            return (Criteria) this;
        }

        public Criteria andObcValidperiodendBetween(Date value1, Date value2) {
            addCriterion("OBC_ValidPeriodEnd between", value1, value2, "obcValidperiodend");
            return (Criteria) this;
        }

        public Criteria andObcValidperiodendNotBetween(Date value1, Date value2) {
            addCriterion("OBC_ValidPeriodEnd not between", value1, value2, "obcValidperiodend");
            return (Criteria) this;
        }

        public Criteria andObcAdmintidIsNull() {
            addCriterion("OBC_AdminTid is null");
            return (Criteria) this;
        }

        public Criteria andObcAdmintidIsNotNull() {
            addCriterion("OBC_AdminTid is not null");
            return (Criteria) this;
        }

        public Criteria andObcAdmintidEqualTo(Long value) {
            addCriterion("OBC_AdminTid =", value, "obcAdmintid");
            return (Criteria) this;
        }

        public Criteria andObcAdmintidNotEqualTo(Long value) {
            addCriterion("OBC_AdminTid <>", value, "obcAdmintid");
            return (Criteria) this;
        }

        public Criteria andObcAdmintidGreaterThan(Long value) {
            addCriterion("OBC_AdminTid >", value, "obcAdmintid");
            return (Criteria) this;
        }

        public Criteria andObcAdmintidGreaterThanOrEqualTo(Long value) {
            addCriterion("OBC_AdminTid >=", value, "obcAdmintid");
            return (Criteria) this;
        }

        public Criteria andObcAdmintidLessThan(Long value) {
            addCriterion("OBC_AdminTid <", value, "obcAdmintid");
            return (Criteria) this;
        }

        public Criteria andObcAdmintidLessThanOrEqualTo(Long value) {
            addCriterion("OBC_AdminTid <=", value, "obcAdmintid");
            return (Criteria) this;
        }

        public Criteria andObcAdmintidIn(List<Long> values) {
            addCriterion("OBC_AdminTid in", values, "obcAdmintid");
            return (Criteria) this;
        }

        public Criteria andObcAdmintidNotIn(List<Long> values) {
            addCriterion("OBC_AdminTid not in", values, "obcAdmintid");
            return (Criteria) this;
        }

        public Criteria andObcAdmintidBetween(Long value1, Long value2) {
            addCriterion("OBC_AdminTid between", value1, value2, "obcAdmintid");
            return (Criteria) this;
        }

        public Criteria andObcAdmintidNotBetween(Long value1, Long value2) {
            addCriterion("OBC_AdminTid not between", value1, value2, "obcAdmintid");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeIsNull() {
            addCriterion("OBC_CustomerType is null");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeIsNotNull() {
            addCriterion("OBC_CustomerType is not null");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeEqualTo(Integer value) {
            addCriterion("OBC_CustomerType =", value, "obcCustomertype");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeNotEqualTo(Integer value) {
            addCriterion("OBC_CustomerType <>", value, "obcCustomertype");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeGreaterThan(Integer value) {
            addCriterion("OBC_CustomerType >", value, "obcCustomertype");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("OBC_CustomerType >=", value, "obcCustomertype");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeLessThan(Integer value) {
            addCriterion("OBC_CustomerType <", value, "obcCustomertype");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeLessThanOrEqualTo(Integer value) {
            addCriterion("OBC_CustomerType <=", value, "obcCustomertype");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeIn(List<Integer> values) {
            addCriterion("OBC_CustomerType in", values, "obcCustomertype");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeNotIn(List<Integer> values) {
            addCriterion("OBC_CustomerType not in", values, "obcCustomertype");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeBetween(Integer value1, Integer value2) {
            addCriterion("OBC_CustomerType between", value1, value2, "obcCustomertype");
            return (Criteria) this;
        }

        public Criteria andObcCustomertypeNotBetween(Integer value1, Integer value2) {
            addCriterion("OBC_CustomerType not between", value1, value2, "obcCustomertype");
            return (Criteria) this;
        }

        public Criteria andObcAddressIsNull() {
            addCriterion("OBC_Address is null");
            return (Criteria) this;
        }

        public Criteria andObcAddressIsNotNull() {
            addCriterion("OBC_Address is not null");
            return (Criteria) this;
        }

        public Criteria andObcAddressEqualTo(String value) {
            addCriterion("OBC_Address =", value, "obcAddress");
            return (Criteria) this;
        }

        public Criteria andObcAddressNotEqualTo(String value) {
            addCriterion("OBC_Address <>", value, "obcAddress");
            return (Criteria) this;
        }

        public Criteria andObcAddressGreaterThan(String value) {
            addCriterion("OBC_Address >", value, "obcAddress");
            return (Criteria) this;
        }

        public Criteria andObcAddressGreaterThanOrEqualTo(String value) {
            addCriterion("OBC_Address >=", value, "obcAddress");
            return (Criteria) this;
        }

        public Criteria andObcAddressLessThan(String value) {
            addCriterion("OBC_Address <", value, "obcAddress");
            return (Criteria) this;
        }

        public Criteria andObcAddressLessThanOrEqualTo(String value) {
            addCriterion("OBC_Address <=", value, "obcAddress");
            return (Criteria) this;
        }

        public Criteria andObcAddressLike(String value) {
            addCriterion("OBC_Address like", value, "obcAddress");
            return (Criteria) this;
        }

        public Criteria andObcAddressNotLike(String value) {
            addCriterion("OBC_Address not like", value, "obcAddress");
            return (Criteria) this;
        }

        public Criteria andObcAddressIn(List<String> values) {
            addCriterion("OBC_Address in", values, "obcAddress");
            return (Criteria) this;
        }

        public Criteria andObcAddressNotIn(List<String> values) {
            addCriterion("OBC_Address not in", values, "obcAddress");
            return (Criteria) this;
        }

        public Criteria andObcAddressBetween(String value1, String value2) {
            addCriterion("OBC_Address between", value1, value2, "obcAddress");
            return (Criteria) this;
        }

        public Criteria andObcAddressNotBetween(String value1, String value2) {
            addCriterion("OBC_Address not between", value1, value2, "obcAddress");
            return (Criteria) this;
        }

        public Criteria andObcEmailIsNull() {
            addCriterion("OBC_Email is null");
            return (Criteria) this;
        }

        public Criteria andObcEmailIsNotNull() {
            addCriterion("OBC_Email is not null");
            return (Criteria) this;
        }

        public Criteria andObcEmailEqualTo(String value) {
            addCriterion("OBC_Email =", value, "obcEmail");
            return (Criteria) this;
        }

        public Criteria andObcEmailNotEqualTo(String value) {
            addCriterion("OBC_Email <>", value, "obcEmail");
            return (Criteria) this;
        }

        public Criteria andObcEmailGreaterThan(String value) {
            addCriterion("OBC_Email >", value, "obcEmail");
            return (Criteria) this;
        }

        public Criteria andObcEmailGreaterThanOrEqualTo(String value) {
            addCriterion("OBC_Email >=", value, "obcEmail");
            return (Criteria) this;
        }

        public Criteria andObcEmailLessThan(String value) {
            addCriterion("OBC_Email <", value, "obcEmail");
            return (Criteria) this;
        }

        public Criteria andObcEmailLessThanOrEqualTo(String value) {
            addCriterion("OBC_Email <=", value, "obcEmail");
            return (Criteria) this;
        }

        public Criteria andObcEmailLike(String value) {
            addCriterion("OBC_Email like", value, "obcEmail");
            return (Criteria) this;
        }

        public Criteria andObcEmailNotLike(String value) {
            addCriterion("OBC_Email not like", value, "obcEmail");
            return (Criteria) this;
        }

        public Criteria andObcEmailIn(List<String> values) {
            addCriterion("OBC_Email in", values, "obcEmail");
            return (Criteria) this;
        }

        public Criteria andObcEmailNotIn(List<String> values) {
            addCriterion("OBC_Email not in", values, "obcEmail");
            return (Criteria) this;
        }

        public Criteria andObcEmailBetween(String value1, String value2) {
            addCriterion("OBC_Email between", value1, value2, "obcEmail");
            return (Criteria) this;
        }

        public Criteria andObcEmailNotBetween(String value1, String value2) {
            addCriterion("OBC_Email not between", value1, value2, "obcEmail");
            return (Criteria) this;
        }

        public Criteria andObcFaxIsNull() {
            addCriterion("OBC_Fax is null");
            return (Criteria) this;
        }

        public Criteria andObcFaxIsNotNull() {
            addCriterion("OBC_Fax is not null");
            return (Criteria) this;
        }

        public Criteria andObcFaxEqualTo(String value) {
            addCriterion("OBC_Fax =", value, "obcFax");
            return (Criteria) this;
        }

        public Criteria andObcFaxNotEqualTo(String value) {
            addCriterion("OBC_Fax <>", value, "obcFax");
            return (Criteria) this;
        }

        public Criteria andObcFaxGreaterThan(String value) {
            addCriterion("OBC_Fax >", value, "obcFax");
            return (Criteria) this;
        }

        public Criteria andObcFaxGreaterThanOrEqualTo(String value) {
            addCriterion("OBC_Fax >=", value, "obcFax");
            return (Criteria) this;
        }

        public Criteria andObcFaxLessThan(String value) {
            addCriterion("OBC_Fax <", value, "obcFax");
            return (Criteria) this;
        }

        public Criteria andObcFaxLessThanOrEqualTo(String value) {
            addCriterion("OBC_Fax <=", value, "obcFax");
            return (Criteria) this;
        }

        public Criteria andObcFaxLike(String value) {
            addCriterion("OBC_Fax like", value, "obcFax");
            return (Criteria) this;
        }

        public Criteria andObcFaxNotLike(String value) {
            addCriterion("OBC_Fax not like", value, "obcFax");
            return (Criteria) this;
        }

        public Criteria andObcFaxIn(List<String> values) {
            addCriterion("OBC_Fax in", values, "obcFax");
            return (Criteria) this;
        }

        public Criteria andObcFaxNotIn(List<String> values) {
            addCriterion("OBC_Fax not in", values, "obcFax");
            return (Criteria) this;
        }

        public Criteria andObcFaxBetween(String value1, String value2) {
            addCriterion("OBC_Fax between", value1, value2, "obcFax");
            return (Criteria) this;
        }

        public Criteria andObcFaxNotBetween(String value1, String value2) {
            addCriterion("OBC_Fax not between", value1, value2, "obcFax");
            return (Criteria) this;
        }

        public Criteria andObcZipcodeIsNull() {
            addCriterion("OBC_ZipCode is null");
            return (Criteria) this;
        }

        public Criteria andObcZipcodeIsNotNull() {
            addCriterion("OBC_ZipCode is not null");
            return (Criteria) this;
        }

        public Criteria andObcZipcodeEqualTo(String value) {
            addCriterion("OBC_ZipCode =", value, "obcZipcode");
            return (Criteria) this;
        }

        public Criteria andObcZipcodeNotEqualTo(String value) {
            addCriterion("OBC_ZipCode <>", value, "obcZipcode");
            return (Criteria) this;
        }

        public Criteria andObcZipcodeGreaterThan(String value) {
            addCriterion("OBC_ZipCode >", value, "obcZipcode");
            return (Criteria) this;
        }

        public Criteria andObcZipcodeGreaterThanOrEqualTo(String value) {
            addCriterion("OBC_ZipCode >=", value, "obcZipcode");
            return (Criteria) this;
        }

        public Criteria andObcZipcodeLessThan(String value) {
            addCriterion("OBC_ZipCode <", value, "obcZipcode");
            return (Criteria) this;
        }

        public Criteria andObcZipcodeLessThanOrEqualTo(String value) {
            addCriterion("OBC_ZipCode <=", value, "obcZipcode");
            return (Criteria) this;
        }

        public Criteria andObcZipcodeLike(String value) {
            addCriterion("OBC_ZipCode like", value, "obcZipcode");
            return (Criteria) this;
        }

        public Criteria andObcZipcodeNotLike(String value) {
            addCriterion("OBC_ZipCode not like", value, "obcZipcode");
            return (Criteria) this;
        }

        public Criteria andObcZipcodeIn(List<String> values) {
            addCriterion("OBC_ZipCode in", values, "obcZipcode");
            return (Criteria) this;
        }

        public Criteria andObcZipcodeNotIn(List<String> values) {
            addCriterion("OBC_ZipCode not in", values, "obcZipcode");
            return (Criteria) this;
        }

        public Criteria andObcZipcodeBetween(String value1, String value2) {
            addCriterion("OBC_ZipCode between", value1, value2, "obcZipcode");
            return (Criteria) this;
        }

        public Criteria andObcZipcodeNotBetween(String value1, String value2) {
            addCriterion("OBC_ZipCode not between", value1, value2, "obcZipcode");
            return (Criteria) this;
        }

        public Criteria andObcLegalpersonIsNull() {
            addCriterion("OBC_LegalPerson is null");
            return (Criteria) this;
        }

        public Criteria andObcLegalpersonIsNotNull() {
            addCriterion("OBC_LegalPerson is not null");
            return (Criteria) this;
        }

        public Criteria andObcLegalpersonEqualTo(String value) {
            addCriterion("OBC_LegalPerson =", value, "obcLegalperson");
            return (Criteria) this;
        }

        public Criteria andObcLegalpersonNotEqualTo(String value) {
            addCriterion("OBC_LegalPerson <>", value, "obcLegalperson");
            return (Criteria) this;
        }

        public Criteria andObcLegalpersonGreaterThan(String value) {
            addCriterion("OBC_LegalPerson >", value, "obcLegalperson");
            return (Criteria) this;
        }

        public Criteria andObcLegalpersonGreaterThanOrEqualTo(String value) {
            addCriterion("OBC_LegalPerson >=", value, "obcLegalperson");
            return (Criteria) this;
        }

        public Criteria andObcLegalpersonLessThan(String value) {
            addCriterion("OBC_LegalPerson <", value, "obcLegalperson");
            return (Criteria) this;
        }

        public Criteria andObcLegalpersonLessThanOrEqualTo(String value) {
            addCriterion("OBC_LegalPerson <=", value, "obcLegalperson");
            return (Criteria) this;
        }

        public Criteria andObcLegalpersonLike(String value) {
            addCriterion("OBC_LegalPerson like", value, "obcLegalperson");
            return (Criteria) this;
        }

        public Criteria andObcLegalpersonNotLike(String value) {
            addCriterion("OBC_LegalPerson not like", value, "obcLegalperson");
            return (Criteria) this;
        }

        public Criteria andObcLegalpersonIn(List<String> values) {
            addCriterion("OBC_LegalPerson in", values, "obcLegalperson");
            return (Criteria) this;
        }

        public Criteria andObcLegalpersonNotIn(List<String> values) {
            addCriterion("OBC_LegalPerson not in", values, "obcLegalperson");
            return (Criteria) this;
        }

        public Criteria andObcLegalpersonBetween(String value1, String value2) {
            addCriterion("OBC_LegalPerson between", value1, value2, "obcLegalperson");
            return (Criteria) this;
        }

        public Criteria andObcLegalpersonNotBetween(String value1, String value2) {
            addCriterion("OBC_LegalPerson not between", value1, value2, "obcLegalperson");
            return (Criteria) this;
        }

        public Criteria andObcManagerIsNull() {
            addCriterion("OBC_Manager is null");
            return (Criteria) this;
        }

        public Criteria andObcManagerIsNotNull() {
            addCriterion("OBC_Manager is not null");
            return (Criteria) this;
        }

        public Criteria andObcManagerEqualTo(String value) {
            addCriterion("OBC_Manager =", value, "obcManager");
            return (Criteria) this;
        }

        public Criteria andObcManagerNotEqualTo(String value) {
            addCriterion("OBC_Manager <>", value, "obcManager");
            return (Criteria) this;
        }

        public Criteria andObcManagerGreaterThan(String value) {
            addCriterion("OBC_Manager >", value, "obcManager");
            return (Criteria) this;
        }

        public Criteria andObcManagerGreaterThanOrEqualTo(String value) {
            addCriterion("OBC_Manager >=", value, "obcManager");
            return (Criteria) this;
        }

        public Criteria andObcManagerLessThan(String value) {
            addCriterion("OBC_Manager <", value, "obcManager");
            return (Criteria) this;
        }

        public Criteria andObcManagerLessThanOrEqualTo(String value) {
            addCriterion("OBC_Manager <=", value, "obcManager");
            return (Criteria) this;
        }

        public Criteria andObcManagerLike(String value) {
            addCriterion("OBC_Manager like", value, "obcManager");
            return (Criteria) this;
        }

        public Criteria andObcManagerNotLike(String value) {
            addCriterion("OBC_Manager not like", value, "obcManager");
            return (Criteria) this;
        }

        public Criteria andObcManagerIn(List<String> values) {
            addCriterion("OBC_Manager in", values, "obcManager");
            return (Criteria) this;
        }

        public Criteria andObcManagerNotIn(List<String> values) {
            addCriterion("OBC_Manager not in", values, "obcManager");
            return (Criteria) this;
        }

        public Criteria andObcManagerBetween(String value1, String value2) {
            addCriterion("OBC_Manager between", value1, value2, "obcManager");
            return (Criteria) this;
        }

        public Criteria andObcManagerNotBetween(String value1, String value2) {
            addCriterion("OBC_Manager not between", value1, value2, "obcManager");
            return (Criteria) this;
        }

        public Criteria andObcContactsIsNull() {
            addCriterion("OBC_Contacts is null");
            return (Criteria) this;
        }

        public Criteria andObcContactsIsNotNull() {
            addCriterion("OBC_Contacts is not null");
            return (Criteria) this;
        }

        public Criteria andObcContactsEqualTo(String value) {
            addCriterion("OBC_Contacts =", value, "obcContacts");
            return (Criteria) this;
        }

        public Criteria andObcContactsNotEqualTo(String value) {
            addCriterion("OBC_Contacts <>", value, "obcContacts");
            return (Criteria) this;
        }

        public Criteria andObcContactsGreaterThan(String value) {
            addCriterion("OBC_Contacts >", value, "obcContacts");
            return (Criteria) this;
        }

        public Criteria andObcContactsGreaterThanOrEqualTo(String value) {
            addCriterion("OBC_Contacts >=", value, "obcContacts");
            return (Criteria) this;
        }

        public Criteria andObcContactsLessThan(String value) {
            addCriterion("OBC_Contacts <", value, "obcContacts");
            return (Criteria) this;
        }

        public Criteria andObcContactsLessThanOrEqualTo(String value) {
            addCriterion("OBC_Contacts <=", value, "obcContacts");
            return (Criteria) this;
        }

        public Criteria andObcContactsLike(String value) {
            addCriterion("OBC_Contacts like", value, "obcContacts");
            return (Criteria) this;
        }

        public Criteria andObcContactsNotLike(String value) {
            addCriterion("OBC_Contacts not like", value, "obcContacts");
            return (Criteria) this;
        }

        public Criteria andObcContactsIn(List<String> values) {
            addCriterion("OBC_Contacts in", values, "obcContacts");
            return (Criteria) this;
        }

        public Criteria andObcContactsNotIn(List<String> values) {
            addCriterion("OBC_Contacts not in", values, "obcContacts");
            return (Criteria) this;
        }

        public Criteria andObcContactsBetween(String value1, String value2) {
            addCriterion("OBC_Contacts between", value1, value2, "obcContacts");
            return (Criteria) this;
        }

        public Criteria andObcContactsNotBetween(String value1, String value2) {
            addCriterion("OBC_Contacts not between", value1, value2, "obcContacts");
            return (Criteria) this;
        }

        public Criteria andObcContactphoneIsNull() {
            addCriterion("OBC_ContactPhone is null");
            return (Criteria) this;
        }

        public Criteria andObcContactphoneIsNotNull() {
            addCriterion("OBC_ContactPhone is not null");
            return (Criteria) this;
        }

        public Criteria andObcContactphoneEqualTo(String value) {
            addCriterion("OBC_ContactPhone =", value, "obcContactphone");
            return (Criteria) this;
        }

        public Criteria andObcContactphoneNotEqualTo(String value) {
            addCriterion("OBC_ContactPhone <>", value, "obcContactphone");
            return (Criteria) this;
        }

        public Criteria andObcContactphoneGreaterThan(String value) {
            addCriterion("OBC_ContactPhone >", value, "obcContactphone");
            return (Criteria) this;
        }

        public Criteria andObcContactphoneGreaterThanOrEqualTo(String value) {
            addCriterion("OBC_ContactPhone >=", value, "obcContactphone");
            return (Criteria) this;
        }

        public Criteria andObcContactphoneLessThan(String value) {
            addCriterion("OBC_ContactPhone <", value, "obcContactphone");
            return (Criteria) this;
        }

        public Criteria andObcContactphoneLessThanOrEqualTo(String value) {
            addCriterion("OBC_ContactPhone <=", value, "obcContactphone");
            return (Criteria) this;
        }

        public Criteria andObcContactphoneLike(String value) {
            addCriterion("OBC_ContactPhone like", value, "obcContactphone");
            return (Criteria) this;
        }

        public Criteria andObcContactphoneNotLike(String value) {
            addCriterion("OBC_ContactPhone not like", value, "obcContactphone");
            return (Criteria) this;
        }

        public Criteria andObcContactphoneIn(List<String> values) {
            addCriterion("OBC_ContactPhone in", values, "obcContactphone");
            return (Criteria) this;
        }

        public Criteria andObcContactphoneNotIn(List<String> values) {
            addCriterion("OBC_ContactPhone not in", values, "obcContactphone");
            return (Criteria) this;
        }

        public Criteria andObcContactphoneBetween(String value1, String value2) {
            addCriterion("OBC_ContactPhone between", value1, value2, "obcContactphone");
            return (Criteria) this;
        }

        public Criteria andObcContactphoneNotBetween(String value1, String value2) {
            addCriterion("OBC_ContactPhone not between", value1, value2, "obcContactphone");
            return (Criteria) this;
        }

        public Criteria andObcContractnumberIsNull() {
            addCriterion("OBC_ContractNumber is null");
            return (Criteria) this;
        }

        public Criteria andObcContractnumberIsNotNull() {
            addCriterion("OBC_ContractNumber is not null");
            return (Criteria) this;
        }

        public Criteria andObcContractnumberEqualTo(String value) {
            addCriterion("OBC_ContractNumber =", value, "obcContractnumber");
            return (Criteria) this;
        }

        public Criteria andObcContractnumberNotEqualTo(String value) {
            addCriterion("OBC_ContractNumber <>", value, "obcContractnumber");
            return (Criteria) this;
        }

        public Criteria andObcContractnumberGreaterThan(String value) {
            addCriterion("OBC_ContractNumber >", value, "obcContractnumber");
            return (Criteria) this;
        }

        public Criteria andObcContractnumberGreaterThanOrEqualTo(String value) {
            addCriterion("OBC_ContractNumber >=", value, "obcContractnumber");
            return (Criteria) this;
        }

        public Criteria andObcContractnumberLessThan(String value) {
            addCriterion("OBC_ContractNumber <", value, "obcContractnumber");
            return (Criteria) this;
        }

        public Criteria andObcContractnumberLessThanOrEqualTo(String value) {
            addCriterion("OBC_ContractNumber <=", value, "obcContractnumber");
            return (Criteria) this;
        }

        public Criteria andObcContractnumberLike(String value) {
            addCriterion("OBC_ContractNumber like", value, "obcContractnumber");
            return (Criteria) this;
        }

        public Criteria andObcContractnumberNotLike(String value) {
            addCriterion("OBC_ContractNumber not like", value, "obcContractnumber");
            return (Criteria) this;
        }

        public Criteria andObcContractnumberIn(List<String> values) {
            addCriterion("OBC_ContractNumber in", values, "obcContractnumber");
            return (Criteria) this;
        }

        public Criteria andObcContractnumberNotIn(List<String> values) {
            addCriterion("OBC_ContractNumber not in", values, "obcContractnumber");
            return (Criteria) this;
        }

        public Criteria andObcContractnumberBetween(String value1, String value2) {
            addCriterion("OBC_ContractNumber between", value1, value2, "obcContractnumber");
            return (Criteria) this;
        }

        public Criteria andObcContractnumberNotBetween(String value1, String value2) {
            addCriterion("OBC_ContractNumber not between", value1, value2, "obcContractnumber");
            return (Criteria) this;
        }

        public Criteria andObcProtocolnoIsNull() {
            addCriterion("OBC_ProtocolNo is null");
            return (Criteria) this;
        }

        public Criteria andObcProtocolnoIsNotNull() {
            addCriterion("OBC_ProtocolNo is not null");
            return (Criteria) this;
        }

        public Criteria andObcProtocolnoEqualTo(String value) {
            addCriterion("OBC_ProtocolNo =", value, "obcProtocolno");
            return (Criteria) this;
        }

        public Criteria andObcProtocolnoNotEqualTo(String value) {
            addCriterion("OBC_ProtocolNo <>", value, "obcProtocolno");
            return (Criteria) this;
        }

        public Criteria andObcProtocolnoGreaterThan(String value) {
            addCriterion("OBC_ProtocolNo >", value, "obcProtocolno");
            return (Criteria) this;
        }

        public Criteria andObcProtocolnoGreaterThanOrEqualTo(String value) {
            addCriterion("OBC_ProtocolNo >=", value, "obcProtocolno");
            return (Criteria) this;
        }

        public Criteria andObcProtocolnoLessThan(String value) {
            addCriterion("OBC_ProtocolNo <", value, "obcProtocolno");
            return (Criteria) this;
        }

        public Criteria andObcProtocolnoLessThanOrEqualTo(String value) {
            addCriterion("OBC_ProtocolNo <=", value, "obcProtocolno");
            return (Criteria) this;
        }

        public Criteria andObcProtocolnoLike(String value) {
            addCriterion("OBC_ProtocolNo like", value, "obcProtocolno");
            return (Criteria) this;
        }

        public Criteria andObcProtocolnoNotLike(String value) {
            addCriterion("OBC_ProtocolNo not like", value, "obcProtocolno");
            return (Criteria) this;
        }

        public Criteria andObcProtocolnoIn(List<String> values) {
            addCriterion("OBC_ProtocolNo in", values, "obcProtocolno");
            return (Criteria) this;
        }

        public Criteria andObcProtocolnoNotIn(List<String> values) {
            addCriterion("OBC_ProtocolNo not in", values, "obcProtocolno");
            return (Criteria) this;
        }

        public Criteria andObcProtocolnoBetween(String value1, String value2) {
            addCriterion("OBC_ProtocolNo between", value1, value2, "obcProtocolno");
            return (Criteria) this;
        }

        public Criteria andObcProtocolnoNotBetween(String value1, String value2) {
            addCriterion("OBC_ProtocolNo not between", value1, value2, "obcProtocolno");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardintegraltidIsNull() {
            addCriterion("OBC_BCRewardIntegralTid is null");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardintegraltidIsNotNull() {
            addCriterion("OBC_BCRewardIntegralTid is not null");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardintegraltidEqualTo(Long value) {
            addCriterion("OBC_BCRewardIntegralTid =", value, "obcBcrewardintegraltid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardintegraltidNotEqualTo(Long value) {
            addCriterion("OBC_BCRewardIntegralTid <>", value, "obcBcrewardintegraltid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardintegraltidGreaterThan(Long value) {
            addCriterion("OBC_BCRewardIntegralTid >", value, "obcBcrewardintegraltid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardintegraltidGreaterThanOrEqualTo(Long value) {
            addCriterion("OBC_BCRewardIntegralTid >=", value, "obcBcrewardintegraltid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardintegraltidLessThan(Long value) {
            addCriterion("OBC_BCRewardIntegralTid <", value, "obcBcrewardintegraltid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardintegraltidLessThanOrEqualTo(Long value) {
            addCriterion("OBC_BCRewardIntegralTid <=", value, "obcBcrewardintegraltid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardintegraltidIn(List<Long> values) {
            addCriterion("OBC_BCRewardIntegralTid in", values, "obcBcrewardintegraltid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardintegraltidNotIn(List<Long> values) {
            addCriterion("OBC_BCRewardIntegralTid not in", values, "obcBcrewardintegraltid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardintegraltidBetween(Long value1, Long value2) {
            addCriterion("OBC_BCRewardIntegralTid between", value1, value2, "obcBcrewardintegraltid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardintegraltidNotBetween(Long value1, Long value2) {
            addCriterion("OBC_BCRewardIntegralTid not between", value1, value2, "obcBcrewardintegraltid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardcashtidIsNull() {
            addCriterion("OBC_BCRewardCashTid is null");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardcashtidIsNotNull() {
            addCriterion("OBC_BCRewardCashTid is not null");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardcashtidEqualTo(Long value) {
            addCriterion("OBC_BCRewardCashTid =", value, "obcBcrewardcashtid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardcashtidNotEqualTo(Long value) {
            addCriterion("OBC_BCRewardCashTid <>", value, "obcBcrewardcashtid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardcashtidGreaterThan(Long value) {
            addCriterion("OBC_BCRewardCashTid >", value, "obcBcrewardcashtid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardcashtidGreaterThanOrEqualTo(Long value) {
            addCriterion("OBC_BCRewardCashTid >=", value, "obcBcrewardcashtid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardcashtidLessThan(Long value) {
            addCriterion("OBC_BCRewardCashTid <", value, "obcBcrewardcashtid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardcashtidLessThanOrEqualTo(Long value) {
            addCriterion("OBC_BCRewardCashTid <=", value, "obcBcrewardcashtid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardcashtidIn(List<Long> values) {
            addCriterion("OBC_BCRewardCashTid in", values, "obcBcrewardcashtid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardcashtidNotIn(List<Long> values) {
            addCriterion("OBC_BCRewardCashTid not in", values, "obcBcrewardcashtid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardcashtidBetween(Long value1, Long value2) {
            addCriterion("OBC_BCRewardCashTid between", value1, value2, "obcBcrewardcashtid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardcashtidNotBetween(Long value1, Long value2) {
            addCriterion("OBC_BCRewardCashTid not between", value1, value2, "obcBcrewardcashtid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardfreetickettidIsNull() {
            addCriterion("OBC_BCRewardFreeTicketTid is null");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardfreetickettidIsNotNull() {
            addCriterion("OBC_BCRewardFreeTicketTid is not null");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardfreetickettidEqualTo(Long value) {
            addCriterion("OBC_BCRewardFreeTicketTid =", value, "obcBcrewardfreetickettid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardfreetickettidNotEqualTo(Long value) {
            addCriterion("OBC_BCRewardFreeTicketTid <>", value, "obcBcrewardfreetickettid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardfreetickettidGreaterThan(Long value) {
            addCriterion("OBC_BCRewardFreeTicketTid >", value, "obcBcrewardfreetickettid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardfreetickettidGreaterThanOrEqualTo(Long value) {
            addCriterion("OBC_BCRewardFreeTicketTid >=", value, "obcBcrewardfreetickettid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardfreetickettidLessThan(Long value) {
            addCriterion("OBC_BCRewardFreeTicketTid <", value, "obcBcrewardfreetickettid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardfreetickettidLessThanOrEqualTo(Long value) {
            addCriterion("OBC_BCRewardFreeTicketTid <=", value, "obcBcrewardfreetickettid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardfreetickettidIn(List<Long> values) {
            addCriterion("OBC_BCRewardFreeTicketTid in", values, "obcBcrewardfreetickettid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardfreetickettidNotIn(List<Long> values) {
            addCriterion("OBC_BCRewardFreeTicketTid not in", values, "obcBcrewardfreetickettid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardfreetickettidBetween(Long value1, Long value2) {
            addCriterion("OBC_BCRewardFreeTicketTid between", value1, value2, "obcBcrewardfreetickettid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardfreetickettidNotBetween(Long value1, Long value2) {
            addCriterion("OBC_BCRewardFreeTicketTid not between", value1, value2, "obcBcrewardfreetickettid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardservicetidIsNull() {
            addCriterion("OBC_BCRewardServiceTid is null");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardservicetidIsNotNull() {
            addCriterion("OBC_BCRewardServiceTid is not null");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardservicetidEqualTo(Long value) {
            addCriterion("OBC_BCRewardServiceTid =", value, "obcBcrewardservicetid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardservicetidNotEqualTo(Long value) {
            addCriterion("OBC_BCRewardServiceTid <>", value, "obcBcrewardservicetid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardservicetidGreaterThan(Long value) {
            addCriterion("OBC_BCRewardServiceTid >", value, "obcBcrewardservicetid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardservicetidGreaterThanOrEqualTo(Long value) {
            addCriterion("OBC_BCRewardServiceTid >=", value, "obcBcrewardservicetid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardservicetidLessThan(Long value) {
            addCriterion("OBC_BCRewardServiceTid <", value, "obcBcrewardservicetid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardservicetidLessThanOrEqualTo(Long value) {
            addCriterion("OBC_BCRewardServiceTid <=", value, "obcBcrewardservicetid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardservicetidIn(List<Long> values) {
            addCriterion("OBC_BCRewardServiceTid in", values, "obcBcrewardservicetid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardservicetidNotIn(List<Long> values) {
            addCriterion("OBC_BCRewardServiceTid not in", values, "obcBcrewardservicetid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardservicetidBetween(Long value1, Long value2) {
            addCriterion("OBC_BCRewardServiceTid between", value1, value2, "obcBcrewardservicetid");
            return (Criteria) this;
        }

        public Criteria andObcBcrewardservicetidNotBetween(Long value1, Long value2) {
            addCriterion("OBC_BCRewardServiceTid not between", value1, value2, "obcBcrewardservicetid");
            return (Criteria) this;
        }

        public Criteria andObcRecommendedpersonIsNull() {
            addCriterion("OBC_RecommendedPerson is null");
            return (Criteria) this;
        }

        public Criteria andObcRecommendedpersonIsNotNull() {
            addCriterion("OBC_RecommendedPerson is not null");
            return (Criteria) this;
        }

        public Criteria andObcRecommendedpersonEqualTo(String value) {
            addCriterion("OBC_RecommendedPerson =", value, "obcRecommendedperson");
            return (Criteria) this;
        }

        public Criteria andObcRecommendedpersonNotEqualTo(String value) {
            addCriterion("OBC_RecommendedPerson <>", value, "obcRecommendedperson");
            return (Criteria) this;
        }

        public Criteria andObcRecommendedpersonGreaterThan(String value) {
            addCriterion("OBC_RecommendedPerson >", value, "obcRecommendedperson");
            return (Criteria) this;
        }

        public Criteria andObcRecommendedpersonGreaterThanOrEqualTo(String value) {
            addCriterion("OBC_RecommendedPerson >=", value, "obcRecommendedperson");
            return (Criteria) this;
        }

        public Criteria andObcRecommendedpersonLessThan(String value) {
            addCriterion("OBC_RecommendedPerson <", value, "obcRecommendedperson");
            return (Criteria) this;
        }

        public Criteria andObcRecommendedpersonLessThanOrEqualTo(String value) {
            addCriterion("OBC_RecommendedPerson <=", value, "obcRecommendedperson");
            return (Criteria) this;
        }

        public Criteria andObcRecommendedpersonLike(String value) {
            addCriterion("OBC_RecommendedPerson like", value, "obcRecommendedperson");
            return (Criteria) this;
        }

        public Criteria andObcRecommendedpersonNotLike(String value) {
            addCriterion("OBC_RecommendedPerson not like", value, "obcRecommendedperson");
            return (Criteria) this;
        }

        public Criteria andObcRecommendedpersonIn(List<String> values) {
            addCriterion("OBC_RecommendedPerson in", values, "obcRecommendedperson");
            return (Criteria) this;
        }

        public Criteria andObcRecommendedpersonNotIn(List<String> values) {
            addCriterion("OBC_RecommendedPerson not in", values, "obcRecommendedperson");
            return (Criteria) this;
        }

        public Criteria andObcRecommendedpersonBetween(String value1, String value2) {
            addCriterion("OBC_RecommendedPerson between", value1, value2, "obcRecommendedperson");
            return (Criteria) this;
        }

        public Criteria andObcRecommendedpersonNotBetween(String value1, String value2) {
            addCriterion("OBC_RecommendedPerson not between", value1, value2, "obcRecommendedperson");
            return (Criteria) this;
        }

        public Criteria andObcRecommentdejobnoIsNull() {
            addCriterion("OBC_RecommentdeJobNo is null");
            return (Criteria) this;
        }

        public Criteria andObcRecommentdejobnoIsNotNull() {
            addCriterion("OBC_RecommentdeJobNo is not null");
            return (Criteria) this;
        }

        public Criteria andObcRecommentdejobnoEqualTo(String value) {
            addCriterion("OBC_RecommentdeJobNo =", value, "obcRecommentdejobno");
            return (Criteria) this;
        }

        public Criteria andObcRecommentdejobnoNotEqualTo(String value) {
            addCriterion("OBC_RecommentdeJobNo <>", value, "obcRecommentdejobno");
            return (Criteria) this;
        }

        public Criteria andObcRecommentdejobnoGreaterThan(String value) {
            addCriterion("OBC_RecommentdeJobNo >", value, "obcRecommentdejobno");
            return (Criteria) this;
        }

        public Criteria andObcRecommentdejobnoGreaterThanOrEqualTo(String value) {
            addCriterion("OBC_RecommentdeJobNo >=", value, "obcRecommentdejobno");
            return (Criteria) this;
        }

        public Criteria andObcRecommentdejobnoLessThan(String value) {
            addCriterion("OBC_RecommentdeJobNo <", value, "obcRecommentdejobno");
            return (Criteria) this;
        }

        public Criteria andObcRecommentdejobnoLessThanOrEqualTo(String value) {
            addCriterion("OBC_RecommentdeJobNo <=", value, "obcRecommentdejobno");
            return (Criteria) this;
        }

        public Criteria andObcRecommentdejobnoLike(String value) {
            addCriterion("OBC_RecommentdeJobNo like", value, "obcRecommentdejobno");
            return (Criteria) this;
        }

        public Criteria andObcRecommentdejobnoNotLike(String value) {
            addCriterion("OBC_RecommentdeJobNo not like", value, "obcRecommentdejobno");
            return (Criteria) this;
        }

        public Criteria andObcRecommentdejobnoIn(List<String> values) {
            addCriterion("OBC_RecommentdeJobNo in", values, "obcRecommentdejobno");
            return (Criteria) this;
        }

        public Criteria andObcRecommentdejobnoNotIn(List<String> values) {
            addCriterion("OBC_RecommentdeJobNo not in", values, "obcRecommentdejobno");
            return (Criteria) this;
        }

        public Criteria andObcRecommentdejobnoBetween(String value1, String value2) {
            addCriterion("OBC_RecommentdeJobNo between", value1, value2, "obcRecommentdejobno");
            return (Criteria) this;
        }

        public Criteria andObcRecommentdejobnoNotBetween(String value1, String value2) {
            addCriterion("OBC_RecommentdeJobNo not between", value1, value2, "obcRecommentdejobno");
            return (Criteria) this;
        }

        public Criteria andObcSalespersonIsNull() {
            addCriterion("OBC_Salesperson is null");
            return (Criteria) this;
        }

        public Criteria andObcSalespersonIsNotNull() {
            addCriterion("OBC_Salesperson is not null");
            return (Criteria) this;
        }

        public Criteria andObcSalespersonEqualTo(String value) {
            addCriterion("OBC_Salesperson =", value, "obcSalesperson");
            return (Criteria) this;
        }

        public Criteria andObcSalespersonNotEqualTo(String value) {
            addCriterion("OBC_Salesperson <>", value, "obcSalesperson");
            return (Criteria) this;
        }

        public Criteria andObcSalespersonGreaterThan(String value) {
            addCriterion("OBC_Salesperson >", value, "obcSalesperson");
            return (Criteria) this;
        }

        public Criteria andObcSalespersonGreaterThanOrEqualTo(String value) {
            addCriterion("OBC_Salesperson >=", value, "obcSalesperson");
            return (Criteria) this;
        }

        public Criteria andObcSalespersonLessThan(String value) {
            addCriterion("OBC_Salesperson <", value, "obcSalesperson");
            return (Criteria) this;
        }

        public Criteria andObcSalespersonLessThanOrEqualTo(String value) {
            addCriterion("OBC_Salesperson <=", value, "obcSalesperson");
            return (Criteria) this;
        }

        public Criteria andObcSalespersonLike(String value) {
            addCriterion("OBC_Salesperson like", value, "obcSalesperson");
            return (Criteria) this;
        }

        public Criteria andObcSalespersonNotLike(String value) {
            addCriterion("OBC_Salesperson not like", value, "obcSalesperson");
            return (Criteria) this;
        }

        public Criteria andObcSalespersonIn(List<String> values) {
            addCriterion("OBC_Salesperson in", values, "obcSalesperson");
            return (Criteria) this;
        }

        public Criteria andObcSalespersonNotIn(List<String> values) {
            addCriterion("OBC_Salesperson not in", values, "obcSalesperson");
            return (Criteria) this;
        }

        public Criteria andObcSalespersonBetween(String value1, String value2) {
            addCriterion("OBC_Salesperson between", value1, value2, "obcSalesperson");
            return (Criteria) this;
        }

        public Criteria andObcSalespersonNotBetween(String value1, String value2) {
            addCriterion("OBC_Salesperson not between", value1, value2, "obcSalesperson");
            return (Criteria) this;
        }

        public Criteria andObcTerminationreasonIsNull() {
            addCriterion("OBC_TerminationReason is null");
            return (Criteria) this;
        }

        public Criteria andObcTerminationreasonIsNotNull() {
            addCriterion("OBC_TerminationReason is not null");
            return (Criteria) this;
        }

        public Criteria andObcTerminationreasonEqualTo(String value) {
            addCriterion("OBC_TerminationReason =", value, "obcTerminationreason");
            return (Criteria) this;
        }

        public Criteria andObcTerminationreasonNotEqualTo(String value) {
            addCriterion("OBC_TerminationReason <>", value, "obcTerminationreason");
            return (Criteria) this;
        }

        public Criteria andObcTerminationreasonGreaterThan(String value) {
            addCriterion("OBC_TerminationReason >", value, "obcTerminationreason");
            return (Criteria) this;
        }

        public Criteria andObcTerminationreasonGreaterThanOrEqualTo(String value) {
            addCriterion("OBC_TerminationReason >=", value, "obcTerminationreason");
            return (Criteria) this;
        }

        public Criteria andObcTerminationreasonLessThan(String value) {
            addCriterion("OBC_TerminationReason <", value, "obcTerminationreason");
            return (Criteria) this;
        }

        public Criteria andObcTerminationreasonLessThanOrEqualTo(String value) {
            addCriterion("OBC_TerminationReason <=", value, "obcTerminationreason");
            return (Criteria) this;
        }

        public Criteria andObcTerminationreasonLike(String value) {
            addCriterion("OBC_TerminationReason like", value, "obcTerminationreason");
            return (Criteria) this;
        }

        public Criteria andObcTerminationreasonNotLike(String value) {
            addCriterion("OBC_TerminationReason not like", value, "obcTerminationreason");
            return (Criteria) this;
        }

        public Criteria andObcTerminationreasonIn(List<String> values) {
            addCriterion("OBC_TerminationReason in", values, "obcTerminationreason");
            return (Criteria) this;
        }

        public Criteria andObcTerminationreasonNotIn(List<String> values) {
            addCriterion("OBC_TerminationReason not in", values, "obcTerminationreason");
            return (Criteria) this;
        }

        public Criteria andObcTerminationreasonBetween(String value1, String value2) {
            addCriterion("OBC_TerminationReason between", value1, value2, "obcTerminationreason");
            return (Criteria) this;
        }

        public Criteria andObcTerminationreasonNotBetween(String value1, String value2) {
            addCriterion("OBC_TerminationReason not between", value1, value2, "obcTerminationreason");
            return (Criteria) this;
        }

        public Criteria andObcCardnumstrIsNull() {
            addCriterion("OBC_CardNumStr is null");
            return (Criteria) this;
        }

        public Criteria andObcCardnumstrIsNotNull() {
            addCriterion("OBC_CardNumStr is not null");
            return (Criteria) this;
        }

        public Criteria andObcCardnumstrEqualTo(String value) {
            addCriterion("OBC_CardNumStr =", value, "obcCardnumstr");
            return (Criteria) this;
        }

        public Criteria andObcCardnumstrNotEqualTo(String value) {
            addCriterion("OBC_CardNumStr <>", value, "obcCardnumstr");
            return (Criteria) this;
        }

        public Criteria andObcCardnumstrGreaterThan(String value) {
            addCriterion("OBC_CardNumStr >", value, "obcCardnumstr");
            return (Criteria) this;
        }

        public Criteria andObcCardnumstrGreaterThanOrEqualTo(String value) {
            addCriterion("OBC_CardNumStr >=", value, "obcCardnumstr");
            return (Criteria) this;
        }

        public Criteria andObcCardnumstrLessThan(String value) {
            addCriterion("OBC_CardNumStr <", value, "obcCardnumstr");
            return (Criteria) this;
        }

        public Criteria andObcCardnumstrLessThanOrEqualTo(String value) {
            addCriterion("OBC_CardNumStr <=", value, "obcCardnumstr");
            return (Criteria) this;
        }

        public Criteria andObcCardnumstrLike(String value) {
            addCriterion("OBC_CardNumStr like", value, "obcCardnumstr");
            return (Criteria) this;
        }

        public Criteria andObcCardnumstrNotLike(String value) {
            addCriterion("OBC_CardNumStr not like", value, "obcCardnumstr");
            return (Criteria) this;
        }

        public Criteria andObcCardnumstrIn(List<String> values) {
            addCriterion("OBC_CardNumStr in", values, "obcCardnumstr");
            return (Criteria) this;
        }

        public Criteria andObcCardnumstrNotIn(List<String> values) {
            addCriterion("OBC_CardNumStr not in", values, "obcCardnumstr");
            return (Criteria) this;
        }

        public Criteria andObcCardnumstrBetween(String value1, String value2) {
            addCriterion("OBC_CardNumStr between", value1, value2, "obcCardnumstr");
            return (Criteria) this;
        }

        public Criteria andObcCardnumstrNotBetween(String value1, String value2) {
            addCriterion("OBC_CardNumStr not between", value1, value2, "obcCardnumstr");
            return (Criteria) this;
        }

        public Criteria andObcOfficecodeIsNull() {
            addCriterion("OBC_OfficeCode is null");
            return (Criteria) this;
        }

        public Criteria andObcOfficecodeIsNotNull() {
            addCriterion("OBC_OfficeCode is not null");
            return (Criteria) this;
        }

        public Criteria andObcOfficecodeEqualTo(String value) {
            addCriterion("OBC_OfficeCode =", value, "obcOfficecode");
            return (Criteria) this;
        }

        public Criteria andObcOfficecodeNotEqualTo(String value) {
            addCriterion("OBC_OfficeCode <>", value, "obcOfficecode");
            return (Criteria) this;
        }

        public Criteria andObcOfficecodeGreaterThan(String value) {
            addCriterion("OBC_OfficeCode >", value, "obcOfficecode");
            return (Criteria) this;
        }

        public Criteria andObcOfficecodeGreaterThanOrEqualTo(String value) {
            addCriterion("OBC_OfficeCode >=", value, "obcOfficecode");
            return (Criteria) this;
        }

        public Criteria andObcOfficecodeLessThan(String value) {
            addCriterion("OBC_OfficeCode <", value, "obcOfficecode");
            return (Criteria) this;
        }

        public Criteria andObcOfficecodeLessThanOrEqualTo(String value) {
            addCriterion("OBC_OfficeCode <=", value, "obcOfficecode");
            return (Criteria) this;
        }

        public Criteria andObcOfficecodeLike(String value) {
            addCriterion("OBC_OfficeCode like", value, "obcOfficecode");
            return (Criteria) this;
        }

        public Criteria andObcOfficecodeNotLike(String value) {
            addCriterion("OBC_OfficeCode not like", value, "obcOfficecode");
            return (Criteria) this;
        }

        public Criteria andObcOfficecodeIn(List<String> values) {
            addCriterion("OBC_OfficeCode in", values, "obcOfficecode");
            return (Criteria) this;
        }

        public Criteria andObcOfficecodeNotIn(List<String> values) {
            addCriterion("OBC_OfficeCode not in", values, "obcOfficecode");
            return (Criteria) this;
        }

        public Criteria andObcOfficecodeBetween(String value1, String value2) {
            addCriterion("OBC_OfficeCode between", value1, value2, "obcOfficecode");
            return (Criteria) this;
        }

        public Criteria andObcOfficecodeNotBetween(String value1, String value2) {
            addCriterion("OBC_OfficeCode not between", value1, value2, "obcOfficecode");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}