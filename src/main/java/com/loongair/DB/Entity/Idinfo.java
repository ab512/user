package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Idinfo extends BaseEntity {


    private Long idiLausertid;

    private String idiName;

    private String idiId;

    private Integer idiType;



    public Long getIdiLausertid() {
        return idiLausertid;
    }

    public void setIdiLausertid(Long idiLausertid) {
        this.idiLausertid = idiLausertid;
    }

    public String getIdiName() {
        return idiName;
    }

    public void setIdiName(String idiName) {
        this.idiName = idiName == null ? null : idiName.trim();
    }

    public String getIdiId() {
        return idiId;
    }

    public void setIdiId(String idiId) {
        this.idiId = idiId == null ? null : idiId.trim();
    }

    public Integer getIdiType() {
        return idiType;
    }

    public void setIdiType(Integer idiType) {
        this.idiType = idiType;
    }
}