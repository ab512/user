package com.loongair.DB.Entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UseragentExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UseragentExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTidIsNull() {
            addCriterion("Tid is null");
            return (Criteria) this;
        }

        public Criteria andTidIsNotNull() {
            addCriterion("Tid is not null");
            return (Criteria) this;
        }

        public Criteria andTidEqualTo(Long value) {
            addCriterion("Tid =", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotEqualTo(Long value) {
            addCriterion("Tid <>", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThan(Long value) {
            addCriterion("Tid >", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThanOrEqualTo(Long value) {
            addCriterion("Tid >=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThan(Long value) {
            addCriterion("Tid <", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThanOrEqualTo(Long value) {
            addCriterion("Tid <=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidIn(List<Long> values) {
            addCriterion("Tid in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotIn(List<Long> values) {
            addCriterion("Tid not in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidBetween(Long value1, Long value2) {
            addCriterion("Tid between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotBetween(Long value1, Long value2) {
            addCriterion("Tid not between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNull() {
            addCriterion("CreateId is null");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNotNull() {
            addCriterion("CreateId is not null");
            return (Criteria) this;
        }

        public Criteria andCreateidEqualTo(Long value) {
            addCriterion("CreateId =", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotEqualTo(Long value) {
            addCriterion("CreateId <>", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThan(Long value) {
            addCriterion("CreateId >", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThanOrEqualTo(Long value) {
            addCriterion("CreateId >=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThan(Long value) {
            addCriterion("CreateId <", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThanOrEqualTo(Long value) {
            addCriterion("CreateId <=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidIn(List<Long> values) {
            addCriterion("CreateId in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotIn(List<Long> values) {
            addCriterion("CreateId not in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidBetween(Long value1, Long value2) {
            addCriterion("CreateId between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotBetween(Long value1, Long value2) {
            addCriterion("CreateId not between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNull() {
            addCriterion("ProxyUserId is null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNotNull() {
            addCriterion("ProxyUserId is not null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridEqualTo(Long value) {
            addCriterion("ProxyUserId =", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotEqualTo(Long value) {
            addCriterion("ProxyUserId <>", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThan(Long value) {
            addCriterion("ProxyUserId >", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId >=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThan(Long value) {
            addCriterion("ProxyUserId <", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId <=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIn(List<Long> values) {
            addCriterion("ProxyUserId in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotIn(List<Long> values) {
            addCriterion("ProxyUserId not in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId not between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNull() {
            addCriterion("LastGuid is null");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNotNull() {
            addCriterion("LastGuid is not null");
            return (Criteria) this;
        }

        public Criteria andLastguidEqualTo(Long value) {
            addCriterion("LastGuid =", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotEqualTo(Long value) {
            addCriterion("LastGuid <>", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThan(Long value) {
            addCriterion("LastGuid >", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThanOrEqualTo(Long value) {
            addCriterion("LastGuid >=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThan(Long value) {
            addCriterion("LastGuid <", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThanOrEqualTo(Long value) {
            addCriterion("LastGuid <=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidIn(List<Long> values) {
            addCriterion("LastGuid in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotIn(List<Long> values) {
            addCriterion("LastGuid not in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidBetween(Long value1, Long value2) {
            addCriterion("LastGuid between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotBetween(Long value1, Long value2) {
            addCriterion("LastGuid not between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andCloneIsNull() {
            addCriterion("Clone is null");
            return (Criteria) this;
        }

        public Criteria andCloneIsNotNull() {
            addCriterion("Clone is not null");
            return (Criteria) this;
        }

        public Criteria andCloneEqualTo(Integer value) {
            addCriterion("Clone =", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotEqualTo(Integer value) {
            addCriterion("Clone <>", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThan(Integer value) {
            addCriterion("Clone >", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThanOrEqualTo(Integer value) {
            addCriterion("Clone >=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThan(Integer value) {
            addCriterion("Clone <", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThanOrEqualTo(Integer value) {
            addCriterion("Clone <=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneIn(List<Integer> values) {
            addCriterion("Clone in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotIn(List<Integer> values) {
            addCriterion("Clone not in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneBetween(Integer value1, Integer value2) {
            addCriterion("Clone between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotBetween(Integer value1, Integer value2) {
            addCriterion("Clone not between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNull() {
            addCriterion("UpdateId is null");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNotNull() {
            addCriterion("UpdateId is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateidEqualTo(Long value) {
            addCriterion("UpdateId =", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotEqualTo(Long value) {
            addCriterion("UpdateId <>", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThan(Long value) {
            addCriterion("UpdateId >", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThanOrEqualTo(Long value) {
            addCriterion("UpdateId >=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThan(Long value) {
            addCriterion("UpdateId <", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThanOrEqualTo(Long value) {
            addCriterion("UpdateId <=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidIn(List<Long> values) {
            addCriterion("UpdateId in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotIn(List<Long> values) {
            addCriterion("UpdateId not in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidBetween(Long value1, Long value2) {
            addCriterion("UpdateId between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotBetween(Long value1, Long value2) {
            addCriterion("UpdateId not between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("UpdateDate is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("UpdateDate is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("UpdateDate =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("UpdateDate <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("UpdateDate >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("UpdateDate >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("UpdateDate <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("UpdateDate <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("UpdateDate in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("UpdateDate not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("UpdateDate between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("UpdateDate not between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("Status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("Status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("Status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("Status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("Status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("Status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("Status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("Status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("Status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("Status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("Status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("Status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andUaLausertidIsNull() {
            addCriterion("UA_LaUserTid is null");
            return (Criteria) this;
        }

        public Criteria andUaLausertidIsNotNull() {
            addCriterion("UA_LaUserTid is not null");
            return (Criteria) this;
        }

        public Criteria andUaLausertidEqualTo(Long value) {
            addCriterion("UA_LaUserTid =", value, "uaLausertid");
            return (Criteria) this;
        }

        public Criteria andUaLausertidNotEqualTo(Long value) {
            addCriterion("UA_LaUserTid <>", value, "uaLausertid");
            return (Criteria) this;
        }

        public Criteria andUaLausertidGreaterThan(Long value) {
            addCriterion("UA_LaUserTid >", value, "uaLausertid");
            return (Criteria) this;
        }

        public Criteria andUaLausertidGreaterThanOrEqualTo(Long value) {
            addCriterion("UA_LaUserTid >=", value, "uaLausertid");
            return (Criteria) this;
        }

        public Criteria andUaLausertidLessThan(Long value) {
            addCriterion("UA_LaUserTid <", value, "uaLausertid");
            return (Criteria) this;
        }

        public Criteria andUaLausertidLessThanOrEqualTo(Long value) {
            addCriterion("UA_LaUserTid <=", value, "uaLausertid");
            return (Criteria) this;
        }

        public Criteria andUaLausertidIn(List<Long> values) {
            addCriterion("UA_LaUserTid in", values, "uaLausertid");
            return (Criteria) this;
        }

        public Criteria andUaLausertidNotIn(List<Long> values) {
            addCriterion("UA_LaUserTid not in", values, "uaLausertid");
            return (Criteria) this;
        }

        public Criteria andUaLausertidBetween(Long value1, Long value2) {
            addCriterion("UA_LaUserTid between", value1, value2, "uaLausertid");
            return (Criteria) this;
        }

        public Criteria andUaLausertidNotBetween(Long value1, Long value2) {
            addCriterion("UA_LaUserTid not between", value1, value2, "uaLausertid");
            return (Criteria) this;
        }

        public Criteria andUaDefaultszmIsNull() {
            addCriterion("UA_DefaultSZM is null");
            return (Criteria) this;
        }

        public Criteria andUaDefaultszmIsNotNull() {
            addCriterion("UA_DefaultSZM is not null");
            return (Criteria) this;
        }

        public Criteria andUaDefaultszmEqualTo(String value) {
            addCriterion("UA_DefaultSZM =", value, "uaDefaultszm");
            return (Criteria) this;
        }

        public Criteria andUaDefaultszmNotEqualTo(String value) {
            addCriterion("UA_DefaultSZM <>", value, "uaDefaultszm");
            return (Criteria) this;
        }

        public Criteria andUaDefaultszmGreaterThan(String value) {
            addCriterion("UA_DefaultSZM >", value, "uaDefaultszm");
            return (Criteria) this;
        }

        public Criteria andUaDefaultszmGreaterThanOrEqualTo(String value) {
            addCriterion("UA_DefaultSZM >=", value, "uaDefaultszm");
            return (Criteria) this;
        }

        public Criteria andUaDefaultszmLessThan(String value) {
            addCriterion("UA_DefaultSZM <", value, "uaDefaultszm");
            return (Criteria) this;
        }

        public Criteria andUaDefaultszmLessThanOrEqualTo(String value) {
            addCriterion("UA_DefaultSZM <=", value, "uaDefaultszm");
            return (Criteria) this;
        }

        public Criteria andUaDefaultszmLike(String value) {
            addCriterion("UA_DefaultSZM like", value, "uaDefaultszm");
            return (Criteria) this;
        }

        public Criteria andUaDefaultszmNotLike(String value) {
            addCriterion("UA_DefaultSZM not like", value, "uaDefaultszm");
            return (Criteria) this;
        }

        public Criteria andUaDefaultszmIn(List<String> values) {
            addCriterion("UA_DefaultSZM in", values, "uaDefaultszm");
            return (Criteria) this;
        }

        public Criteria andUaDefaultszmNotIn(List<String> values) {
            addCriterion("UA_DefaultSZM not in", values, "uaDefaultszm");
            return (Criteria) this;
        }

        public Criteria andUaDefaultszmBetween(String value1, String value2) {
            addCriterion("UA_DefaultSZM between", value1, value2, "uaDefaultszm");
            return (Criteria) this;
        }

        public Criteria andUaDefaultszmNotBetween(String value1, String value2) {
            addCriterion("UA_DefaultSZM not between", value1, value2, "uaDefaultszm");
            return (Criteria) this;
        }

        public Criteria andUaDeparturecityIsNull() {
            addCriterion("UA_DepartureCity is null");
            return (Criteria) this;
        }

        public Criteria andUaDeparturecityIsNotNull() {
            addCriterion("UA_DepartureCity is not null");
            return (Criteria) this;
        }

        public Criteria andUaDeparturecityEqualTo(String value) {
            addCriterion("UA_DepartureCity =", value, "uaDeparturecity");
            return (Criteria) this;
        }

        public Criteria andUaDeparturecityNotEqualTo(String value) {
            addCriterion("UA_DepartureCity <>", value, "uaDeparturecity");
            return (Criteria) this;
        }

        public Criteria andUaDeparturecityGreaterThan(String value) {
            addCriterion("UA_DepartureCity >", value, "uaDeparturecity");
            return (Criteria) this;
        }

        public Criteria andUaDeparturecityGreaterThanOrEqualTo(String value) {
            addCriterion("UA_DepartureCity >=", value, "uaDeparturecity");
            return (Criteria) this;
        }

        public Criteria andUaDeparturecityLessThan(String value) {
            addCriterion("UA_DepartureCity <", value, "uaDeparturecity");
            return (Criteria) this;
        }

        public Criteria andUaDeparturecityLessThanOrEqualTo(String value) {
            addCriterion("UA_DepartureCity <=", value, "uaDeparturecity");
            return (Criteria) this;
        }

        public Criteria andUaDeparturecityLike(String value) {
            addCriterion("UA_DepartureCity like", value, "uaDeparturecity");
            return (Criteria) this;
        }

        public Criteria andUaDeparturecityNotLike(String value) {
            addCriterion("UA_DepartureCity not like", value, "uaDeparturecity");
            return (Criteria) this;
        }

        public Criteria andUaDeparturecityIn(List<String> values) {
            addCriterion("UA_DepartureCity in", values, "uaDeparturecity");
            return (Criteria) this;
        }

        public Criteria andUaDeparturecityNotIn(List<String> values) {
            addCriterion("UA_DepartureCity not in", values, "uaDeparturecity");
            return (Criteria) this;
        }

        public Criteria andUaDeparturecityBetween(String value1, String value2) {
            addCriterion("UA_DepartureCity between", value1, value2, "uaDeparturecity");
            return (Criteria) this;
        }

        public Criteria andUaDeparturecityNotBetween(String value1, String value2) {
            addCriterion("UA_DepartureCity not between", value1, value2, "uaDeparturecity");
            return (Criteria) this;
        }

        public Criteria andUaDepartureairportIsNull() {
            addCriterion("UA_DepartureAirport is null");
            return (Criteria) this;
        }

        public Criteria andUaDepartureairportIsNotNull() {
            addCriterion("UA_DepartureAirport is not null");
            return (Criteria) this;
        }

        public Criteria andUaDepartureairportEqualTo(String value) {
            addCriterion("UA_DepartureAirport =", value, "uaDepartureairport");
            return (Criteria) this;
        }

        public Criteria andUaDepartureairportNotEqualTo(String value) {
            addCriterion("UA_DepartureAirport <>", value, "uaDepartureairport");
            return (Criteria) this;
        }

        public Criteria andUaDepartureairportGreaterThan(String value) {
            addCriterion("UA_DepartureAirport >", value, "uaDepartureairport");
            return (Criteria) this;
        }

        public Criteria andUaDepartureairportGreaterThanOrEqualTo(String value) {
            addCriterion("UA_DepartureAirport >=", value, "uaDepartureairport");
            return (Criteria) this;
        }

        public Criteria andUaDepartureairportLessThan(String value) {
            addCriterion("UA_DepartureAirport <", value, "uaDepartureairport");
            return (Criteria) this;
        }

        public Criteria andUaDepartureairportLessThanOrEqualTo(String value) {
            addCriterion("UA_DepartureAirport <=", value, "uaDepartureairport");
            return (Criteria) this;
        }

        public Criteria andUaDepartureairportLike(String value) {
            addCriterion("UA_DepartureAirport like", value, "uaDepartureairport");
            return (Criteria) this;
        }

        public Criteria andUaDepartureairportNotLike(String value) {
            addCriterion("UA_DepartureAirport not like", value, "uaDepartureairport");
            return (Criteria) this;
        }

        public Criteria andUaDepartureairportIn(List<String> values) {
            addCriterion("UA_DepartureAirport in", values, "uaDepartureairport");
            return (Criteria) this;
        }

        public Criteria andUaDepartureairportNotIn(List<String> values) {
            addCriterion("UA_DepartureAirport not in", values, "uaDepartureairport");
            return (Criteria) this;
        }

        public Criteria andUaDepartureairportBetween(String value1, String value2) {
            addCriterion("UA_DepartureAirport between", value1, value2, "uaDepartureairport");
            return (Criteria) this;
        }

        public Criteria andUaDepartureairportNotBetween(String value1, String value2) {
            addCriterion("UA_DepartureAirport not between", value1, value2, "uaDepartureairport");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}