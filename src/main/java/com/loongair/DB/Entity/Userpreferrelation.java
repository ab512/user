package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Userpreferrelation extends BaseEntity {


    private Long uprLausertid;

    private Long uprPrefertid;



    public Long getUprLausertid() {
        return uprLausertid;
    }

    public void setUprLausertid(Long uprLausertid) {
        this.uprLausertid = uprLausertid;
    }

    public Long getUprPrefertid() {
        return uprPrefertid;
    }

    public void setUprPrefertid(Long uprPrefertid) {
        this.uprPrefertid = uprPrefertid;
    }
}