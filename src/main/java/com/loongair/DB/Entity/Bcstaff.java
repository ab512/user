package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Bcstaff extends BaseEntity {


    private String bcsName;

    private Integer bcsSextype;

    private String bcsPosition;

    private String bcsPhone;

    private String bcsAddress;

    private Long bcsBcorgtid;

    private Integer bcsIsexecutive;

    private Long bcsLausertid;


    public String getBcsName() {
        return bcsName;
    }

    public void setBcsName(String bcsName) {
        this.bcsName = bcsName == null ? null : bcsName.trim();
    }

    public Integer getBcsSextype() {
        return bcsSextype;
    }

    public void setBcsSextype(Integer bcsSextype) {
        this.bcsSextype = bcsSextype;
    }

    public String getBcsPosition() {
        return bcsPosition;
    }

    public void setBcsPosition(String bcsPosition) {
        this.bcsPosition = bcsPosition == null ? null : bcsPosition.trim();
    }

    public String getBcsPhone() {
        return bcsPhone;
    }

    public void setBcsPhone(String bcsPhone) {
        this.bcsPhone = bcsPhone == null ? null : bcsPhone.trim();
    }

    public String getBcsAddress() {
        return bcsAddress;
    }

    public void setBcsAddress(String bcsAddress) {
        this.bcsAddress = bcsAddress == null ? null : bcsAddress.trim();
    }

    public Long getBcsBcorgtid() {
        return bcsBcorgtid;
    }

    public void setBcsBcorgtid(Long bcsBcorgtid) {
        this.bcsBcorgtid = bcsBcorgtid;
    }

    public Integer getBcsIsexecutive() {
        return bcsIsexecutive;
    }

    public void setBcsIsexecutive(Integer bcsIsexecutive) {
        this.bcsIsexecutive = bcsIsexecutive;
    }

    public Long getBcsLausertid() {
        return bcsLausertid;
    }

    public void setBcsLausertid(Long bcsLausertid) {
        this.bcsLausertid = bcsLausertid;
    }
}