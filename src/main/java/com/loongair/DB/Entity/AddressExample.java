package com.loongair.DB.Entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AddressExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public AddressExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTidIsNull() {
            addCriterion("Tid is null");
            return (Criteria) this;
        }

        public Criteria andTidIsNotNull() {
            addCriterion("Tid is not null");
            return (Criteria) this;
        }

        public Criteria andTidEqualTo(Long value) {
            addCriterion("Tid =", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotEqualTo(Long value) {
            addCriterion("Tid <>", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThan(Long value) {
            addCriterion("Tid >", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThanOrEqualTo(Long value) {
            addCriterion("Tid >=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThan(Long value) {
            addCriterion("Tid <", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThanOrEqualTo(Long value) {
            addCriterion("Tid <=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidIn(List<Long> values) {
            addCriterion("Tid in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotIn(List<Long> values) {
            addCriterion("Tid not in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidBetween(Long value1, Long value2) {
            addCriterion("Tid between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotBetween(Long value1, Long value2) {
            addCriterion("Tid not between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNull() {
            addCriterion("CreateId is null");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNotNull() {
            addCriterion("CreateId is not null");
            return (Criteria) this;
        }

        public Criteria andCreateidEqualTo(Long value) {
            addCriterion("CreateId =", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotEqualTo(Long value) {
            addCriterion("CreateId <>", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThan(Long value) {
            addCriterion("CreateId >", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThanOrEqualTo(Long value) {
            addCriterion("CreateId >=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThan(Long value) {
            addCriterion("CreateId <", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThanOrEqualTo(Long value) {
            addCriterion("CreateId <=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidIn(List<Long> values) {
            addCriterion("CreateId in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotIn(List<Long> values) {
            addCriterion("CreateId not in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidBetween(Long value1, Long value2) {
            addCriterion("CreateId between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotBetween(Long value1, Long value2) {
            addCriterion("CreateId not between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNull() {
            addCriterion("ProxyUserId is null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNotNull() {
            addCriterion("ProxyUserId is not null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridEqualTo(Long value) {
            addCriterion("ProxyUserId =", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotEqualTo(Long value) {
            addCriterion("ProxyUserId <>", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThan(Long value) {
            addCriterion("ProxyUserId >", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId >=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThan(Long value) {
            addCriterion("ProxyUserId <", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId <=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIn(List<Long> values) {
            addCriterion("ProxyUserId in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotIn(List<Long> values) {
            addCriterion("ProxyUserId not in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId not between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNull() {
            addCriterion("LastGuid is null");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNotNull() {
            addCriterion("LastGuid is not null");
            return (Criteria) this;
        }

        public Criteria andLastguidEqualTo(Long value) {
            addCriterion("LastGuid =", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotEqualTo(Long value) {
            addCriterion("LastGuid <>", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThan(Long value) {
            addCriterion("LastGuid >", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThanOrEqualTo(Long value) {
            addCriterion("LastGuid >=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThan(Long value) {
            addCriterion("LastGuid <", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThanOrEqualTo(Long value) {
            addCriterion("LastGuid <=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidIn(List<Long> values) {
            addCriterion("LastGuid in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotIn(List<Long> values) {
            addCriterion("LastGuid not in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidBetween(Long value1, Long value2) {
            addCriterion("LastGuid between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotBetween(Long value1, Long value2) {
            addCriterion("LastGuid not between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andCloneIsNull() {
            addCriterion("Clone is null");
            return (Criteria) this;
        }

        public Criteria andCloneIsNotNull() {
            addCriterion("Clone is not null");
            return (Criteria) this;
        }

        public Criteria andCloneEqualTo(Integer value) {
            addCriterion("Clone =", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotEqualTo(Integer value) {
            addCriterion("Clone <>", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThan(Integer value) {
            addCriterion("Clone >", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThanOrEqualTo(Integer value) {
            addCriterion("Clone >=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThan(Integer value) {
            addCriterion("Clone <", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThanOrEqualTo(Integer value) {
            addCriterion("Clone <=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneIn(List<Integer> values) {
            addCriterion("Clone in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotIn(List<Integer> values) {
            addCriterion("Clone not in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneBetween(Integer value1, Integer value2) {
            addCriterion("Clone between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotBetween(Integer value1, Integer value2) {
            addCriterion("Clone not between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNull() {
            addCriterion("UpdateId is null");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNotNull() {
            addCriterion("UpdateId is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateidEqualTo(Long value) {
            addCriterion("UpdateId =", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotEqualTo(Long value) {
            addCriterion("UpdateId <>", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThan(Long value) {
            addCriterion("UpdateId >", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThanOrEqualTo(Long value) {
            addCriterion("UpdateId >=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThan(Long value) {
            addCriterion("UpdateId <", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThanOrEqualTo(Long value) {
            addCriterion("UpdateId <=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidIn(List<Long> values) {
            addCriterion("UpdateId in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotIn(List<Long> values) {
            addCriterion("UpdateId not in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidBetween(Long value1, Long value2) {
            addCriterion("UpdateId between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotBetween(Long value1, Long value2) {
            addCriterion("UpdateId not between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("UpdateDate is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("UpdateDate is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("UpdateDate =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("UpdateDate <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("UpdateDate >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("UpdateDate >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("UpdateDate <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("UpdateDate <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("UpdateDate in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("UpdateDate not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("UpdateDate between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("UpdateDate not between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("Status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("Status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("Status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("Status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("Status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("Status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("Status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("Status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("Status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("Status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("Status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("Status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andAdProvinceIsNull() {
            addCriterion("AD_Province is null");
            return (Criteria) this;
        }

        public Criteria andAdProvinceIsNotNull() {
            addCriterion("AD_Province is not null");
            return (Criteria) this;
        }

        public Criteria andAdProvinceEqualTo(Integer value) {
            addCriterion("AD_Province =", value, "adProvince");
            return (Criteria) this;
        }

        public Criteria andAdProvinceNotEqualTo(Integer value) {
            addCriterion("AD_Province <>", value, "adProvince");
            return (Criteria) this;
        }

        public Criteria andAdProvinceGreaterThan(Integer value) {
            addCriterion("AD_Province >", value, "adProvince");
            return (Criteria) this;
        }

        public Criteria andAdProvinceGreaterThanOrEqualTo(Integer value) {
            addCriterion("AD_Province >=", value, "adProvince");
            return (Criteria) this;
        }

        public Criteria andAdProvinceLessThan(Integer value) {
            addCriterion("AD_Province <", value, "adProvince");
            return (Criteria) this;
        }

        public Criteria andAdProvinceLessThanOrEqualTo(Integer value) {
            addCriterion("AD_Province <=", value, "adProvince");
            return (Criteria) this;
        }

        public Criteria andAdProvinceIn(List<Integer> values) {
            addCriterion("AD_Province in", values, "adProvince");
            return (Criteria) this;
        }

        public Criteria andAdProvinceNotIn(List<Integer> values) {
            addCriterion("AD_Province not in", values, "adProvince");
            return (Criteria) this;
        }

        public Criteria andAdProvinceBetween(Integer value1, Integer value2) {
            addCriterion("AD_Province between", value1, value2, "adProvince");
            return (Criteria) this;
        }

        public Criteria andAdProvinceNotBetween(Integer value1, Integer value2) {
            addCriterion("AD_Province not between", value1, value2, "adProvince");
            return (Criteria) this;
        }

        public Criteria andAdCityIsNull() {
            addCriterion("AD_City is null");
            return (Criteria) this;
        }

        public Criteria andAdCityIsNotNull() {
            addCriterion("AD_City is not null");
            return (Criteria) this;
        }

        public Criteria andAdCityEqualTo(String value) {
            addCriterion("AD_City =", value, "adCity");
            return (Criteria) this;
        }

        public Criteria andAdCityNotEqualTo(String value) {
            addCriterion("AD_City <>", value, "adCity");
            return (Criteria) this;
        }

        public Criteria andAdCityGreaterThan(String value) {
            addCriterion("AD_City >", value, "adCity");
            return (Criteria) this;
        }

        public Criteria andAdCityGreaterThanOrEqualTo(String value) {
            addCriterion("AD_City >=", value, "adCity");
            return (Criteria) this;
        }

        public Criteria andAdCityLessThan(String value) {
            addCriterion("AD_City <", value, "adCity");
            return (Criteria) this;
        }

        public Criteria andAdCityLessThanOrEqualTo(String value) {
            addCriterion("AD_City <=", value, "adCity");
            return (Criteria) this;
        }

        public Criteria andAdCityLike(String value) {
            addCriterion("AD_City like", value, "adCity");
            return (Criteria) this;
        }

        public Criteria andAdCityNotLike(String value) {
            addCriterion("AD_City not like", value, "adCity");
            return (Criteria) this;
        }

        public Criteria andAdCityIn(List<String> values) {
            addCriterion("AD_City in", values, "adCity");
            return (Criteria) this;
        }

        public Criteria andAdCityNotIn(List<String> values) {
            addCriterion("AD_City not in", values, "adCity");
            return (Criteria) this;
        }

        public Criteria andAdCityBetween(String value1, String value2) {
            addCriterion("AD_City between", value1, value2, "adCity");
            return (Criteria) this;
        }

        public Criteria andAdCityNotBetween(String value1, String value2) {
            addCriterion("AD_City not between", value1, value2, "adCity");
            return (Criteria) this;
        }

        public Criteria andAdPostcodeIsNull() {
            addCriterion("AD_PostCode is null");
            return (Criteria) this;
        }

        public Criteria andAdPostcodeIsNotNull() {
            addCriterion("AD_PostCode is not null");
            return (Criteria) this;
        }

        public Criteria andAdPostcodeEqualTo(String value) {
            addCriterion("AD_PostCode =", value, "adPostcode");
            return (Criteria) this;
        }

        public Criteria andAdPostcodeNotEqualTo(String value) {
            addCriterion("AD_PostCode <>", value, "adPostcode");
            return (Criteria) this;
        }

        public Criteria andAdPostcodeGreaterThan(String value) {
            addCriterion("AD_PostCode >", value, "adPostcode");
            return (Criteria) this;
        }

        public Criteria andAdPostcodeGreaterThanOrEqualTo(String value) {
            addCriterion("AD_PostCode >=", value, "adPostcode");
            return (Criteria) this;
        }

        public Criteria andAdPostcodeLessThan(String value) {
            addCriterion("AD_PostCode <", value, "adPostcode");
            return (Criteria) this;
        }

        public Criteria andAdPostcodeLessThanOrEqualTo(String value) {
            addCriterion("AD_PostCode <=", value, "adPostcode");
            return (Criteria) this;
        }

        public Criteria andAdPostcodeLike(String value) {
            addCriterion("AD_PostCode like", value, "adPostcode");
            return (Criteria) this;
        }

        public Criteria andAdPostcodeNotLike(String value) {
            addCriterion("AD_PostCode not like", value, "adPostcode");
            return (Criteria) this;
        }

        public Criteria andAdPostcodeIn(List<String> values) {
            addCriterion("AD_PostCode in", values, "adPostcode");
            return (Criteria) this;
        }

        public Criteria andAdPostcodeNotIn(List<String> values) {
            addCriterion("AD_PostCode not in", values, "adPostcode");
            return (Criteria) this;
        }

        public Criteria andAdPostcodeBetween(String value1, String value2) {
            addCriterion("AD_PostCode between", value1, value2, "adPostcode");
            return (Criteria) this;
        }

        public Criteria andAdPostcodeNotBetween(String value1, String value2) {
            addCriterion("AD_PostCode not between", value1, value2, "adPostcode");
            return (Criteria) this;
        }

        public Criteria andAdCompanynameIsNull() {
            addCriterion("AD_CompanyName is null");
            return (Criteria) this;
        }

        public Criteria andAdCompanynameIsNotNull() {
            addCriterion("AD_CompanyName is not null");
            return (Criteria) this;
        }

        public Criteria andAdCompanynameEqualTo(String value) {
            addCriterion("AD_CompanyName =", value, "adCompanyname");
            return (Criteria) this;
        }

        public Criteria andAdCompanynameNotEqualTo(String value) {
            addCriterion("AD_CompanyName <>", value, "adCompanyname");
            return (Criteria) this;
        }

        public Criteria andAdCompanynameGreaterThan(String value) {
            addCriterion("AD_CompanyName >", value, "adCompanyname");
            return (Criteria) this;
        }

        public Criteria andAdCompanynameGreaterThanOrEqualTo(String value) {
            addCriterion("AD_CompanyName >=", value, "adCompanyname");
            return (Criteria) this;
        }

        public Criteria andAdCompanynameLessThan(String value) {
            addCriterion("AD_CompanyName <", value, "adCompanyname");
            return (Criteria) this;
        }

        public Criteria andAdCompanynameLessThanOrEqualTo(String value) {
            addCriterion("AD_CompanyName <=", value, "adCompanyname");
            return (Criteria) this;
        }

        public Criteria andAdCompanynameLike(String value) {
            addCriterion("AD_CompanyName like", value, "adCompanyname");
            return (Criteria) this;
        }

        public Criteria andAdCompanynameNotLike(String value) {
            addCriterion("AD_CompanyName not like", value, "adCompanyname");
            return (Criteria) this;
        }

        public Criteria andAdCompanynameIn(List<String> values) {
            addCriterion("AD_CompanyName in", values, "adCompanyname");
            return (Criteria) this;
        }

        public Criteria andAdCompanynameNotIn(List<String> values) {
            addCriterion("AD_CompanyName not in", values, "adCompanyname");
            return (Criteria) this;
        }

        public Criteria andAdCompanynameBetween(String value1, String value2) {
            addCriterion("AD_CompanyName between", value1, value2, "adCompanyname");
            return (Criteria) this;
        }

        public Criteria andAdCompanynameNotBetween(String value1, String value2) {
            addCriterion("AD_CompanyName not between", value1, value2, "adCompanyname");
            return (Criteria) this;
        }

        public Criteria andAdPositionIsNull() {
            addCriterion("AD_Position is null");
            return (Criteria) this;
        }

        public Criteria andAdPositionIsNotNull() {
            addCriterion("AD_Position is not null");
            return (Criteria) this;
        }

        public Criteria andAdPositionEqualTo(String value) {
            addCriterion("AD_Position =", value, "adPosition");
            return (Criteria) this;
        }

        public Criteria andAdPositionNotEqualTo(String value) {
            addCriterion("AD_Position <>", value, "adPosition");
            return (Criteria) this;
        }

        public Criteria andAdPositionGreaterThan(String value) {
            addCriterion("AD_Position >", value, "adPosition");
            return (Criteria) this;
        }

        public Criteria andAdPositionGreaterThanOrEqualTo(String value) {
            addCriterion("AD_Position >=", value, "adPosition");
            return (Criteria) this;
        }

        public Criteria andAdPositionLessThan(String value) {
            addCriterion("AD_Position <", value, "adPosition");
            return (Criteria) this;
        }

        public Criteria andAdPositionLessThanOrEqualTo(String value) {
            addCriterion("AD_Position <=", value, "adPosition");
            return (Criteria) this;
        }

        public Criteria andAdPositionLike(String value) {
            addCriterion("AD_Position like", value, "adPosition");
            return (Criteria) this;
        }

        public Criteria andAdPositionNotLike(String value) {
            addCriterion("AD_Position not like", value, "adPosition");
            return (Criteria) this;
        }

        public Criteria andAdPositionIn(List<String> values) {
            addCriterion("AD_Position in", values, "adPosition");
            return (Criteria) this;
        }

        public Criteria andAdPositionNotIn(List<String> values) {
            addCriterion("AD_Position not in", values, "adPosition");
            return (Criteria) this;
        }

        public Criteria andAdPositionBetween(String value1, String value2) {
            addCriterion("AD_Position between", value1, value2, "adPosition");
            return (Criteria) this;
        }

        public Criteria andAdPositionNotBetween(String value1, String value2) {
            addCriterion("AD_Position not between", value1, value2, "adPosition");
            return (Criteria) this;
        }

        public Criteria andAdCountryIsNull() {
            addCriterion("AD_Country is null");
            return (Criteria) this;
        }

        public Criteria andAdCountryIsNotNull() {
            addCriterion("AD_Country is not null");
            return (Criteria) this;
        }

        public Criteria andAdCountryEqualTo(Integer value) {
            addCriterion("AD_Country =", value, "adCountry");
            return (Criteria) this;
        }

        public Criteria andAdCountryNotEqualTo(Integer value) {
            addCriterion("AD_Country <>", value, "adCountry");
            return (Criteria) this;
        }

        public Criteria andAdCountryGreaterThan(Integer value) {
            addCriterion("AD_Country >", value, "adCountry");
            return (Criteria) this;
        }

        public Criteria andAdCountryGreaterThanOrEqualTo(Integer value) {
            addCriterion("AD_Country >=", value, "adCountry");
            return (Criteria) this;
        }

        public Criteria andAdCountryLessThan(Integer value) {
            addCriterion("AD_Country <", value, "adCountry");
            return (Criteria) this;
        }

        public Criteria andAdCountryLessThanOrEqualTo(Integer value) {
            addCriterion("AD_Country <=", value, "adCountry");
            return (Criteria) this;
        }

        public Criteria andAdCountryIn(List<Integer> values) {
            addCriterion("AD_Country in", values, "adCountry");
            return (Criteria) this;
        }

        public Criteria andAdCountryNotIn(List<Integer> values) {
            addCriterion("AD_Country not in", values, "adCountry");
            return (Criteria) this;
        }

        public Criteria andAdCountryBetween(Integer value1, Integer value2) {
            addCriterion("AD_Country between", value1, value2, "adCountry");
            return (Criteria) this;
        }

        public Criteria andAdCountryNotBetween(Integer value1, Integer value2) {
            addCriterion("AD_Country not between", value1, value2, "adCountry");
            return (Criteria) this;
        }

        public Criteria andAdDetailIsNull() {
            addCriterion("AD_Detail is null");
            return (Criteria) this;
        }

        public Criteria andAdDetailIsNotNull() {
            addCriterion("AD_Detail is not null");
            return (Criteria) this;
        }

        public Criteria andAdDetailEqualTo(String value) {
            addCriterion("AD_Detail =", value, "adDetail");
            return (Criteria) this;
        }

        public Criteria andAdDetailNotEqualTo(String value) {
            addCriterion("AD_Detail <>", value, "adDetail");
            return (Criteria) this;
        }

        public Criteria andAdDetailGreaterThan(String value) {
            addCriterion("AD_Detail >", value, "adDetail");
            return (Criteria) this;
        }

        public Criteria andAdDetailGreaterThanOrEqualTo(String value) {
            addCriterion("AD_Detail >=", value, "adDetail");
            return (Criteria) this;
        }

        public Criteria andAdDetailLessThan(String value) {
            addCriterion("AD_Detail <", value, "adDetail");
            return (Criteria) this;
        }

        public Criteria andAdDetailLessThanOrEqualTo(String value) {
            addCriterion("AD_Detail <=", value, "adDetail");
            return (Criteria) this;
        }

        public Criteria andAdDetailLike(String value) {
            addCriterion("AD_Detail like", value, "adDetail");
            return (Criteria) this;
        }

        public Criteria andAdDetailNotLike(String value) {
            addCriterion("AD_Detail not like", value, "adDetail");
            return (Criteria) this;
        }

        public Criteria andAdDetailIn(List<String> values) {
            addCriterion("AD_Detail in", values, "adDetail");
            return (Criteria) this;
        }

        public Criteria andAdDetailNotIn(List<String> values) {
            addCriterion("AD_Detail not in", values, "adDetail");
            return (Criteria) this;
        }

        public Criteria andAdDetailBetween(String value1, String value2) {
            addCriterion("AD_Detail between", value1, value2, "adDetail");
            return (Criteria) this;
        }

        public Criteria andAdDetailNotBetween(String value1, String value2) {
            addCriterion("AD_Detail not between", value1, value2, "adDetail");
            return (Criteria) this;
        }

        public Criteria andAdTelIsNull() {
            addCriterion("AD_Tel is null");
            return (Criteria) this;
        }

        public Criteria andAdTelIsNotNull() {
            addCriterion("AD_Tel is not null");
            return (Criteria) this;
        }

        public Criteria andAdTelEqualTo(String value) {
            addCriterion("AD_Tel =", value, "adTel");
            return (Criteria) this;
        }

        public Criteria andAdTelNotEqualTo(String value) {
            addCriterion("AD_Tel <>", value, "adTel");
            return (Criteria) this;
        }

        public Criteria andAdTelGreaterThan(String value) {
            addCriterion("AD_Tel >", value, "adTel");
            return (Criteria) this;
        }

        public Criteria andAdTelGreaterThanOrEqualTo(String value) {
            addCriterion("AD_Tel >=", value, "adTel");
            return (Criteria) this;
        }

        public Criteria andAdTelLessThan(String value) {
            addCriterion("AD_Tel <", value, "adTel");
            return (Criteria) this;
        }

        public Criteria andAdTelLessThanOrEqualTo(String value) {
            addCriterion("AD_Tel <=", value, "adTel");
            return (Criteria) this;
        }

        public Criteria andAdTelLike(String value) {
            addCriterion("AD_Tel like", value, "adTel");
            return (Criteria) this;
        }

        public Criteria andAdTelNotLike(String value) {
            addCriterion("AD_Tel not like", value, "adTel");
            return (Criteria) this;
        }

        public Criteria andAdTelIn(List<String> values) {
            addCriterion("AD_Tel in", values, "adTel");
            return (Criteria) this;
        }

        public Criteria andAdTelNotIn(List<String> values) {
            addCriterion("AD_Tel not in", values, "adTel");
            return (Criteria) this;
        }

        public Criteria andAdTelBetween(String value1, String value2) {
            addCriterion("AD_Tel between", value1, value2, "adTel");
            return (Criteria) this;
        }

        public Criteria andAdTelNotBetween(String value1, String value2) {
            addCriterion("AD_Tel not between", value1, value2, "adTel");
            return (Criteria) this;
        }

        public Criteria andAdJobIsNull() {
            addCriterion("AD_Job is null");
            return (Criteria) this;
        }

        public Criteria andAdJobIsNotNull() {
            addCriterion("AD_Job is not null");
            return (Criteria) this;
        }

        public Criteria andAdJobEqualTo(String value) {
            addCriterion("AD_Job =", value, "adJob");
            return (Criteria) this;
        }

        public Criteria andAdJobNotEqualTo(String value) {
            addCriterion("AD_Job <>", value, "adJob");
            return (Criteria) this;
        }

        public Criteria andAdJobGreaterThan(String value) {
            addCriterion("AD_Job >", value, "adJob");
            return (Criteria) this;
        }

        public Criteria andAdJobGreaterThanOrEqualTo(String value) {
            addCriterion("AD_Job >=", value, "adJob");
            return (Criteria) this;
        }

        public Criteria andAdJobLessThan(String value) {
            addCriterion("AD_Job <", value, "adJob");
            return (Criteria) this;
        }

        public Criteria andAdJobLessThanOrEqualTo(String value) {
            addCriterion("AD_Job <=", value, "adJob");
            return (Criteria) this;
        }

        public Criteria andAdJobLike(String value) {
            addCriterion("AD_Job like", value, "adJob");
            return (Criteria) this;
        }

        public Criteria andAdJobNotLike(String value) {
            addCriterion("AD_Job not like", value, "adJob");
            return (Criteria) this;
        }

        public Criteria andAdJobIn(List<String> values) {
            addCriterion("AD_Job in", values, "adJob");
            return (Criteria) this;
        }

        public Criteria andAdJobNotIn(List<String> values) {
            addCriterion("AD_Job not in", values, "adJob");
            return (Criteria) this;
        }

        public Criteria andAdJobBetween(String value1, String value2) {
            addCriterion("AD_Job between", value1, value2, "adJob");
            return (Criteria) this;
        }

        public Criteria andAdJobNotBetween(String value1, String value2) {
            addCriterion("AD_Job not between", value1, value2, "adJob");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}