package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Memberbeneficiary extends BaseEntity {


    private String mbFpcardno;

    private Integer mbSextype;

    private Date mbBirthday;

    private String mbName;

    private String mbSecondname;

    private String mbFirstname;

    private String mbSecondnamepinyin;

    private String mbFirstnamepinyin;

    private Date mbEffectstarttime;

    private Long mbFptid;

    private Integer mbFoidtype;

    private String mbFoid;

    private String mbEmail;

    private Date mbCreatedate;



    public String getMbFpcardno() {
        return mbFpcardno;
    }

    public void setMbFpcardno(String mbFpcardno) {
        this.mbFpcardno = mbFpcardno == null ? null : mbFpcardno.trim();
    }

    public Integer getMbSextype() {
        return mbSextype;
    }

    public void setMbSextype(Integer mbSextype) {
        this.mbSextype = mbSextype;
    }

    public Date getMbBirthday() {
        return mbBirthday;
    }

    public void setMbBirthday(Date mbBirthday) {
        this.mbBirthday = mbBirthday;
    }

    public String getMbName() {
        return mbName;
    }

    public void setMbName(String mbName) {
        this.mbName = mbName == null ? null : mbName.trim();
    }

    public String getMbSecondname() {
        return mbSecondname;
    }

    public void setMbSecondname(String mbSecondname) {
        this.mbSecondname = mbSecondname == null ? null : mbSecondname.trim();
    }

    public String getMbFirstname() {
        return mbFirstname;
    }

    public void setMbFirstname(String mbFirstname) {
        this.mbFirstname = mbFirstname == null ? null : mbFirstname.trim();
    }

    public String getMbSecondnamepinyin() {
        return mbSecondnamepinyin;
    }

    public void setMbSecondnamepinyin(String mbSecondnamepinyin) {
        this.mbSecondnamepinyin = mbSecondnamepinyin == null ? null : mbSecondnamepinyin.trim();
    }

    public String getMbFirstnamepinyin() {
        return mbFirstnamepinyin;
    }

    public void setMbFirstnamepinyin(String mbFirstnamepinyin) {
        this.mbFirstnamepinyin = mbFirstnamepinyin == null ? null : mbFirstnamepinyin.trim();
    }

    public Date getMbEffectstarttime() {
        return mbEffectstarttime;
    }

    public void setMbEffectstarttime(Date mbEffectstarttime) {
        this.mbEffectstarttime = mbEffectstarttime;
    }

    public Long getMbFptid() {
        return mbFptid;
    }

    public void setMbFptid(Long mbFptid) {
        this.mbFptid = mbFptid;
    }

    public Integer getMbFoidtype() {
        return mbFoidtype;
    }

    public void setMbFoidtype(Integer mbFoidtype) {
        this.mbFoidtype = mbFoidtype;
    }

    public String getMbFoid() {
        return mbFoid;
    }

    public void setMbFoid(String mbFoid) {
        this.mbFoid = mbFoid == null ? null : mbFoid.trim();
    }

    public String getMbEmail() {
        return mbEmail;
    }

    public void setMbEmail(String mbEmail) {
        this.mbEmail = mbEmail == null ? null : mbEmail.trim();
    }

    public Date getMbCreatedate() {
        return mbCreatedate;
    }

    public void setMbCreatedate(Date mbCreatedate) {
        this.mbCreatedate = mbCreatedate;
    }
}