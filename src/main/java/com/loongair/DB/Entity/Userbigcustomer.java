package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Userbigcustomer extends BaseEntity {


    private Long ubcLausertid;

    private String ubcFirstnamecn;

    private String ubcSecondnamecn;

    private String ubcFirstnameen;

    private String ubcSecondnameen;



    public Long getUbcLausertid() {
        return ubcLausertid;
    }

    public void setUbcLausertid(Long ubcLausertid) {
        this.ubcLausertid = ubcLausertid;
    }

    public String getUbcFirstnamecn() {
        return ubcFirstnamecn;
    }

    public void setUbcFirstnamecn(String ubcFirstnamecn) {
        this.ubcFirstnamecn = ubcFirstnamecn == null ? null : ubcFirstnamecn.trim();
    }

    public String getUbcSecondnamecn() {
        return ubcSecondnamecn;
    }

    public void setUbcSecondnamecn(String ubcSecondnamecn) {
        this.ubcSecondnamecn = ubcSecondnamecn == null ? null : ubcSecondnamecn.trim();
    }

    public String getUbcFirstnameen() {
        return ubcFirstnameen;
    }

    public void setUbcFirstnameen(String ubcFirstnameen) {
        this.ubcFirstnameen = ubcFirstnameen == null ? null : ubcFirstnameen.trim();
    }

    public String getUbcSecondnameen() {
        return ubcSecondnameen;
    }

    public void setUbcSecondnameen(String ubcSecondnameen) {
        this.ubcSecondnameen = ubcSecondnameen == null ? null : ubcSecondnameen.trim();
    }
}