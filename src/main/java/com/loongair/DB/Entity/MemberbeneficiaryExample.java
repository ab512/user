package com.loongair.DB.Entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MemberbeneficiaryExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public MemberbeneficiaryExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTidIsNull() {
            addCriterion("Tid is null");
            return (Criteria) this;
        }

        public Criteria andTidIsNotNull() {
            addCriterion("Tid is not null");
            return (Criteria) this;
        }

        public Criteria andTidEqualTo(Long value) {
            addCriterion("Tid =", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotEqualTo(Long value) {
            addCriterion("Tid <>", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThan(Long value) {
            addCriterion("Tid >", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThanOrEqualTo(Long value) {
            addCriterion("Tid >=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThan(Long value) {
            addCriterion("Tid <", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThanOrEqualTo(Long value) {
            addCriterion("Tid <=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidIn(List<Long> values) {
            addCriterion("Tid in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotIn(List<Long> values) {
            addCriterion("Tid not in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidBetween(Long value1, Long value2) {
            addCriterion("Tid between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotBetween(Long value1, Long value2) {
            addCriterion("Tid not between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNull() {
            addCriterion("CreateId is null");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNotNull() {
            addCriterion("CreateId is not null");
            return (Criteria) this;
        }

        public Criteria andCreateidEqualTo(Long value) {
            addCriterion("CreateId =", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotEqualTo(Long value) {
            addCriterion("CreateId <>", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThan(Long value) {
            addCriterion("CreateId >", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThanOrEqualTo(Long value) {
            addCriterion("CreateId >=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThan(Long value) {
            addCriterion("CreateId <", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThanOrEqualTo(Long value) {
            addCriterion("CreateId <=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidIn(List<Long> values) {
            addCriterion("CreateId in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotIn(List<Long> values) {
            addCriterion("CreateId not in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidBetween(Long value1, Long value2) {
            addCriterion("CreateId between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotBetween(Long value1, Long value2) {
            addCriterion("CreateId not between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNull() {
            addCriterion("ProxyUserId is null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNotNull() {
            addCriterion("ProxyUserId is not null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridEqualTo(Long value) {
            addCriterion("ProxyUserId =", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotEqualTo(Long value) {
            addCriterion("ProxyUserId <>", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThan(Long value) {
            addCriterion("ProxyUserId >", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId >=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThan(Long value) {
            addCriterion("ProxyUserId <", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId <=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIn(List<Long> values) {
            addCriterion("ProxyUserId in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotIn(List<Long> values) {
            addCriterion("ProxyUserId not in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId not between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNull() {
            addCriterion("LastGuid is null");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNotNull() {
            addCriterion("LastGuid is not null");
            return (Criteria) this;
        }

        public Criteria andLastguidEqualTo(Long value) {
            addCriterion("LastGuid =", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotEqualTo(Long value) {
            addCriterion("LastGuid <>", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThan(Long value) {
            addCriterion("LastGuid >", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThanOrEqualTo(Long value) {
            addCriterion("LastGuid >=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThan(Long value) {
            addCriterion("LastGuid <", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThanOrEqualTo(Long value) {
            addCriterion("LastGuid <=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidIn(List<Long> values) {
            addCriterion("LastGuid in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotIn(List<Long> values) {
            addCriterion("LastGuid not in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidBetween(Long value1, Long value2) {
            addCriterion("LastGuid between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotBetween(Long value1, Long value2) {
            addCriterion("LastGuid not between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andCloneIsNull() {
            addCriterion("Clone is null");
            return (Criteria) this;
        }

        public Criteria andCloneIsNotNull() {
            addCriterion("Clone is not null");
            return (Criteria) this;
        }

        public Criteria andCloneEqualTo(Integer value) {
            addCriterion("Clone =", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotEqualTo(Integer value) {
            addCriterion("Clone <>", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThan(Integer value) {
            addCriterion("Clone >", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThanOrEqualTo(Integer value) {
            addCriterion("Clone >=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThan(Integer value) {
            addCriterion("Clone <", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThanOrEqualTo(Integer value) {
            addCriterion("Clone <=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneIn(List<Integer> values) {
            addCriterion("Clone in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotIn(List<Integer> values) {
            addCriterion("Clone not in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneBetween(Integer value1, Integer value2) {
            addCriterion("Clone between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotBetween(Integer value1, Integer value2) {
            addCriterion("Clone not between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNull() {
            addCriterion("UpdateId is null");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNotNull() {
            addCriterion("UpdateId is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateidEqualTo(Long value) {
            addCriterion("UpdateId =", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotEqualTo(Long value) {
            addCriterion("UpdateId <>", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThan(Long value) {
            addCriterion("UpdateId >", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThanOrEqualTo(Long value) {
            addCriterion("UpdateId >=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThan(Long value) {
            addCriterion("UpdateId <", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThanOrEqualTo(Long value) {
            addCriterion("UpdateId <=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidIn(List<Long> values) {
            addCriterion("UpdateId in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotIn(List<Long> values) {
            addCriterion("UpdateId not in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidBetween(Long value1, Long value2) {
            addCriterion("UpdateId between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotBetween(Long value1, Long value2) {
            addCriterion("UpdateId not between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("UpdateDate is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("UpdateDate is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("UpdateDate =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("UpdateDate <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("UpdateDate >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("UpdateDate >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("UpdateDate <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("UpdateDate <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("UpdateDate in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("UpdateDate not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("UpdateDate between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("UpdateDate not between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("Status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("Status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("Status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("Status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("Status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("Status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("Status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("Status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("Status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("Status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("Status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("Status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andMbFpcardnoIsNull() {
            addCriterion("MB_FPCardNo is null");
            return (Criteria) this;
        }

        public Criteria andMbFpcardnoIsNotNull() {
            addCriterion("MB_FPCardNo is not null");
            return (Criteria) this;
        }

        public Criteria andMbFpcardnoEqualTo(String value) {
            addCriterion("MB_FPCardNo =", value, "mbFpcardno");
            return (Criteria) this;
        }

        public Criteria andMbFpcardnoNotEqualTo(String value) {
            addCriterion("MB_FPCardNo <>", value, "mbFpcardno");
            return (Criteria) this;
        }

        public Criteria andMbFpcardnoGreaterThan(String value) {
            addCriterion("MB_FPCardNo >", value, "mbFpcardno");
            return (Criteria) this;
        }

        public Criteria andMbFpcardnoGreaterThanOrEqualTo(String value) {
            addCriterion("MB_FPCardNo >=", value, "mbFpcardno");
            return (Criteria) this;
        }

        public Criteria andMbFpcardnoLessThan(String value) {
            addCriterion("MB_FPCardNo <", value, "mbFpcardno");
            return (Criteria) this;
        }

        public Criteria andMbFpcardnoLessThanOrEqualTo(String value) {
            addCriterion("MB_FPCardNo <=", value, "mbFpcardno");
            return (Criteria) this;
        }

        public Criteria andMbFpcardnoLike(String value) {
            addCriterion("MB_FPCardNo like", value, "mbFpcardno");
            return (Criteria) this;
        }

        public Criteria andMbFpcardnoNotLike(String value) {
            addCriterion("MB_FPCardNo not like", value, "mbFpcardno");
            return (Criteria) this;
        }

        public Criteria andMbFpcardnoIn(List<String> values) {
            addCriterion("MB_FPCardNo in", values, "mbFpcardno");
            return (Criteria) this;
        }

        public Criteria andMbFpcardnoNotIn(List<String> values) {
            addCriterion("MB_FPCardNo not in", values, "mbFpcardno");
            return (Criteria) this;
        }

        public Criteria andMbFpcardnoBetween(String value1, String value2) {
            addCriterion("MB_FPCardNo between", value1, value2, "mbFpcardno");
            return (Criteria) this;
        }

        public Criteria andMbFpcardnoNotBetween(String value1, String value2) {
            addCriterion("MB_FPCardNo not between", value1, value2, "mbFpcardno");
            return (Criteria) this;
        }

        public Criteria andMbSextypeIsNull() {
            addCriterion("MB_SexType is null");
            return (Criteria) this;
        }

        public Criteria andMbSextypeIsNotNull() {
            addCriterion("MB_SexType is not null");
            return (Criteria) this;
        }

        public Criteria andMbSextypeEqualTo(Integer value) {
            addCriterion("MB_SexType =", value, "mbSextype");
            return (Criteria) this;
        }

        public Criteria andMbSextypeNotEqualTo(Integer value) {
            addCriterion("MB_SexType <>", value, "mbSextype");
            return (Criteria) this;
        }

        public Criteria andMbSextypeGreaterThan(Integer value) {
            addCriterion("MB_SexType >", value, "mbSextype");
            return (Criteria) this;
        }

        public Criteria andMbSextypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("MB_SexType >=", value, "mbSextype");
            return (Criteria) this;
        }

        public Criteria andMbSextypeLessThan(Integer value) {
            addCriterion("MB_SexType <", value, "mbSextype");
            return (Criteria) this;
        }

        public Criteria andMbSextypeLessThanOrEqualTo(Integer value) {
            addCriterion("MB_SexType <=", value, "mbSextype");
            return (Criteria) this;
        }

        public Criteria andMbSextypeIn(List<Integer> values) {
            addCriterion("MB_SexType in", values, "mbSextype");
            return (Criteria) this;
        }

        public Criteria andMbSextypeNotIn(List<Integer> values) {
            addCriterion("MB_SexType not in", values, "mbSextype");
            return (Criteria) this;
        }

        public Criteria andMbSextypeBetween(Integer value1, Integer value2) {
            addCriterion("MB_SexType between", value1, value2, "mbSextype");
            return (Criteria) this;
        }

        public Criteria andMbSextypeNotBetween(Integer value1, Integer value2) {
            addCriterion("MB_SexType not between", value1, value2, "mbSextype");
            return (Criteria) this;
        }

        public Criteria andMbBirthdayIsNull() {
            addCriterion("MB_Birthday is null");
            return (Criteria) this;
        }

        public Criteria andMbBirthdayIsNotNull() {
            addCriterion("MB_Birthday is not null");
            return (Criteria) this;
        }

        public Criteria andMbBirthdayEqualTo(Date value) {
            addCriterion("MB_Birthday =", value, "mbBirthday");
            return (Criteria) this;
        }

        public Criteria andMbBirthdayNotEqualTo(Date value) {
            addCriterion("MB_Birthday <>", value, "mbBirthday");
            return (Criteria) this;
        }

        public Criteria andMbBirthdayGreaterThan(Date value) {
            addCriterion("MB_Birthday >", value, "mbBirthday");
            return (Criteria) this;
        }

        public Criteria andMbBirthdayGreaterThanOrEqualTo(Date value) {
            addCriterion("MB_Birthday >=", value, "mbBirthday");
            return (Criteria) this;
        }

        public Criteria andMbBirthdayLessThan(Date value) {
            addCriterion("MB_Birthday <", value, "mbBirthday");
            return (Criteria) this;
        }

        public Criteria andMbBirthdayLessThanOrEqualTo(Date value) {
            addCriterion("MB_Birthday <=", value, "mbBirthday");
            return (Criteria) this;
        }

        public Criteria andMbBirthdayIn(List<Date> values) {
            addCriterion("MB_Birthday in", values, "mbBirthday");
            return (Criteria) this;
        }

        public Criteria andMbBirthdayNotIn(List<Date> values) {
            addCriterion("MB_Birthday not in", values, "mbBirthday");
            return (Criteria) this;
        }

        public Criteria andMbBirthdayBetween(Date value1, Date value2) {
            addCriterion("MB_Birthday between", value1, value2, "mbBirthday");
            return (Criteria) this;
        }

        public Criteria andMbBirthdayNotBetween(Date value1, Date value2) {
            addCriterion("MB_Birthday not between", value1, value2, "mbBirthday");
            return (Criteria) this;
        }

        public Criteria andMbNameIsNull() {
            addCriterion("MB_Name is null");
            return (Criteria) this;
        }

        public Criteria andMbNameIsNotNull() {
            addCriterion("MB_Name is not null");
            return (Criteria) this;
        }

        public Criteria andMbNameEqualTo(String value) {
            addCriterion("MB_Name =", value, "mbName");
            return (Criteria) this;
        }

        public Criteria andMbNameNotEqualTo(String value) {
            addCriterion("MB_Name <>", value, "mbName");
            return (Criteria) this;
        }

        public Criteria andMbNameGreaterThan(String value) {
            addCriterion("MB_Name >", value, "mbName");
            return (Criteria) this;
        }

        public Criteria andMbNameGreaterThanOrEqualTo(String value) {
            addCriterion("MB_Name >=", value, "mbName");
            return (Criteria) this;
        }

        public Criteria andMbNameLessThan(String value) {
            addCriterion("MB_Name <", value, "mbName");
            return (Criteria) this;
        }

        public Criteria andMbNameLessThanOrEqualTo(String value) {
            addCriterion("MB_Name <=", value, "mbName");
            return (Criteria) this;
        }

        public Criteria andMbNameLike(String value) {
            addCriterion("MB_Name like", value, "mbName");
            return (Criteria) this;
        }

        public Criteria andMbNameNotLike(String value) {
            addCriterion("MB_Name not like", value, "mbName");
            return (Criteria) this;
        }

        public Criteria andMbNameIn(List<String> values) {
            addCriterion("MB_Name in", values, "mbName");
            return (Criteria) this;
        }

        public Criteria andMbNameNotIn(List<String> values) {
            addCriterion("MB_Name not in", values, "mbName");
            return (Criteria) this;
        }

        public Criteria andMbNameBetween(String value1, String value2) {
            addCriterion("MB_Name between", value1, value2, "mbName");
            return (Criteria) this;
        }

        public Criteria andMbNameNotBetween(String value1, String value2) {
            addCriterion("MB_Name not between", value1, value2, "mbName");
            return (Criteria) this;
        }

        public Criteria andMbSecondnameIsNull() {
            addCriterion("MB_SecondName is null");
            return (Criteria) this;
        }

        public Criteria andMbSecondnameIsNotNull() {
            addCriterion("MB_SecondName is not null");
            return (Criteria) this;
        }

        public Criteria andMbSecondnameEqualTo(String value) {
            addCriterion("MB_SecondName =", value, "mbSecondname");
            return (Criteria) this;
        }

        public Criteria andMbSecondnameNotEqualTo(String value) {
            addCriterion("MB_SecondName <>", value, "mbSecondname");
            return (Criteria) this;
        }

        public Criteria andMbSecondnameGreaterThan(String value) {
            addCriterion("MB_SecondName >", value, "mbSecondname");
            return (Criteria) this;
        }

        public Criteria andMbSecondnameGreaterThanOrEqualTo(String value) {
            addCriterion("MB_SecondName >=", value, "mbSecondname");
            return (Criteria) this;
        }

        public Criteria andMbSecondnameLessThan(String value) {
            addCriterion("MB_SecondName <", value, "mbSecondname");
            return (Criteria) this;
        }

        public Criteria andMbSecondnameLessThanOrEqualTo(String value) {
            addCriterion("MB_SecondName <=", value, "mbSecondname");
            return (Criteria) this;
        }

        public Criteria andMbSecondnameLike(String value) {
            addCriterion("MB_SecondName like", value, "mbSecondname");
            return (Criteria) this;
        }

        public Criteria andMbSecondnameNotLike(String value) {
            addCriterion("MB_SecondName not like", value, "mbSecondname");
            return (Criteria) this;
        }

        public Criteria andMbSecondnameIn(List<String> values) {
            addCriterion("MB_SecondName in", values, "mbSecondname");
            return (Criteria) this;
        }

        public Criteria andMbSecondnameNotIn(List<String> values) {
            addCriterion("MB_SecondName not in", values, "mbSecondname");
            return (Criteria) this;
        }

        public Criteria andMbSecondnameBetween(String value1, String value2) {
            addCriterion("MB_SecondName between", value1, value2, "mbSecondname");
            return (Criteria) this;
        }

        public Criteria andMbSecondnameNotBetween(String value1, String value2) {
            addCriterion("MB_SecondName not between", value1, value2, "mbSecondname");
            return (Criteria) this;
        }

        public Criteria andMbFirstnameIsNull() {
            addCriterion("MB_FirstName is null");
            return (Criteria) this;
        }

        public Criteria andMbFirstnameIsNotNull() {
            addCriterion("MB_FirstName is not null");
            return (Criteria) this;
        }

        public Criteria andMbFirstnameEqualTo(String value) {
            addCriterion("MB_FirstName =", value, "mbFirstname");
            return (Criteria) this;
        }

        public Criteria andMbFirstnameNotEqualTo(String value) {
            addCriterion("MB_FirstName <>", value, "mbFirstname");
            return (Criteria) this;
        }

        public Criteria andMbFirstnameGreaterThan(String value) {
            addCriterion("MB_FirstName >", value, "mbFirstname");
            return (Criteria) this;
        }

        public Criteria andMbFirstnameGreaterThanOrEqualTo(String value) {
            addCriterion("MB_FirstName >=", value, "mbFirstname");
            return (Criteria) this;
        }

        public Criteria andMbFirstnameLessThan(String value) {
            addCriterion("MB_FirstName <", value, "mbFirstname");
            return (Criteria) this;
        }

        public Criteria andMbFirstnameLessThanOrEqualTo(String value) {
            addCriterion("MB_FirstName <=", value, "mbFirstname");
            return (Criteria) this;
        }

        public Criteria andMbFirstnameLike(String value) {
            addCriterion("MB_FirstName like", value, "mbFirstname");
            return (Criteria) this;
        }

        public Criteria andMbFirstnameNotLike(String value) {
            addCriterion("MB_FirstName not like", value, "mbFirstname");
            return (Criteria) this;
        }

        public Criteria andMbFirstnameIn(List<String> values) {
            addCriterion("MB_FirstName in", values, "mbFirstname");
            return (Criteria) this;
        }

        public Criteria andMbFirstnameNotIn(List<String> values) {
            addCriterion("MB_FirstName not in", values, "mbFirstname");
            return (Criteria) this;
        }

        public Criteria andMbFirstnameBetween(String value1, String value2) {
            addCriterion("MB_FirstName between", value1, value2, "mbFirstname");
            return (Criteria) this;
        }

        public Criteria andMbFirstnameNotBetween(String value1, String value2) {
            addCriterion("MB_FirstName not between", value1, value2, "mbFirstname");
            return (Criteria) this;
        }

        public Criteria andMbSecondnamepinyinIsNull() {
            addCriterion("MB_SecondNamePinYin is null");
            return (Criteria) this;
        }

        public Criteria andMbSecondnamepinyinIsNotNull() {
            addCriterion("MB_SecondNamePinYin is not null");
            return (Criteria) this;
        }

        public Criteria andMbSecondnamepinyinEqualTo(String value) {
            addCriterion("MB_SecondNamePinYin =", value, "mbSecondnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbSecondnamepinyinNotEqualTo(String value) {
            addCriterion("MB_SecondNamePinYin <>", value, "mbSecondnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbSecondnamepinyinGreaterThan(String value) {
            addCriterion("MB_SecondNamePinYin >", value, "mbSecondnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbSecondnamepinyinGreaterThanOrEqualTo(String value) {
            addCriterion("MB_SecondNamePinYin >=", value, "mbSecondnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbSecondnamepinyinLessThan(String value) {
            addCriterion("MB_SecondNamePinYin <", value, "mbSecondnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbSecondnamepinyinLessThanOrEqualTo(String value) {
            addCriterion("MB_SecondNamePinYin <=", value, "mbSecondnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbSecondnamepinyinLike(String value) {
            addCriterion("MB_SecondNamePinYin like", value, "mbSecondnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbSecondnamepinyinNotLike(String value) {
            addCriterion("MB_SecondNamePinYin not like", value, "mbSecondnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbSecondnamepinyinIn(List<String> values) {
            addCriterion("MB_SecondNamePinYin in", values, "mbSecondnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbSecondnamepinyinNotIn(List<String> values) {
            addCriterion("MB_SecondNamePinYin not in", values, "mbSecondnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbSecondnamepinyinBetween(String value1, String value2) {
            addCriterion("MB_SecondNamePinYin between", value1, value2, "mbSecondnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbSecondnamepinyinNotBetween(String value1, String value2) {
            addCriterion("MB_SecondNamePinYin not between", value1, value2, "mbSecondnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbFirstnamepinyinIsNull() {
            addCriterion("MB_FirstNamePinYin is null");
            return (Criteria) this;
        }

        public Criteria andMbFirstnamepinyinIsNotNull() {
            addCriterion("MB_FirstNamePinYin is not null");
            return (Criteria) this;
        }

        public Criteria andMbFirstnamepinyinEqualTo(String value) {
            addCriterion("MB_FirstNamePinYin =", value, "mbFirstnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbFirstnamepinyinNotEqualTo(String value) {
            addCriterion("MB_FirstNamePinYin <>", value, "mbFirstnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbFirstnamepinyinGreaterThan(String value) {
            addCriterion("MB_FirstNamePinYin >", value, "mbFirstnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbFirstnamepinyinGreaterThanOrEqualTo(String value) {
            addCriterion("MB_FirstNamePinYin >=", value, "mbFirstnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbFirstnamepinyinLessThan(String value) {
            addCriterion("MB_FirstNamePinYin <", value, "mbFirstnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbFirstnamepinyinLessThanOrEqualTo(String value) {
            addCriterion("MB_FirstNamePinYin <=", value, "mbFirstnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbFirstnamepinyinLike(String value) {
            addCriterion("MB_FirstNamePinYin like", value, "mbFirstnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbFirstnamepinyinNotLike(String value) {
            addCriterion("MB_FirstNamePinYin not like", value, "mbFirstnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbFirstnamepinyinIn(List<String> values) {
            addCriterion("MB_FirstNamePinYin in", values, "mbFirstnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbFirstnamepinyinNotIn(List<String> values) {
            addCriterion("MB_FirstNamePinYin not in", values, "mbFirstnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbFirstnamepinyinBetween(String value1, String value2) {
            addCriterion("MB_FirstNamePinYin between", value1, value2, "mbFirstnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbFirstnamepinyinNotBetween(String value1, String value2) {
            addCriterion("MB_FirstNamePinYin not between", value1, value2, "mbFirstnamepinyin");
            return (Criteria) this;
        }

        public Criteria andMbEffectstarttimeIsNull() {
            addCriterion("MB_EffectStartTime is null");
            return (Criteria) this;
        }

        public Criteria andMbEffectstarttimeIsNotNull() {
            addCriterion("MB_EffectStartTime is not null");
            return (Criteria) this;
        }

        public Criteria andMbEffectstarttimeEqualTo(Date value) {
            addCriterion("MB_EffectStartTime =", value, "mbEffectstarttime");
            return (Criteria) this;
        }

        public Criteria andMbEffectstarttimeNotEqualTo(Date value) {
            addCriterion("MB_EffectStartTime <>", value, "mbEffectstarttime");
            return (Criteria) this;
        }

        public Criteria andMbEffectstarttimeGreaterThan(Date value) {
            addCriterion("MB_EffectStartTime >", value, "mbEffectstarttime");
            return (Criteria) this;
        }

        public Criteria andMbEffectstarttimeGreaterThanOrEqualTo(Date value) {
            addCriterion("MB_EffectStartTime >=", value, "mbEffectstarttime");
            return (Criteria) this;
        }

        public Criteria andMbEffectstarttimeLessThan(Date value) {
            addCriterion("MB_EffectStartTime <", value, "mbEffectstarttime");
            return (Criteria) this;
        }

        public Criteria andMbEffectstarttimeLessThanOrEqualTo(Date value) {
            addCriterion("MB_EffectStartTime <=", value, "mbEffectstarttime");
            return (Criteria) this;
        }

        public Criteria andMbEffectstarttimeIn(List<Date> values) {
            addCriterion("MB_EffectStartTime in", values, "mbEffectstarttime");
            return (Criteria) this;
        }

        public Criteria andMbEffectstarttimeNotIn(List<Date> values) {
            addCriterion("MB_EffectStartTime not in", values, "mbEffectstarttime");
            return (Criteria) this;
        }

        public Criteria andMbEffectstarttimeBetween(Date value1, Date value2) {
            addCriterion("MB_EffectStartTime between", value1, value2, "mbEffectstarttime");
            return (Criteria) this;
        }

        public Criteria andMbEffectstarttimeNotBetween(Date value1, Date value2) {
            addCriterion("MB_EffectStartTime not between", value1, value2, "mbEffectstarttime");
            return (Criteria) this;
        }

        public Criteria andMbFptidIsNull() {
            addCriterion("MB_FPTid is null");
            return (Criteria) this;
        }

        public Criteria andMbFptidIsNotNull() {
            addCriterion("MB_FPTid is not null");
            return (Criteria) this;
        }

        public Criteria andMbFptidEqualTo(Long value) {
            addCriterion("MB_FPTid =", value, "mbFptid");
            return (Criteria) this;
        }

        public Criteria andMbFptidNotEqualTo(Long value) {
            addCriterion("MB_FPTid <>", value, "mbFptid");
            return (Criteria) this;
        }

        public Criteria andMbFptidGreaterThan(Long value) {
            addCriterion("MB_FPTid >", value, "mbFptid");
            return (Criteria) this;
        }

        public Criteria andMbFptidGreaterThanOrEqualTo(Long value) {
            addCriterion("MB_FPTid >=", value, "mbFptid");
            return (Criteria) this;
        }

        public Criteria andMbFptidLessThan(Long value) {
            addCriterion("MB_FPTid <", value, "mbFptid");
            return (Criteria) this;
        }

        public Criteria andMbFptidLessThanOrEqualTo(Long value) {
            addCriterion("MB_FPTid <=", value, "mbFptid");
            return (Criteria) this;
        }

        public Criteria andMbFptidIn(List<Long> values) {
            addCriterion("MB_FPTid in", values, "mbFptid");
            return (Criteria) this;
        }

        public Criteria andMbFptidNotIn(List<Long> values) {
            addCriterion("MB_FPTid not in", values, "mbFptid");
            return (Criteria) this;
        }

        public Criteria andMbFptidBetween(Long value1, Long value2) {
            addCriterion("MB_FPTid between", value1, value2, "mbFptid");
            return (Criteria) this;
        }

        public Criteria andMbFptidNotBetween(Long value1, Long value2) {
            addCriterion("MB_FPTid not between", value1, value2, "mbFptid");
            return (Criteria) this;
        }

        public Criteria andMbFoidtypeIsNull() {
            addCriterion("MB_FOIDType is null");
            return (Criteria) this;
        }

        public Criteria andMbFoidtypeIsNotNull() {
            addCriterion("MB_FOIDType is not null");
            return (Criteria) this;
        }

        public Criteria andMbFoidtypeEqualTo(Integer value) {
            addCriterion("MB_FOIDType =", value, "mbFoidtype");
            return (Criteria) this;
        }

        public Criteria andMbFoidtypeNotEqualTo(Integer value) {
            addCriterion("MB_FOIDType <>", value, "mbFoidtype");
            return (Criteria) this;
        }

        public Criteria andMbFoidtypeGreaterThan(Integer value) {
            addCriterion("MB_FOIDType >", value, "mbFoidtype");
            return (Criteria) this;
        }

        public Criteria andMbFoidtypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("MB_FOIDType >=", value, "mbFoidtype");
            return (Criteria) this;
        }

        public Criteria andMbFoidtypeLessThan(Integer value) {
            addCriterion("MB_FOIDType <", value, "mbFoidtype");
            return (Criteria) this;
        }

        public Criteria andMbFoidtypeLessThanOrEqualTo(Integer value) {
            addCriterion("MB_FOIDType <=", value, "mbFoidtype");
            return (Criteria) this;
        }

        public Criteria andMbFoidtypeIn(List<Integer> values) {
            addCriterion("MB_FOIDType in", values, "mbFoidtype");
            return (Criteria) this;
        }

        public Criteria andMbFoidtypeNotIn(List<Integer> values) {
            addCriterion("MB_FOIDType not in", values, "mbFoidtype");
            return (Criteria) this;
        }

        public Criteria andMbFoidtypeBetween(Integer value1, Integer value2) {
            addCriterion("MB_FOIDType between", value1, value2, "mbFoidtype");
            return (Criteria) this;
        }

        public Criteria andMbFoidtypeNotBetween(Integer value1, Integer value2) {
            addCriterion("MB_FOIDType not between", value1, value2, "mbFoidtype");
            return (Criteria) this;
        }

        public Criteria andMbFoidIsNull() {
            addCriterion("MB_FOID is null");
            return (Criteria) this;
        }

        public Criteria andMbFoidIsNotNull() {
            addCriterion("MB_FOID is not null");
            return (Criteria) this;
        }

        public Criteria andMbFoidEqualTo(String value) {
            addCriterion("MB_FOID =", value, "mbFoid");
            return (Criteria) this;
        }

        public Criteria andMbFoidNotEqualTo(String value) {
            addCriterion("MB_FOID <>", value, "mbFoid");
            return (Criteria) this;
        }

        public Criteria andMbFoidGreaterThan(String value) {
            addCriterion("MB_FOID >", value, "mbFoid");
            return (Criteria) this;
        }

        public Criteria andMbFoidGreaterThanOrEqualTo(String value) {
            addCriterion("MB_FOID >=", value, "mbFoid");
            return (Criteria) this;
        }

        public Criteria andMbFoidLessThan(String value) {
            addCriterion("MB_FOID <", value, "mbFoid");
            return (Criteria) this;
        }

        public Criteria andMbFoidLessThanOrEqualTo(String value) {
            addCriterion("MB_FOID <=", value, "mbFoid");
            return (Criteria) this;
        }

        public Criteria andMbFoidLike(String value) {
            addCriterion("MB_FOID like", value, "mbFoid");
            return (Criteria) this;
        }

        public Criteria andMbFoidNotLike(String value) {
            addCriterion("MB_FOID not like", value, "mbFoid");
            return (Criteria) this;
        }

        public Criteria andMbFoidIn(List<String> values) {
            addCriterion("MB_FOID in", values, "mbFoid");
            return (Criteria) this;
        }

        public Criteria andMbFoidNotIn(List<String> values) {
            addCriterion("MB_FOID not in", values, "mbFoid");
            return (Criteria) this;
        }

        public Criteria andMbFoidBetween(String value1, String value2) {
            addCriterion("MB_FOID between", value1, value2, "mbFoid");
            return (Criteria) this;
        }

        public Criteria andMbFoidNotBetween(String value1, String value2) {
            addCriterion("MB_FOID not between", value1, value2, "mbFoid");
            return (Criteria) this;
        }

        public Criteria andMbEmailIsNull() {
            addCriterion("MB_Email is null");
            return (Criteria) this;
        }

        public Criteria andMbEmailIsNotNull() {
            addCriterion("MB_Email is not null");
            return (Criteria) this;
        }

        public Criteria andMbEmailEqualTo(String value) {
            addCriterion("MB_Email =", value, "mbEmail");
            return (Criteria) this;
        }

        public Criteria andMbEmailNotEqualTo(String value) {
            addCriterion("MB_Email <>", value, "mbEmail");
            return (Criteria) this;
        }

        public Criteria andMbEmailGreaterThan(String value) {
            addCriterion("MB_Email >", value, "mbEmail");
            return (Criteria) this;
        }

        public Criteria andMbEmailGreaterThanOrEqualTo(String value) {
            addCriterion("MB_Email >=", value, "mbEmail");
            return (Criteria) this;
        }

        public Criteria andMbEmailLessThan(String value) {
            addCriterion("MB_Email <", value, "mbEmail");
            return (Criteria) this;
        }

        public Criteria andMbEmailLessThanOrEqualTo(String value) {
            addCriterion("MB_Email <=", value, "mbEmail");
            return (Criteria) this;
        }

        public Criteria andMbEmailLike(String value) {
            addCriterion("MB_Email like", value, "mbEmail");
            return (Criteria) this;
        }

        public Criteria andMbEmailNotLike(String value) {
            addCriterion("MB_Email not like", value, "mbEmail");
            return (Criteria) this;
        }

        public Criteria andMbEmailIn(List<String> values) {
            addCriterion("MB_Email in", values, "mbEmail");
            return (Criteria) this;
        }

        public Criteria andMbEmailNotIn(List<String> values) {
            addCriterion("MB_Email not in", values, "mbEmail");
            return (Criteria) this;
        }

        public Criteria andMbEmailBetween(String value1, String value2) {
            addCriterion("MB_Email between", value1, value2, "mbEmail");
            return (Criteria) this;
        }

        public Criteria andMbEmailNotBetween(String value1, String value2) {
            addCriterion("MB_Email not between", value1, value2, "mbEmail");
            return (Criteria) this;
        }

        public Criteria andMbCreatedateIsNull() {
            addCriterion("MB_CreateDate is null");
            return (Criteria) this;
        }

        public Criteria andMbCreatedateIsNotNull() {
            addCriterion("MB_CreateDate is not null");
            return (Criteria) this;
        }

        public Criteria andMbCreatedateEqualTo(Date value) {
            addCriterion("MB_CreateDate =", value, "mbCreatedate");
            return (Criteria) this;
        }

        public Criteria andMbCreatedateNotEqualTo(Date value) {
            addCriterion("MB_CreateDate <>", value, "mbCreatedate");
            return (Criteria) this;
        }

        public Criteria andMbCreatedateGreaterThan(Date value) {
            addCriterion("MB_CreateDate >", value, "mbCreatedate");
            return (Criteria) this;
        }

        public Criteria andMbCreatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("MB_CreateDate >=", value, "mbCreatedate");
            return (Criteria) this;
        }

        public Criteria andMbCreatedateLessThan(Date value) {
            addCriterion("MB_CreateDate <", value, "mbCreatedate");
            return (Criteria) this;
        }

        public Criteria andMbCreatedateLessThanOrEqualTo(Date value) {
            addCriterion("MB_CreateDate <=", value, "mbCreatedate");
            return (Criteria) this;
        }

        public Criteria andMbCreatedateIn(List<Date> values) {
            addCriterion("MB_CreateDate in", values, "mbCreatedate");
            return (Criteria) this;
        }

        public Criteria andMbCreatedateNotIn(List<Date> values) {
            addCriterion("MB_CreateDate not in", values, "mbCreatedate");
            return (Criteria) this;
        }

        public Criteria andMbCreatedateBetween(Date value1, Date value2) {
            addCriterion("MB_CreateDate between", value1, value2, "mbCreatedate");
            return (Criteria) this;
        }

        public Criteria andMbCreatedateNotBetween(Date value1, Date value2) {
            addCriterion("MB_CreateDate not between", value1, value2, "mbCreatedate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}