package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Bcsidinfo extends BaseEntity {


    private Long bsiStafftid;

    private String bsiId;

    private Integer bsiType;


    public Long getBsiStafftid() {
        return bsiStafftid;
    }

    public void setBsiStafftid(Long bsiStafftid) {
        this.bsiStafftid = bsiStafftid;
    }

    public String getBsiId() {
        return bsiId;
    }

    public void setBsiId(String bsiId) {
        this.bsiId = bsiId == null ? null : bsiId.trim();
    }

    public Integer getBsiType() {
        return bsiType;
    }

    public void setBsiType(Integer bsiType) {
        this.bsiType = bsiType;
    }
}