package com.loongair.DB.Entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DepositrecordExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public DepositrecordExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTidIsNull() {
            addCriterion("Tid is null");
            return (Criteria) this;
        }

        public Criteria andTidIsNotNull() {
            addCriterion("Tid is not null");
            return (Criteria) this;
        }

        public Criteria andTidEqualTo(Long value) {
            addCriterion("Tid =", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotEqualTo(Long value) {
            addCriterion("Tid <>", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThan(Long value) {
            addCriterion("Tid >", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThanOrEqualTo(Long value) {
            addCriterion("Tid >=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThan(Long value) {
            addCriterion("Tid <", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThanOrEqualTo(Long value) {
            addCriterion("Tid <=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidIn(List<Long> values) {
            addCriterion("Tid in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotIn(List<Long> values) {
            addCriterion("Tid not in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidBetween(Long value1, Long value2) {
            addCriterion("Tid between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotBetween(Long value1, Long value2) {
            addCriterion("Tid not between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNull() {
            addCriterion("CreateId is null");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNotNull() {
            addCriterion("CreateId is not null");
            return (Criteria) this;
        }

        public Criteria andCreateidEqualTo(Long value) {
            addCriterion("CreateId =", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotEqualTo(Long value) {
            addCriterion("CreateId <>", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThan(Long value) {
            addCriterion("CreateId >", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThanOrEqualTo(Long value) {
            addCriterion("CreateId >=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThan(Long value) {
            addCriterion("CreateId <", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThanOrEqualTo(Long value) {
            addCriterion("CreateId <=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidIn(List<Long> values) {
            addCriterion("CreateId in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotIn(List<Long> values) {
            addCriterion("CreateId not in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidBetween(Long value1, Long value2) {
            addCriterion("CreateId between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotBetween(Long value1, Long value2) {
            addCriterion("CreateId not between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNull() {
            addCriterion("ProxyUserId is null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNotNull() {
            addCriterion("ProxyUserId is not null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridEqualTo(Long value) {
            addCriterion("ProxyUserId =", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotEqualTo(Long value) {
            addCriterion("ProxyUserId <>", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThan(Long value) {
            addCriterion("ProxyUserId >", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId >=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThan(Long value) {
            addCriterion("ProxyUserId <", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId <=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIn(List<Long> values) {
            addCriterion("ProxyUserId in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotIn(List<Long> values) {
            addCriterion("ProxyUserId not in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId not between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNull() {
            addCriterion("LastGuid is null");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNotNull() {
            addCriterion("LastGuid is not null");
            return (Criteria) this;
        }

        public Criteria andLastguidEqualTo(Long value) {
            addCriterion("LastGuid =", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotEqualTo(Long value) {
            addCriterion("LastGuid <>", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThan(Long value) {
            addCriterion("LastGuid >", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThanOrEqualTo(Long value) {
            addCriterion("LastGuid >=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThan(Long value) {
            addCriterion("LastGuid <", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThanOrEqualTo(Long value) {
            addCriterion("LastGuid <=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidIn(List<Long> values) {
            addCriterion("LastGuid in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotIn(List<Long> values) {
            addCriterion("LastGuid not in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidBetween(Long value1, Long value2) {
            addCriterion("LastGuid between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotBetween(Long value1, Long value2) {
            addCriterion("LastGuid not between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andCloneIsNull() {
            addCriterion("Clone is null");
            return (Criteria) this;
        }

        public Criteria andCloneIsNotNull() {
            addCriterion("Clone is not null");
            return (Criteria) this;
        }

        public Criteria andCloneEqualTo(Integer value) {
            addCriterion("Clone =", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotEqualTo(Integer value) {
            addCriterion("Clone <>", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThan(Integer value) {
            addCriterion("Clone >", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThanOrEqualTo(Integer value) {
            addCriterion("Clone >=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThan(Integer value) {
            addCriterion("Clone <", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThanOrEqualTo(Integer value) {
            addCriterion("Clone <=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneIn(List<Integer> values) {
            addCriterion("Clone in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotIn(List<Integer> values) {
            addCriterion("Clone not in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneBetween(Integer value1, Integer value2) {
            addCriterion("Clone between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotBetween(Integer value1, Integer value2) {
            addCriterion("Clone not between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNull() {
            addCriterion("UpdateId is null");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNotNull() {
            addCriterion("UpdateId is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateidEqualTo(Long value) {
            addCriterion("UpdateId =", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotEqualTo(Long value) {
            addCriterion("UpdateId <>", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThan(Long value) {
            addCriterion("UpdateId >", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThanOrEqualTo(Long value) {
            addCriterion("UpdateId >=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThan(Long value) {
            addCriterion("UpdateId <", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThanOrEqualTo(Long value) {
            addCriterion("UpdateId <=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidIn(List<Long> values) {
            addCriterion("UpdateId in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotIn(List<Long> values) {
            addCriterion("UpdateId not in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidBetween(Long value1, Long value2) {
            addCriterion("UpdateId between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotBetween(Long value1, Long value2) {
            addCriterion("UpdateId not between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("UpdateDate is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("UpdateDate is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("UpdateDate =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("UpdateDate <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("UpdateDate >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("UpdateDate >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("UpdateDate <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("UpdateDate <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("UpdateDate in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("UpdateDate not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("UpdateDate between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("UpdateDate not between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("Status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("Status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("Status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("Status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("Status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("Status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("Status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("Status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("Status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("Status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("Status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("Status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andPrLaorgtidIsNull() {
            addCriterion("PR_LaOrgTid is null");
            return (Criteria) this;
        }

        public Criteria andPrLaorgtidIsNotNull() {
            addCriterion("PR_LaOrgTid is not null");
            return (Criteria) this;
        }

        public Criteria andPrLaorgtidEqualTo(Long value) {
            addCriterion("PR_LaOrgTid =", value, "prLaorgtid");
            return (Criteria) this;
        }

        public Criteria andPrLaorgtidNotEqualTo(Long value) {
            addCriterion("PR_LaOrgTid <>", value, "prLaorgtid");
            return (Criteria) this;
        }

        public Criteria andPrLaorgtidGreaterThan(Long value) {
            addCriterion("PR_LaOrgTid >", value, "prLaorgtid");
            return (Criteria) this;
        }

        public Criteria andPrLaorgtidGreaterThanOrEqualTo(Long value) {
            addCriterion("PR_LaOrgTid >=", value, "prLaorgtid");
            return (Criteria) this;
        }

        public Criteria andPrLaorgtidLessThan(Long value) {
            addCriterion("PR_LaOrgTid <", value, "prLaorgtid");
            return (Criteria) this;
        }

        public Criteria andPrLaorgtidLessThanOrEqualTo(Long value) {
            addCriterion("PR_LaOrgTid <=", value, "prLaorgtid");
            return (Criteria) this;
        }

        public Criteria andPrLaorgtidIn(List<Long> values) {
            addCriterion("PR_LaOrgTid in", values, "prLaorgtid");
            return (Criteria) this;
        }

        public Criteria andPrLaorgtidNotIn(List<Long> values) {
            addCriterion("PR_LaOrgTid not in", values, "prLaorgtid");
            return (Criteria) this;
        }

        public Criteria andPrLaorgtidBetween(Long value1, Long value2) {
            addCriterion("PR_LaOrgTid between", value1, value2, "prLaorgtid");
            return (Criteria) this;
        }

        public Criteria andPrLaorgtidNotBetween(Long value1, Long value2) {
            addCriterion("PR_LaOrgTid not between", value1, value2, "prLaorgtid");
            return (Criteria) this;
        }

        public Criteria andPrDepositIsNull() {
            addCriterion("PR_Deposit is null");
            return (Criteria) this;
        }

        public Criteria andPrDepositIsNotNull() {
            addCriterion("PR_Deposit is not null");
            return (Criteria) this;
        }

        public Criteria andPrDepositEqualTo(Float value) {
            addCriterion("PR_Deposit =", value, "prDeposit");
            return (Criteria) this;
        }

        public Criteria andPrDepositNotEqualTo(Float value) {
            addCriterion("PR_Deposit <>", value, "prDeposit");
            return (Criteria) this;
        }

        public Criteria andPrDepositGreaterThan(Float value) {
            addCriterion("PR_Deposit >", value, "prDeposit");
            return (Criteria) this;
        }

        public Criteria andPrDepositGreaterThanOrEqualTo(Float value) {
            addCriterion("PR_Deposit >=", value, "prDeposit");
            return (Criteria) this;
        }

        public Criteria andPrDepositLessThan(Float value) {
            addCriterion("PR_Deposit <", value, "prDeposit");
            return (Criteria) this;
        }

        public Criteria andPrDepositLessThanOrEqualTo(Float value) {
            addCriterion("PR_Deposit <=", value, "prDeposit");
            return (Criteria) this;
        }

        public Criteria andPrDepositIn(List<Float> values) {
            addCriterion("PR_Deposit in", values, "prDeposit");
            return (Criteria) this;
        }

        public Criteria andPrDepositNotIn(List<Float> values) {
            addCriterion("PR_Deposit not in", values, "prDeposit");
            return (Criteria) this;
        }

        public Criteria andPrDepositBetween(Float value1, Float value2) {
            addCriterion("PR_Deposit between", value1, value2, "prDeposit");
            return (Criteria) this;
        }

        public Criteria andPrDepositNotBetween(Float value1, Float value2) {
            addCriterion("PR_Deposit not between", value1, value2, "prDeposit");
            return (Criteria) this;
        }

        public Criteria andPrPaytimeIsNull() {
            addCriterion("PR_PayTime is null");
            return (Criteria) this;
        }

        public Criteria andPrPaytimeIsNotNull() {
            addCriterion("PR_PayTime is not null");
            return (Criteria) this;
        }

        public Criteria andPrPaytimeEqualTo(Date value) {
            addCriterion("PR_PayTime =", value, "prPaytime");
            return (Criteria) this;
        }

        public Criteria andPrPaytimeNotEqualTo(Date value) {
            addCriterion("PR_PayTime <>", value, "prPaytime");
            return (Criteria) this;
        }

        public Criteria andPrPaytimeGreaterThan(Date value) {
            addCriterion("PR_PayTime >", value, "prPaytime");
            return (Criteria) this;
        }

        public Criteria andPrPaytimeGreaterThanOrEqualTo(Date value) {
            addCriterion("PR_PayTime >=", value, "prPaytime");
            return (Criteria) this;
        }

        public Criteria andPrPaytimeLessThan(Date value) {
            addCriterion("PR_PayTime <", value, "prPaytime");
            return (Criteria) this;
        }

        public Criteria andPrPaytimeLessThanOrEqualTo(Date value) {
            addCriterion("PR_PayTime <=", value, "prPaytime");
            return (Criteria) this;
        }

        public Criteria andPrPaytimeIn(List<Date> values) {
            addCriterion("PR_PayTime in", values, "prPaytime");
            return (Criteria) this;
        }

        public Criteria andPrPaytimeNotIn(List<Date> values) {
            addCriterion("PR_PayTime not in", values, "prPaytime");
            return (Criteria) this;
        }

        public Criteria andPrPaytimeBetween(Date value1, Date value2) {
            addCriterion("PR_PayTime between", value1, value2, "prPaytime");
            return (Criteria) this;
        }

        public Criteria andPrPaytimeNotBetween(Date value1, Date value2) {
            addCriterion("PR_PayTime not between", value1, value2, "prPaytime");
            return (Criteria) this;
        }

        public Criteria andPrPaybankIsNull() {
            addCriterion("PR_PayBank is null");
            return (Criteria) this;
        }

        public Criteria andPrPaybankIsNotNull() {
            addCriterion("PR_PayBank is not null");
            return (Criteria) this;
        }

        public Criteria andPrPaybankEqualTo(String value) {
            addCriterion("PR_PayBank =", value, "prPaybank");
            return (Criteria) this;
        }

        public Criteria andPrPaybankNotEqualTo(String value) {
            addCriterion("PR_PayBank <>", value, "prPaybank");
            return (Criteria) this;
        }

        public Criteria andPrPaybankGreaterThan(String value) {
            addCriterion("PR_PayBank >", value, "prPaybank");
            return (Criteria) this;
        }

        public Criteria andPrPaybankGreaterThanOrEqualTo(String value) {
            addCriterion("PR_PayBank >=", value, "prPaybank");
            return (Criteria) this;
        }

        public Criteria andPrPaybankLessThan(String value) {
            addCriterion("PR_PayBank <", value, "prPaybank");
            return (Criteria) this;
        }

        public Criteria andPrPaybankLessThanOrEqualTo(String value) {
            addCriterion("PR_PayBank <=", value, "prPaybank");
            return (Criteria) this;
        }

        public Criteria andPrPaybankLike(String value) {
            addCriterion("PR_PayBank like", value, "prPaybank");
            return (Criteria) this;
        }

        public Criteria andPrPaybankNotLike(String value) {
            addCriterion("PR_PayBank not like", value, "prPaybank");
            return (Criteria) this;
        }

        public Criteria andPrPaybankIn(List<String> values) {
            addCriterion("PR_PayBank in", values, "prPaybank");
            return (Criteria) this;
        }

        public Criteria andPrPaybankNotIn(List<String> values) {
            addCriterion("PR_PayBank not in", values, "prPaybank");
            return (Criteria) this;
        }

        public Criteria andPrPaybankBetween(String value1, String value2) {
            addCriterion("PR_PayBank between", value1, value2, "prPaybank");
            return (Criteria) this;
        }

        public Criteria andPrPaybankNotBetween(String value1, String value2) {
            addCriterion("PR_PayBank not between", value1, value2, "prPaybank");
            return (Criteria) this;
        }

        public Criteria andPrRegardingIsNull() {
            addCriterion("PR_Regarding is null");
            return (Criteria) this;
        }

        public Criteria andPrRegardingIsNotNull() {
            addCriterion("PR_Regarding is not null");
            return (Criteria) this;
        }

        public Criteria andPrRegardingEqualTo(String value) {
            addCriterion("PR_Regarding =", value, "prRegarding");
            return (Criteria) this;
        }

        public Criteria andPrRegardingNotEqualTo(String value) {
            addCriterion("PR_Regarding <>", value, "prRegarding");
            return (Criteria) this;
        }

        public Criteria andPrRegardingGreaterThan(String value) {
            addCriterion("PR_Regarding >", value, "prRegarding");
            return (Criteria) this;
        }

        public Criteria andPrRegardingGreaterThanOrEqualTo(String value) {
            addCriterion("PR_Regarding >=", value, "prRegarding");
            return (Criteria) this;
        }

        public Criteria andPrRegardingLessThan(String value) {
            addCriterion("PR_Regarding <", value, "prRegarding");
            return (Criteria) this;
        }

        public Criteria andPrRegardingLessThanOrEqualTo(String value) {
            addCriterion("PR_Regarding <=", value, "prRegarding");
            return (Criteria) this;
        }

        public Criteria andPrRegardingLike(String value) {
            addCriterion("PR_Regarding like", value, "prRegarding");
            return (Criteria) this;
        }

        public Criteria andPrRegardingNotLike(String value) {
            addCriterion("PR_Regarding not like", value, "prRegarding");
            return (Criteria) this;
        }

        public Criteria andPrRegardingIn(List<String> values) {
            addCriterion("PR_Regarding in", values, "prRegarding");
            return (Criteria) this;
        }

        public Criteria andPrRegardingNotIn(List<String> values) {
            addCriterion("PR_Regarding not in", values, "prRegarding");
            return (Criteria) this;
        }

        public Criteria andPrRegardingBetween(String value1, String value2) {
            addCriterion("PR_Regarding between", value1, value2, "prRegarding");
            return (Criteria) this;
        }

        public Criteria andPrRegardingNotBetween(String value1, String value2) {
            addCriterion("PR_Regarding not between", value1, value2, "prRegarding");
            return (Criteria) this;
        }

        public Criteria andPrBankorderidIsNull() {
            addCriterion("PR_BankOrderId is null");
            return (Criteria) this;
        }

        public Criteria andPrBankorderidIsNotNull() {
            addCriterion("PR_BankOrderId is not null");
            return (Criteria) this;
        }

        public Criteria andPrBankorderidEqualTo(String value) {
            addCriterion("PR_BankOrderId =", value, "prBankorderid");
            return (Criteria) this;
        }

        public Criteria andPrBankorderidNotEqualTo(String value) {
            addCriterion("PR_BankOrderId <>", value, "prBankorderid");
            return (Criteria) this;
        }

        public Criteria andPrBankorderidGreaterThan(String value) {
            addCriterion("PR_BankOrderId >", value, "prBankorderid");
            return (Criteria) this;
        }

        public Criteria andPrBankorderidGreaterThanOrEqualTo(String value) {
            addCriterion("PR_BankOrderId >=", value, "prBankorderid");
            return (Criteria) this;
        }

        public Criteria andPrBankorderidLessThan(String value) {
            addCriterion("PR_BankOrderId <", value, "prBankorderid");
            return (Criteria) this;
        }

        public Criteria andPrBankorderidLessThanOrEqualTo(String value) {
            addCriterion("PR_BankOrderId <=", value, "prBankorderid");
            return (Criteria) this;
        }

        public Criteria andPrBankorderidLike(String value) {
            addCriterion("PR_BankOrderId like", value, "prBankorderid");
            return (Criteria) this;
        }

        public Criteria andPrBankorderidNotLike(String value) {
            addCriterion("PR_BankOrderId not like", value, "prBankorderid");
            return (Criteria) this;
        }

        public Criteria andPrBankorderidIn(List<String> values) {
            addCriterion("PR_BankOrderId in", values, "prBankorderid");
            return (Criteria) this;
        }

        public Criteria andPrBankorderidNotIn(List<String> values) {
            addCriterion("PR_BankOrderId not in", values, "prBankorderid");
            return (Criteria) this;
        }

        public Criteria andPrBankorderidBetween(String value1, String value2) {
            addCriterion("PR_BankOrderId between", value1, value2, "prBankorderid");
            return (Criteria) this;
        }

        public Criteria andPrBankorderidNotBetween(String value1, String value2) {
            addCriterion("PR_BankOrderId not between", value1, value2, "prBankorderid");
            return (Criteria) this;
        }

        public Criteria andPrIsreceiptIsNull() {
            addCriterion("PR_IsReceipt is null");
            return (Criteria) this;
        }

        public Criteria andPrIsreceiptIsNotNull() {
            addCriterion("PR_IsReceipt is not null");
            return (Criteria) this;
        }

        public Criteria andPrIsreceiptEqualTo(Integer value) {
            addCriterion("PR_IsReceipt =", value, "prIsreceipt");
            return (Criteria) this;
        }

        public Criteria andPrIsreceiptNotEqualTo(Integer value) {
            addCriterion("PR_IsReceipt <>", value, "prIsreceipt");
            return (Criteria) this;
        }

        public Criteria andPrIsreceiptGreaterThan(Integer value) {
            addCriterion("PR_IsReceipt >", value, "prIsreceipt");
            return (Criteria) this;
        }

        public Criteria andPrIsreceiptGreaterThanOrEqualTo(Integer value) {
            addCriterion("PR_IsReceipt >=", value, "prIsreceipt");
            return (Criteria) this;
        }

        public Criteria andPrIsreceiptLessThan(Integer value) {
            addCriterion("PR_IsReceipt <", value, "prIsreceipt");
            return (Criteria) this;
        }

        public Criteria andPrIsreceiptLessThanOrEqualTo(Integer value) {
            addCriterion("PR_IsReceipt <=", value, "prIsreceipt");
            return (Criteria) this;
        }

        public Criteria andPrIsreceiptIn(List<Integer> values) {
            addCriterion("PR_IsReceipt in", values, "prIsreceipt");
            return (Criteria) this;
        }

        public Criteria andPrIsreceiptNotIn(List<Integer> values) {
            addCriterion("PR_IsReceipt not in", values, "prIsreceipt");
            return (Criteria) this;
        }

        public Criteria andPrIsreceiptBetween(Integer value1, Integer value2) {
            addCriterion("PR_IsReceipt between", value1, value2, "prIsreceipt");
            return (Criteria) this;
        }

        public Criteria andPrIsreceiptNotBetween(Integer value1, Integer value2) {
            addCriterion("PR_IsReceipt not between", value1, value2, "prIsreceipt");
            return (Criteria) this;
        }

        public Criteria andPrIspaidIsNull() {
            addCriterion("PR_IsPaid is null");
            return (Criteria) this;
        }

        public Criteria andPrIspaidIsNotNull() {
            addCriterion("PR_IsPaid is not null");
            return (Criteria) this;
        }

        public Criteria andPrIspaidEqualTo(Long value) {
            addCriterion("PR_IsPaid =", value, "prIspaid");
            return (Criteria) this;
        }

        public Criteria andPrIspaidNotEqualTo(Long value) {
            addCriterion("PR_IsPaid <>", value, "prIspaid");
            return (Criteria) this;
        }

        public Criteria andPrIspaidGreaterThan(Long value) {
            addCriterion("PR_IsPaid >", value, "prIspaid");
            return (Criteria) this;
        }

        public Criteria andPrIspaidGreaterThanOrEqualTo(Long value) {
            addCriterion("PR_IsPaid >=", value, "prIspaid");
            return (Criteria) this;
        }

        public Criteria andPrIspaidLessThan(Long value) {
            addCriterion("PR_IsPaid <", value, "prIspaid");
            return (Criteria) this;
        }

        public Criteria andPrIspaidLessThanOrEqualTo(Long value) {
            addCriterion("PR_IsPaid <=", value, "prIspaid");
            return (Criteria) this;
        }

        public Criteria andPrIspaidIn(List<Long> values) {
            addCriterion("PR_IsPaid in", values, "prIspaid");
            return (Criteria) this;
        }

        public Criteria andPrIspaidNotIn(List<Long> values) {
            addCriterion("PR_IsPaid not in", values, "prIspaid");
            return (Criteria) this;
        }

        public Criteria andPrIspaidBetween(Long value1, Long value2) {
            addCriterion("PR_IsPaid between", value1, value2, "prIspaid");
            return (Criteria) this;
        }

        public Criteria andPrIspaidNotBetween(Long value1, Long value2) {
            addCriterion("PR_IsPaid not between", value1, value2, "prIspaid");
            return (Criteria) this;
        }

        public Criteria andPrCityIsNull() {
            addCriterion("PR_City is null");
            return (Criteria) this;
        }

        public Criteria andPrCityIsNotNull() {
            addCriterion("PR_City is not null");
            return (Criteria) this;
        }

        public Criteria andPrCityEqualTo(String value) {
            addCriterion("PR_City =", value, "prCity");
            return (Criteria) this;
        }

        public Criteria andPrCityNotEqualTo(String value) {
            addCriterion("PR_City <>", value, "prCity");
            return (Criteria) this;
        }

        public Criteria andPrCityGreaterThan(String value) {
            addCriterion("PR_City >", value, "prCity");
            return (Criteria) this;
        }

        public Criteria andPrCityGreaterThanOrEqualTo(String value) {
            addCriterion("PR_City >=", value, "prCity");
            return (Criteria) this;
        }

        public Criteria andPrCityLessThan(String value) {
            addCriterion("PR_City <", value, "prCity");
            return (Criteria) this;
        }

        public Criteria andPrCityLessThanOrEqualTo(String value) {
            addCriterion("PR_City <=", value, "prCity");
            return (Criteria) this;
        }

        public Criteria andPrCityLike(String value) {
            addCriterion("PR_City like", value, "prCity");
            return (Criteria) this;
        }

        public Criteria andPrCityNotLike(String value) {
            addCriterion("PR_City not like", value, "prCity");
            return (Criteria) this;
        }

        public Criteria andPrCityIn(List<String> values) {
            addCriterion("PR_City in", values, "prCity");
            return (Criteria) this;
        }

        public Criteria andPrCityNotIn(List<String> values) {
            addCriterion("PR_City not in", values, "prCity");
            return (Criteria) this;
        }

        public Criteria andPrCityBetween(String value1, String value2) {
            addCriterion("PR_City between", value1, value2, "prCity");
            return (Criteria) this;
        }

        public Criteria andPrCityNotBetween(String value1, String value2) {
            addCriterion("PR_City not between", value1, value2, "prCity");
            return (Criteria) this;
        }

        public Criteria andPrAreaIsNull() {
            addCriterion("PR_Area is null");
            return (Criteria) this;
        }

        public Criteria andPrAreaIsNotNull() {
            addCriterion("PR_Area is not null");
            return (Criteria) this;
        }

        public Criteria andPrAreaEqualTo(String value) {
            addCriterion("PR_Area =", value, "prArea");
            return (Criteria) this;
        }

        public Criteria andPrAreaNotEqualTo(String value) {
            addCriterion("PR_Area <>", value, "prArea");
            return (Criteria) this;
        }

        public Criteria andPrAreaGreaterThan(String value) {
            addCriterion("PR_Area >", value, "prArea");
            return (Criteria) this;
        }

        public Criteria andPrAreaGreaterThanOrEqualTo(String value) {
            addCriterion("PR_Area >=", value, "prArea");
            return (Criteria) this;
        }

        public Criteria andPrAreaLessThan(String value) {
            addCriterion("PR_Area <", value, "prArea");
            return (Criteria) this;
        }

        public Criteria andPrAreaLessThanOrEqualTo(String value) {
            addCriterion("PR_Area <=", value, "prArea");
            return (Criteria) this;
        }

        public Criteria andPrAreaLike(String value) {
            addCriterion("PR_Area like", value, "prArea");
            return (Criteria) this;
        }

        public Criteria andPrAreaNotLike(String value) {
            addCriterion("PR_Area not like", value, "prArea");
            return (Criteria) this;
        }

        public Criteria andPrAreaIn(List<String> values) {
            addCriterion("PR_Area in", values, "prArea");
            return (Criteria) this;
        }

        public Criteria andPrAreaNotIn(List<String> values) {
            addCriterion("PR_Area not in", values, "prArea");
            return (Criteria) this;
        }

        public Criteria andPrAreaBetween(String value1, String value2) {
            addCriterion("PR_Area between", value1, value2, "prArea");
            return (Criteria) this;
        }

        public Criteria andPrAreaNotBetween(String value1, String value2) {
            addCriterion("PR_Area not between", value1, value2, "prArea");
            return (Criteria) this;
        }

        public Criteria andPrMailaddressIsNull() {
            addCriterion("PR_MailAddress is null");
            return (Criteria) this;
        }

        public Criteria andPrMailaddressIsNotNull() {
            addCriterion("PR_MailAddress is not null");
            return (Criteria) this;
        }

        public Criteria andPrMailaddressEqualTo(String value) {
            addCriterion("PR_MailAddress =", value, "prMailaddress");
            return (Criteria) this;
        }

        public Criteria andPrMailaddressNotEqualTo(String value) {
            addCriterion("PR_MailAddress <>", value, "prMailaddress");
            return (Criteria) this;
        }

        public Criteria andPrMailaddressGreaterThan(String value) {
            addCriterion("PR_MailAddress >", value, "prMailaddress");
            return (Criteria) this;
        }

        public Criteria andPrMailaddressGreaterThanOrEqualTo(String value) {
            addCriterion("PR_MailAddress >=", value, "prMailaddress");
            return (Criteria) this;
        }

        public Criteria andPrMailaddressLessThan(String value) {
            addCriterion("PR_MailAddress <", value, "prMailaddress");
            return (Criteria) this;
        }

        public Criteria andPrMailaddressLessThanOrEqualTo(String value) {
            addCriterion("PR_MailAddress <=", value, "prMailaddress");
            return (Criteria) this;
        }

        public Criteria andPrMailaddressLike(String value) {
            addCriterion("PR_MailAddress like", value, "prMailaddress");
            return (Criteria) this;
        }

        public Criteria andPrMailaddressNotLike(String value) {
            addCriterion("PR_MailAddress not like", value, "prMailaddress");
            return (Criteria) this;
        }

        public Criteria andPrMailaddressIn(List<String> values) {
            addCriterion("PR_MailAddress in", values, "prMailaddress");
            return (Criteria) this;
        }

        public Criteria andPrMailaddressNotIn(List<String> values) {
            addCriterion("PR_MailAddress not in", values, "prMailaddress");
            return (Criteria) this;
        }

        public Criteria andPrMailaddressBetween(String value1, String value2) {
            addCriterion("PR_MailAddress between", value1, value2, "prMailaddress");
            return (Criteria) this;
        }

        public Criteria andPrMailaddressNotBetween(String value1, String value2) {
            addCriterion("PR_MailAddress not between", value1, value2, "prMailaddress");
            return (Criteria) this;
        }

        public Criteria andPrContactsIsNull() {
            addCriterion("PR_Contacts is null");
            return (Criteria) this;
        }

        public Criteria andPrContactsIsNotNull() {
            addCriterion("PR_Contacts is not null");
            return (Criteria) this;
        }

        public Criteria andPrContactsEqualTo(String value) {
            addCriterion("PR_Contacts =", value, "prContacts");
            return (Criteria) this;
        }

        public Criteria andPrContactsNotEqualTo(String value) {
            addCriterion("PR_Contacts <>", value, "prContacts");
            return (Criteria) this;
        }

        public Criteria andPrContactsGreaterThan(String value) {
            addCriterion("PR_Contacts >", value, "prContacts");
            return (Criteria) this;
        }

        public Criteria andPrContactsGreaterThanOrEqualTo(String value) {
            addCriterion("PR_Contacts >=", value, "prContacts");
            return (Criteria) this;
        }

        public Criteria andPrContactsLessThan(String value) {
            addCriterion("PR_Contacts <", value, "prContacts");
            return (Criteria) this;
        }

        public Criteria andPrContactsLessThanOrEqualTo(String value) {
            addCriterion("PR_Contacts <=", value, "prContacts");
            return (Criteria) this;
        }

        public Criteria andPrContactsLike(String value) {
            addCriterion("PR_Contacts like", value, "prContacts");
            return (Criteria) this;
        }

        public Criteria andPrContactsNotLike(String value) {
            addCriterion("PR_Contacts not like", value, "prContacts");
            return (Criteria) this;
        }

        public Criteria andPrContactsIn(List<String> values) {
            addCriterion("PR_Contacts in", values, "prContacts");
            return (Criteria) this;
        }

        public Criteria andPrContactsNotIn(List<String> values) {
            addCriterion("PR_Contacts not in", values, "prContacts");
            return (Criteria) this;
        }

        public Criteria andPrContactsBetween(String value1, String value2) {
            addCriterion("PR_Contacts between", value1, value2, "prContacts");
            return (Criteria) this;
        }

        public Criteria andPrContactsNotBetween(String value1, String value2) {
            addCriterion("PR_Contacts not between", value1, value2, "prContacts");
            return (Criteria) this;
        }

        public Criteria andPrPhoneIsNull() {
            addCriterion("PR_Phone is null");
            return (Criteria) this;
        }

        public Criteria andPrPhoneIsNotNull() {
            addCriterion("PR_Phone is not null");
            return (Criteria) this;
        }

        public Criteria andPrPhoneEqualTo(String value) {
            addCriterion("PR_Phone =", value, "prPhone");
            return (Criteria) this;
        }

        public Criteria andPrPhoneNotEqualTo(String value) {
            addCriterion("PR_Phone <>", value, "prPhone");
            return (Criteria) this;
        }

        public Criteria andPrPhoneGreaterThan(String value) {
            addCriterion("PR_Phone >", value, "prPhone");
            return (Criteria) this;
        }

        public Criteria andPrPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("PR_Phone >=", value, "prPhone");
            return (Criteria) this;
        }

        public Criteria andPrPhoneLessThan(String value) {
            addCriterion("PR_Phone <", value, "prPhone");
            return (Criteria) this;
        }

        public Criteria andPrPhoneLessThanOrEqualTo(String value) {
            addCriterion("PR_Phone <=", value, "prPhone");
            return (Criteria) this;
        }

        public Criteria andPrPhoneLike(String value) {
            addCriterion("PR_Phone like", value, "prPhone");
            return (Criteria) this;
        }

        public Criteria andPrPhoneNotLike(String value) {
            addCriterion("PR_Phone not like", value, "prPhone");
            return (Criteria) this;
        }

        public Criteria andPrPhoneIn(List<String> values) {
            addCriterion("PR_Phone in", values, "prPhone");
            return (Criteria) this;
        }

        public Criteria andPrPhoneNotIn(List<String> values) {
            addCriterion("PR_Phone not in", values, "prPhone");
            return (Criteria) this;
        }

        public Criteria andPrPhoneBetween(String value1, String value2) {
            addCriterion("PR_Phone between", value1, value2, "prPhone");
            return (Criteria) this;
        }

        public Criteria andPrPhoneNotBetween(String value1, String value2) {
            addCriterion("PR_Phone not between", value1, value2, "prPhone");
            return (Criteria) this;
        }

        public Criteria andPrMailtypeIsNull() {
            addCriterion("PR_MailType is null");
            return (Criteria) this;
        }

        public Criteria andPrMailtypeIsNotNull() {
            addCriterion("PR_MailType is not null");
            return (Criteria) this;
        }

        public Criteria andPrMailtypeEqualTo(String value) {
            addCriterion("PR_MailType =", value, "prMailtype");
            return (Criteria) this;
        }

        public Criteria andPrMailtypeNotEqualTo(String value) {
            addCriterion("PR_MailType <>", value, "prMailtype");
            return (Criteria) this;
        }

        public Criteria andPrMailtypeGreaterThan(String value) {
            addCriterion("PR_MailType >", value, "prMailtype");
            return (Criteria) this;
        }

        public Criteria andPrMailtypeGreaterThanOrEqualTo(String value) {
            addCriterion("PR_MailType >=", value, "prMailtype");
            return (Criteria) this;
        }

        public Criteria andPrMailtypeLessThan(String value) {
            addCriterion("PR_MailType <", value, "prMailtype");
            return (Criteria) this;
        }

        public Criteria andPrMailtypeLessThanOrEqualTo(String value) {
            addCriterion("PR_MailType <=", value, "prMailtype");
            return (Criteria) this;
        }

        public Criteria andPrMailtypeLike(String value) {
            addCriterion("PR_MailType like", value, "prMailtype");
            return (Criteria) this;
        }

        public Criteria andPrMailtypeNotLike(String value) {
            addCriterion("PR_MailType not like", value, "prMailtype");
            return (Criteria) this;
        }

        public Criteria andPrMailtypeIn(List<String> values) {
            addCriterion("PR_MailType in", values, "prMailtype");
            return (Criteria) this;
        }

        public Criteria andPrMailtypeNotIn(List<String> values) {
            addCriterion("PR_MailType not in", values, "prMailtype");
            return (Criteria) this;
        }

        public Criteria andPrMailtypeBetween(String value1, String value2) {
            addCriterion("PR_MailType between", value1, value2, "prMailtype");
            return (Criteria) this;
        }

        public Criteria andPrMailtypeNotBetween(String value1, String value2) {
            addCriterion("PR_MailType not between", value1, value2, "prMailtype");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}