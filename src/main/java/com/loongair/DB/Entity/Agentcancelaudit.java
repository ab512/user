package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Agentcancelaudit extends BaseEntity {

    private Long agcaAgentorgtid;

    private Integer agcaIndex;

    private Long agcaAuditortid;

    private String agcaAuditor;

    private Integer agcaAuditret;

    private String agcaNote;



    public Long getAgcaAgentorgtid() {
        return agcaAgentorgtid;
    }

    public void setAgcaAgentorgtid(Long agcaAgentorgtid) {
        this.agcaAgentorgtid = agcaAgentorgtid;
    }

    public Integer getAgcaIndex() {
        return agcaIndex;
    }

    public void setAgcaIndex(Integer agcaIndex) {
        this.agcaIndex = agcaIndex;
    }

    public Long getAgcaAuditortid() {
        return agcaAuditortid;
    }

    public void setAgcaAuditortid(Long agcaAuditortid) {
        this.agcaAuditortid = agcaAuditortid;
    }

    public String getAgcaAuditor() {
        return agcaAuditor;
    }

    public void setAgcaAuditor(String agcaAuditor) {
        this.agcaAuditor = agcaAuditor == null ? null : agcaAuditor.trim();
    }

    public Integer getAgcaAuditret() {
        return agcaAuditret;
    }

    public void setAgcaAuditret(Integer agcaAuditret) {
        this.agcaAuditret = agcaAuditret;
    }

    public String getAgcaNote() {
        return agcaNote;
    }

    public void setAgcaNote(String agcaNote) {
        this.agcaNote = agcaNote == null ? null : agcaNote.trim();
    }
}