package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Grouporgrelation extends BaseEntity {


    private Long gorGrouptid;

    private Long gorOrgtid;



    public Long getGorGrouptid() {
        return gorGrouptid;
    }

    public void setGorGrouptid(Long gorGrouptid) {
        this.gorGrouptid = gorGrouptid;
    }

    public Long getGorOrgtid() {
        return gorOrgtid;
    }

    public void setGorOrgtid(Long gorOrgtid) {
        this.gorOrgtid = gorOrgtid;
    }
}