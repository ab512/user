package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Larole extends BaseEntity {


    private String lrDescription;

    private Integer lrPlat;

    private String lrPermissions;



    public String getLrDescription() {
        return lrDescription;
    }

    public void setLrDescription(String lrDescription) {
        this.lrDescription = lrDescription == null ? null : lrDescription.trim();
    }

    public Integer getLrPlat() {
        return lrPlat;
    }

    public void setLrPlat(Integer lrPlat) {
        this.lrPlat = lrPlat;
    }

    public String getLrPermissions() {
        return lrPermissions;
    }

    public void setLrPermissions(String lrPermissions) {
        this.lrPermissions = lrPermissions == null ? null : lrPermissions.trim();
    }
}