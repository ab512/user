package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Orgbigcustomer extends BaseEntity {


    private Long obcOrgtid;

    private Integer obcAuditstatus;

    private String obcDkhcode;

    private Date obcDateofagreement;

    private Date obcValidperiodend;

    private Long obcAdmintid;

    private Integer obcCustomertype;

    private String obcAddress;

    private String obcEmail;

    private String obcFax;

    private String obcZipcode;

    private String obcLegalperson;

    private String obcManager;

    private String obcContacts;

    private String obcContactphone;

    private String obcContractnumber;

    private String obcProtocolno;

    private Long obcBcrewardintegraltid;

    private Long obcBcrewardcashtid;

    private Long obcBcrewardfreetickettid;

    private Long obcBcrewardservicetid;

    private String obcRecommendedperson;

    private String obcRecommentdejobno;

    private String obcSalesperson;

    private String obcTerminationreason;

    private String obcCardnumstr;

    private String obcOfficecode;

    private String obcLicenseaddress;



    public Long getObcOrgtid() {
        return obcOrgtid;
    }

    public void setObcOrgtid(Long obcOrgtid) {
        this.obcOrgtid = obcOrgtid;
    }

    public Integer getObcAuditstatus() {
        return obcAuditstatus;
    }

    public void setObcAuditstatus(Integer obcAuditstatus) {
        this.obcAuditstatus = obcAuditstatus;
    }

    public String getObcDkhcode() {
        return obcDkhcode;
    }

    public void setObcDkhcode(String obcDkhcode) {
        this.obcDkhcode = obcDkhcode == null ? null : obcDkhcode.trim();
    }

    public Date getObcDateofagreement() {
        return obcDateofagreement;
    }

    public void setObcDateofagreement(Date obcDateofagreement) {
        this.obcDateofagreement = obcDateofagreement;
    }

    public Date getObcValidperiodend() {
        return obcValidperiodend;
    }

    public void setObcValidperiodend(Date obcValidperiodend) {
        this.obcValidperiodend = obcValidperiodend;
    }

    public Long getObcAdmintid() {
        return obcAdmintid;
    }

    public void setObcAdmintid(Long obcAdmintid) {
        this.obcAdmintid = obcAdmintid;
    }

    public Integer getObcCustomertype() {
        return obcCustomertype;
    }

    public void setObcCustomertype(Integer obcCustomertype) {
        this.obcCustomertype = obcCustomertype;
    }

    public String getObcAddress() {
        return obcAddress;
    }

    public void setObcAddress(String obcAddress) {
        this.obcAddress = obcAddress == null ? null : obcAddress.trim();
    }

    public String getObcEmail() {
        return obcEmail;
    }

    public void setObcEmail(String obcEmail) {
        this.obcEmail = obcEmail == null ? null : obcEmail.trim();
    }

    public String getObcFax() {
        return obcFax;
    }

    public void setObcFax(String obcFax) {
        this.obcFax = obcFax == null ? null : obcFax.trim();
    }

    public String getObcZipcode() {
        return obcZipcode;
    }

    public void setObcZipcode(String obcZipcode) {
        this.obcZipcode = obcZipcode == null ? null : obcZipcode.trim();
    }

    public String getObcLegalperson() {
        return obcLegalperson;
    }

    public void setObcLegalperson(String obcLegalperson) {
        this.obcLegalperson = obcLegalperson == null ? null : obcLegalperson.trim();
    }

    public String getObcManager() {
        return obcManager;
    }

    public void setObcManager(String obcManager) {
        this.obcManager = obcManager == null ? null : obcManager.trim();
    }

    public String getObcContacts() {
        return obcContacts;
    }

    public void setObcContacts(String obcContacts) {
        this.obcContacts = obcContacts == null ? null : obcContacts.trim();
    }

    public String getObcContactphone() {
        return obcContactphone;
    }

    public void setObcContactphone(String obcContactphone) {
        this.obcContactphone = obcContactphone == null ? null : obcContactphone.trim();
    }

    public String getObcContractnumber() {
        return obcContractnumber;
    }

    public void setObcContractnumber(String obcContractnumber) {
        this.obcContractnumber = obcContractnumber == null ? null : obcContractnumber.trim();
    }

    public String getObcProtocolno() {
        return obcProtocolno;
    }

    public void setObcProtocolno(String obcProtocolno) {
        this.obcProtocolno = obcProtocolno == null ? null : obcProtocolno.trim();
    }

    public Long getObcBcrewardintegraltid() {
        return obcBcrewardintegraltid;
    }

    public void setObcBcrewardintegraltid(Long obcBcrewardintegraltid) {
        this.obcBcrewardintegraltid = obcBcrewardintegraltid;
    }

    public Long getObcBcrewardcashtid() {
        return obcBcrewardcashtid;
    }

    public void setObcBcrewardcashtid(Long obcBcrewardcashtid) {
        this.obcBcrewardcashtid = obcBcrewardcashtid;
    }

    public Long getObcBcrewardfreetickettid() {
        return obcBcrewardfreetickettid;
    }

    public void setObcBcrewardfreetickettid(Long obcBcrewardfreetickettid) {
        this.obcBcrewardfreetickettid = obcBcrewardfreetickettid;
    }

    public Long getObcBcrewardservicetid() {
        return obcBcrewardservicetid;
    }

    public void setObcBcrewardservicetid(Long obcBcrewardservicetid) {
        this.obcBcrewardservicetid = obcBcrewardservicetid;
    }

    public String getObcRecommendedperson() {
        return obcRecommendedperson;
    }

    public void setObcRecommendedperson(String obcRecommendedperson) {
        this.obcRecommendedperson = obcRecommendedperson == null ? null : obcRecommendedperson.trim();
    }

    public String getObcRecommentdejobno() {
        return obcRecommentdejobno;
    }

    public void setObcRecommentdejobno(String obcRecommentdejobno) {
        this.obcRecommentdejobno = obcRecommentdejobno == null ? null : obcRecommentdejobno.trim();
    }

    public String getObcSalesperson() {
        return obcSalesperson;
    }

    public void setObcSalesperson(String obcSalesperson) {
        this.obcSalesperson = obcSalesperson == null ? null : obcSalesperson.trim();
    }

    public String getObcTerminationreason() {
        return obcTerminationreason;
    }

    public void setObcTerminationreason(String obcTerminationreason) {
        this.obcTerminationreason = obcTerminationreason == null ? null : obcTerminationreason.trim();
    }

    public String getObcCardnumstr() {
        return obcCardnumstr;
    }

    public void setObcCardnumstr(String obcCardnumstr) {
        this.obcCardnumstr = obcCardnumstr == null ? null : obcCardnumstr.trim();
    }

    public String getObcOfficecode() {
        return obcOfficecode;
    }

    public void setObcOfficecode(String obcOfficecode) {
        this.obcOfficecode = obcOfficecode == null ? null : obcOfficecode.trim();
    }

    public String getObcLicenseaddress() {
        return obcLicenseaddress;
    }

    public void setObcLicenseaddress(String obcLicenseaddress) {
        this.obcLicenseaddress = obcLicenseaddress == null ? null : obcLicenseaddress.trim();
    }
}