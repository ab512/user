package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Performance extends BaseEntity {


    private String pfPerformancename;

    private Integer pfSettlementcycle;

    private Date pfEffectivestartdate;

    private Date pfEffectiveenddate;

    private Float pfSaleperformancerate;

    private Float pfRecommandperformancerate;



    public String getPfPerformancename() {
        return pfPerformancename;
    }

    public void setPfPerformancename(String pfPerformancename) {
        this.pfPerformancename = pfPerformancename == null ? null : pfPerformancename.trim();
    }

    public Integer getPfSettlementcycle() {
        return pfSettlementcycle;
    }

    public void setPfSettlementcycle(Integer pfSettlementcycle) {
        this.pfSettlementcycle = pfSettlementcycle;
    }

    public Date getPfEffectivestartdate() {
        return pfEffectivestartdate;
    }

    public void setPfEffectivestartdate(Date pfEffectivestartdate) {
        this.pfEffectivestartdate = pfEffectivestartdate;
    }

    public Date getPfEffectiveenddate() {
        return pfEffectiveenddate;
    }

    public void setPfEffectiveenddate(Date pfEffectiveenddate) {
        this.pfEffectiveenddate = pfEffectiveenddate;
    }

    public Float getPfSaleperformancerate() {
        return pfSaleperformancerate;
    }

    public void setPfSaleperformancerate(Float pfSaleperformancerate) {
        this.pfSaleperformancerate = pfSaleperformancerate;
    }

    public Float getPfRecommandperformancerate() {
        return pfRecommandperformancerate;
    }

    public void setPfRecommandperformancerate(Float pfRecommandperformancerate) {
        this.pfRecommandperformancerate = pfRecommandperformancerate;
    }
}