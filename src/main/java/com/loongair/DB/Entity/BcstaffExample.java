package com.loongair.DB.Entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BcstaffExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BcstaffExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTidIsNull() {
            addCriterion("Tid is null");
            return (Criteria) this;
        }

        public Criteria andTidIsNotNull() {
            addCriterion("Tid is not null");
            return (Criteria) this;
        }

        public Criteria andTidEqualTo(Long value) {
            addCriterion("Tid =", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotEqualTo(Long value) {
            addCriterion("Tid <>", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThan(Long value) {
            addCriterion("Tid >", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThanOrEqualTo(Long value) {
            addCriterion("Tid >=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThan(Long value) {
            addCriterion("Tid <", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThanOrEqualTo(Long value) {
            addCriterion("Tid <=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidIn(List<Long> values) {
            addCriterion("Tid in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotIn(List<Long> values) {
            addCriterion("Tid not in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidBetween(Long value1, Long value2) {
            addCriterion("Tid between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotBetween(Long value1, Long value2) {
            addCriterion("Tid not between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNull() {
            addCriterion("CreateId is null");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNotNull() {
            addCriterion("CreateId is not null");
            return (Criteria) this;
        }

        public Criteria andCreateidEqualTo(Long value) {
            addCriterion("CreateId =", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotEqualTo(Long value) {
            addCriterion("CreateId <>", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThan(Long value) {
            addCriterion("CreateId >", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThanOrEqualTo(Long value) {
            addCriterion("CreateId >=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThan(Long value) {
            addCriterion("CreateId <", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThanOrEqualTo(Long value) {
            addCriterion("CreateId <=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidIn(List<Long> values) {
            addCriterion("CreateId in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotIn(List<Long> values) {
            addCriterion("CreateId not in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidBetween(Long value1, Long value2) {
            addCriterion("CreateId between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotBetween(Long value1, Long value2) {
            addCriterion("CreateId not between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNull() {
            addCriterion("ProxyUserId is null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNotNull() {
            addCriterion("ProxyUserId is not null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridEqualTo(Long value) {
            addCriterion("ProxyUserId =", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotEqualTo(Long value) {
            addCriterion("ProxyUserId <>", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThan(Long value) {
            addCriterion("ProxyUserId >", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId >=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThan(Long value) {
            addCriterion("ProxyUserId <", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId <=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIn(List<Long> values) {
            addCriterion("ProxyUserId in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotIn(List<Long> values) {
            addCriterion("ProxyUserId not in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId not between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNull() {
            addCriterion("LastGuid is null");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNotNull() {
            addCriterion("LastGuid is not null");
            return (Criteria) this;
        }

        public Criteria andLastguidEqualTo(Long value) {
            addCriterion("LastGuid =", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotEqualTo(Long value) {
            addCriterion("LastGuid <>", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThan(Long value) {
            addCriterion("LastGuid >", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThanOrEqualTo(Long value) {
            addCriterion("LastGuid >=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThan(Long value) {
            addCriterion("LastGuid <", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThanOrEqualTo(Long value) {
            addCriterion("LastGuid <=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidIn(List<Long> values) {
            addCriterion("LastGuid in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotIn(List<Long> values) {
            addCriterion("LastGuid not in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidBetween(Long value1, Long value2) {
            addCriterion("LastGuid between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotBetween(Long value1, Long value2) {
            addCriterion("LastGuid not between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andCloneIsNull() {
            addCriterion("Clone is null");
            return (Criteria) this;
        }

        public Criteria andCloneIsNotNull() {
            addCriterion("Clone is not null");
            return (Criteria) this;
        }

        public Criteria andCloneEqualTo(Integer value) {
            addCriterion("Clone =", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotEqualTo(Integer value) {
            addCriterion("Clone <>", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThan(Integer value) {
            addCriterion("Clone >", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThanOrEqualTo(Integer value) {
            addCriterion("Clone >=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThan(Integer value) {
            addCriterion("Clone <", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThanOrEqualTo(Integer value) {
            addCriterion("Clone <=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneIn(List<Integer> values) {
            addCriterion("Clone in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotIn(List<Integer> values) {
            addCriterion("Clone not in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneBetween(Integer value1, Integer value2) {
            addCriterion("Clone between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotBetween(Integer value1, Integer value2) {
            addCriterion("Clone not between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNull() {
            addCriterion("UpdateId is null");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNotNull() {
            addCriterion("UpdateId is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateidEqualTo(Long value) {
            addCriterion("UpdateId =", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotEqualTo(Long value) {
            addCriterion("UpdateId <>", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThan(Long value) {
            addCriterion("UpdateId >", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThanOrEqualTo(Long value) {
            addCriterion("UpdateId >=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThan(Long value) {
            addCriterion("UpdateId <", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThanOrEqualTo(Long value) {
            addCriterion("UpdateId <=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidIn(List<Long> values) {
            addCriterion("UpdateId in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotIn(List<Long> values) {
            addCriterion("UpdateId not in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidBetween(Long value1, Long value2) {
            addCriterion("UpdateId between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotBetween(Long value1, Long value2) {
            addCriterion("UpdateId not between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("UpdateDate is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("UpdateDate is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("UpdateDate =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("UpdateDate <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("UpdateDate >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("UpdateDate >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("UpdateDate <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("UpdateDate <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("UpdateDate in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("UpdateDate not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("UpdateDate between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("UpdateDate not between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("Status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("Status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("Status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("Status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("Status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("Status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("Status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("Status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("Status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("Status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("Status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("Status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andBcsNameIsNull() {
            addCriterion("BCS_Name is null");
            return (Criteria) this;
        }

        public Criteria andBcsNameIsNotNull() {
            addCriterion("BCS_Name is not null");
            return (Criteria) this;
        }

        public Criteria andBcsNameEqualTo(String value) {
            addCriterion("BCS_Name =", value, "bcsName");
            return (Criteria) this;
        }

        public Criteria andBcsNameNotEqualTo(String value) {
            addCriterion("BCS_Name <>", value, "bcsName");
            return (Criteria) this;
        }

        public Criteria andBcsNameGreaterThan(String value) {
            addCriterion("BCS_Name >", value, "bcsName");
            return (Criteria) this;
        }

        public Criteria andBcsNameGreaterThanOrEqualTo(String value) {
            addCriterion("BCS_Name >=", value, "bcsName");
            return (Criteria) this;
        }

        public Criteria andBcsNameLessThan(String value) {
            addCriterion("BCS_Name <", value, "bcsName");
            return (Criteria) this;
        }

        public Criteria andBcsNameLessThanOrEqualTo(String value) {
            addCriterion("BCS_Name <=", value, "bcsName");
            return (Criteria) this;
        }

        public Criteria andBcsNameLike(String value) {
            addCriterion("BCS_Name like", value, "bcsName");
            return (Criteria) this;
        }

        public Criteria andBcsNameNotLike(String value) {
            addCriterion("BCS_Name not like", value, "bcsName");
            return (Criteria) this;
        }

        public Criteria andBcsNameIn(List<String> values) {
            addCriterion("BCS_Name in", values, "bcsName");
            return (Criteria) this;
        }

        public Criteria andBcsNameNotIn(List<String> values) {
            addCriterion("BCS_Name not in", values, "bcsName");
            return (Criteria) this;
        }

        public Criteria andBcsNameBetween(String value1, String value2) {
            addCriterion("BCS_Name between", value1, value2, "bcsName");
            return (Criteria) this;
        }

        public Criteria andBcsNameNotBetween(String value1, String value2) {
            addCriterion("BCS_Name not between", value1, value2, "bcsName");
            return (Criteria) this;
        }

        public Criteria andBcsSextypeIsNull() {
            addCriterion("BCS_SexType is null");
            return (Criteria) this;
        }

        public Criteria andBcsSextypeIsNotNull() {
            addCriterion("BCS_SexType is not null");
            return (Criteria) this;
        }

        public Criteria andBcsSextypeEqualTo(Integer value) {
            addCriterion("BCS_SexType =", value, "bcsSextype");
            return (Criteria) this;
        }

        public Criteria andBcsSextypeNotEqualTo(Integer value) {
            addCriterion("BCS_SexType <>", value, "bcsSextype");
            return (Criteria) this;
        }

        public Criteria andBcsSextypeGreaterThan(Integer value) {
            addCriterion("BCS_SexType >", value, "bcsSextype");
            return (Criteria) this;
        }

        public Criteria andBcsSextypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("BCS_SexType >=", value, "bcsSextype");
            return (Criteria) this;
        }

        public Criteria andBcsSextypeLessThan(Integer value) {
            addCriterion("BCS_SexType <", value, "bcsSextype");
            return (Criteria) this;
        }

        public Criteria andBcsSextypeLessThanOrEqualTo(Integer value) {
            addCriterion("BCS_SexType <=", value, "bcsSextype");
            return (Criteria) this;
        }

        public Criteria andBcsSextypeIn(List<Integer> values) {
            addCriterion("BCS_SexType in", values, "bcsSextype");
            return (Criteria) this;
        }

        public Criteria andBcsSextypeNotIn(List<Integer> values) {
            addCriterion("BCS_SexType not in", values, "bcsSextype");
            return (Criteria) this;
        }

        public Criteria andBcsSextypeBetween(Integer value1, Integer value2) {
            addCriterion("BCS_SexType between", value1, value2, "bcsSextype");
            return (Criteria) this;
        }

        public Criteria andBcsSextypeNotBetween(Integer value1, Integer value2) {
            addCriterion("BCS_SexType not between", value1, value2, "bcsSextype");
            return (Criteria) this;
        }

        public Criteria andBcsPositionIsNull() {
            addCriterion("BCS_Position is null");
            return (Criteria) this;
        }

        public Criteria andBcsPositionIsNotNull() {
            addCriterion("BCS_Position is not null");
            return (Criteria) this;
        }

        public Criteria andBcsPositionEqualTo(String value) {
            addCriterion("BCS_Position =", value, "bcsPosition");
            return (Criteria) this;
        }

        public Criteria andBcsPositionNotEqualTo(String value) {
            addCriterion("BCS_Position <>", value, "bcsPosition");
            return (Criteria) this;
        }

        public Criteria andBcsPositionGreaterThan(String value) {
            addCriterion("BCS_Position >", value, "bcsPosition");
            return (Criteria) this;
        }

        public Criteria andBcsPositionGreaterThanOrEqualTo(String value) {
            addCriterion("BCS_Position >=", value, "bcsPosition");
            return (Criteria) this;
        }

        public Criteria andBcsPositionLessThan(String value) {
            addCriterion("BCS_Position <", value, "bcsPosition");
            return (Criteria) this;
        }

        public Criteria andBcsPositionLessThanOrEqualTo(String value) {
            addCriterion("BCS_Position <=", value, "bcsPosition");
            return (Criteria) this;
        }

        public Criteria andBcsPositionLike(String value) {
            addCriterion("BCS_Position like", value, "bcsPosition");
            return (Criteria) this;
        }

        public Criteria andBcsPositionNotLike(String value) {
            addCriterion("BCS_Position not like", value, "bcsPosition");
            return (Criteria) this;
        }

        public Criteria andBcsPositionIn(List<String> values) {
            addCriterion("BCS_Position in", values, "bcsPosition");
            return (Criteria) this;
        }

        public Criteria andBcsPositionNotIn(List<String> values) {
            addCriterion("BCS_Position not in", values, "bcsPosition");
            return (Criteria) this;
        }

        public Criteria andBcsPositionBetween(String value1, String value2) {
            addCriterion("BCS_Position between", value1, value2, "bcsPosition");
            return (Criteria) this;
        }

        public Criteria andBcsPositionNotBetween(String value1, String value2) {
            addCriterion("BCS_Position not between", value1, value2, "bcsPosition");
            return (Criteria) this;
        }

        public Criteria andBcsPhoneIsNull() {
            addCriterion("BCS_Phone is null");
            return (Criteria) this;
        }

        public Criteria andBcsPhoneIsNotNull() {
            addCriterion("BCS_Phone is not null");
            return (Criteria) this;
        }

        public Criteria andBcsPhoneEqualTo(String value) {
            addCriterion("BCS_Phone =", value, "bcsPhone");
            return (Criteria) this;
        }

        public Criteria andBcsPhoneNotEqualTo(String value) {
            addCriterion("BCS_Phone <>", value, "bcsPhone");
            return (Criteria) this;
        }

        public Criteria andBcsPhoneGreaterThan(String value) {
            addCriterion("BCS_Phone >", value, "bcsPhone");
            return (Criteria) this;
        }

        public Criteria andBcsPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("BCS_Phone >=", value, "bcsPhone");
            return (Criteria) this;
        }

        public Criteria andBcsPhoneLessThan(String value) {
            addCriterion("BCS_Phone <", value, "bcsPhone");
            return (Criteria) this;
        }

        public Criteria andBcsPhoneLessThanOrEqualTo(String value) {
            addCriterion("BCS_Phone <=", value, "bcsPhone");
            return (Criteria) this;
        }

        public Criteria andBcsPhoneLike(String value) {
            addCriterion("BCS_Phone like", value, "bcsPhone");
            return (Criteria) this;
        }

        public Criteria andBcsPhoneNotLike(String value) {
            addCriterion("BCS_Phone not like", value, "bcsPhone");
            return (Criteria) this;
        }

        public Criteria andBcsPhoneIn(List<String> values) {
            addCriterion("BCS_Phone in", values, "bcsPhone");
            return (Criteria) this;
        }

        public Criteria andBcsPhoneNotIn(List<String> values) {
            addCriterion("BCS_Phone not in", values, "bcsPhone");
            return (Criteria) this;
        }

        public Criteria andBcsPhoneBetween(String value1, String value2) {
            addCriterion("BCS_Phone between", value1, value2, "bcsPhone");
            return (Criteria) this;
        }

        public Criteria andBcsPhoneNotBetween(String value1, String value2) {
            addCriterion("BCS_Phone not between", value1, value2, "bcsPhone");
            return (Criteria) this;
        }

        public Criteria andBcsAddressIsNull() {
            addCriterion("BCS_Address is null");
            return (Criteria) this;
        }

        public Criteria andBcsAddressIsNotNull() {
            addCriterion("BCS_Address is not null");
            return (Criteria) this;
        }

        public Criteria andBcsAddressEqualTo(String value) {
            addCriterion("BCS_Address =", value, "bcsAddress");
            return (Criteria) this;
        }

        public Criteria andBcsAddressNotEqualTo(String value) {
            addCriterion("BCS_Address <>", value, "bcsAddress");
            return (Criteria) this;
        }

        public Criteria andBcsAddressGreaterThan(String value) {
            addCriterion("BCS_Address >", value, "bcsAddress");
            return (Criteria) this;
        }

        public Criteria andBcsAddressGreaterThanOrEqualTo(String value) {
            addCriterion("BCS_Address >=", value, "bcsAddress");
            return (Criteria) this;
        }

        public Criteria andBcsAddressLessThan(String value) {
            addCriterion("BCS_Address <", value, "bcsAddress");
            return (Criteria) this;
        }

        public Criteria andBcsAddressLessThanOrEqualTo(String value) {
            addCriterion("BCS_Address <=", value, "bcsAddress");
            return (Criteria) this;
        }

        public Criteria andBcsAddressLike(String value) {
            addCriterion("BCS_Address like", value, "bcsAddress");
            return (Criteria) this;
        }

        public Criteria andBcsAddressNotLike(String value) {
            addCriterion("BCS_Address not like", value, "bcsAddress");
            return (Criteria) this;
        }

        public Criteria andBcsAddressIn(List<String> values) {
            addCriterion("BCS_Address in", values, "bcsAddress");
            return (Criteria) this;
        }

        public Criteria andBcsAddressNotIn(List<String> values) {
            addCriterion("BCS_Address not in", values, "bcsAddress");
            return (Criteria) this;
        }

        public Criteria andBcsAddressBetween(String value1, String value2) {
            addCriterion("BCS_Address between", value1, value2, "bcsAddress");
            return (Criteria) this;
        }

        public Criteria andBcsAddressNotBetween(String value1, String value2) {
            addCriterion("BCS_Address not between", value1, value2, "bcsAddress");
            return (Criteria) this;
        }

        public Criteria andBcsBcorgtidIsNull() {
            addCriterion("BCS_BCOrgTid is null");
            return (Criteria) this;
        }

        public Criteria andBcsBcorgtidIsNotNull() {
            addCriterion("BCS_BCOrgTid is not null");
            return (Criteria) this;
        }

        public Criteria andBcsBcorgtidEqualTo(Long value) {
            addCriterion("BCS_BCOrgTid =", value, "bcsBcorgtid");
            return (Criteria) this;
        }

        public Criteria andBcsBcorgtidNotEqualTo(Long value) {
            addCriterion("BCS_BCOrgTid <>", value, "bcsBcorgtid");
            return (Criteria) this;
        }

        public Criteria andBcsBcorgtidGreaterThan(Long value) {
            addCriterion("BCS_BCOrgTid >", value, "bcsBcorgtid");
            return (Criteria) this;
        }

        public Criteria andBcsBcorgtidGreaterThanOrEqualTo(Long value) {
            addCriterion("BCS_BCOrgTid >=", value, "bcsBcorgtid");
            return (Criteria) this;
        }

        public Criteria andBcsBcorgtidLessThan(Long value) {
            addCriterion("BCS_BCOrgTid <", value, "bcsBcorgtid");
            return (Criteria) this;
        }

        public Criteria andBcsBcorgtidLessThanOrEqualTo(Long value) {
            addCriterion("BCS_BCOrgTid <=", value, "bcsBcorgtid");
            return (Criteria) this;
        }

        public Criteria andBcsBcorgtidIn(List<Long> values) {
            addCriterion("BCS_BCOrgTid in", values, "bcsBcorgtid");
            return (Criteria) this;
        }

        public Criteria andBcsBcorgtidNotIn(List<Long> values) {
            addCriterion("BCS_BCOrgTid not in", values, "bcsBcorgtid");
            return (Criteria) this;
        }

        public Criteria andBcsBcorgtidBetween(Long value1, Long value2) {
            addCriterion("BCS_BCOrgTid between", value1, value2, "bcsBcorgtid");
            return (Criteria) this;
        }

        public Criteria andBcsBcorgtidNotBetween(Long value1, Long value2) {
            addCriterion("BCS_BCOrgTid not between", value1, value2, "bcsBcorgtid");
            return (Criteria) this;
        }

        public Criteria andBcsIsexecutiveIsNull() {
            addCriterion("BCS_IsExecutive is null");
            return (Criteria) this;
        }

        public Criteria andBcsIsexecutiveIsNotNull() {
            addCriterion("BCS_IsExecutive is not null");
            return (Criteria) this;
        }

        public Criteria andBcsIsexecutiveEqualTo(Integer value) {
            addCriterion("BCS_IsExecutive =", value, "bcsIsexecutive");
            return (Criteria) this;
        }

        public Criteria andBcsIsexecutiveNotEqualTo(Integer value) {
            addCriterion("BCS_IsExecutive <>", value, "bcsIsexecutive");
            return (Criteria) this;
        }

        public Criteria andBcsIsexecutiveGreaterThan(Integer value) {
            addCriterion("BCS_IsExecutive >", value, "bcsIsexecutive");
            return (Criteria) this;
        }

        public Criteria andBcsIsexecutiveGreaterThanOrEqualTo(Integer value) {
            addCriterion("BCS_IsExecutive >=", value, "bcsIsexecutive");
            return (Criteria) this;
        }

        public Criteria andBcsIsexecutiveLessThan(Integer value) {
            addCriterion("BCS_IsExecutive <", value, "bcsIsexecutive");
            return (Criteria) this;
        }

        public Criteria andBcsIsexecutiveLessThanOrEqualTo(Integer value) {
            addCriterion("BCS_IsExecutive <=", value, "bcsIsexecutive");
            return (Criteria) this;
        }

        public Criteria andBcsIsexecutiveIn(List<Integer> values) {
            addCriterion("BCS_IsExecutive in", values, "bcsIsexecutive");
            return (Criteria) this;
        }

        public Criteria andBcsIsexecutiveNotIn(List<Integer> values) {
            addCriterion("BCS_IsExecutive not in", values, "bcsIsexecutive");
            return (Criteria) this;
        }

        public Criteria andBcsIsexecutiveBetween(Integer value1, Integer value2) {
            addCriterion("BCS_IsExecutive between", value1, value2, "bcsIsexecutive");
            return (Criteria) this;
        }

        public Criteria andBcsIsexecutiveNotBetween(Integer value1, Integer value2) {
            addCriterion("BCS_IsExecutive not between", value1, value2, "bcsIsexecutive");
            return (Criteria) this;
        }

        public Criteria andBcsLausertidIsNull() {
            addCriterion("BCS_LaUserTid is null");
            return (Criteria) this;
        }

        public Criteria andBcsLausertidIsNotNull() {
            addCriterion("BCS_LaUserTid is not null");
            return (Criteria) this;
        }

        public Criteria andBcsLausertidEqualTo(Long value) {
            addCriterion("BCS_LaUserTid =", value, "bcsLausertid");
            return (Criteria) this;
        }

        public Criteria andBcsLausertidNotEqualTo(Long value) {
            addCriterion("BCS_LaUserTid <>", value, "bcsLausertid");
            return (Criteria) this;
        }

        public Criteria andBcsLausertidGreaterThan(Long value) {
            addCriterion("BCS_LaUserTid >", value, "bcsLausertid");
            return (Criteria) this;
        }

        public Criteria andBcsLausertidGreaterThanOrEqualTo(Long value) {
            addCriterion("BCS_LaUserTid >=", value, "bcsLausertid");
            return (Criteria) this;
        }

        public Criteria andBcsLausertidLessThan(Long value) {
            addCriterion("BCS_LaUserTid <", value, "bcsLausertid");
            return (Criteria) this;
        }

        public Criteria andBcsLausertidLessThanOrEqualTo(Long value) {
            addCriterion("BCS_LaUserTid <=", value, "bcsLausertid");
            return (Criteria) this;
        }

        public Criteria andBcsLausertidIn(List<Long> values) {
            addCriterion("BCS_LaUserTid in", values, "bcsLausertid");
            return (Criteria) this;
        }

        public Criteria andBcsLausertidNotIn(List<Long> values) {
            addCriterion("BCS_LaUserTid not in", values, "bcsLausertid");
            return (Criteria) this;
        }

        public Criteria andBcsLausertidBetween(Long value1, Long value2) {
            addCriterion("BCS_LaUserTid between", value1, value2, "bcsLausertid");
            return (Criteria) this;
        }

        public Criteria andBcsLausertidNotBetween(Long value1, Long value2) {
            addCriterion("BCS_LaUserTid not between", value1, value2, "bcsLausertid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}