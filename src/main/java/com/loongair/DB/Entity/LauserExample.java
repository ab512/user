package com.loongair.DB.Entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LauserExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public LauserExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTidIsNull() {
            addCriterion("Tid is null");
            return (Criteria) this;
        }

        public Criteria andTidIsNotNull() {
            addCriterion("Tid is not null");
            return (Criteria) this;
        }

        public Criteria andTidEqualTo(Long value) {
            addCriterion("Tid =", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotEqualTo(Long value) {
            addCriterion("Tid <>", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThan(Long value) {
            addCriterion("Tid >", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThanOrEqualTo(Long value) {
            addCriterion("Tid >=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThan(Long value) {
            addCriterion("Tid <", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThanOrEqualTo(Long value) {
            addCriterion("Tid <=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidIn(List<Long> values) {
            addCriterion("Tid in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotIn(List<Long> values) {
            addCriterion("Tid not in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidBetween(Long value1, Long value2) {
            addCriterion("Tid between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotBetween(Long value1, Long value2) {
            addCriterion("Tid not between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNull() {
            addCriterion("CreateId is null");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNotNull() {
            addCriterion("CreateId is not null");
            return (Criteria) this;
        }

        public Criteria andCreateidEqualTo(Long value) {
            addCriterion("CreateId =", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotEqualTo(Long value) {
            addCriterion("CreateId <>", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThan(Long value) {
            addCriterion("CreateId >", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThanOrEqualTo(Long value) {
            addCriterion("CreateId >=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThan(Long value) {
            addCriterion("CreateId <", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThanOrEqualTo(Long value) {
            addCriterion("CreateId <=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidIn(List<Long> values) {
            addCriterion("CreateId in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotIn(List<Long> values) {
            addCriterion("CreateId not in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidBetween(Long value1, Long value2) {
            addCriterion("CreateId between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotBetween(Long value1, Long value2) {
            addCriterion("CreateId not between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNull() {
            addCriterion("ProxyUserId is null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNotNull() {
            addCriterion("ProxyUserId is not null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridEqualTo(Long value) {
            addCriterion("ProxyUserId =", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotEqualTo(Long value) {
            addCriterion("ProxyUserId <>", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThan(Long value) {
            addCriterion("ProxyUserId >", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId >=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThan(Long value) {
            addCriterion("ProxyUserId <", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId <=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIn(List<Long> values) {
            addCriterion("ProxyUserId in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotIn(List<Long> values) {
            addCriterion("ProxyUserId not in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId not between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNull() {
            addCriterion("LastGuid is null");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNotNull() {
            addCriterion("LastGuid is not null");
            return (Criteria) this;
        }

        public Criteria andLastguidEqualTo(Long value) {
            addCriterion("LastGuid =", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotEqualTo(Long value) {
            addCriterion("LastGuid <>", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThan(Long value) {
            addCriterion("LastGuid >", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThanOrEqualTo(Long value) {
            addCriterion("LastGuid >=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThan(Long value) {
            addCriterion("LastGuid <", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThanOrEqualTo(Long value) {
            addCriterion("LastGuid <=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidIn(List<Long> values) {
            addCriterion("LastGuid in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotIn(List<Long> values) {
            addCriterion("LastGuid not in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidBetween(Long value1, Long value2) {
            addCriterion("LastGuid between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotBetween(Long value1, Long value2) {
            addCriterion("LastGuid not between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andCloneIsNull() {
            addCriterion("Clone is null");
            return (Criteria) this;
        }

        public Criteria andCloneIsNotNull() {
            addCriterion("Clone is not null");
            return (Criteria) this;
        }

        public Criteria andCloneEqualTo(Integer value) {
            addCriterion("Clone =", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotEqualTo(Integer value) {
            addCriterion("Clone <>", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThan(Integer value) {
            addCriterion("Clone >", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThanOrEqualTo(Integer value) {
            addCriterion("Clone >=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThan(Integer value) {
            addCriterion("Clone <", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThanOrEqualTo(Integer value) {
            addCriterion("Clone <=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneIn(List<Integer> values) {
            addCriterion("Clone in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotIn(List<Integer> values) {
            addCriterion("Clone not in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneBetween(Integer value1, Integer value2) {
            addCriterion("Clone between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotBetween(Integer value1, Integer value2) {
            addCriterion("Clone not between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNull() {
            addCriterion("UpdateId is null");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNotNull() {
            addCriterion("UpdateId is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateidEqualTo(Long value) {
            addCriterion("UpdateId =", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotEqualTo(Long value) {
            addCriterion("UpdateId <>", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThan(Long value) {
            addCriterion("UpdateId >", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThanOrEqualTo(Long value) {
            addCriterion("UpdateId >=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThan(Long value) {
            addCriterion("UpdateId <", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThanOrEqualTo(Long value) {
            addCriterion("UpdateId <=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidIn(List<Long> values) {
            addCriterion("UpdateId in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotIn(List<Long> values) {
            addCriterion("UpdateId not in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidBetween(Long value1, Long value2) {
            addCriterion("UpdateId between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotBetween(Long value1, Long value2) {
            addCriterion("UpdateId not between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("UpdateDate is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("UpdateDate is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("UpdateDate =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("UpdateDate <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("UpdateDate >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("UpdateDate >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("UpdateDate <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("UpdateDate <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("UpdateDate in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("UpdateDate not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("UpdateDate between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("UpdateDate not between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("Status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("Status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("Status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("Status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("Status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("Status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("Status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("Status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("Status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("Status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("Status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("Status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andBuTokenIsNull() {
            addCriterion("BU_Token is null");
            return (Criteria) this;
        }

        public Criteria andBuTokenIsNotNull() {
            addCriterion("BU_Token is not null");
            return (Criteria) this;
        }

        public Criteria andBuTokenEqualTo(String value) {
            addCriterion("BU_Token =", value, "buToken");
            return (Criteria) this;
        }

        public Criteria andBuTokenNotEqualTo(String value) {
            addCriterion("BU_Token <>", value, "buToken");
            return (Criteria) this;
        }

        public Criteria andBuTokenGreaterThan(String value) {
            addCriterion("BU_Token >", value, "buToken");
            return (Criteria) this;
        }

        public Criteria andBuTokenGreaterThanOrEqualTo(String value) {
            addCriterion("BU_Token >=", value, "buToken");
            return (Criteria) this;
        }

        public Criteria andBuTokenLessThan(String value) {
            addCriterion("BU_Token <", value, "buToken");
            return (Criteria) this;
        }

        public Criteria andBuTokenLessThanOrEqualTo(String value) {
            addCriterion("BU_Token <=", value, "buToken");
            return (Criteria) this;
        }

        public Criteria andBuTokenLike(String value) {
            addCriterion("BU_Token like", value, "buToken");
            return (Criteria) this;
        }

        public Criteria andBuTokenNotLike(String value) {
            addCriterion("BU_Token not like", value, "buToken");
            return (Criteria) this;
        }

        public Criteria andBuTokenIn(List<String> values) {
            addCriterion("BU_Token in", values, "buToken");
            return (Criteria) this;
        }

        public Criteria andBuTokenNotIn(List<String> values) {
            addCriterion("BU_Token not in", values, "buToken");
            return (Criteria) this;
        }

        public Criteria andBuTokenBetween(String value1, String value2) {
            addCriterion("BU_Token between", value1, value2, "buToken");
            return (Criteria) this;
        }

        public Criteria andBuTokenNotBetween(String value1, String value2) {
            addCriterion("BU_Token not between", value1, value2, "buToken");
            return (Criteria) this;
        }

        public Criteria andUCompanyidIsNull() {
            addCriterion("U_CompanyId is null");
            return (Criteria) this;
        }

        public Criteria andUCompanyidIsNotNull() {
            addCriterion("U_CompanyId is not null");
            return (Criteria) this;
        }

        public Criteria andUCompanyidEqualTo(Long value) {
            addCriterion("U_CompanyId =", value, "uCompanyid");
            return (Criteria) this;
        }

        public Criteria andUCompanyidNotEqualTo(Long value) {
            addCriterion("U_CompanyId <>", value, "uCompanyid");
            return (Criteria) this;
        }

        public Criteria andUCompanyidGreaterThan(Long value) {
            addCriterion("U_CompanyId >", value, "uCompanyid");
            return (Criteria) this;
        }

        public Criteria andUCompanyidGreaterThanOrEqualTo(Long value) {
            addCriterion("U_CompanyId >=", value, "uCompanyid");
            return (Criteria) this;
        }

        public Criteria andUCompanyidLessThan(Long value) {
            addCriterion("U_CompanyId <", value, "uCompanyid");
            return (Criteria) this;
        }

        public Criteria andUCompanyidLessThanOrEqualTo(Long value) {
            addCriterion("U_CompanyId <=", value, "uCompanyid");
            return (Criteria) this;
        }

        public Criteria andUCompanyidIn(List<Long> values) {
            addCriterion("U_CompanyId in", values, "uCompanyid");
            return (Criteria) this;
        }

        public Criteria andUCompanyidNotIn(List<Long> values) {
            addCriterion("U_CompanyId not in", values, "uCompanyid");
            return (Criteria) this;
        }

        public Criteria andUCompanyidBetween(Long value1, Long value2) {
            addCriterion("U_CompanyId between", value1, value2, "uCompanyid");
            return (Criteria) this;
        }

        public Criteria andUCompanyidNotBetween(Long value1, Long value2) {
            addCriterion("U_CompanyId not between", value1, value2, "uCompanyid");
            return (Criteria) this;
        }

        public Criteria andUUserorgcodeIsNull() {
            addCriterion("U_UserOrgCode is null");
            return (Criteria) this;
        }

        public Criteria andUUserorgcodeIsNotNull() {
            addCriterion("U_UserOrgCode is not null");
            return (Criteria) this;
        }

        public Criteria andUUserorgcodeEqualTo(String value) {
            addCriterion("U_UserOrgCode =", value, "uUserorgcode");
            return (Criteria) this;
        }

        public Criteria andUUserorgcodeNotEqualTo(String value) {
            addCriterion("U_UserOrgCode <>", value, "uUserorgcode");
            return (Criteria) this;
        }

        public Criteria andUUserorgcodeGreaterThan(String value) {
            addCriterion("U_UserOrgCode >", value, "uUserorgcode");
            return (Criteria) this;
        }

        public Criteria andUUserorgcodeGreaterThanOrEqualTo(String value) {
            addCriterion("U_UserOrgCode >=", value, "uUserorgcode");
            return (Criteria) this;
        }

        public Criteria andUUserorgcodeLessThan(String value) {
            addCriterion("U_UserOrgCode <", value, "uUserorgcode");
            return (Criteria) this;
        }

        public Criteria andUUserorgcodeLessThanOrEqualTo(String value) {
            addCriterion("U_UserOrgCode <=", value, "uUserorgcode");
            return (Criteria) this;
        }

        public Criteria andUUserorgcodeLike(String value) {
            addCriterion("U_UserOrgCode like", value, "uUserorgcode");
            return (Criteria) this;
        }

        public Criteria andUUserorgcodeNotLike(String value) {
            addCriterion("U_UserOrgCode not like", value, "uUserorgcode");
            return (Criteria) this;
        }

        public Criteria andUUserorgcodeIn(List<String> values) {
            addCriterion("U_UserOrgCode in", values, "uUserorgcode");
            return (Criteria) this;
        }

        public Criteria andUUserorgcodeNotIn(List<String> values) {
            addCriterion("U_UserOrgCode not in", values, "uUserorgcode");
            return (Criteria) this;
        }

        public Criteria andUUserorgcodeBetween(String value1, String value2) {
            addCriterion("U_UserOrgCode between", value1, value2, "uUserorgcode");
            return (Criteria) this;
        }

        public Criteria andUUserorgcodeNotBetween(String value1, String value2) {
            addCriterion("U_UserOrgCode not between", value1, value2, "uUserorgcode");
            return (Criteria) this;
        }

        public Criteria andULoginnameIsNull() {
            addCriterion("U_LoginName is null");
            return (Criteria) this;
        }

        public Criteria andULoginnameIsNotNull() {
            addCriterion("U_LoginName is not null");
            return (Criteria) this;
        }

        public Criteria andULoginnameEqualTo(String value) {
            addCriterion("U_LoginName =", value, "uLoginname");
            return (Criteria) this;
        }

        public Criteria andULoginnameNotEqualTo(String value) {
            addCriterion("U_LoginName <>", value, "uLoginname");
            return (Criteria) this;
        }

        public Criteria andULoginnameGreaterThan(String value) {
            addCriterion("U_LoginName >", value, "uLoginname");
            return (Criteria) this;
        }

        public Criteria andULoginnameGreaterThanOrEqualTo(String value) {
            addCriterion("U_LoginName >=", value, "uLoginname");
            return (Criteria) this;
        }

        public Criteria andULoginnameLessThan(String value) {
            addCriterion("U_LoginName <", value, "uLoginname");
            return (Criteria) this;
        }

        public Criteria andULoginnameLessThanOrEqualTo(String value) {
            addCriterion("U_LoginName <=", value, "uLoginname");
            return (Criteria) this;
        }

        public Criteria andULoginnameLike(String value) {
            addCriterion("U_LoginName like", value, "uLoginname");
            return (Criteria) this;
        }

        public Criteria andULoginnameNotLike(String value) {
            addCriterion("U_LoginName not like", value, "uLoginname");
            return (Criteria) this;
        }

        public Criteria andULoginnameIn(List<String> values) {
            addCriterion("U_LoginName in", values, "uLoginname");
            return (Criteria) this;
        }

        public Criteria andULoginnameNotIn(List<String> values) {
            addCriterion("U_LoginName not in", values, "uLoginname");
            return (Criteria) this;
        }

        public Criteria andULoginnameBetween(String value1, String value2) {
            addCriterion("U_LoginName between", value1, value2, "uLoginname");
            return (Criteria) this;
        }

        public Criteria andULoginnameNotBetween(String value1, String value2) {
            addCriterion("U_LoginName not between", value1, value2, "uLoginname");
            return (Criteria) this;
        }

        public Criteria andUPasswordIsNull() {
            addCriterion("U_Password is null");
            return (Criteria) this;
        }

        public Criteria andUPasswordIsNotNull() {
            addCriterion("U_Password is not null");
            return (Criteria) this;
        }

        public Criteria andUPasswordEqualTo(String value) {
            addCriterion("U_Password =", value, "uPassword");
            return (Criteria) this;
        }

        public Criteria andUPasswordNotEqualTo(String value) {
            addCriterion("U_Password <>", value, "uPassword");
            return (Criteria) this;
        }

        public Criteria andUPasswordGreaterThan(String value) {
            addCriterion("U_Password >", value, "uPassword");
            return (Criteria) this;
        }

        public Criteria andUPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("U_Password >=", value, "uPassword");
            return (Criteria) this;
        }

        public Criteria andUPasswordLessThan(String value) {
            addCriterion("U_Password <", value, "uPassword");
            return (Criteria) this;
        }

        public Criteria andUPasswordLessThanOrEqualTo(String value) {
            addCriterion("U_Password <=", value, "uPassword");
            return (Criteria) this;
        }

        public Criteria andUPasswordLike(String value) {
            addCriterion("U_Password like", value, "uPassword");
            return (Criteria) this;
        }

        public Criteria andUPasswordNotLike(String value) {
            addCriterion("U_Password not like", value, "uPassword");
            return (Criteria) this;
        }

        public Criteria andUPasswordIn(List<String> values) {
            addCriterion("U_Password in", values, "uPassword");
            return (Criteria) this;
        }

        public Criteria andUPasswordNotIn(List<String> values) {
            addCriterion("U_Password not in", values, "uPassword");
            return (Criteria) this;
        }

        public Criteria andUPasswordBetween(String value1, String value2) {
            addCriterion("U_Password between", value1, value2, "uPassword");
            return (Criteria) this;
        }

        public Criteria andUPasswordNotBetween(String value1, String value2) {
            addCriterion("U_Password not between", value1, value2, "uPassword");
            return (Criteria) this;
        }

        public Criteria andUNameIsNull() {
            addCriterion("U_Name is null");
            return (Criteria) this;
        }

        public Criteria andUNameIsNotNull() {
            addCriterion("U_Name is not null");
            return (Criteria) this;
        }

        public Criteria andUNameEqualTo(String value) {
            addCriterion("U_Name =", value, "uName");
            return (Criteria) this;
        }

        public Criteria andUNameNotEqualTo(String value) {
            addCriterion("U_Name <>", value, "uName");
            return (Criteria) this;
        }

        public Criteria andUNameGreaterThan(String value) {
            addCriterion("U_Name >", value, "uName");
            return (Criteria) this;
        }

        public Criteria andUNameGreaterThanOrEqualTo(String value) {
            addCriterion("U_Name >=", value, "uName");
            return (Criteria) this;
        }

        public Criteria andUNameLessThan(String value) {
            addCriterion("U_Name <", value, "uName");
            return (Criteria) this;
        }

        public Criteria andUNameLessThanOrEqualTo(String value) {
            addCriterion("U_Name <=", value, "uName");
            return (Criteria) this;
        }

        public Criteria andUNameLike(String value) {
            addCriterion("U_Name like", value, "uName");
            return (Criteria) this;
        }

        public Criteria andUNameNotLike(String value) {
            addCriterion("U_Name not like", value, "uName");
            return (Criteria) this;
        }

        public Criteria andUNameIn(List<String> values) {
            addCriterion("U_Name in", values, "uName");
            return (Criteria) this;
        }

        public Criteria andUNameNotIn(List<String> values) {
            addCriterion("U_Name not in", values, "uName");
            return (Criteria) this;
        }

        public Criteria andUNameBetween(String value1, String value2) {
            addCriterion("U_Name between", value1, value2, "uName");
            return (Criteria) this;
        }

        public Criteria andUNameNotBetween(String value1, String value2) {
            addCriterion("U_Name not between", value1, value2, "uName");
            return (Criteria) this;
        }

        public Criteria andULastlogintimeIsNull() {
            addCriterion("U_LastLoginTime is null");
            return (Criteria) this;
        }

        public Criteria andULastlogintimeIsNotNull() {
            addCriterion("U_LastLoginTime is not null");
            return (Criteria) this;
        }

        public Criteria andULastlogintimeEqualTo(Date value) {
            addCriterion("U_LastLoginTime =", value, "uLastlogintime");
            return (Criteria) this;
        }

        public Criteria andULastlogintimeNotEqualTo(Date value) {
            addCriterion("U_LastLoginTime <>", value, "uLastlogintime");
            return (Criteria) this;
        }

        public Criteria andULastlogintimeGreaterThan(Date value) {
            addCriterion("U_LastLoginTime >", value, "uLastlogintime");
            return (Criteria) this;
        }

        public Criteria andULastlogintimeGreaterThanOrEqualTo(Date value) {
            addCriterion("U_LastLoginTime >=", value, "uLastlogintime");
            return (Criteria) this;
        }

        public Criteria andULastlogintimeLessThan(Date value) {
            addCriterion("U_LastLoginTime <", value, "uLastlogintime");
            return (Criteria) this;
        }

        public Criteria andULastlogintimeLessThanOrEqualTo(Date value) {
            addCriterion("U_LastLoginTime <=", value, "uLastlogintime");
            return (Criteria) this;
        }

        public Criteria andULastlogintimeIn(List<Date> values) {
            addCriterion("U_LastLoginTime in", values, "uLastlogintime");
            return (Criteria) this;
        }

        public Criteria andULastlogintimeNotIn(List<Date> values) {
            addCriterion("U_LastLoginTime not in", values, "uLastlogintime");
            return (Criteria) this;
        }

        public Criteria andULastlogintimeBetween(Date value1, Date value2) {
            addCriterion("U_LastLoginTime between", value1, value2, "uLastlogintime");
            return (Criteria) this;
        }

        public Criteria andULastlogintimeNotBetween(Date value1, Date value2) {
            addCriterion("U_LastLoginTime not between", value1, value2, "uLastlogintime");
            return (Criteria) this;
        }

        public Criteria andULastloginipIsNull() {
            addCriterion("U_LastLoginIp is null");
            return (Criteria) this;
        }

        public Criteria andULastloginipIsNotNull() {
            addCriterion("U_LastLoginIp is not null");
            return (Criteria) this;
        }

        public Criteria andULastloginipEqualTo(String value) {
            addCriterion("U_LastLoginIp =", value, "uLastloginip");
            return (Criteria) this;
        }

        public Criteria andULastloginipNotEqualTo(String value) {
            addCriterion("U_LastLoginIp <>", value, "uLastloginip");
            return (Criteria) this;
        }

        public Criteria andULastloginipGreaterThan(String value) {
            addCriterion("U_LastLoginIp >", value, "uLastloginip");
            return (Criteria) this;
        }

        public Criteria andULastloginipGreaterThanOrEqualTo(String value) {
            addCriterion("U_LastLoginIp >=", value, "uLastloginip");
            return (Criteria) this;
        }

        public Criteria andULastloginipLessThan(String value) {
            addCriterion("U_LastLoginIp <", value, "uLastloginip");
            return (Criteria) this;
        }

        public Criteria andULastloginipLessThanOrEqualTo(String value) {
            addCriterion("U_LastLoginIp <=", value, "uLastloginip");
            return (Criteria) this;
        }

        public Criteria andULastloginipLike(String value) {
            addCriterion("U_LastLoginIp like", value, "uLastloginip");
            return (Criteria) this;
        }

        public Criteria andULastloginipNotLike(String value) {
            addCriterion("U_LastLoginIp not like", value, "uLastloginip");
            return (Criteria) this;
        }

        public Criteria andULastloginipIn(List<String> values) {
            addCriterion("U_LastLoginIp in", values, "uLastloginip");
            return (Criteria) this;
        }

        public Criteria andULastloginipNotIn(List<String> values) {
            addCriterion("U_LastLoginIp not in", values, "uLastloginip");
            return (Criteria) this;
        }

        public Criteria andULastloginipBetween(String value1, String value2) {
            addCriterion("U_LastLoginIp between", value1, value2, "uLastloginip");
            return (Criteria) this;
        }

        public Criteria andULastloginipNotBetween(String value1, String value2) {
            addCriterion("U_LastLoginIp not between", value1, value2, "uLastloginip");
            return (Criteria) this;
        }

        public Criteria andULogintimesIsNull() {
            addCriterion("U_LoginTimes is null");
            return (Criteria) this;
        }

        public Criteria andULogintimesIsNotNull() {
            addCriterion("U_LoginTimes is not null");
            return (Criteria) this;
        }

        public Criteria andULogintimesEqualTo(Integer value) {
            addCriterion("U_LoginTimes =", value, "uLogintimes");
            return (Criteria) this;
        }

        public Criteria andULogintimesNotEqualTo(Integer value) {
            addCriterion("U_LoginTimes <>", value, "uLogintimes");
            return (Criteria) this;
        }

        public Criteria andULogintimesGreaterThan(Integer value) {
            addCriterion("U_LoginTimes >", value, "uLogintimes");
            return (Criteria) this;
        }

        public Criteria andULogintimesGreaterThanOrEqualTo(Integer value) {
            addCriterion("U_LoginTimes >=", value, "uLogintimes");
            return (Criteria) this;
        }

        public Criteria andULogintimesLessThan(Integer value) {
            addCriterion("U_LoginTimes <", value, "uLogintimes");
            return (Criteria) this;
        }

        public Criteria andULogintimesLessThanOrEqualTo(Integer value) {
            addCriterion("U_LoginTimes <=", value, "uLogintimes");
            return (Criteria) this;
        }

        public Criteria andULogintimesIn(List<Integer> values) {
            addCriterion("U_LoginTimes in", values, "uLogintimes");
            return (Criteria) this;
        }

        public Criteria andULogintimesNotIn(List<Integer> values) {
            addCriterion("U_LoginTimes not in", values, "uLogintimes");
            return (Criteria) this;
        }

        public Criteria andULogintimesBetween(Integer value1, Integer value2) {
            addCriterion("U_LoginTimes between", value1, value2, "uLogintimes");
            return (Criteria) this;
        }

        public Criteria andULogintimesNotBetween(Integer value1, Integer value2) {
            addCriterion("U_LoginTimes not between", value1, value2, "uLogintimes");
            return (Criteria) this;
        }

        public Criteria andULoginlocktimeIsNull() {
            addCriterion("U_LoginLockTime is null");
            return (Criteria) this;
        }

        public Criteria andULoginlocktimeIsNotNull() {
            addCriterion("U_LoginLockTime is not null");
            return (Criteria) this;
        }

        public Criteria andULoginlocktimeEqualTo(Date value) {
            addCriterion("U_LoginLockTime =", value, "uLoginlocktime");
            return (Criteria) this;
        }

        public Criteria andULoginlocktimeNotEqualTo(Date value) {
            addCriterion("U_LoginLockTime <>", value, "uLoginlocktime");
            return (Criteria) this;
        }

        public Criteria andULoginlocktimeGreaterThan(Date value) {
            addCriterion("U_LoginLockTime >", value, "uLoginlocktime");
            return (Criteria) this;
        }

        public Criteria andULoginlocktimeGreaterThanOrEqualTo(Date value) {
            addCriterion("U_LoginLockTime >=", value, "uLoginlocktime");
            return (Criteria) this;
        }

        public Criteria andULoginlocktimeLessThan(Date value) {
            addCriterion("U_LoginLockTime <", value, "uLoginlocktime");
            return (Criteria) this;
        }

        public Criteria andULoginlocktimeLessThanOrEqualTo(Date value) {
            addCriterion("U_LoginLockTime <=", value, "uLoginlocktime");
            return (Criteria) this;
        }

        public Criteria andULoginlocktimeIn(List<Date> values) {
            addCriterion("U_LoginLockTime in", values, "uLoginlocktime");
            return (Criteria) this;
        }

        public Criteria andULoginlocktimeNotIn(List<Date> values) {
            addCriterion("U_LoginLockTime not in", values, "uLoginlocktime");
            return (Criteria) this;
        }

        public Criteria andULoginlocktimeBetween(Date value1, Date value2) {
            addCriterion("U_LoginLockTime between", value1, value2, "uLoginlocktime");
            return (Criteria) this;
        }

        public Criteria andULoginlocktimeNotBetween(Date value1, Date value2) {
            addCriterion("U_LoginLockTime not between", value1, value2, "uLoginlocktime");
            return (Criteria) this;
        }

        public Criteria andULoginerrortimeIsNull() {
            addCriterion("U_LoginErrorTime is null");
            return (Criteria) this;
        }

        public Criteria andULoginerrortimeIsNotNull() {
            addCriterion("U_LoginErrorTime is not null");
            return (Criteria) this;
        }

        public Criteria andULoginerrortimeEqualTo(Integer value) {
            addCriterion("U_LoginErrorTime =", value, "uLoginerrortime");
            return (Criteria) this;
        }

        public Criteria andULoginerrortimeNotEqualTo(Integer value) {
            addCriterion("U_LoginErrorTime <>", value, "uLoginerrortime");
            return (Criteria) this;
        }

        public Criteria andULoginerrortimeGreaterThan(Integer value) {
            addCriterion("U_LoginErrorTime >", value, "uLoginerrortime");
            return (Criteria) this;
        }

        public Criteria andULoginerrortimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("U_LoginErrorTime >=", value, "uLoginerrortime");
            return (Criteria) this;
        }

        public Criteria andULoginerrortimeLessThan(Integer value) {
            addCriterion("U_LoginErrorTime <", value, "uLoginerrortime");
            return (Criteria) this;
        }

        public Criteria andULoginerrortimeLessThanOrEqualTo(Integer value) {
            addCriterion("U_LoginErrorTime <=", value, "uLoginerrortime");
            return (Criteria) this;
        }

        public Criteria andULoginerrortimeIn(List<Integer> values) {
            addCriterion("U_LoginErrorTime in", values, "uLoginerrortime");
            return (Criteria) this;
        }

        public Criteria andULoginerrortimeNotIn(List<Integer> values) {
            addCriterion("U_LoginErrorTime not in", values, "uLoginerrortime");
            return (Criteria) this;
        }

        public Criteria andULoginerrortimeBetween(Integer value1, Integer value2) {
            addCriterion("U_LoginErrorTime between", value1, value2, "uLoginerrortime");
            return (Criteria) this;
        }

        public Criteria andULoginerrortimeNotBetween(Integer value1, Integer value2) {
            addCriterion("U_LoginErrorTime not between", value1, value2, "uLoginerrortime");
            return (Criteria) this;
        }

        public Criteria andUPhoneIsNull() {
            addCriterion("U_Phone is null");
            return (Criteria) this;
        }

        public Criteria andUPhoneIsNotNull() {
            addCriterion("U_Phone is not null");
            return (Criteria) this;
        }

        public Criteria andUPhoneEqualTo(String value) {
            addCriterion("U_Phone =", value, "uPhone");
            return (Criteria) this;
        }

        public Criteria andUPhoneNotEqualTo(String value) {
            addCriterion("U_Phone <>", value, "uPhone");
            return (Criteria) this;
        }

        public Criteria andUPhoneGreaterThan(String value) {
            addCriterion("U_Phone >", value, "uPhone");
            return (Criteria) this;
        }

        public Criteria andUPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("U_Phone >=", value, "uPhone");
            return (Criteria) this;
        }

        public Criteria andUPhoneLessThan(String value) {
            addCriterion("U_Phone <", value, "uPhone");
            return (Criteria) this;
        }

        public Criteria andUPhoneLessThanOrEqualTo(String value) {
            addCriterion("U_Phone <=", value, "uPhone");
            return (Criteria) this;
        }

        public Criteria andUPhoneLike(String value) {
            addCriterion("U_Phone like", value, "uPhone");
            return (Criteria) this;
        }

        public Criteria andUPhoneNotLike(String value) {
            addCriterion("U_Phone not like", value, "uPhone");
            return (Criteria) this;
        }

        public Criteria andUPhoneIn(List<String> values) {
            addCriterion("U_Phone in", values, "uPhone");
            return (Criteria) this;
        }

        public Criteria andUPhoneNotIn(List<String> values) {
            addCriterion("U_Phone not in", values, "uPhone");
            return (Criteria) this;
        }

        public Criteria andUPhoneBetween(String value1, String value2) {
            addCriterion("U_Phone between", value1, value2, "uPhone");
            return (Criteria) this;
        }

        public Criteria andUPhoneNotBetween(String value1, String value2) {
            addCriterion("U_Phone not between", value1, value2, "uPhone");
            return (Criteria) this;
        }

        public Criteria andUQqIsNull() {
            addCriterion("U_QQ is null");
            return (Criteria) this;
        }

        public Criteria andUQqIsNotNull() {
            addCriterion("U_QQ is not null");
            return (Criteria) this;
        }

        public Criteria andUQqEqualTo(String value) {
            addCriterion("U_QQ =", value, "uQq");
            return (Criteria) this;
        }

        public Criteria andUQqNotEqualTo(String value) {
            addCriterion("U_QQ <>", value, "uQq");
            return (Criteria) this;
        }

        public Criteria andUQqGreaterThan(String value) {
            addCriterion("U_QQ >", value, "uQq");
            return (Criteria) this;
        }

        public Criteria andUQqGreaterThanOrEqualTo(String value) {
            addCriterion("U_QQ >=", value, "uQq");
            return (Criteria) this;
        }

        public Criteria andUQqLessThan(String value) {
            addCriterion("U_QQ <", value, "uQq");
            return (Criteria) this;
        }

        public Criteria andUQqLessThanOrEqualTo(String value) {
            addCriterion("U_QQ <=", value, "uQq");
            return (Criteria) this;
        }

        public Criteria andUQqLike(String value) {
            addCriterion("U_QQ like", value, "uQq");
            return (Criteria) this;
        }

        public Criteria andUQqNotLike(String value) {
            addCriterion("U_QQ not like", value, "uQq");
            return (Criteria) this;
        }

        public Criteria andUQqIn(List<String> values) {
            addCriterion("U_QQ in", values, "uQq");
            return (Criteria) this;
        }

        public Criteria andUQqNotIn(List<String> values) {
            addCriterion("U_QQ not in", values, "uQq");
            return (Criteria) this;
        }

        public Criteria andUQqBetween(String value1, String value2) {
            addCriterion("U_QQ between", value1, value2, "uQq");
            return (Criteria) this;
        }

        public Criteria andUQqNotBetween(String value1, String value2) {
            addCriterion("U_QQ not between", value1, value2, "uQq");
            return (Criteria) this;
        }

        public Criteria andUEmailIsNull() {
            addCriterion("U_Email is null");
            return (Criteria) this;
        }

        public Criteria andUEmailIsNotNull() {
            addCriterion("U_Email is not null");
            return (Criteria) this;
        }

        public Criteria andUEmailEqualTo(String value) {
            addCriterion("U_Email =", value, "uEmail");
            return (Criteria) this;
        }

        public Criteria andUEmailNotEqualTo(String value) {
            addCriterion("U_Email <>", value, "uEmail");
            return (Criteria) this;
        }

        public Criteria andUEmailGreaterThan(String value) {
            addCriterion("U_Email >", value, "uEmail");
            return (Criteria) this;
        }

        public Criteria andUEmailGreaterThanOrEqualTo(String value) {
            addCriterion("U_Email >=", value, "uEmail");
            return (Criteria) this;
        }

        public Criteria andUEmailLessThan(String value) {
            addCriterion("U_Email <", value, "uEmail");
            return (Criteria) this;
        }

        public Criteria andUEmailLessThanOrEqualTo(String value) {
            addCriterion("U_Email <=", value, "uEmail");
            return (Criteria) this;
        }

        public Criteria andUEmailLike(String value) {
            addCriterion("U_Email like", value, "uEmail");
            return (Criteria) this;
        }

        public Criteria andUEmailNotLike(String value) {
            addCriterion("U_Email not like", value, "uEmail");
            return (Criteria) this;
        }

        public Criteria andUEmailIn(List<String> values) {
            addCriterion("U_Email in", values, "uEmail");
            return (Criteria) this;
        }

        public Criteria andUEmailNotIn(List<String> values) {
            addCriterion("U_Email not in", values, "uEmail");
            return (Criteria) this;
        }

        public Criteria andUEmailBetween(String value1, String value2) {
            addCriterion("U_Email between", value1, value2, "uEmail");
            return (Criteria) this;
        }

        public Criteria andUEmailNotBetween(String value1, String value2) {
            addCriterion("U_Email not between", value1, value2, "uEmail");
            return (Criteria) this;
        }

        public Criteria andUAddressIsNull() {
            addCriterion("U_Address is null");
            return (Criteria) this;
        }

        public Criteria andUAddressIsNotNull() {
            addCriterion("U_Address is not null");
            return (Criteria) this;
        }

        public Criteria andUAddressEqualTo(String value) {
            addCriterion("U_Address =", value, "uAddress");
            return (Criteria) this;
        }

        public Criteria andUAddressNotEqualTo(String value) {
            addCriterion("U_Address <>", value, "uAddress");
            return (Criteria) this;
        }

        public Criteria andUAddressGreaterThan(String value) {
            addCriterion("U_Address >", value, "uAddress");
            return (Criteria) this;
        }

        public Criteria andUAddressGreaterThanOrEqualTo(String value) {
            addCriterion("U_Address >=", value, "uAddress");
            return (Criteria) this;
        }

        public Criteria andUAddressLessThan(String value) {
            addCriterion("U_Address <", value, "uAddress");
            return (Criteria) this;
        }

        public Criteria andUAddressLessThanOrEqualTo(String value) {
            addCriterion("U_Address <=", value, "uAddress");
            return (Criteria) this;
        }

        public Criteria andUAddressLike(String value) {
            addCriterion("U_Address like", value, "uAddress");
            return (Criteria) this;
        }

        public Criteria andUAddressNotLike(String value) {
            addCriterion("U_Address not like", value, "uAddress");
            return (Criteria) this;
        }

        public Criteria andUAddressIn(List<String> values) {
            addCriterion("U_Address in", values, "uAddress");
            return (Criteria) this;
        }

        public Criteria andUAddressNotIn(List<String> values) {
            addCriterion("U_Address not in", values, "uAddress");
            return (Criteria) this;
        }

        public Criteria andUAddressBetween(String value1, String value2) {
            addCriterion("U_Address between", value1, value2, "uAddress");
            return (Criteria) this;
        }

        public Criteria andUAddressNotBetween(String value1, String value2) {
            addCriterion("U_Address not between", value1, value2, "uAddress");
            return (Criteria) this;
        }

        public Criteria andUCityIsNull() {
            addCriterion("U_City is null");
            return (Criteria) this;
        }

        public Criteria andUCityIsNotNull() {
            addCriterion("U_City is not null");
            return (Criteria) this;
        }

        public Criteria andUCityEqualTo(String value) {
            addCriterion("U_City =", value, "uCity");
            return (Criteria) this;
        }

        public Criteria andUCityNotEqualTo(String value) {
            addCriterion("U_City <>", value, "uCity");
            return (Criteria) this;
        }

        public Criteria andUCityGreaterThan(String value) {
            addCriterion("U_City >", value, "uCity");
            return (Criteria) this;
        }

        public Criteria andUCityGreaterThanOrEqualTo(String value) {
            addCriterion("U_City >=", value, "uCity");
            return (Criteria) this;
        }

        public Criteria andUCityLessThan(String value) {
            addCriterion("U_City <", value, "uCity");
            return (Criteria) this;
        }

        public Criteria andUCityLessThanOrEqualTo(String value) {
            addCriterion("U_City <=", value, "uCity");
            return (Criteria) this;
        }

        public Criteria andUCityLike(String value) {
            addCriterion("U_City like", value, "uCity");
            return (Criteria) this;
        }

        public Criteria andUCityNotLike(String value) {
            addCriterion("U_City not like", value, "uCity");
            return (Criteria) this;
        }

        public Criteria andUCityIn(List<String> values) {
            addCriterion("U_City in", values, "uCity");
            return (Criteria) this;
        }

        public Criteria andUCityNotIn(List<String> values) {
            addCriterion("U_City not in", values, "uCity");
            return (Criteria) this;
        }

        public Criteria andUCityBetween(String value1, String value2) {
            addCriterion("U_City between", value1, value2, "uCity");
            return (Criteria) this;
        }

        public Criteria andUCityNotBetween(String value1, String value2) {
            addCriterion("U_City not between", value1, value2, "uCity");
            return (Criteria) this;
        }

        public Criteria andUSecurityquestionIsNull() {
            addCriterion("U_SecurityQuestion is null");
            return (Criteria) this;
        }

        public Criteria andUSecurityquestionIsNotNull() {
            addCriterion("U_SecurityQuestion is not null");
            return (Criteria) this;
        }

        public Criteria andUSecurityquestionEqualTo(String value) {
            addCriterion("U_SecurityQuestion =", value, "uSecurityquestion");
            return (Criteria) this;
        }

        public Criteria andUSecurityquestionNotEqualTo(String value) {
            addCriterion("U_SecurityQuestion <>", value, "uSecurityquestion");
            return (Criteria) this;
        }

        public Criteria andUSecurityquestionGreaterThan(String value) {
            addCriterion("U_SecurityQuestion >", value, "uSecurityquestion");
            return (Criteria) this;
        }

        public Criteria andUSecurityquestionGreaterThanOrEqualTo(String value) {
            addCriterion("U_SecurityQuestion >=", value, "uSecurityquestion");
            return (Criteria) this;
        }

        public Criteria andUSecurityquestionLessThan(String value) {
            addCriterion("U_SecurityQuestion <", value, "uSecurityquestion");
            return (Criteria) this;
        }

        public Criteria andUSecurityquestionLessThanOrEqualTo(String value) {
            addCriterion("U_SecurityQuestion <=", value, "uSecurityquestion");
            return (Criteria) this;
        }

        public Criteria andUSecurityquestionLike(String value) {
            addCriterion("U_SecurityQuestion like", value, "uSecurityquestion");
            return (Criteria) this;
        }

        public Criteria andUSecurityquestionNotLike(String value) {
            addCriterion("U_SecurityQuestion not like", value, "uSecurityquestion");
            return (Criteria) this;
        }

        public Criteria andUSecurityquestionIn(List<String> values) {
            addCriterion("U_SecurityQuestion in", values, "uSecurityquestion");
            return (Criteria) this;
        }

        public Criteria andUSecurityquestionNotIn(List<String> values) {
            addCriterion("U_SecurityQuestion not in", values, "uSecurityquestion");
            return (Criteria) this;
        }

        public Criteria andUSecurityquestionBetween(String value1, String value2) {
            addCriterion("U_SecurityQuestion between", value1, value2, "uSecurityquestion");
            return (Criteria) this;
        }

        public Criteria andUSecurityquestionNotBetween(String value1, String value2) {
            addCriterion("U_SecurityQuestion not between", value1, value2, "uSecurityquestion");
            return (Criteria) this;
        }

        public Criteria andUSecurityanswerIsNull() {
            addCriterion("U_SecurityAnswer is null");
            return (Criteria) this;
        }

        public Criteria andUSecurityanswerIsNotNull() {
            addCriterion("U_SecurityAnswer is not null");
            return (Criteria) this;
        }

        public Criteria andUSecurityanswerEqualTo(String value) {
            addCriterion("U_SecurityAnswer =", value, "uSecurityanswer");
            return (Criteria) this;
        }

        public Criteria andUSecurityanswerNotEqualTo(String value) {
            addCriterion("U_SecurityAnswer <>", value, "uSecurityanswer");
            return (Criteria) this;
        }

        public Criteria andUSecurityanswerGreaterThan(String value) {
            addCriterion("U_SecurityAnswer >", value, "uSecurityanswer");
            return (Criteria) this;
        }

        public Criteria andUSecurityanswerGreaterThanOrEqualTo(String value) {
            addCriterion("U_SecurityAnswer >=", value, "uSecurityanswer");
            return (Criteria) this;
        }

        public Criteria andUSecurityanswerLessThan(String value) {
            addCriterion("U_SecurityAnswer <", value, "uSecurityanswer");
            return (Criteria) this;
        }

        public Criteria andUSecurityanswerLessThanOrEqualTo(String value) {
            addCriterion("U_SecurityAnswer <=", value, "uSecurityanswer");
            return (Criteria) this;
        }

        public Criteria andUSecurityanswerLike(String value) {
            addCriterion("U_SecurityAnswer like", value, "uSecurityanswer");
            return (Criteria) this;
        }

        public Criteria andUSecurityanswerNotLike(String value) {
            addCriterion("U_SecurityAnswer not like", value, "uSecurityanswer");
            return (Criteria) this;
        }

        public Criteria andUSecurityanswerIn(List<String> values) {
            addCriterion("U_SecurityAnswer in", values, "uSecurityanswer");
            return (Criteria) this;
        }

        public Criteria andUSecurityanswerNotIn(List<String> values) {
            addCriterion("U_SecurityAnswer not in", values, "uSecurityanswer");
            return (Criteria) this;
        }

        public Criteria andUSecurityanswerBetween(String value1, String value2) {
            addCriterion("U_SecurityAnswer between", value1, value2, "uSecurityanswer");
            return (Criteria) this;
        }

        public Criteria andUSecurityanswerNotBetween(String value1, String value2) {
            addCriterion("U_SecurityAnswer not between", value1, value2, "uSecurityanswer");
            return (Criteria) this;
        }

        public Criteria andULoginsecurityIsNull() {
            addCriterion("U_LoginSecurity is null");
            return (Criteria) this;
        }

        public Criteria andULoginsecurityIsNotNull() {
            addCriterion("U_LoginSecurity is not null");
            return (Criteria) this;
        }

        public Criteria andULoginsecurityEqualTo(Integer value) {
            addCriterion("U_LoginSecurity =", value, "uLoginsecurity");
            return (Criteria) this;
        }

        public Criteria andULoginsecurityNotEqualTo(Integer value) {
            addCriterion("U_LoginSecurity <>", value, "uLoginsecurity");
            return (Criteria) this;
        }

        public Criteria andULoginsecurityGreaterThan(Integer value) {
            addCriterion("U_LoginSecurity >", value, "uLoginsecurity");
            return (Criteria) this;
        }

        public Criteria andULoginsecurityGreaterThanOrEqualTo(Integer value) {
            addCriterion("U_LoginSecurity >=", value, "uLoginsecurity");
            return (Criteria) this;
        }

        public Criteria andULoginsecurityLessThan(Integer value) {
            addCriterion("U_LoginSecurity <", value, "uLoginsecurity");
            return (Criteria) this;
        }

        public Criteria andULoginsecurityLessThanOrEqualTo(Integer value) {
            addCriterion("U_LoginSecurity <=", value, "uLoginsecurity");
            return (Criteria) this;
        }

        public Criteria andULoginsecurityIn(List<Integer> values) {
            addCriterion("U_LoginSecurity in", values, "uLoginsecurity");
            return (Criteria) this;
        }

        public Criteria andULoginsecurityNotIn(List<Integer> values) {
            addCriterion("U_LoginSecurity not in", values, "uLoginsecurity");
            return (Criteria) this;
        }

        public Criteria andULoginsecurityBetween(Integer value1, Integer value2) {
            addCriterion("U_LoginSecurity between", value1, value2, "uLoginsecurity");
            return (Criteria) this;
        }

        public Criteria andULoginsecurityNotBetween(Integer value1, Integer value2) {
            addCriterion("U_LoginSecurity not between", value1, value2, "uLoginsecurity");
            return (Criteria) this;
        }

        public Criteria andUTmpguidIsNull() {
            addCriterion("U_TmpGuid is null");
            return (Criteria) this;
        }

        public Criteria andUTmpguidIsNotNull() {
            addCriterion("U_TmpGuid is not null");
            return (Criteria) this;
        }

        public Criteria andUTmpguidEqualTo(String value) {
            addCriterion("U_TmpGuid =", value, "uTmpguid");
            return (Criteria) this;
        }

        public Criteria andUTmpguidNotEqualTo(String value) {
            addCriterion("U_TmpGuid <>", value, "uTmpguid");
            return (Criteria) this;
        }

        public Criteria andUTmpguidGreaterThan(String value) {
            addCriterion("U_TmpGuid >", value, "uTmpguid");
            return (Criteria) this;
        }

        public Criteria andUTmpguidGreaterThanOrEqualTo(String value) {
            addCriterion("U_TmpGuid >=", value, "uTmpguid");
            return (Criteria) this;
        }

        public Criteria andUTmpguidLessThan(String value) {
            addCriterion("U_TmpGuid <", value, "uTmpguid");
            return (Criteria) this;
        }

        public Criteria andUTmpguidLessThanOrEqualTo(String value) {
            addCriterion("U_TmpGuid <=", value, "uTmpguid");
            return (Criteria) this;
        }

        public Criteria andUTmpguidLike(String value) {
            addCriterion("U_TmpGuid like", value, "uTmpguid");
            return (Criteria) this;
        }

        public Criteria andUTmpguidNotLike(String value) {
            addCriterion("U_TmpGuid not like", value, "uTmpguid");
            return (Criteria) this;
        }

        public Criteria andUTmpguidIn(List<String> values) {
            addCriterion("U_TmpGuid in", values, "uTmpguid");
            return (Criteria) this;
        }

        public Criteria andUTmpguidNotIn(List<String> values) {
            addCriterion("U_TmpGuid not in", values, "uTmpguid");
            return (Criteria) this;
        }

        public Criteria andUTmpguidBetween(String value1, String value2) {
            addCriterion("U_TmpGuid between", value1, value2, "uTmpguid");
            return (Criteria) this;
        }

        public Criteria andUTmpguidNotBetween(String value1, String value2) {
            addCriterion("U_TmpGuid not between", value1, value2, "uTmpguid");
            return (Criteria) this;
        }

        public Criteria andUSextypeIsNull() {
            addCriterion("U_SexType is null");
            return (Criteria) this;
        }

        public Criteria andUSextypeIsNotNull() {
            addCriterion("U_SexType is not null");
            return (Criteria) this;
        }

        public Criteria andUSextypeEqualTo(Integer value) {
            addCriterion("U_SexType =", value, "uSextype");
            return (Criteria) this;
        }

        public Criteria andUSextypeNotEqualTo(Integer value) {
            addCriterion("U_SexType <>", value, "uSextype");
            return (Criteria) this;
        }

        public Criteria andUSextypeGreaterThan(Integer value) {
            addCriterion("U_SexType >", value, "uSextype");
            return (Criteria) this;
        }

        public Criteria andUSextypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("U_SexType >=", value, "uSextype");
            return (Criteria) this;
        }

        public Criteria andUSextypeLessThan(Integer value) {
            addCriterion("U_SexType <", value, "uSextype");
            return (Criteria) this;
        }

        public Criteria andUSextypeLessThanOrEqualTo(Integer value) {
            addCriterion("U_SexType <=", value, "uSextype");
            return (Criteria) this;
        }

        public Criteria andUSextypeIn(List<Integer> values) {
            addCriterion("U_SexType in", values, "uSextype");
            return (Criteria) this;
        }

        public Criteria andUSextypeNotIn(List<Integer> values) {
            addCriterion("U_SexType not in", values, "uSextype");
            return (Criteria) this;
        }

        public Criteria andUSextypeBetween(Integer value1, Integer value2) {
            addCriterion("U_SexType between", value1, value2, "uSextype");
            return (Criteria) this;
        }

        public Criteria andUSextypeNotBetween(Integer value1, Integer value2) {
            addCriterion("U_SexType not between", value1, value2, "uSextype");
            return (Criteria) this;
        }

        public Criteria andUWorkphoneIsNull() {
            addCriterion("U_WorkPhone is null");
            return (Criteria) this;
        }

        public Criteria andUWorkphoneIsNotNull() {
            addCriterion("U_WorkPhone is not null");
            return (Criteria) this;
        }

        public Criteria andUWorkphoneEqualTo(String value) {
            addCriterion("U_WorkPhone =", value, "uWorkphone");
            return (Criteria) this;
        }

        public Criteria andUWorkphoneNotEqualTo(String value) {
            addCriterion("U_WorkPhone <>", value, "uWorkphone");
            return (Criteria) this;
        }

        public Criteria andUWorkphoneGreaterThan(String value) {
            addCriterion("U_WorkPhone >", value, "uWorkphone");
            return (Criteria) this;
        }

        public Criteria andUWorkphoneGreaterThanOrEqualTo(String value) {
            addCriterion("U_WorkPhone >=", value, "uWorkphone");
            return (Criteria) this;
        }

        public Criteria andUWorkphoneLessThan(String value) {
            addCriterion("U_WorkPhone <", value, "uWorkphone");
            return (Criteria) this;
        }

        public Criteria andUWorkphoneLessThanOrEqualTo(String value) {
            addCriterion("U_WorkPhone <=", value, "uWorkphone");
            return (Criteria) this;
        }

        public Criteria andUWorkphoneLike(String value) {
            addCriterion("U_WorkPhone like", value, "uWorkphone");
            return (Criteria) this;
        }

        public Criteria andUWorkphoneNotLike(String value) {
            addCriterion("U_WorkPhone not like", value, "uWorkphone");
            return (Criteria) this;
        }

        public Criteria andUWorkphoneIn(List<String> values) {
            addCriterion("U_WorkPhone in", values, "uWorkphone");
            return (Criteria) this;
        }

        public Criteria andUWorkphoneNotIn(List<String> values) {
            addCriterion("U_WorkPhone not in", values, "uWorkphone");
            return (Criteria) this;
        }

        public Criteria andUWorkphoneBetween(String value1, String value2) {
            addCriterion("U_WorkPhone between", value1, value2, "uWorkphone");
            return (Criteria) this;
        }

        public Criteria andUWorkphoneNotBetween(String value1, String value2) {
            addCriterion("U_WorkPhone not between", value1, value2, "uWorkphone");
            return (Criteria) this;
        }

        public Criteria andUBirthdayIsNull() {
            addCriterion("U_Birthday is null");
            return (Criteria) this;
        }

        public Criteria andUBirthdayIsNotNull() {
            addCriterion("U_Birthday is not null");
            return (Criteria) this;
        }

        public Criteria andUBirthdayEqualTo(Date value) {
            addCriterion("U_Birthday =", value, "uBirthday");
            return (Criteria) this;
        }

        public Criteria andUBirthdayNotEqualTo(Date value) {
            addCriterion("U_Birthday <>", value, "uBirthday");
            return (Criteria) this;
        }

        public Criteria andUBirthdayGreaterThan(Date value) {
            addCriterion("U_Birthday >", value, "uBirthday");
            return (Criteria) this;
        }

        public Criteria andUBirthdayGreaterThanOrEqualTo(Date value) {
            addCriterion("U_Birthday >=", value, "uBirthday");
            return (Criteria) this;
        }

        public Criteria andUBirthdayLessThan(Date value) {
            addCriterion("U_Birthday <", value, "uBirthday");
            return (Criteria) this;
        }

        public Criteria andUBirthdayLessThanOrEqualTo(Date value) {
            addCriterion("U_Birthday <=", value, "uBirthday");
            return (Criteria) this;
        }

        public Criteria andUBirthdayIn(List<Date> values) {
            addCriterion("U_Birthday in", values, "uBirthday");
            return (Criteria) this;
        }

        public Criteria andUBirthdayNotIn(List<Date> values) {
            addCriterion("U_Birthday not in", values, "uBirthday");
            return (Criteria) this;
        }

        public Criteria andUBirthdayBetween(Date value1, Date value2) {
            addCriterion("U_Birthday between", value1, value2, "uBirthday");
            return (Criteria) this;
        }

        public Criteria andUBirthdayNotBetween(Date value1, Date value2) {
            addCriterion("U_Birthday not between", value1, value2, "uBirthday");
            return (Criteria) this;
        }

        public Criteria andUUserrolesIsNull() {
            addCriterion("U_UserRoles is null");
            return (Criteria) this;
        }

        public Criteria andUUserrolesIsNotNull() {
            addCriterion("U_UserRoles is not null");
            return (Criteria) this;
        }

        public Criteria andUUserrolesEqualTo(String value) {
            addCriterion("U_UserRoles =", value, "uUserroles");
            return (Criteria) this;
        }

        public Criteria andUUserrolesNotEqualTo(String value) {
            addCriterion("U_UserRoles <>", value, "uUserroles");
            return (Criteria) this;
        }

        public Criteria andUUserrolesGreaterThan(String value) {
            addCriterion("U_UserRoles >", value, "uUserroles");
            return (Criteria) this;
        }

        public Criteria andUUserrolesGreaterThanOrEqualTo(String value) {
            addCriterion("U_UserRoles >=", value, "uUserroles");
            return (Criteria) this;
        }

        public Criteria andUUserrolesLessThan(String value) {
            addCriterion("U_UserRoles <", value, "uUserroles");
            return (Criteria) this;
        }

        public Criteria andUUserrolesLessThanOrEqualTo(String value) {
            addCriterion("U_UserRoles <=", value, "uUserroles");
            return (Criteria) this;
        }

        public Criteria andUUserrolesLike(String value) {
            addCriterion("U_UserRoles like", value, "uUserroles");
            return (Criteria) this;
        }

        public Criteria andUUserrolesNotLike(String value) {
            addCriterion("U_UserRoles not like", value, "uUserroles");
            return (Criteria) this;
        }

        public Criteria andUUserrolesIn(List<String> values) {
            addCriterion("U_UserRoles in", values, "uUserroles");
            return (Criteria) this;
        }

        public Criteria andUUserrolesNotIn(List<String> values) {
            addCriterion("U_UserRoles not in", values, "uUserroles");
            return (Criteria) this;
        }

        public Criteria andUUserrolesBetween(String value1, String value2) {
            addCriterion("U_UserRoles between", value1, value2, "uUserroles");
            return (Criteria) this;
        }

        public Criteria andUUserrolesNotBetween(String value1, String value2) {
            addCriterion("U_UserRoles not between", value1, value2, "uUserroles");
            return (Criteria) this;
        }

        public Criteria andBuContacthopeIsNull() {
            addCriterion("BU_ContactHope is null");
            return (Criteria) this;
        }

        public Criteria andBuContacthopeIsNotNull() {
            addCriterion("BU_ContactHope is not null");
            return (Criteria) this;
        }

        public Criteria andBuContacthopeEqualTo(String value) {
            addCriterion("BU_ContactHope =", value, "buContacthope");
            return (Criteria) this;
        }

        public Criteria andBuContacthopeNotEqualTo(String value) {
            addCriterion("BU_ContactHope <>", value, "buContacthope");
            return (Criteria) this;
        }

        public Criteria andBuContacthopeGreaterThan(String value) {
            addCriterion("BU_ContactHope >", value, "buContacthope");
            return (Criteria) this;
        }

        public Criteria andBuContacthopeGreaterThanOrEqualTo(String value) {
            addCriterion("BU_ContactHope >=", value, "buContacthope");
            return (Criteria) this;
        }

        public Criteria andBuContacthopeLessThan(String value) {
            addCriterion("BU_ContactHope <", value, "buContacthope");
            return (Criteria) this;
        }

        public Criteria andBuContacthopeLessThanOrEqualTo(String value) {
            addCriterion("BU_ContactHope <=", value, "buContacthope");
            return (Criteria) this;
        }

        public Criteria andBuContacthopeLike(String value) {
            addCriterion("BU_ContactHope like", value, "buContacthope");
            return (Criteria) this;
        }

        public Criteria andBuContacthopeNotLike(String value) {
            addCriterion("BU_ContactHope not like", value, "buContacthope");
            return (Criteria) this;
        }

        public Criteria andBuContacthopeIn(List<String> values) {
            addCriterion("BU_ContactHope in", values, "buContacthope");
            return (Criteria) this;
        }

        public Criteria andBuContacthopeNotIn(List<String> values) {
            addCriterion("BU_ContactHope not in", values, "buContacthope");
            return (Criteria) this;
        }

        public Criteria andBuContacthopeBetween(String value1, String value2) {
            addCriterion("BU_ContactHope between", value1, value2, "buContacthope");
            return (Criteria) this;
        }

        public Criteria andBuContacthopeNotBetween(String value1, String value2) {
            addCriterion("BU_ContactHope not between", value1, value2, "buContacthope");
            return (Criteria) this;
        }

        public Criteria andBuFaxIsNull() {
            addCriterion("BU_Fax is null");
            return (Criteria) this;
        }

        public Criteria andBuFaxIsNotNull() {
            addCriterion("BU_Fax is not null");
            return (Criteria) this;
        }

        public Criteria andBuFaxEqualTo(String value) {
            addCriterion("BU_Fax =", value, "buFax");
            return (Criteria) this;
        }

        public Criteria andBuFaxNotEqualTo(String value) {
            addCriterion("BU_Fax <>", value, "buFax");
            return (Criteria) this;
        }

        public Criteria andBuFaxGreaterThan(String value) {
            addCriterion("BU_Fax >", value, "buFax");
            return (Criteria) this;
        }

        public Criteria andBuFaxGreaterThanOrEqualTo(String value) {
            addCriterion("BU_Fax >=", value, "buFax");
            return (Criteria) this;
        }

        public Criteria andBuFaxLessThan(String value) {
            addCriterion("BU_Fax <", value, "buFax");
            return (Criteria) this;
        }

        public Criteria andBuFaxLessThanOrEqualTo(String value) {
            addCriterion("BU_Fax <=", value, "buFax");
            return (Criteria) this;
        }

        public Criteria andBuFaxLike(String value) {
            addCriterion("BU_Fax like", value, "buFax");
            return (Criteria) this;
        }

        public Criteria andBuFaxNotLike(String value) {
            addCriterion("BU_Fax not like", value, "buFax");
            return (Criteria) this;
        }

        public Criteria andBuFaxIn(List<String> values) {
            addCriterion("BU_Fax in", values, "buFax");
            return (Criteria) this;
        }

        public Criteria andBuFaxNotIn(List<String> values) {
            addCriterion("BU_Fax not in", values, "buFax");
            return (Criteria) this;
        }

        public Criteria andBuFaxBetween(String value1, String value2) {
            addCriterion("BU_Fax between", value1, value2, "buFax");
            return (Criteria) this;
        }

        public Criteria andBuFaxNotBetween(String value1, String value2) {
            addCriterion("BU_Fax not between", value1, value2, "buFax");
            return (Criteria) this;
        }

        public Criteria andBuPpmealsIsNull() {
            addCriterion("BU_PPMeals is null");
            return (Criteria) this;
        }

        public Criteria andBuPpmealsIsNotNull() {
            addCriterion("BU_PPMeals is not null");
            return (Criteria) this;
        }

        public Criteria andBuPpmealsEqualTo(String value) {
            addCriterion("BU_PPMeals =", value, "buPpmeals");
            return (Criteria) this;
        }

        public Criteria andBuPpmealsNotEqualTo(String value) {
            addCriterion("BU_PPMeals <>", value, "buPpmeals");
            return (Criteria) this;
        }

        public Criteria andBuPpmealsGreaterThan(String value) {
            addCriterion("BU_PPMeals >", value, "buPpmeals");
            return (Criteria) this;
        }

        public Criteria andBuPpmealsGreaterThanOrEqualTo(String value) {
            addCriterion("BU_PPMeals >=", value, "buPpmeals");
            return (Criteria) this;
        }

        public Criteria andBuPpmealsLessThan(String value) {
            addCriterion("BU_PPMeals <", value, "buPpmeals");
            return (Criteria) this;
        }

        public Criteria andBuPpmealsLessThanOrEqualTo(String value) {
            addCriterion("BU_PPMeals <=", value, "buPpmeals");
            return (Criteria) this;
        }

        public Criteria andBuPpmealsLike(String value) {
            addCriterion("BU_PPMeals like", value, "buPpmeals");
            return (Criteria) this;
        }

        public Criteria andBuPpmealsNotLike(String value) {
            addCriterion("BU_PPMeals not like", value, "buPpmeals");
            return (Criteria) this;
        }

        public Criteria andBuPpmealsIn(List<String> values) {
            addCriterion("BU_PPMeals in", values, "buPpmeals");
            return (Criteria) this;
        }

        public Criteria andBuPpmealsNotIn(List<String> values) {
            addCriterion("BU_PPMeals not in", values, "buPpmeals");
            return (Criteria) this;
        }

        public Criteria andBuPpmealsBetween(String value1, String value2) {
            addCriterion("BU_PPMeals between", value1, value2, "buPpmeals");
            return (Criteria) this;
        }

        public Criteria andBuPpmealsNotBetween(String value1, String value2) {
            addCriterion("BU_PPMeals not between", value1, value2, "buPpmeals");
            return (Criteria) this;
        }

        public Criteria andBuPpseatsIsNull() {
            addCriterion("BU_PPSeats is null");
            return (Criteria) this;
        }

        public Criteria andBuPpseatsIsNotNull() {
            addCriterion("BU_PPSeats is not null");
            return (Criteria) this;
        }

        public Criteria andBuPpseatsEqualTo(String value) {
            addCriterion("BU_PPSeats =", value, "buPpseats");
            return (Criteria) this;
        }

        public Criteria andBuPpseatsNotEqualTo(String value) {
            addCriterion("BU_PPSeats <>", value, "buPpseats");
            return (Criteria) this;
        }

        public Criteria andBuPpseatsGreaterThan(String value) {
            addCriterion("BU_PPSeats >", value, "buPpseats");
            return (Criteria) this;
        }

        public Criteria andBuPpseatsGreaterThanOrEqualTo(String value) {
            addCriterion("BU_PPSeats >=", value, "buPpseats");
            return (Criteria) this;
        }

        public Criteria andBuPpseatsLessThan(String value) {
            addCriterion("BU_PPSeats <", value, "buPpseats");
            return (Criteria) this;
        }

        public Criteria andBuPpseatsLessThanOrEqualTo(String value) {
            addCriterion("BU_PPSeats <=", value, "buPpseats");
            return (Criteria) this;
        }

        public Criteria andBuPpseatsLike(String value) {
            addCriterion("BU_PPSeats like", value, "buPpseats");
            return (Criteria) this;
        }

        public Criteria andBuPpseatsNotLike(String value) {
            addCriterion("BU_PPSeats not like", value, "buPpseats");
            return (Criteria) this;
        }

        public Criteria andBuPpseatsIn(List<String> values) {
            addCriterion("BU_PPSeats in", values, "buPpseats");
            return (Criteria) this;
        }

        public Criteria andBuPpseatsNotIn(List<String> values) {
            addCriterion("BU_PPSeats not in", values, "buPpseats");
            return (Criteria) this;
        }

        public Criteria andBuPpseatsBetween(String value1, String value2) {
            addCriterion("BU_PPSeats between", value1, value2, "buPpseats");
            return (Criteria) this;
        }

        public Criteria andBuPpseatsNotBetween(String value1, String value2) {
            addCriterion("BU_PPSeats not between", value1, value2, "buPpseats");
            return (Criteria) this;
        }

        public Criteria andBuPpdiscountIsNull() {
            addCriterion("BU_PPDiscount is null");
            return (Criteria) this;
        }

        public Criteria andBuPpdiscountIsNotNull() {
            addCriterion("BU_PPDiscount is not null");
            return (Criteria) this;
        }

        public Criteria andBuPpdiscountEqualTo(String value) {
            addCriterion("BU_PPDiscount =", value, "buPpdiscount");
            return (Criteria) this;
        }

        public Criteria andBuPpdiscountNotEqualTo(String value) {
            addCriterion("BU_PPDiscount <>", value, "buPpdiscount");
            return (Criteria) this;
        }

        public Criteria andBuPpdiscountGreaterThan(String value) {
            addCriterion("BU_PPDiscount >", value, "buPpdiscount");
            return (Criteria) this;
        }

        public Criteria andBuPpdiscountGreaterThanOrEqualTo(String value) {
            addCriterion("BU_PPDiscount >=", value, "buPpdiscount");
            return (Criteria) this;
        }

        public Criteria andBuPpdiscountLessThan(String value) {
            addCriterion("BU_PPDiscount <", value, "buPpdiscount");
            return (Criteria) this;
        }

        public Criteria andBuPpdiscountLessThanOrEqualTo(String value) {
            addCriterion("BU_PPDiscount <=", value, "buPpdiscount");
            return (Criteria) this;
        }

        public Criteria andBuPpdiscountLike(String value) {
            addCriterion("BU_PPDiscount like", value, "buPpdiscount");
            return (Criteria) this;
        }

        public Criteria andBuPpdiscountNotLike(String value) {
            addCriterion("BU_PPDiscount not like", value, "buPpdiscount");
            return (Criteria) this;
        }

        public Criteria andBuPpdiscountIn(List<String> values) {
            addCriterion("BU_PPDiscount in", values, "buPpdiscount");
            return (Criteria) this;
        }

        public Criteria andBuPpdiscountNotIn(List<String> values) {
            addCriterion("BU_PPDiscount not in", values, "buPpdiscount");
            return (Criteria) this;
        }

        public Criteria andBuPpdiscountBetween(String value1, String value2) {
            addCriterion("BU_PPDiscount between", value1, value2, "buPpdiscount");
            return (Criteria) this;
        }

        public Criteria andBuPpdiscountNotBetween(String value1, String value2) {
            addCriterion("BU_PPDiscount not between", value1, value2, "buPpdiscount");
            return (Criteria) this;
        }

        public Criteria andBuPpchannelIsNull() {
            addCriterion("BU_PPChannel is null");
            return (Criteria) this;
        }

        public Criteria andBuPpchannelIsNotNull() {
            addCriterion("BU_PPChannel is not null");
            return (Criteria) this;
        }

        public Criteria andBuPpchannelEqualTo(String value) {
            addCriterion("BU_PPChannel =", value, "buPpchannel");
            return (Criteria) this;
        }

        public Criteria andBuPpchannelNotEqualTo(String value) {
            addCriterion("BU_PPChannel <>", value, "buPpchannel");
            return (Criteria) this;
        }

        public Criteria andBuPpchannelGreaterThan(String value) {
            addCriterion("BU_PPChannel >", value, "buPpchannel");
            return (Criteria) this;
        }

        public Criteria andBuPpchannelGreaterThanOrEqualTo(String value) {
            addCriterion("BU_PPChannel >=", value, "buPpchannel");
            return (Criteria) this;
        }

        public Criteria andBuPpchannelLessThan(String value) {
            addCriterion("BU_PPChannel <", value, "buPpchannel");
            return (Criteria) this;
        }

        public Criteria andBuPpchannelLessThanOrEqualTo(String value) {
            addCriterion("BU_PPChannel <=", value, "buPpchannel");
            return (Criteria) this;
        }

        public Criteria andBuPpchannelLike(String value) {
            addCriterion("BU_PPChannel like", value, "buPpchannel");
            return (Criteria) this;
        }

        public Criteria andBuPpchannelNotLike(String value) {
            addCriterion("BU_PPChannel not like", value, "buPpchannel");
            return (Criteria) this;
        }

        public Criteria andBuPpchannelIn(List<String> values) {
            addCriterion("BU_PPChannel in", values, "buPpchannel");
            return (Criteria) this;
        }

        public Criteria andBuPpchannelNotIn(List<String> values) {
            addCriterion("BU_PPChannel not in", values, "buPpchannel");
            return (Criteria) this;
        }

        public Criteria andBuPpchannelBetween(String value1, String value2) {
            addCriterion("BU_PPChannel between", value1, value2, "buPpchannel");
            return (Criteria) this;
        }

        public Criteria andBuPpchannelNotBetween(String value1, String value2) {
            addCriterion("BU_PPChannel not between", value1, value2, "buPpchannel");
            return (Criteria) this;
        }

        public Criteria andBuPppaymentmethodIsNull() {
            addCriterion("BU_PPPaymentMethod is null");
            return (Criteria) this;
        }

        public Criteria andBuPppaymentmethodIsNotNull() {
            addCriterion("BU_PPPaymentMethod is not null");
            return (Criteria) this;
        }

        public Criteria andBuPppaymentmethodEqualTo(String value) {
            addCriterion("BU_PPPaymentMethod =", value, "buPppaymentmethod");
            return (Criteria) this;
        }

        public Criteria andBuPppaymentmethodNotEqualTo(String value) {
            addCriterion("BU_PPPaymentMethod <>", value, "buPppaymentmethod");
            return (Criteria) this;
        }

        public Criteria andBuPppaymentmethodGreaterThan(String value) {
            addCriterion("BU_PPPaymentMethod >", value, "buPppaymentmethod");
            return (Criteria) this;
        }

        public Criteria andBuPppaymentmethodGreaterThanOrEqualTo(String value) {
            addCriterion("BU_PPPaymentMethod >=", value, "buPppaymentmethod");
            return (Criteria) this;
        }

        public Criteria andBuPppaymentmethodLessThan(String value) {
            addCriterion("BU_PPPaymentMethod <", value, "buPppaymentmethod");
            return (Criteria) this;
        }

        public Criteria andBuPppaymentmethodLessThanOrEqualTo(String value) {
            addCriterion("BU_PPPaymentMethod <=", value, "buPppaymentmethod");
            return (Criteria) this;
        }

        public Criteria andBuPppaymentmethodLike(String value) {
            addCriterion("BU_PPPaymentMethod like", value, "buPppaymentmethod");
            return (Criteria) this;
        }

        public Criteria andBuPppaymentmethodNotLike(String value) {
            addCriterion("BU_PPPaymentMethod not like", value, "buPppaymentmethod");
            return (Criteria) this;
        }

        public Criteria andBuPppaymentmethodIn(List<String> values) {
            addCriterion("BU_PPPaymentMethod in", values, "buPppaymentmethod");
            return (Criteria) this;
        }

        public Criteria andBuPppaymentmethodNotIn(List<String> values) {
            addCriterion("BU_PPPaymentMethod not in", values, "buPppaymentmethod");
            return (Criteria) this;
        }

        public Criteria andBuPppaymentmethodBetween(String value1, String value2) {
            addCriterion("BU_PPPaymentMethod between", value1, value2, "buPppaymentmethod");
            return (Criteria) this;
        }

        public Criteria andBuPppaymentmethodNotBetween(String value1, String value2) {
            addCriterion("BU_PPPaymentMethod not between", value1, value2, "buPppaymentmethod");
            return (Criteria) this;
        }

        public Criteria andBuContactaddresshopeidIsNull() {
            addCriterion("BU_ContactAddressHopeID is null");
            return (Criteria) this;
        }

        public Criteria andBuContactaddresshopeidIsNotNull() {
            addCriterion("BU_ContactAddressHopeID is not null");
            return (Criteria) this;
        }

        public Criteria andBuContactaddresshopeidEqualTo(Long value) {
            addCriterion("BU_ContactAddressHopeID =", value, "buContactaddresshopeid");
            return (Criteria) this;
        }

        public Criteria andBuContactaddresshopeidNotEqualTo(Long value) {
            addCriterion("BU_ContactAddressHopeID <>", value, "buContactaddresshopeid");
            return (Criteria) this;
        }

        public Criteria andBuContactaddresshopeidGreaterThan(Long value) {
            addCriterion("BU_ContactAddressHopeID >", value, "buContactaddresshopeid");
            return (Criteria) this;
        }

        public Criteria andBuContactaddresshopeidGreaterThanOrEqualTo(Long value) {
            addCriterion("BU_ContactAddressHopeID >=", value, "buContactaddresshopeid");
            return (Criteria) this;
        }

        public Criteria andBuContactaddresshopeidLessThan(Long value) {
            addCriterion("BU_ContactAddressHopeID <", value, "buContactaddresshopeid");
            return (Criteria) this;
        }

        public Criteria andBuContactaddresshopeidLessThanOrEqualTo(Long value) {
            addCriterion("BU_ContactAddressHopeID <=", value, "buContactaddresshopeid");
            return (Criteria) this;
        }

        public Criteria andBuContactaddresshopeidIn(List<Long> values) {
            addCriterion("BU_ContactAddressHopeID in", values, "buContactaddresshopeid");
            return (Criteria) this;
        }

        public Criteria andBuContactaddresshopeidNotIn(List<Long> values) {
            addCriterion("BU_ContactAddressHopeID not in", values, "buContactaddresshopeid");
            return (Criteria) this;
        }

        public Criteria andBuContactaddresshopeidBetween(Long value1, Long value2) {
            addCriterion("BU_ContactAddressHopeID between", value1, value2, "buContactaddresshopeid");
            return (Criteria) this;
        }

        public Criteria andBuContactaddresshopeidNotBetween(Long value1, Long value2) {
            addCriterion("BU_ContactAddressHopeID not between", value1, value2, "buContactaddresshopeid");
            return (Criteria) this;
        }

        public Criteria andBuCompanyaddressidIsNull() {
            addCriterion("BU_CompanyAddressID is null");
            return (Criteria) this;
        }

        public Criteria andBuCompanyaddressidIsNotNull() {
            addCriterion("BU_CompanyAddressID is not null");
            return (Criteria) this;
        }

        public Criteria andBuCompanyaddressidEqualTo(Long value) {
            addCriterion("BU_CompanyAddressID =", value, "buCompanyaddressid");
            return (Criteria) this;
        }

        public Criteria andBuCompanyaddressidNotEqualTo(Long value) {
            addCriterion("BU_CompanyAddressID <>", value, "buCompanyaddressid");
            return (Criteria) this;
        }

        public Criteria andBuCompanyaddressidGreaterThan(Long value) {
            addCriterion("BU_CompanyAddressID >", value, "buCompanyaddressid");
            return (Criteria) this;
        }

        public Criteria andBuCompanyaddressidGreaterThanOrEqualTo(Long value) {
            addCriterion("BU_CompanyAddressID >=", value, "buCompanyaddressid");
            return (Criteria) this;
        }

        public Criteria andBuCompanyaddressidLessThan(Long value) {
            addCriterion("BU_CompanyAddressID <", value, "buCompanyaddressid");
            return (Criteria) this;
        }

        public Criteria andBuCompanyaddressidLessThanOrEqualTo(Long value) {
            addCriterion("BU_CompanyAddressID <=", value, "buCompanyaddressid");
            return (Criteria) this;
        }

        public Criteria andBuCompanyaddressidIn(List<Long> values) {
            addCriterion("BU_CompanyAddressID in", values, "buCompanyaddressid");
            return (Criteria) this;
        }

        public Criteria andBuCompanyaddressidNotIn(List<Long> values) {
            addCriterion("BU_CompanyAddressID not in", values, "buCompanyaddressid");
            return (Criteria) this;
        }

        public Criteria andBuCompanyaddressidBetween(Long value1, Long value2) {
            addCriterion("BU_CompanyAddressID between", value1, value2, "buCompanyaddressid");
            return (Criteria) this;
        }

        public Criteria andBuCompanyaddressidNotBetween(Long value1, Long value2) {
            addCriterion("BU_CompanyAddressID not between", value1, value2, "buCompanyaddressid");
            return (Criteria) this;
        }

        public Criteria andBuHomeaddressidIsNull() {
            addCriterion("BU_HomeAddressID is null");
            return (Criteria) this;
        }

        public Criteria andBuHomeaddressidIsNotNull() {
            addCriterion("BU_HomeAddressID is not null");
            return (Criteria) this;
        }

        public Criteria andBuHomeaddressidEqualTo(Long value) {
            addCriterion("BU_HomeAddressID =", value, "buHomeaddressid");
            return (Criteria) this;
        }

        public Criteria andBuHomeaddressidNotEqualTo(Long value) {
            addCriterion("BU_HomeAddressID <>", value, "buHomeaddressid");
            return (Criteria) this;
        }

        public Criteria andBuHomeaddressidGreaterThan(Long value) {
            addCriterion("BU_HomeAddressID >", value, "buHomeaddressid");
            return (Criteria) this;
        }

        public Criteria andBuHomeaddressidGreaterThanOrEqualTo(Long value) {
            addCriterion("BU_HomeAddressID >=", value, "buHomeaddressid");
            return (Criteria) this;
        }

        public Criteria andBuHomeaddressidLessThan(Long value) {
            addCriterion("BU_HomeAddressID <", value, "buHomeaddressid");
            return (Criteria) this;
        }

        public Criteria andBuHomeaddressidLessThanOrEqualTo(Long value) {
            addCriterion("BU_HomeAddressID <=", value, "buHomeaddressid");
            return (Criteria) this;
        }

        public Criteria andBuHomeaddressidIn(List<Long> values) {
            addCriterion("BU_HomeAddressID in", values, "buHomeaddressid");
            return (Criteria) this;
        }

        public Criteria andBuHomeaddressidNotIn(List<Long> values) {
            addCriterion("BU_HomeAddressID not in", values, "buHomeaddressid");
            return (Criteria) this;
        }

        public Criteria andBuHomeaddressidBetween(Long value1, Long value2) {
            addCriterion("BU_HomeAddressID between", value1, value2, "buHomeaddressid");
            return (Criteria) this;
        }

        public Criteria andBuHomeaddressidNotBetween(Long value1, Long value2) {
            addCriterion("BU_HomeAddressID not between", value1, value2, "buHomeaddressid");
            return (Criteria) this;
        }

        public Criteria andBuNationalityIsNull() {
            addCriterion("BU_Nationality is null");
            return (Criteria) this;
        }

        public Criteria andBuNationalityIsNotNull() {
            addCriterion("BU_Nationality is not null");
            return (Criteria) this;
        }

        public Criteria andBuNationalityEqualTo(Integer value) {
            addCriterion("BU_Nationality =", value, "buNationality");
            return (Criteria) this;
        }

        public Criteria andBuNationalityNotEqualTo(Integer value) {
            addCriterion("BU_Nationality <>", value, "buNationality");
            return (Criteria) this;
        }

        public Criteria andBuNationalityGreaterThan(Integer value) {
            addCriterion("BU_Nationality >", value, "buNationality");
            return (Criteria) this;
        }

        public Criteria andBuNationalityGreaterThanOrEqualTo(Integer value) {
            addCriterion("BU_Nationality >=", value, "buNationality");
            return (Criteria) this;
        }

        public Criteria andBuNationalityLessThan(Integer value) {
            addCriterion("BU_Nationality <", value, "buNationality");
            return (Criteria) this;
        }

        public Criteria andBuNationalityLessThanOrEqualTo(Integer value) {
            addCriterion("BU_Nationality <=", value, "buNationality");
            return (Criteria) this;
        }

        public Criteria andBuNationalityIn(List<Integer> values) {
            addCriterion("BU_Nationality in", values, "buNationality");
            return (Criteria) this;
        }

        public Criteria andBuNationalityNotIn(List<Integer> values) {
            addCriterion("BU_Nationality not in", values, "buNationality");
            return (Criteria) this;
        }

        public Criteria andBuNationalityBetween(Integer value1, Integer value2) {
            addCriterion("BU_Nationality between", value1, value2, "buNationality");
            return (Criteria) this;
        }

        public Criteria andBuNationalityNotBetween(Integer value1, Integer value2) {
            addCriterion("BU_Nationality not between", value1, value2, "buNationality");
            return (Criteria) this;
        }

        public Criteria andBuLanguagehopeIsNull() {
            addCriterion("BU_LanguageHope is null");
            return (Criteria) this;
        }

        public Criteria andBuLanguagehopeIsNotNull() {
            addCriterion("BU_LanguageHope is not null");
            return (Criteria) this;
        }

        public Criteria andBuLanguagehopeEqualTo(Integer value) {
            addCriterion("BU_LanguageHope =", value, "buLanguagehope");
            return (Criteria) this;
        }

        public Criteria andBuLanguagehopeNotEqualTo(Integer value) {
            addCriterion("BU_LanguageHope <>", value, "buLanguagehope");
            return (Criteria) this;
        }

        public Criteria andBuLanguagehopeGreaterThan(Integer value) {
            addCriterion("BU_LanguageHope >", value, "buLanguagehope");
            return (Criteria) this;
        }

        public Criteria andBuLanguagehopeGreaterThanOrEqualTo(Integer value) {
            addCriterion("BU_LanguageHope >=", value, "buLanguagehope");
            return (Criteria) this;
        }

        public Criteria andBuLanguagehopeLessThan(Integer value) {
            addCriterion("BU_LanguageHope <", value, "buLanguagehope");
            return (Criteria) this;
        }

        public Criteria andBuLanguagehopeLessThanOrEqualTo(Integer value) {
            addCriterion("BU_LanguageHope <=", value, "buLanguagehope");
            return (Criteria) this;
        }

        public Criteria andBuLanguagehopeIn(List<Integer> values) {
            addCriterion("BU_LanguageHope in", values, "buLanguagehope");
            return (Criteria) this;
        }

        public Criteria andBuLanguagehopeNotIn(List<Integer> values) {
            addCriterion("BU_LanguageHope not in", values, "buLanguagehope");
            return (Criteria) this;
        }

        public Criteria andBuLanguagehopeBetween(Integer value1, Integer value2) {
            addCriterion("BU_LanguageHope between", value1, value2, "buLanguagehope");
            return (Criteria) this;
        }

        public Criteria andBuLanguagehopeNotBetween(Integer value1, Integer value2) {
            addCriterion("BU_LanguageHope not between", value1, value2, "buLanguagehope");
            return (Criteria) this;
        }

        public Criteria andBuWechatIsNull() {
            addCriterion("BU_WeChat is null");
            return (Criteria) this;
        }

        public Criteria andBuWechatIsNotNull() {
            addCriterion("BU_WeChat is not null");
            return (Criteria) this;
        }

        public Criteria andBuWechatEqualTo(String value) {
            addCriterion("BU_WeChat =", value, "buWechat");
            return (Criteria) this;
        }

        public Criteria andBuWechatNotEqualTo(String value) {
            addCriterion("BU_WeChat <>", value, "buWechat");
            return (Criteria) this;
        }

        public Criteria andBuWechatGreaterThan(String value) {
            addCriterion("BU_WeChat >", value, "buWechat");
            return (Criteria) this;
        }

        public Criteria andBuWechatGreaterThanOrEqualTo(String value) {
            addCriterion("BU_WeChat >=", value, "buWechat");
            return (Criteria) this;
        }

        public Criteria andBuWechatLessThan(String value) {
            addCriterion("BU_WeChat <", value, "buWechat");
            return (Criteria) this;
        }

        public Criteria andBuWechatLessThanOrEqualTo(String value) {
            addCriterion("BU_WeChat <=", value, "buWechat");
            return (Criteria) this;
        }

        public Criteria andBuWechatLike(String value) {
            addCriterion("BU_WeChat like", value, "buWechat");
            return (Criteria) this;
        }

        public Criteria andBuWechatNotLike(String value) {
            addCriterion("BU_WeChat not like", value, "buWechat");
            return (Criteria) this;
        }

        public Criteria andBuWechatIn(List<String> values) {
            addCriterion("BU_WeChat in", values, "buWechat");
            return (Criteria) this;
        }

        public Criteria andBuWechatNotIn(List<String> values) {
            addCriterion("BU_WeChat not in", values, "buWechat");
            return (Criteria) this;
        }

        public Criteria andBuWechatBetween(String value1, String value2) {
            addCriterion("BU_WeChat between", value1, value2, "buWechat");
            return (Criteria) this;
        }

        public Criteria andBuWechatNotBetween(String value1, String value2) {
            addCriterion("BU_WeChat not between", value1, value2, "buWechat");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}