package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Userrolerelation extends BaseEntity {


    private Long urrUsertid;

    private Long urrRoletid;



    public Long getUrrUsertid() {
        return urrUsertid;
    }

    public void setUrrUsertid(Long urrUsertid) {
        this.urrUsertid = urrUsertid;
    }

    public Long getUrrRoletid() {
        return urrRoletid;
    }

    public void setUrrRoletid(Long urrRoletid) {
        this.urrRoletid = urrRoletid;
    }
}