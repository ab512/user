package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Depositrecord extends BaseEntity {


    private Long prLaorgtid;

    private Float prDeposit;

    private Date prPaytime;

    private String prPaybank;

    private String prRegarding;

    private String prBankorderid;

    private Integer prIsreceipt;

    private Long prIspaid;

    private String prCity;

    private String prArea;

    private String prMailaddress;

    private String prContacts;

    private String prPhone;

    private String prMailtype;



    public Long getPrLaorgtid() {
        return prLaorgtid;
    }

    public void setPrLaorgtid(Long prLaorgtid) {
        this.prLaorgtid = prLaorgtid;
    }

    public Float getPrDeposit() {
        return prDeposit;
    }

    public void setPrDeposit(Float prDeposit) {
        this.prDeposit = prDeposit;
    }

    public Date getPrPaytime() {
        return prPaytime;
    }

    public void setPrPaytime(Date prPaytime) {
        this.prPaytime = prPaytime;
    }

    public String getPrPaybank() {
        return prPaybank;
    }

    public void setPrPaybank(String prPaybank) {
        this.prPaybank = prPaybank == null ? null : prPaybank.trim();
    }

    public String getPrRegarding() {
        return prRegarding;
    }

    public void setPrRegarding(String prRegarding) {
        this.prRegarding = prRegarding == null ? null : prRegarding.trim();
    }

    public String getPrBankorderid() {
        return prBankorderid;
    }

    public void setPrBankorderid(String prBankorderid) {
        this.prBankorderid = prBankorderid == null ? null : prBankorderid.trim();
    }

    public Integer getPrIsreceipt() {
        return prIsreceipt;
    }

    public void setPrIsreceipt(Integer prIsreceipt) {
        this.prIsreceipt = prIsreceipt;
    }

    public Long getPrIspaid() {
        return prIspaid;
    }

    public void setPrIspaid(Long prIspaid) {
        this.prIspaid = prIspaid;
    }

    public String getPrCity() {
        return prCity;
    }

    public void setPrCity(String prCity) {
        this.prCity = prCity == null ? null : prCity.trim();
    }

    public String getPrArea() {
        return prArea;
    }

    public void setPrArea(String prArea) {
        this.prArea = prArea == null ? null : prArea.trim();
    }

    public String getPrMailaddress() {
        return prMailaddress;
    }

    public void setPrMailaddress(String prMailaddress) {
        this.prMailaddress = prMailaddress == null ? null : prMailaddress.trim();
    }

    public String getPrContacts() {
        return prContacts;
    }

    public void setPrContacts(String prContacts) {
        this.prContacts = prContacts == null ? null : prContacts.trim();
    }

    public String getPrPhone() {
        return prPhone;
    }

    public void setPrPhone(String prPhone) {
        this.prPhone = prPhone == null ? null : prPhone.trim();
    }

    public String getPrMailtype() {
        return prMailtype;
    }

    public void setPrMailtype(String prMailtype) {
        this.prMailtype = prMailtype == null ? null : prMailtype.trim();
    }
}