package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Userfrepassenger extends BaseEntity {


    private Long ufpLausertid;

    private String ufpFirstnamecn;

    private String ufpSecondnamecn;

    private String ufpFirstnameen;

    private String ufpSecondnameen;

    private Integer ufpIntegral;

    private String ufpVipcardnum;

    private Integer ufpViplevel;

    private Date ufpLevelupdatetime;

    private Integer ufpVipstatus;

    private String ufpMobile2;

    private String ufpMobile3;

    private String ufpReferrer;



    public Long getUfpLausertid() {
        return ufpLausertid;
    }

    public void setUfpLausertid(Long ufpLausertid) {
        this.ufpLausertid = ufpLausertid;
    }

    public String getUfpFirstnamecn() {
        return ufpFirstnamecn;
    }

    public void setUfpFirstnamecn(String ufpFirstnamecn) {
        this.ufpFirstnamecn = ufpFirstnamecn == null ? null : ufpFirstnamecn.trim();
    }

    public String getUfpSecondnamecn() {
        return ufpSecondnamecn;
    }

    public void setUfpSecondnamecn(String ufpSecondnamecn) {
        this.ufpSecondnamecn = ufpSecondnamecn == null ? null : ufpSecondnamecn.trim();
    }

    public String getUfpFirstnameen() {
        return ufpFirstnameen;
    }

    public void setUfpFirstnameen(String ufpFirstnameen) {
        this.ufpFirstnameen = ufpFirstnameen == null ? null : ufpFirstnameen.trim();
    }

    public String getUfpSecondnameen() {
        return ufpSecondnameen;
    }

    public void setUfpSecondnameen(String ufpSecondnameen) {
        this.ufpSecondnameen = ufpSecondnameen == null ? null : ufpSecondnameen.trim();
    }

    public Integer getUfpIntegral() {
        return ufpIntegral;
    }

    public void setUfpIntegral(Integer ufpIntegral) {
        this.ufpIntegral = ufpIntegral;
    }

    public String getUfpVipcardnum() {
        return ufpVipcardnum;
    }

    public void setUfpVipcardnum(String ufpVipcardnum) {
        this.ufpVipcardnum = ufpVipcardnum == null ? null : ufpVipcardnum.trim();
    }

    public Integer getUfpViplevel() {
        return ufpViplevel;
    }

    public void setUfpViplevel(Integer ufpViplevel) {
        this.ufpViplevel = ufpViplevel;
    }

    public Date getUfpLevelupdatetime() {
        return ufpLevelupdatetime;
    }

    public void setUfpLevelupdatetime(Date ufpLevelupdatetime) {
        this.ufpLevelupdatetime = ufpLevelupdatetime;
    }

    public Integer getUfpVipstatus() {
        return ufpVipstatus;
    }

    public void setUfpVipstatus(Integer ufpVipstatus) {
        this.ufpVipstatus = ufpVipstatus;
    }

    public String getUfpMobile2() {
        return ufpMobile2;
    }

    public void setUfpMobile2(String ufpMobile2) {
        this.ufpMobile2 = ufpMobile2 == null ? null : ufpMobile2.trim();
    }

    public String getUfpMobile3() {
        return ufpMobile3;
    }

    public void setUfpMobile3(String ufpMobile3) {
        this.ufpMobile3 = ufpMobile3 == null ? null : ufpMobile3.trim();
    }

    public String getUfpReferrer() {
        return ufpReferrer;
    }

    public void setUfpReferrer(String ufpReferrer) {
        this.ufpReferrer = ufpReferrer == null ? null : ufpReferrer.trim();
    }
}