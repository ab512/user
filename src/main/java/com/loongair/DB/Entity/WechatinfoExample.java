package com.loongair.DB.Entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WechatinfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public WechatinfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTidIsNull() {
            addCriterion("Tid is null");
            return (Criteria) this;
        }

        public Criteria andTidIsNotNull() {
            addCriterion("Tid is not null");
            return (Criteria) this;
        }

        public Criteria andTidEqualTo(Long value) {
            addCriterion("Tid =", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotEqualTo(Long value) {
            addCriterion("Tid <>", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThan(Long value) {
            addCriterion("Tid >", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThanOrEqualTo(Long value) {
            addCriterion("Tid >=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThan(Long value) {
            addCriterion("Tid <", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThanOrEqualTo(Long value) {
            addCriterion("Tid <=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidIn(List<Long> values) {
            addCriterion("Tid in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotIn(List<Long> values) {
            addCriterion("Tid not in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidBetween(Long value1, Long value2) {
            addCriterion("Tid between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotBetween(Long value1, Long value2) {
            addCriterion("Tid not between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNull() {
            addCriterion("CreateId is null");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNotNull() {
            addCriterion("CreateId is not null");
            return (Criteria) this;
        }

        public Criteria andCreateidEqualTo(Long value) {
            addCriterion("CreateId =", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotEqualTo(Long value) {
            addCriterion("CreateId <>", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThan(Long value) {
            addCriterion("CreateId >", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThanOrEqualTo(Long value) {
            addCriterion("CreateId >=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThan(Long value) {
            addCriterion("CreateId <", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThanOrEqualTo(Long value) {
            addCriterion("CreateId <=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidIn(List<Long> values) {
            addCriterion("CreateId in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotIn(List<Long> values) {
            addCriterion("CreateId not in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidBetween(Long value1, Long value2) {
            addCriterion("CreateId between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotBetween(Long value1, Long value2) {
            addCriterion("CreateId not between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNull() {
            addCriterion("ProxyUserId is null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNotNull() {
            addCriterion("ProxyUserId is not null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridEqualTo(Long value) {
            addCriterion("ProxyUserId =", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotEqualTo(Long value) {
            addCriterion("ProxyUserId <>", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThan(Long value) {
            addCriterion("ProxyUserId >", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId >=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThan(Long value) {
            addCriterion("ProxyUserId <", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId <=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIn(List<Long> values) {
            addCriterion("ProxyUserId in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotIn(List<Long> values) {
            addCriterion("ProxyUserId not in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId not between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNull() {
            addCriterion("LastGuid is null");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNotNull() {
            addCriterion("LastGuid is not null");
            return (Criteria) this;
        }

        public Criteria andLastguidEqualTo(Long value) {
            addCriterion("LastGuid =", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotEqualTo(Long value) {
            addCriterion("LastGuid <>", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThan(Long value) {
            addCriterion("LastGuid >", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThanOrEqualTo(Long value) {
            addCriterion("LastGuid >=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThan(Long value) {
            addCriterion("LastGuid <", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThanOrEqualTo(Long value) {
            addCriterion("LastGuid <=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidIn(List<Long> values) {
            addCriterion("LastGuid in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotIn(List<Long> values) {
            addCriterion("LastGuid not in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidBetween(Long value1, Long value2) {
            addCriterion("LastGuid between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotBetween(Long value1, Long value2) {
            addCriterion("LastGuid not between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andCloneIsNull() {
            addCriterion("Clone is null");
            return (Criteria) this;
        }

        public Criteria andCloneIsNotNull() {
            addCriterion("Clone is not null");
            return (Criteria) this;
        }

        public Criteria andCloneEqualTo(Integer value) {
            addCriterion("Clone =", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotEqualTo(Integer value) {
            addCriterion("Clone <>", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThan(Integer value) {
            addCriterion("Clone >", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThanOrEqualTo(Integer value) {
            addCriterion("Clone >=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThan(Integer value) {
            addCriterion("Clone <", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThanOrEqualTo(Integer value) {
            addCriterion("Clone <=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneIn(List<Integer> values) {
            addCriterion("Clone in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotIn(List<Integer> values) {
            addCriterion("Clone not in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneBetween(Integer value1, Integer value2) {
            addCriterion("Clone between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotBetween(Integer value1, Integer value2) {
            addCriterion("Clone not between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNull() {
            addCriterion("UpdateId is null");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNotNull() {
            addCriterion("UpdateId is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateidEqualTo(Long value) {
            addCriterion("UpdateId =", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotEqualTo(Long value) {
            addCriterion("UpdateId <>", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThan(Long value) {
            addCriterion("UpdateId >", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThanOrEqualTo(Long value) {
            addCriterion("UpdateId >=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThan(Long value) {
            addCriterion("UpdateId <", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThanOrEqualTo(Long value) {
            addCriterion("UpdateId <=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidIn(List<Long> values) {
            addCriterion("UpdateId in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotIn(List<Long> values) {
            addCriterion("UpdateId not in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidBetween(Long value1, Long value2) {
            addCriterion("UpdateId between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotBetween(Long value1, Long value2) {
            addCriterion("UpdateId not between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("UpdateDate is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("UpdateDate is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("UpdateDate =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("UpdateDate <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("UpdateDate >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("UpdateDate >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("UpdateDate <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("UpdateDate <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("UpdateDate in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("UpdateDate not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("UpdateDate between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("UpdateDate not between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("Status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("Status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("Status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("Status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("Status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("Status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("Status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("Status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("Status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("Status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("Status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("Status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andWciLausertidIsNull() {
            addCriterion("WCI_LaUserTid is null");
            return (Criteria) this;
        }

        public Criteria andWciLausertidIsNotNull() {
            addCriterion("WCI_LaUserTid is not null");
            return (Criteria) this;
        }

        public Criteria andWciLausertidEqualTo(Long value) {
            addCriterion("WCI_LaUserTid =", value, "wciLausertid");
            return (Criteria) this;
        }

        public Criteria andWciLausertidNotEqualTo(Long value) {
            addCriterion("WCI_LaUserTid <>", value, "wciLausertid");
            return (Criteria) this;
        }

        public Criteria andWciLausertidGreaterThan(Long value) {
            addCriterion("WCI_LaUserTid >", value, "wciLausertid");
            return (Criteria) this;
        }

        public Criteria andWciLausertidGreaterThanOrEqualTo(Long value) {
            addCriterion("WCI_LaUserTid >=", value, "wciLausertid");
            return (Criteria) this;
        }

        public Criteria andWciLausertidLessThan(Long value) {
            addCriterion("WCI_LaUserTid <", value, "wciLausertid");
            return (Criteria) this;
        }

        public Criteria andWciLausertidLessThanOrEqualTo(Long value) {
            addCriterion("WCI_LaUserTid <=", value, "wciLausertid");
            return (Criteria) this;
        }

        public Criteria andWciLausertidIn(List<Long> values) {
            addCriterion("WCI_LaUserTid in", values, "wciLausertid");
            return (Criteria) this;
        }

        public Criteria andWciLausertidNotIn(List<Long> values) {
            addCriterion("WCI_LaUserTid not in", values, "wciLausertid");
            return (Criteria) this;
        }

        public Criteria andWciLausertidBetween(Long value1, Long value2) {
            addCriterion("WCI_LaUserTid between", value1, value2, "wciLausertid");
            return (Criteria) this;
        }

        public Criteria andWciLausertidNotBetween(Long value1, Long value2) {
            addCriterion("WCI_LaUserTid not between", value1, value2, "wciLausertid");
            return (Criteria) this;
        }

        public Criteria andWciAppidIsNull() {
            addCriterion("WCI_AppID is null");
            return (Criteria) this;
        }

        public Criteria andWciAppidIsNotNull() {
            addCriterion("WCI_AppID is not null");
            return (Criteria) this;
        }

        public Criteria andWciAppidEqualTo(String value) {
            addCriterion("WCI_AppID =", value, "wciAppid");
            return (Criteria) this;
        }

        public Criteria andWciAppidNotEqualTo(String value) {
            addCriterion("WCI_AppID <>", value, "wciAppid");
            return (Criteria) this;
        }

        public Criteria andWciAppidGreaterThan(String value) {
            addCriterion("WCI_AppID >", value, "wciAppid");
            return (Criteria) this;
        }

        public Criteria andWciAppidGreaterThanOrEqualTo(String value) {
            addCriterion("WCI_AppID >=", value, "wciAppid");
            return (Criteria) this;
        }

        public Criteria andWciAppidLessThan(String value) {
            addCriterion("WCI_AppID <", value, "wciAppid");
            return (Criteria) this;
        }

        public Criteria andWciAppidLessThanOrEqualTo(String value) {
            addCriterion("WCI_AppID <=", value, "wciAppid");
            return (Criteria) this;
        }

        public Criteria andWciAppidLike(String value) {
            addCriterion("WCI_AppID like", value, "wciAppid");
            return (Criteria) this;
        }

        public Criteria andWciAppidNotLike(String value) {
            addCriterion("WCI_AppID not like", value, "wciAppid");
            return (Criteria) this;
        }

        public Criteria andWciAppidIn(List<String> values) {
            addCriterion("WCI_AppID in", values, "wciAppid");
            return (Criteria) this;
        }

        public Criteria andWciAppidNotIn(List<String> values) {
            addCriterion("WCI_AppID not in", values, "wciAppid");
            return (Criteria) this;
        }

        public Criteria andWciAppidBetween(String value1, String value2) {
            addCriterion("WCI_AppID between", value1, value2, "wciAppid");
            return (Criteria) this;
        }

        public Criteria andWciAppidNotBetween(String value1, String value2) {
            addCriterion("WCI_AppID not between", value1, value2, "wciAppid");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenIsNull() {
            addCriterion("WCI_AccessToken is null");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenIsNotNull() {
            addCriterion("WCI_AccessToken is not null");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenEqualTo(String value) {
            addCriterion("WCI_AccessToken =", value, "wciAccesstoken");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenNotEqualTo(String value) {
            addCriterion("WCI_AccessToken <>", value, "wciAccesstoken");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenGreaterThan(String value) {
            addCriterion("WCI_AccessToken >", value, "wciAccesstoken");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenGreaterThanOrEqualTo(String value) {
            addCriterion("WCI_AccessToken >=", value, "wciAccesstoken");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenLessThan(String value) {
            addCriterion("WCI_AccessToken <", value, "wciAccesstoken");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenLessThanOrEqualTo(String value) {
            addCriterion("WCI_AccessToken <=", value, "wciAccesstoken");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenLike(String value) {
            addCriterion("WCI_AccessToken like", value, "wciAccesstoken");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenNotLike(String value) {
            addCriterion("WCI_AccessToken not like", value, "wciAccesstoken");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenIn(List<String> values) {
            addCriterion("WCI_AccessToken in", values, "wciAccesstoken");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenNotIn(List<String> values) {
            addCriterion("WCI_AccessToken not in", values, "wciAccesstoken");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenBetween(String value1, String value2) {
            addCriterion("WCI_AccessToken between", value1, value2, "wciAccesstoken");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenNotBetween(String value1, String value2) {
            addCriterion("WCI_AccessToken not between", value1, value2, "wciAccesstoken");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenexpiresIsNull() {
            addCriterion("WCI_AccessTokenExpires is null");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenexpiresIsNotNull() {
            addCriterion("WCI_AccessTokenExpires is not null");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenexpiresEqualTo(Date value) {
            addCriterion("WCI_AccessTokenExpires =", value, "wciAccesstokenexpires");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenexpiresNotEqualTo(Date value) {
            addCriterion("WCI_AccessTokenExpires <>", value, "wciAccesstokenexpires");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenexpiresGreaterThan(Date value) {
            addCriterion("WCI_AccessTokenExpires >", value, "wciAccesstokenexpires");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenexpiresGreaterThanOrEqualTo(Date value) {
            addCriterion("WCI_AccessTokenExpires >=", value, "wciAccesstokenexpires");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenexpiresLessThan(Date value) {
            addCriterion("WCI_AccessTokenExpires <", value, "wciAccesstokenexpires");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenexpiresLessThanOrEqualTo(Date value) {
            addCriterion("WCI_AccessTokenExpires <=", value, "wciAccesstokenexpires");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenexpiresIn(List<Date> values) {
            addCriterion("WCI_AccessTokenExpires in", values, "wciAccesstokenexpires");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenexpiresNotIn(List<Date> values) {
            addCriterion("WCI_AccessTokenExpires not in", values, "wciAccesstokenexpires");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenexpiresBetween(Date value1, Date value2) {
            addCriterion("WCI_AccessTokenExpires between", value1, value2, "wciAccesstokenexpires");
            return (Criteria) this;
        }

        public Criteria andWciAccesstokenexpiresNotBetween(Date value1, Date value2) {
            addCriterion("WCI_AccessTokenExpires not between", value1, value2, "wciAccesstokenexpires");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenIsNull() {
            addCriterion("WCI_RefreshToken is null");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenIsNotNull() {
            addCriterion("WCI_RefreshToken is not null");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenEqualTo(String value) {
            addCriterion("WCI_RefreshToken =", value, "wciRefreshtoken");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenNotEqualTo(String value) {
            addCriterion("WCI_RefreshToken <>", value, "wciRefreshtoken");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenGreaterThan(String value) {
            addCriterion("WCI_RefreshToken >", value, "wciRefreshtoken");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenGreaterThanOrEqualTo(String value) {
            addCriterion("WCI_RefreshToken >=", value, "wciRefreshtoken");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenLessThan(String value) {
            addCriterion("WCI_RefreshToken <", value, "wciRefreshtoken");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenLessThanOrEqualTo(String value) {
            addCriterion("WCI_RefreshToken <=", value, "wciRefreshtoken");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenLike(String value) {
            addCriterion("WCI_RefreshToken like", value, "wciRefreshtoken");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenNotLike(String value) {
            addCriterion("WCI_RefreshToken not like", value, "wciRefreshtoken");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenIn(List<String> values) {
            addCriterion("WCI_RefreshToken in", values, "wciRefreshtoken");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenNotIn(List<String> values) {
            addCriterion("WCI_RefreshToken not in", values, "wciRefreshtoken");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenBetween(String value1, String value2) {
            addCriterion("WCI_RefreshToken between", value1, value2, "wciRefreshtoken");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenNotBetween(String value1, String value2) {
            addCriterion("WCI_RefreshToken not between", value1, value2, "wciRefreshtoken");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenexpiresIsNull() {
            addCriterion("WCI_RefreshTokenExpires is null");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenexpiresIsNotNull() {
            addCriterion("WCI_RefreshTokenExpires is not null");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenexpiresEqualTo(Date value) {
            addCriterion("WCI_RefreshTokenExpires =", value, "wciRefreshtokenexpires");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenexpiresNotEqualTo(Date value) {
            addCriterion("WCI_RefreshTokenExpires <>", value, "wciRefreshtokenexpires");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenexpiresGreaterThan(Date value) {
            addCriterion("WCI_RefreshTokenExpires >", value, "wciRefreshtokenexpires");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenexpiresGreaterThanOrEqualTo(Date value) {
            addCriterion("WCI_RefreshTokenExpires >=", value, "wciRefreshtokenexpires");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenexpiresLessThan(Date value) {
            addCriterion("WCI_RefreshTokenExpires <", value, "wciRefreshtokenexpires");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenexpiresLessThanOrEqualTo(Date value) {
            addCriterion("WCI_RefreshTokenExpires <=", value, "wciRefreshtokenexpires");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenexpiresIn(List<Date> values) {
            addCriterion("WCI_RefreshTokenExpires in", values, "wciRefreshtokenexpires");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenexpiresNotIn(List<Date> values) {
            addCriterion("WCI_RefreshTokenExpires not in", values, "wciRefreshtokenexpires");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenexpiresBetween(Date value1, Date value2) {
            addCriterion("WCI_RefreshTokenExpires between", value1, value2, "wciRefreshtokenexpires");
            return (Criteria) this;
        }

        public Criteria andWciRefreshtokenexpiresNotBetween(Date value1, Date value2) {
            addCriterion("WCI_RefreshTokenExpires not between", value1, value2, "wciRefreshtokenexpires");
            return (Criteria) this;
        }

        public Criteria andWciOpenidIsNull() {
            addCriterion("WCI_Openid is null");
            return (Criteria) this;
        }

        public Criteria andWciOpenidIsNotNull() {
            addCriterion("WCI_Openid is not null");
            return (Criteria) this;
        }

        public Criteria andWciOpenidEqualTo(String value) {
            addCriterion("WCI_Openid =", value, "wciOpenid");
            return (Criteria) this;
        }

        public Criteria andWciOpenidNotEqualTo(String value) {
            addCriterion("WCI_Openid <>", value, "wciOpenid");
            return (Criteria) this;
        }

        public Criteria andWciOpenidGreaterThan(String value) {
            addCriterion("WCI_Openid >", value, "wciOpenid");
            return (Criteria) this;
        }

        public Criteria andWciOpenidGreaterThanOrEqualTo(String value) {
            addCriterion("WCI_Openid >=", value, "wciOpenid");
            return (Criteria) this;
        }

        public Criteria andWciOpenidLessThan(String value) {
            addCriterion("WCI_Openid <", value, "wciOpenid");
            return (Criteria) this;
        }

        public Criteria andWciOpenidLessThanOrEqualTo(String value) {
            addCriterion("WCI_Openid <=", value, "wciOpenid");
            return (Criteria) this;
        }

        public Criteria andWciOpenidLike(String value) {
            addCriterion("WCI_Openid like", value, "wciOpenid");
            return (Criteria) this;
        }

        public Criteria andWciOpenidNotLike(String value) {
            addCriterion("WCI_Openid not like", value, "wciOpenid");
            return (Criteria) this;
        }

        public Criteria andWciOpenidIn(List<String> values) {
            addCriterion("WCI_Openid in", values, "wciOpenid");
            return (Criteria) this;
        }

        public Criteria andWciOpenidNotIn(List<String> values) {
            addCriterion("WCI_Openid not in", values, "wciOpenid");
            return (Criteria) this;
        }

        public Criteria andWciOpenidBetween(String value1, String value2) {
            addCriterion("WCI_Openid between", value1, value2, "wciOpenid");
            return (Criteria) this;
        }

        public Criteria andWciOpenidNotBetween(String value1, String value2) {
            addCriterion("WCI_Openid not between", value1, value2, "wciOpenid");
            return (Criteria) this;
        }

        public Criteria andWciScopeIsNull() {
            addCriterion("WCI_Scope is null");
            return (Criteria) this;
        }

        public Criteria andWciScopeIsNotNull() {
            addCriterion("WCI_Scope is not null");
            return (Criteria) this;
        }

        public Criteria andWciScopeEqualTo(String value) {
            addCriterion("WCI_Scope =", value, "wciScope");
            return (Criteria) this;
        }

        public Criteria andWciScopeNotEqualTo(String value) {
            addCriterion("WCI_Scope <>", value, "wciScope");
            return (Criteria) this;
        }

        public Criteria andWciScopeGreaterThan(String value) {
            addCriterion("WCI_Scope >", value, "wciScope");
            return (Criteria) this;
        }

        public Criteria andWciScopeGreaterThanOrEqualTo(String value) {
            addCriterion("WCI_Scope >=", value, "wciScope");
            return (Criteria) this;
        }

        public Criteria andWciScopeLessThan(String value) {
            addCriterion("WCI_Scope <", value, "wciScope");
            return (Criteria) this;
        }

        public Criteria andWciScopeLessThanOrEqualTo(String value) {
            addCriterion("WCI_Scope <=", value, "wciScope");
            return (Criteria) this;
        }

        public Criteria andWciScopeLike(String value) {
            addCriterion("WCI_Scope like", value, "wciScope");
            return (Criteria) this;
        }

        public Criteria andWciScopeNotLike(String value) {
            addCriterion("WCI_Scope not like", value, "wciScope");
            return (Criteria) this;
        }

        public Criteria andWciScopeIn(List<String> values) {
            addCriterion("WCI_Scope in", values, "wciScope");
            return (Criteria) this;
        }

        public Criteria andWciScopeNotIn(List<String> values) {
            addCriterion("WCI_Scope not in", values, "wciScope");
            return (Criteria) this;
        }

        public Criteria andWciScopeBetween(String value1, String value2) {
            addCriterion("WCI_Scope between", value1, value2, "wciScope");
            return (Criteria) this;
        }

        public Criteria andWciScopeNotBetween(String value1, String value2) {
            addCriterion("WCI_Scope not between", value1, value2, "wciScope");
            return (Criteria) this;
        }

        public Criteria andWciUnionidIsNull() {
            addCriterion("WCI_Unionid is null");
            return (Criteria) this;
        }

        public Criteria andWciUnionidIsNotNull() {
            addCriterion("WCI_Unionid is not null");
            return (Criteria) this;
        }

        public Criteria andWciUnionidEqualTo(String value) {
            addCriterion("WCI_Unionid =", value, "wciUnionid");
            return (Criteria) this;
        }

        public Criteria andWciUnionidNotEqualTo(String value) {
            addCriterion("WCI_Unionid <>", value, "wciUnionid");
            return (Criteria) this;
        }

        public Criteria andWciUnionidGreaterThan(String value) {
            addCriterion("WCI_Unionid >", value, "wciUnionid");
            return (Criteria) this;
        }

        public Criteria andWciUnionidGreaterThanOrEqualTo(String value) {
            addCriterion("WCI_Unionid >=", value, "wciUnionid");
            return (Criteria) this;
        }

        public Criteria andWciUnionidLessThan(String value) {
            addCriterion("WCI_Unionid <", value, "wciUnionid");
            return (Criteria) this;
        }

        public Criteria andWciUnionidLessThanOrEqualTo(String value) {
            addCriterion("WCI_Unionid <=", value, "wciUnionid");
            return (Criteria) this;
        }

        public Criteria andWciUnionidLike(String value) {
            addCriterion("WCI_Unionid like", value, "wciUnionid");
            return (Criteria) this;
        }

        public Criteria andWciUnionidNotLike(String value) {
            addCriterion("WCI_Unionid not like", value, "wciUnionid");
            return (Criteria) this;
        }

        public Criteria andWciUnionidIn(List<String> values) {
            addCriterion("WCI_Unionid in", values, "wciUnionid");
            return (Criteria) this;
        }

        public Criteria andWciUnionidNotIn(List<String> values) {
            addCriterion("WCI_Unionid not in", values, "wciUnionid");
            return (Criteria) this;
        }

        public Criteria andWciUnionidBetween(String value1, String value2) {
            addCriterion("WCI_Unionid between", value1, value2, "wciUnionid");
            return (Criteria) this;
        }

        public Criteria andWciUnionidNotBetween(String value1, String value2) {
            addCriterion("WCI_Unionid not between", value1, value2, "wciUnionid");
            return (Criteria) this;
        }

        public Criteria andWciLoginplatIsNull() {
            addCriterion("WCI_LoginPlat is null");
            return (Criteria) this;
        }

        public Criteria andWciLoginplatIsNotNull() {
            addCriterion("WCI_LoginPlat is not null");
            return (Criteria) this;
        }

        public Criteria andWciLoginplatEqualTo(Integer value) {
            addCriterion("WCI_LoginPlat =", value, "wciLoginplat");
            return (Criteria) this;
        }

        public Criteria andWciLoginplatNotEqualTo(Integer value) {
            addCriterion("WCI_LoginPlat <>", value, "wciLoginplat");
            return (Criteria) this;
        }

        public Criteria andWciLoginplatGreaterThan(Integer value) {
            addCriterion("WCI_LoginPlat >", value, "wciLoginplat");
            return (Criteria) this;
        }

        public Criteria andWciLoginplatGreaterThanOrEqualTo(Integer value) {
            addCriterion("WCI_LoginPlat >=", value, "wciLoginplat");
            return (Criteria) this;
        }

        public Criteria andWciLoginplatLessThan(Integer value) {
            addCriterion("WCI_LoginPlat <", value, "wciLoginplat");
            return (Criteria) this;
        }

        public Criteria andWciLoginplatLessThanOrEqualTo(Integer value) {
            addCriterion("WCI_LoginPlat <=", value, "wciLoginplat");
            return (Criteria) this;
        }

        public Criteria andWciLoginplatIn(List<Integer> values) {
            addCriterion("WCI_LoginPlat in", values, "wciLoginplat");
            return (Criteria) this;
        }

        public Criteria andWciLoginplatNotIn(List<Integer> values) {
            addCriterion("WCI_LoginPlat not in", values, "wciLoginplat");
            return (Criteria) this;
        }

        public Criteria andWciLoginplatBetween(Integer value1, Integer value2) {
            addCriterion("WCI_LoginPlat between", value1, value2, "wciLoginplat");
            return (Criteria) this;
        }

        public Criteria andWciLoginplatNotBetween(Integer value1, Integer value2) {
            addCriterion("WCI_LoginPlat not between", value1, value2, "wciLoginplat");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}