package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Bcvisitrecord extends BaseEntity {


    private String cvSerialnumber;

    private String cvBccode;

    private String cvBcname;

    private Integer obcCustomertype;

    private Date cvVisittime;

    private String cvContactpeople;

    private String cvPhonenumber;

    private String cvVisitrecord;

    private String cvFeedbackproblem;

    private String cvResourceinfo;

    private String cvVisitpeople;

    private Integer cvResult;

    private String cvAgentcompanyname;

    private String cvOfficecode;

    private String cvIatano;



    public String getCvSerialnumber() {
        return cvSerialnumber;
    }

    public void setCvSerialnumber(String cvSerialnumber) {
        this.cvSerialnumber = cvSerialnumber == null ? null : cvSerialnumber.trim();
    }

    public String getCvBccode() {
        return cvBccode;
    }

    public void setCvBccode(String cvBccode) {
        this.cvBccode = cvBccode == null ? null : cvBccode.trim();
    }

    public String getCvBcname() {
        return cvBcname;
    }

    public void setCvBcname(String cvBcname) {
        this.cvBcname = cvBcname == null ? null : cvBcname.trim();
    }

    public Integer getObcCustomertype() {
        return obcCustomertype;
    }

    public void setObcCustomertype(Integer obcCustomertype) {
        this.obcCustomertype = obcCustomertype;
    }

    public Date getCvVisittime() {
        return cvVisittime;
    }

    public void setCvVisittime(Date cvVisittime) {
        this.cvVisittime = cvVisittime;
    }

    public String getCvContactpeople() {
        return cvContactpeople;
    }

    public void setCvContactpeople(String cvContactpeople) {
        this.cvContactpeople = cvContactpeople == null ? null : cvContactpeople.trim();
    }

    public String getCvPhonenumber() {
        return cvPhonenumber;
    }

    public void setCvPhonenumber(String cvPhonenumber) {
        this.cvPhonenumber = cvPhonenumber == null ? null : cvPhonenumber.trim();
    }

    public String getCvVisitrecord() {
        return cvVisitrecord;
    }

    public void setCvVisitrecord(String cvVisitrecord) {
        this.cvVisitrecord = cvVisitrecord == null ? null : cvVisitrecord.trim();
    }

    public String getCvFeedbackproblem() {
        return cvFeedbackproblem;
    }

    public void setCvFeedbackproblem(String cvFeedbackproblem) {
        this.cvFeedbackproblem = cvFeedbackproblem == null ? null : cvFeedbackproblem.trim();
    }

    public String getCvResourceinfo() {
        return cvResourceinfo;
    }

    public void setCvResourceinfo(String cvResourceinfo) {
        this.cvResourceinfo = cvResourceinfo == null ? null : cvResourceinfo.trim();
    }

    public String getCvVisitpeople() {
        return cvVisitpeople;
    }

    public void setCvVisitpeople(String cvVisitpeople) {
        this.cvVisitpeople = cvVisitpeople == null ? null : cvVisitpeople.trim();
    }

    public Integer getCvResult() {
        return cvResult;
    }

    public void setCvResult(Integer cvResult) {
        this.cvResult = cvResult;
    }

    public String getCvAgentcompanyname() {
        return cvAgentcompanyname;
    }

    public void setCvAgentcompanyname(String cvAgentcompanyname) {
        this.cvAgentcompanyname = cvAgentcompanyname == null ? null : cvAgentcompanyname.trim();
    }

    public String getCvOfficecode() {
        return cvOfficecode;
    }

    public void setCvOfficecode(String cvOfficecode) {
        this.cvOfficecode = cvOfficecode == null ? null : cvOfficecode.trim();
    }

    public String getCvIatano() {
        return cvIatano;
    }

    public void setCvIatano(String cvIatano) {
        this.cvIatano = cvIatano == null ? null : cvIatano.trim();
    }
}