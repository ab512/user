package com.loongair.DB.Entity;

public class OrgagentWithBLOBs extends Orgagent {
    private String acLicenseaddress;

    private String acNote;

    public String getAcLicenseaddress() {
        return acLicenseaddress;
    }

    public void setAcLicenseaddress(String acLicenseaddress) {
        this.acLicenseaddress = acLicenseaddress == null ? null : acLicenseaddress.trim();
    }

    public String getAcNote() {
        return acNote;
    }

    public void setAcNote(String acNote) {
        this.acNote = acNote == null ? null : acNote.trim();
    }
}