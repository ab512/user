package com.loongair.DB.Entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrgagentExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public OrgagentExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTidIsNull() {
            addCriterion("Tid is null");
            return (Criteria) this;
        }

        public Criteria andTidIsNotNull() {
            addCriterion("Tid is not null");
            return (Criteria) this;
        }

        public Criteria andTidEqualTo(Long value) {
            addCriterion("Tid =", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotEqualTo(Long value) {
            addCriterion("Tid <>", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThan(Long value) {
            addCriterion("Tid >", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThanOrEqualTo(Long value) {
            addCriterion("Tid >=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThan(Long value) {
            addCriterion("Tid <", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThanOrEqualTo(Long value) {
            addCriterion("Tid <=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidIn(List<Long> values) {
            addCriterion("Tid in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotIn(List<Long> values) {
            addCriterion("Tid not in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidBetween(Long value1, Long value2) {
            addCriterion("Tid between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotBetween(Long value1, Long value2) {
            addCriterion("Tid not between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNull() {
            addCriterion("CreateId is null");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNotNull() {
            addCriterion("CreateId is not null");
            return (Criteria) this;
        }

        public Criteria andCreateidEqualTo(Long value) {
            addCriterion("CreateId =", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotEqualTo(Long value) {
            addCriterion("CreateId <>", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThan(Long value) {
            addCriterion("CreateId >", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThanOrEqualTo(Long value) {
            addCriterion("CreateId >=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThan(Long value) {
            addCriterion("CreateId <", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThanOrEqualTo(Long value) {
            addCriterion("CreateId <=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidIn(List<Long> values) {
            addCriterion("CreateId in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotIn(List<Long> values) {
            addCriterion("CreateId not in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidBetween(Long value1, Long value2) {
            addCriterion("CreateId between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotBetween(Long value1, Long value2) {
            addCriterion("CreateId not between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNull() {
            addCriterion("ProxyUserId is null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNotNull() {
            addCriterion("ProxyUserId is not null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridEqualTo(Long value) {
            addCriterion("ProxyUserId =", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotEqualTo(Long value) {
            addCriterion("ProxyUserId <>", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThan(Long value) {
            addCriterion("ProxyUserId >", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId >=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThan(Long value) {
            addCriterion("ProxyUserId <", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId <=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIn(List<Long> values) {
            addCriterion("ProxyUserId in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotIn(List<Long> values) {
            addCriterion("ProxyUserId not in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId not between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNull() {
            addCriterion("LastGuid is null");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNotNull() {
            addCriterion("LastGuid is not null");
            return (Criteria) this;
        }

        public Criteria andLastguidEqualTo(Long value) {
            addCriterion("LastGuid =", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotEqualTo(Long value) {
            addCriterion("LastGuid <>", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThan(Long value) {
            addCriterion("LastGuid >", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThanOrEqualTo(Long value) {
            addCriterion("LastGuid >=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThan(Long value) {
            addCriterion("LastGuid <", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThanOrEqualTo(Long value) {
            addCriterion("LastGuid <=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidIn(List<Long> values) {
            addCriterion("LastGuid in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotIn(List<Long> values) {
            addCriterion("LastGuid not in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidBetween(Long value1, Long value2) {
            addCriterion("LastGuid between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotBetween(Long value1, Long value2) {
            addCriterion("LastGuid not between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andCloneIsNull() {
            addCriterion("Clone is null");
            return (Criteria) this;
        }

        public Criteria andCloneIsNotNull() {
            addCriterion("Clone is not null");
            return (Criteria) this;
        }

        public Criteria andCloneEqualTo(Integer value) {
            addCriterion("Clone =", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotEqualTo(Integer value) {
            addCriterion("Clone <>", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThan(Integer value) {
            addCriterion("Clone >", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThanOrEqualTo(Integer value) {
            addCriterion("Clone >=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThan(Integer value) {
            addCriterion("Clone <", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThanOrEqualTo(Integer value) {
            addCriterion("Clone <=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneIn(List<Integer> values) {
            addCriterion("Clone in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotIn(List<Integer> values) {
            addCriterion("Clone not in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneBetween(Integer value1, Integer value2) {
            addCriterion("Clone between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotBetween(Integer value1, Integer value2) {
            addCriterion("Clone not between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNull() {
            addCriterion("UpdateId is null");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNotNull() {
            addCriterion("UpdateId is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateidEqualTo(Long value) {
            addCriterion("UpdateId =", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotEqualTo(Long value) {
            addCriterion("UpdateId <>", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThan(Long value) {
            addCriterion("UpdateId >", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThanOrEqualTo(Long value) {
            addCriterion("UpdateId >=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThan(Long value) {
            addCriterion("UpdateId <", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThanOrEqualTo(Long value) {
            addCriterion("UpdateId <=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidIn(List<Long> values) {
            addCriterion("UpdateId in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotIn(List<Long> values) {
            addCriterion("UpdateId not in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidBetween(Long value1, Long value2) {
            addCriterion("UpdateId between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotBetween(Long value1, Long value2) {
            addCriterion("UpdateId not between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("UpdateDate is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("UpdateDate is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("UpdateDate =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("UpdateDate <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("UpdateDate >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("UpdateDate >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("UpdateDate <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("UpdateDate <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("UpdateDate in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("UpdateDate not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("UpdateDate between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("UpdateDate not between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("Status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("Status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("Status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("Status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("Status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("Status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("Status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("Status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("Status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("Status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("Status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("Status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andOaLaorgtidIsNull() {
            addCriterion("OA_LaOrgTid is null");
            return (Criteria) this;
        }

        public Criteria andOaLaorgtidIsNotNull() {
            addCriterion("OA_LaOrgTid is not null");
            return (Criteria) this;
        }

        public Criteria andOaLaorgtidEqualTo(Long value) {
            addCriterion("OA_LaOrgTid =", value, "oaLaorgtid");
            return (Criteria) this;
        }

        public Criteria andOaLaorgtidNotEqualTo(Long value) {
            addCriterion("OA_LaOrgTid <>", value, "oaLaorgtid");
            return (Criteria) this;
        }

        public Criteria andOaLaorgtidGreaterThan(Long value) {
            addCriterion("OA_LaOrgTid >", value, "oaLaorgtid");
            return (Criteria) this;
        }

        public Criteria andOaLaorgtidGreaterThanOrEqualTo(Long value) {
            addCriterion("OA_LaOrgTid >=", value, "oaLaorgtid");
            return (Criteria) this;
        }

        public Criteria andOaLaorgtidLessThan(Long value) {
            addCriterion("OA_LaOrgTid <", value, "oaLaorgtid");
            return (Criteria) this;
        }

        public Criteria andOaLaorgtidLessThanOrEqualTo(Long value) {
            addCriterion("OA_LaOrgTid <=", value, "oaLaorgtid");
            return (Criteria) this;
        }

        public Criteria andOaLaorgtidIn(List<Long> values) {
            addCriterion("OA_LaOrgTid in", values, "oaLaorgtid");
            return (Criteria) this;
        }

        public Criteria andOaLaorgtidNotIn(List<Long> values) {
            addCriterion("OA_LaOrgTid not in", values, "oaLaorgtid");
            return (Criteria) this;
        }

        public Criteria andOaLaorgtidBetween(Long value1, Long value2) {
            addCriterion("OA_LaOrgTid between", value1, value2, "oaLaorgtid");
            return (Criteria) this;
        }

        public Criteria andOaLaorgtidNotBetween(Long value1, Long value2) {
            addCriterion("OA_LaOrgTid not between", value1, value2, "oaLaorgtid");
            return (Criteria) this;
        }

        public Criteria andOaAuditstatusIsNull() {
            addCriterion("OA_AuditStatus is null");
            return (Criteria) this;
        }

        public Criteria andOaAuditstatusIsNotNull() {
            addCriterion("OA_AuditStatus is not null");
            return (Criteria) this;
        }

        public Criteria andOaAuditstatusEqualTo(Integer value) {
            addCriterion("OA_AuditStatus =", value, "oaAuditstatus");
            return (Criteria) this;
        }

        public Criteria andOaAuditstatusNotEqualTo(Integer value) {
            addCriterion("OA_AuditStatus <>", value, "oaAuditstatus");
            return (Criteria) this;
        }

        public Criteria andOaAuditstatusGreaterThan(Integer value) {
            addCriterion("OA_AuditStatus >", value, "oaAuditstatus");
            return (Criteria) this;
        }

        public Criteria andOaAuditstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("OA_AuditStatus >=", value, "oaAuditstatus");
            return (Criteria) this;
        }

        public Criteria andOaAuditstatusLessThan(Integer value) {
            addCriterion("OA_AuditStatus <", value, "oaAuditstatus");
            return (Criteria) this;
        }

        public Criteria andOaAuditstatusLessThanOrEqualTo(Integer value) {
            addCriterion("OA_AuditStatus <=", value, "oaAuditstatus");
            return (Criteria) this;
        }

        public Criteria andOaAuditstatusIn(List<Integer> values) {
            addCriterion("OA_AuditStatus in", values, "oaAuditstatus");
            return (Criteria) this;
        }

        public Criteria andOaAuditstatusNotIn(List<Integer> values) {
            addCriterion("OA_AuditStatus not in", values, "oaAuditstatus");
            return (Criteria) this;
        }

        public Criteria andOaAuditstatusBetween(Integer value1, Integer value2) {
            addCriterion("OA_AuditStatus between", value1, value2, "oaAuditstatus");
            return (Criteria) this;
        }

        public Criteria andOaAuditstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("OA_AuditStatus not between", value1, value2, "oaAuditstatus");
            return (Criteria) this;
        }

        public Criteria andAcAgentcompanynameIsNull() {
            addCriterion("AC_AgentCompanyName is null");
            return (Criteria) this;
        }

        public Criteria andAcAgentcompanynameIsNotNull() {
            addCriterion("AC_AgentCompanyName is not null");
            return (Criteria) this;
        }

        public Criteria andAcAgentcompanynameEqualTo(String value) {
            addCriterion("AC_AgentCompanyName =", value, "acAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andAcAgentcompanynameNotEqualTo(String value) {
            addCriterion("AC_AgentCompanyName <>", value, "acAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andAcAgentcompanynameGreaterThan(String value) {
            addCriterion("AC_AgentCompanyName >", value, "acAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andAcAgentcompanynameGreaterThanOrEqualTo(String value) {
            addCriterion("AC_AgentCompanyName >=", value, "acAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andAcAgentcompanynameLessThan(String value) {
            addCriterion("AC_AgentCompanyName <", value, "acAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andAcAgentcompanynameLessThanOrEqualTo(String value) {
            addCriterion("AC_AgentCompanyName <=", value, "acAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andAcAgentcompanynameLike(String value) {
            addCriterion("AC_AgentCompanyName like", value, "acAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andAcAgentcompanynameNotLike(String value) {
            addCriterion("AC_AgentCompanyName not like", value, "acAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andAcAgentcompanynameIn(List<String> values) {
            addCriterion("AC_AgentCompanyName in", values, "acAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andAcAgentcompanynameNotIn(List<String> values) {
            addCriterion("AC_AgentCompanyName not in", values, "acAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andAcAgentcompanynameBetween(String value1, String value2) {
            addCriterion("AC_AgentCompanyName between", value1, value2, "acAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andAcAgentcompanynameNotBetween(String value1, String value2) {
            addCriterion("AC_AgentCompanyName not between", value1, value2, "acAgentcompanyname");
            return (Criteria) this;
        }

        public Criteria andAcOfficecodeIsNull() {
            addCriterion("AC_OfficeCode is null");
            return (Criteria) this;
        }

        public Criteria andAcOfficecodeIsNotNull() {
            addCriterion("AC_OfficeCode is not null");
            return (Criteria) this;
        }

        public Criteria andAcOfficecodeEqualTo(String value) {
            addCriterion("AC_OfficeCode =", value, "acOfficecode");
            return (Criteria) this;
        }

        public Criteria andAcOfficecodeNotEqualTo(String value) {
            addCriterion("AC_OfficeCode <>", value, "acOfficecode");
            return (Criteria) this;
        }

        public Criteria andAcOfficecodeGreaterThan(String value) {
            addCriterion("AC_OfficeCode >", value, "acOfficecode");
            return (Criteria) this;
        }

        public Criteria andAcOfficecodeGreaterThanOrEqualTo(String value) {
            addCriterion("AC_OfficeCode >=", value, "acOfficecode");
            return (Criteria) this;
        }

        public Criteria andAcOfficecodeLessThan(String value) {
            addCriterion("AC_OfficeCode <", value, "acOfficecode");
            return (Criteria) this;
        }

        public Criteria andAcOfficecodeLessThanOrEqualTo(String value) {
            addCriterion("AC_OfficeCode <=", value, "acOfficecode");
            return (Criteria) this;
        }

        public Criteria andAcOfficecodeLike(String value) {
            addCriterion("AC_OfficeCode like", value, "acOfficecode");
            return (Criteria) this;
        }

        public Criteria andAcOfficecodeNotLike(String value) {
            addCriterion("AC_OfficeCode not like", value, "acOfficecode");
            return (Criteria) this;
        }

        public Criteria andAcOfficecodeIn(List<String> values) {
            addCriterion("AC_OfficeCode in", values, "acOfficecode");
            return (Criteria) this;
        }

        public Criteria andAcOfficecodeNotIn(List<String> values) {
            addCriterion("AC_OfficeCode not in", values, "acOfficecode");
            return (Criteria) this;
        }

        public Criteria andAcOfficecodeBetween(String value1, String value2) {
            addCriterion("AC_OfficeCode between", value1, value2, "acOfficecode");
            return (Criteria) this;
        }

        public Criteria andAcOfficecodeNotBetween(String value1, String value2) {
            addCriterion("AC_OfficeCode not between", value1, value2, "acOfficecode");
            return (Criteria) this;
        }

        public Criteria andAcIatanoIsNull() {
            addCriterion("AC_IATANO is null");
            return (Criteria) this;
        }

        public Criteria andAcIatanoIsNotNull() {
            addCriterion("AC_IATANO is not null");
            return (Criteria) this;
        }

        public Criteria andAcIatanoEqualTo(String value) {
            addCriterion("AC_IATANO =", value, "acIatano");
            return (Criteria) this;
        }

        public Criteria andAcIatanoNotEqualTo(String value) {
            addCriterion("AC_IATANO <>", value, "acIatano");
            return (Criteria) this;
        }

        public Criteria andAcIatanoGreaterThan(String value) {
            addCriterion("AC_IATANO >", value, "acIatano");
            return (Criteria) this;
        }

        public Criteria andAcIatanoGreaterThanOrEqualTo(String value) {
            addCriterion("AC_IATANO >=", value, "acIatano");
            return (Criteria) this;
        }

        public Criteria andAcIatanoLessThan(String value) {
            addCriterion("AC_IATANO <", value, "acIatano");
            return (Criteria) this;
        }

        public Criteria andAcIatanoLessThanOrEqualTo(String value) {
            addCriterion("AC_IATANO <=", value, "acIatano");
            return (Criteria) this;
        }

        public Criteria andAcIatanoLike(String value) {
            addCriterion("AC_IATANO like", value, "acIatano");
            return (Criteria) this;
        }

        public Criteria andAcIatanoNotLike(String value) {
            addCriterion("AC_IATANO not like", value, "acIatano");
            return (Criteria) this;
        }

        public Criteria andAcIatanoIn(List<String> values) {
            addCriterion("AC_IATANO in", values, "acIatano");
            return (Criteria) this;
        }

        public Criteria andAcIatanoNotIn(List<String> values) {
            addCriterion("AC_IATANO not in", values, "acIatano");
            return (Criteria) this;
        }

        public Criteria andAcIatanoBetween(String value1, String value2) {
            addCriterion("AC_IATANO between", value1, value2, "acIatano");
            return (Criteria) this;
        }

        public Criteria andAcIatanoNotBetween(String value1, String value2) {
            addCriterion("AC_IATANO not between", value1, value2, "acIatano");
            return (Criteria) this;
        }

        public Criteria andAcBelongdepartmentIsNull() {
            addCriterion("AC_BelongDepartment is null");
            return (Criteria) this;
        }

        public Criteria andAcBelongdepartmentIsNotNull() {
            addCriterion("AC_BelongDepartment is not null");
            return (Criteria) this;
        }

        public Criteria andAcBelongdepartmentEqualTo(String value) {
            addCriterion("AC_BelongDepartment =", value, "acBelongdepartment");
            return (Criteria) this;
        }

        public Criteria andAcBelongdepartmentNotEqualTo(String value) {
            addCriterion("AC_BelongDepartment <>", value, "acBelongdepartment");
            return (Criteria) this;
        }

        public Criteria andAcBelongdepartmentGreaterThan(String value) {
            addCriterion("AC_BelongDepartment >", value, "acBelongdepartment");
            return (Criteria) this;
        }

        public Criteria andAcBelongdepartmentGreaterThanOrEqualTo(String value) {
            addCriterion("AC_BelongDepartment >=", value, "acBelongdepartment");
            return (Criteria) this;
        }

        public Criteria andAcBelongdepartmentLessThan(String value) {
            addCriterion("AC_BelongDepartment <", value, "acBelongdepartment");
            return (Criteria) this;
        }

        public Criteria andAcBelongdepartmentLessThanOrEqualTo(String value) {
            addCriterion("AC_BelongDepartment <=", value, "acBelongdepartment");
            return (Criteria) this;
        }

        public Criteria andAcBelongdepartmentLike(String value) {
            addCriterion("AC_BelongDepartment like", value, "acBelongdepartment");
            return (Criteria) this;
        }

        public Criteria andAcBelongdepartmentNotLike(String value) {
            addCriterion("AC_BelongDepartment not like", value, "acBelongdepartment");
            return (Criteria) this;
        }

        public Criteria andAcBelongdepartmentIn(List<String> values) {
            addCriterion("AC_BelongDepartment in", values, "acBelongdepartment");
            return (Criteria) this;
        }

        public Criteria andAcBelongdepartmentNotIn(List<String> values) {
            addCriterion("AC_BelongDepartment not in", values, "acBelongdepartment");
            return (Criteria) this;
        }

        public Criteria andAcBelongdepartmentBetween(String value1, String value2) {
            addCriterion("AC_BelongDepartment between", value1, value2, "acBelongdepartment");
            return (Criteria) this;
        }

        public Criteria andAcBelongdepartmentNotBetween(String value1, String value2) {
            addCriterion("AC_BelongDepartment not between", value1, value2, "acBelongdepartment");
            return (Criteria) this;
        }

        public Criteria andAcCountryIsNull() {
            addCriterion("AC_Country is null");
            return (Criteria) this;
        }

        public Criteria andAcCountryIsNotNull() {
            addCriterion("AC_Country is not null");
            return (Criteria) this;
        }

        public Criteria andAcCountryEqualTo(String value) {
            addCriterion("AC_Country =", value, "acCountry");
            return (Criteria) this;
        }

        public Criteria andAcCountryNotEqualTo(String value) {
            addCriterion("AC_Country <>", value, "acCountry");
            return (Criteria) this;
        }

        public Criteria andAcCountryGreaterThan(String value) {
            addCriterion("AC_Country >", value, "acCountry");
            return (Criteria) this;
        }

        public Criteria andAcCountryGreaterThanOrEqualTo(String value) {
            addCriterion("AC_Country >=", value, "acCountry");
            return (Criteria) this;
        }

        public Criteria andAcCountryLessThan(String value) {
            addCriterion("AC_Country <", value, "acCountry");
            return (Criteria) this;
        }

        public Criteria andAcCountryLessThanOrEqualTo(String value) {
            addCriterion("AC_Country <=", value, "acCountry");
            return (Criteria) this;
        }

        public Criteria andAcCountryLike(String value) {
            addCriterion("AC_Country like", value, "acCountry");
            return (Criteria) this;
        }

        public Criteria andAcCountryNotLike(String value) {
            addCriterion("AC_Country not like", value, "acCountry");
            return (Criteria) this;
        }

        public Criteria andAcCountryIn(List<String> values) {
            addCriterion("AC_Country in", values, "acCountry");
            return (Criteria) this;
        }

        public Criteria andAcCountryNotIn(List<String> values) {
            addCriterion("AC_Country not in", values, "acCountry");
            return (Criteria) this;
        }

        public Criteria andAcCountryBetween(String value1, String value2) {
            addCriterion("AC_Country between", value1, value2, "acCountry");
            return (Criteria) this;
        }

        public Criteria andAcCountryNotBetween(String value1, String value2) {
            addCriterion("AC_Country not between", value1, value2, "acCountry");
            return (Criteria) this;
        }

        public Criteria andAcCityIsNull() {
            addCriterion("AC_City is null");
            return (Criteria) this;
        }

        public Criteria andAcCityIsNotNull() {
            addCriterion("AC_City is not null");
            return (Criteria) this;
        }

        public Criteria andAcCityEqualTo(String value) {
            addCriterion("AC_City =", value, "acCity");
            return (Criteria) this;
        }

        public Criteria andAcCityNotEqualTo(String value) {
            addCriterion("AC_City <>", value, "acCity");
            return (Criteria) this;
        }

        public Criteria andAcCityGreaterThan(String value) {
            addCriterion("AC_City >", value, "acCity");
            return (Criteria) this;
        }

        public Criteria andAcCityGreaterThanOrEqualTo(String value) {
            addCriterion("AC_City >=", value, "acCity");
            return (Criteria) this;
        }

        public Criteria andAcCityLessThan(String value) {
            addCriterion("AC_City <", value, "acCity");
            return (Criteria) this;
        }

        public Criteria andAcCityLessThanOrEqualTo(String value) {
            addCriterion("AC_City <=", value, "acCity");
            return (Criteria) this;
        }

        public Criteria andAcCityLike(String value) {
            addCriterion("AC_City like", value, "acCity");
            return (Criteria) this;
        }

        public Criteria andAcCityNotLike(String value) {
            addCriterion("AC_City not like", value, "acCity");
            return (Criteria) this;
        }

        public Criteria andAcCityIn(List<String> values) {
            addCriterion("AC_City in", values, "acCity");
            return (Criteria) this;
        }

        public Criteria andAcCityNotIn(List<String> values) {
            addCriterion("AC_City not in", values, "acCity");
            return (Criteria) this;
        }

        public Criteria andAcCityBetween(String value1, String value2) {
            addCriterion("AC_City between", value1, value2, "acCity");
            return (Criteria) this;
        }

        public Criteria andAcCityNotBetween(String value1, String value2) {
            addCriterion("AC_City not between", value1, value2, "acCity");
            return (Criteria) this;
        }

        public Criteria andAcAgentusertypeIsNull() {
            addCriterion("AC_AgentUserType is null");
            return (Criteria) this;
        }

        public Criteria andAcAgentusertypeIsNotNull() {
            addCriterion("AC_AgentUserType is not null");
            return (Criteria) this;
        }

        public Criteria andAcAgentusertypeEqualTo(Integer value) {
            addCriterion("AC_AgentUserType =", value, "acAgentusertype");
            return (Criteria) this;
        }

        public Criteria andAcAgentusertypeNotEqualTo(Integer value) {
            addCriterion("AC_AgentUserType <>", value, "acAgentusertype");
            return (Criteria) this;
        }

        public Criteria andAcAgentusertypeGreaterThan(Integer value) {
            addCriterion("AC_AgentUserType >", value, "acAgentusertype");
            return (Criteria) this;
        }

        public Criteria andAcAgentusertypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("AC_AgentUserType >=", value, "acAgentusertype");
            return (Criteria) this;
        }

        public Criteria andAcAgentusertypeLessThan(Integer value) {
            addCriterion("AC_AgentUserType <", value, "acAgentusertype");
            return (Criteria) this;
        }

        public Criteria andAcAgentusertypeLessThanOrEqualTo(Integer value) {
            addCriterion("AC_AgentUserType <=", value, "acAgentusertype");
            return (Criteria) this;
        }

        public Criteria andAcAgentusertypeIn(List<Integer> values) {
            addCriterion("AC_AgentUserType in", values, "acAgentusertype");
            return (Criteria) this;
        }

        public Criteria andAcAgentusertypeNotIn(List<Integer> values) {
            addCriterion("AC_AgentUserType not in", values, "acAgentusertype");
            return (Criteria) this;
        }

        public Criteria andAcAgentusertypeBetween(Integer value1, Integer value2) {
            addCriterion("AC_AgentUserType between", value1, value2, "acAgentusertype");
            return (Criteria) this;
        }

        public Criteria andAcAgentusertypeNotBetween(Integer value1, Integer value2) {
            addCriterion("AC_AgentUserType not between", value1, value2, "acAgentusertype");
            return (Criteria) this;
        }

        public Criteria andAcMoneytypeIsNull() {
            addCriterion("AC_MoneyType is null");
            return (Criteria) this;
        }

        public Criteria andAcMoneytypeIsNotNull() {
            addCriterion("AC_MoneyType is not null");
            return (Criteria) this;
        }

        public Criteria andAcMoneytypeEqualTo(Integer value) {
            addCriterion("AC_MoneyType =", value, "acMoneytype");
            return (Criteria) this;
        }

        public Criteria andAcMoneytypeNotEqualTo(Integer value) {
            addCriterion("AC_MoneyType <>", value, "acMoneytype");
            return (Criteria) this;
        }

        public Criteria andAcMoneytypeGreaterThan(Integer value) {
            addCriterion("AC_MoneyType >", value, "acMoneytype");
            return (Criteria) this;
        }

        public Criteria andAcMoneytypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("AC_MoneyType >=", value, "acMoneytype");
            return (Criteria) this;
        }

        public Criteria andAcMoneytypeLessThan(Integer value) {
            addCriterion("AC_MoneyType <", value, "acMoneytype");
            return (Criteria) this;
        }

        public Criteria andAcMoneytypeLessThanOrEqualTo(Integer value) {
            addCriterion("AC_MoneyType <=", value, "acMoneytype");
            return (Criteria) this;
        }

        public Criteria andAcMoneytypeIn(List<Integer> values) {
            addCriterion("AC_MoneyType in", values, "acMoneytype");
            return (Criteria) this;
        }

        public Criteria andAcMoneytypeNotIn(List<Integer> values) {
            addCriterion("AC_MoneyType not in", values, "acMoneytype");
            return (Criteria) this;
        }

        public Criteria andAcMoneytypeBetween(Integer value1, Integer value2) {
            addCriterion("AC_MoneyType between", value1, value2, "acMoneytype");
            return (Criteria) this;
        }

        public Criteria andAcMoneytypeNotBetween(Integer value1, Integer value2) {
            addCriterion("AC_MoneyType not between", value1, value2, "acMoneytype");
            return (Criteria) this;
        }

        public Criteria andAcContactIsNull() {
            addCriterion("AC_Contact is null");
            return (Criteria) this;
        }

        public Criteria andAcContactIsNotNull() {
            addCriterion("AC_Contact is not null");
            return (Criteria) this;
        }

        public Criteria andAcContactEqualTo(String value) {
            addCriterion("AC_Contact =", value, "acContact");
            return (Criteria) this;
        }

        public Criteria andAcContactNotEqualTo(String value) {
            addCriterion("AC_Contact <>", value, "acContact");
            return (Criteria) this;
        }

        public Criteria andAcContactGreaterThan(String value) {
            addCriterion("AC_Contact >", value, "acContact");
            return (Criteria) this;
        }

        public Criteria andAcContactGreaterThanOrEqualTo(String value) {
            addCriterion("AC_Contact >=", value, "acContact");
            return (Criteria) this;
        }

        public Criteria andAcContactLessThan(String value) {
            addCriterion("AC_Contact <", value, "acContact");
            return (Criteria) this;
        }

        public Criteria andAcContactLessThanOrEqualTo(String value) {
            addCriterion("AC_Contact <=", value, "acContact");
            return (Criteria) this;
        }

        public Criteria andAcContactLike(String value) {
            addCriterion("AC_Contact like", value, "acContact");
            return (Criteria) this;
        }

        public Criteria andAcContactNotLike(String value) {
            addCriterion("AC_Contact not like", value, "acContact");
            return (Criteria) this;
        }

        public Criteria andAcContactIn(List<String> values) {
            addCriterion("AC_Contact in", values, "acContact");
            return (Criteria) this;
        }

        public Criteria andAcContactNotIn(List<String> values) {
            addCriterion("AC_Contact not in", values, "acContact");
            return (Criteria) this;
        }

        public Criteria andAcContactBetween(String value1, String value2) {
            addCriterion("AC_Contact between", value1, value2, "acContact");
            return (Criteria) this;
        }

        public Criteria andAcContactNotBetween(String value1, String value2) {
            addCriterion("AC_Contact not between", value1, value2, "acContact");
            return (Criteria) this;
        }

        public Criteria andAcContactphoneIsNull() {
            addCriterion("AC_ContactPhone is null");
            return (Criteria) this;
        }

        public Criteria andAcContactphoneIsNotNull() {
            addCriterion("AC_ContactPhone is not null");
            return (Criteria) this;
        }

        public Criteria andAcContactphoneEqualTo(String value) {
            addCriterion("AC_ContactPhone =", value, "acContactphone");
            return (Criteria) this;
        }

        public Criteria andAcContactphoneNotEqualTo(String value) {
            addCriterion("AC_ContactPhone <>", value, "acContactphone");
            return (Criteria) this;
        }

        public Criteria andAcContactphoneGreaterThan(String value) {
            addCriterion("AC_ContactPhone >", value, "acContactphone");
            return (Criteria) this;
        }

        public Criteria andAcContactphoneGreaterThanOrEqualTo(String value) {
            addCriterion("AC_ContactPhone >=", value, "acContactphone");
            return (Criteria) this;
        }

        public Criteria andAcContactphoneLessThan(String value) {
            addCriterion("AC_ContactPhone <", value, "acContactphone");
            return (Criteria) this;
        }

        public Criteria andAcContactphoneLessThanOrEqualTo(String value) {
            addCriterion("AC_ContactPhone <=", value, "acContactphone");
            return (Criteria) this;
        }

        public Criteria andAcContactphoneLike(String value) {
            addCriterion("AC_ContactPhone like", value, "acContactphone");
            return (Criteria) this;
        }

        public Criteria andAcContactphoneNotLike(String value) {
            addCriterion("AC_ContactPhone not like", value, "acContactphone");
            return (Criteria) this;
        }

        public Criteria andAcContactphoneIn(List<String> values) {
            addCriterion("AC_ContactPhone in", values, "acContactphone");
            return (Criteria) this;
        }

        public Criteria andAcContactphoneNotIn(List<String> values) {
            addCriterion("AC_ContactPhone not in", values, "acContactphone");
            return (Criteria) this;
        }

        public Criteria andAcContactphoneBetween(String value1, String value2) {
            addCriterion("AC_ContactPhone between", value1, value2, "acContactphone");
            return (Criteria) this;
        }

        public Criteria andAcContactphoneNotBetween(String value1, String value2) {
            addCriterion("AC_ContactPhone not between", value1, value2, "acContactphone");
            return (Criteria) this;
        }

        public Criteria andAcAddressIsNull() {
            addCriterion("AC_Address is null");
            return (Criteria) this;
        }

        public Criteria andAcAddressIsNotNull() {
            addCriterion("AC_Address is not null");
            return (Criteria) this;
        }

        public Criteria andAcAddressEqualTo(String value) {
            addCriterion("AC_Address =", value, "acAddress");
            return (Criteria) this;
        }

        public Criteria andAcAddressNotEqualTo(String value) {
            addCriterion("AC_Address <>", value, "acAddress");
            return (Criteria) this;
        }

        public Criteria andAcAddressGreaterThan(String value) {
            addCriterion("AC_Address >", value, "acAddress");
            return (Criteria) this;
        }

        public Criteria andAcAddressGreaterThanOrEqualTo(String value) {
            addCriterion("AC_Address >=", value, "acAddress");
            return (Criteria) this;
        }

        public Criteria andAcAddressLessThan(String value) {
            addCriterion("AC_Address <", value, "acAddress");
            return (Criteria) this;
        }

        public Criteria andAcAddressLessThanOrEqualTo(String value) {
            addCriterion("AC_Address <=", value, "acAddress");
            return (Criteria) this;
        }

        public Criteria andAcAddressLike(String value) {
            addCriterion("AC_Address like", value, "acAddress");
            return (Criteria) this;
        }

        public Criteria andAcAddressNotLike(String value) {
            addCriterion("AC_Address not like", value, "acAddress");
            return (Criteria) this;
        }

        public Criteria andAcAddressIn(List<String> values) {
            addCriterion("AC_Address in", values, "acAddress");
            return (Criteria) this;
        }

        public Criteria andAcAddressNotIn(List<String> values) {
            addCriterion("AC_Address not in", values, "acAddress");
            return (Criteria) this;
        }

        public Criteria andAcAddressBetween(String value1, String value2) {
            addCriterion("AC_Address between", value1, value2, "acAddress");
            return (Criteria) this;
        }

        public Criteria andAcAddressNotBetween(String value1, String value2) {
            addCriterion("AC_Address not between", value1, value2, "acAddress");
            return (Criteria) this;
        }

        public Criteria andAcEmailIsNull() {
            addCriterion("AC_Email is null");
            return (Criteria) this;
        }

        public Criteria andAcEmailIsNotNull() {
            addCriterion("AC_Email is not null");
            return (Criteria) this;
        }

        public Criteria andAcEmailEqualTo(String value) {
            addCriterion("AC_Email =", value, "acEmail");
            return (Criteria) this;
        }

        public Criteria andAcEmailNotEqualTo(String value) {
            addCriterion("AC_Email <>", value, "acEmail");
            return (Criteria) this;
        }

        public Criteria andAcEmailGreaterThan(String value) {
            addCriterion("AC_Email >", value, "acEmail");
            return (Criteria) this;
        }

        public Criteria andAcEmailGreaterThanOrEqualTo(String value) {
            addCriterion("AC_Email >=", value, "acEmail");
            return (Criteria) this;
        }

        public Criteria andAcEmailLessThan(String value) {
            addCriterion("AC_Email <", value, "acEmail");
            return (Criteria) this;
        }

        public Criteria andAcEmailLessThanOrEqualTo(String value) {
            addCriterion("AC_Email <=", value, "acEmail");
            return (Criteria) this;
        }

        public Criteria andAcEmailLike(String value) {
            addCriterion("AC_Email like", value, "acEmail");
            return (Criteria) this;
        }

        public Criteria andAcEmailNotLike(String value) {
            addCriterion("AC_Email not like", value, "acEmail");
            return (Criteria) this;
        }

        public Criteria andAcEmailIn(List<String> values) {
            addCriterion("AC_Email in", values, "acEmail");
            return (Criteria) this;
        }

        public Criteria andAcEmailNotIn(List<String> values) {
            addCriterion("AC_Email not in", values, "acEmail");
            return (Criteria) this;
        }

        public Criteria andAcEmailBetween(String value1, String value2) {
            addCriterion("AC_Email between", value1, value2, "acEmail");
            return (Criteria) this;
        }

        public Criteria andAcEmailNotBetween(String value1, String value2) {
            addCriterion("AC_Email not between", value1, value2, "acEmail");
            return (Criteria) this;
        }

        public Criteria andAcZipcodeIsNull() {
            addCriterion("AC_ZipCode is null");
            return (Criteria) this;
        }

        public Criteria andAcZipcodeIsNotNull() {
            addCriterion("AC_ZipCode is not null");
            return (Criteria) this;
        }

        public Criteria andAcZipcodeEqualTo(String value) {
            addCriterion("AC_ZipCode =", value, "acZipcode");
            return (Criteria) this;
        }

        public Criteria andAcZipcodeNotEqualTo(String value) {
            addCriterion("AC_ZipCode <>", value, "acZipcode");
            return (Criteria) this;
        }

        public Criteria andAcZipcodeGreaterThan(String value) {
            addCriterion("AC_ZipCode >", value, "acZipcode");
            return (Criteria) this;
        }

        public Criteria andAcZipcodeGreaterThanOrEqualTo(String value) {
            addCriterion("AC_ZipCode >=", value, "acZipcode");
            return (Criteria) this;
        }

        public Criteria andAcZipcodeLessThan(String value) {
            addCriterion("AC_ZipCode <", value, "acZipcode");
            return (Criteria) this;
        }

        public Criteria andAcZipcodeLessThanOrEqualTo(String value) {
            addCriterion("AC_ZipCode <=", value, "acZipcode");
            return (Criteria) this;
        }

        public Criteria andAcZipcodeLike(String value) {
            addCriterion("AC_ZipCode like", value, "acZipcode");
            return (Criteria) this;
        }

        public Criteria andAcZipcodeNotLike(String value) {
            addCriterion("AC_ZipCode not like", value, "acZipcode");
            return (Criteria) this;
        }

        public Criteria andAcZipcodeIn(List<String> values) {
            addCriterion("AC_ZipCode in", values, "acZipcode");
            return (Criteria) this;
        }

        public Criteria andAcZipcodeNotIn(List<String> values) {
            addCriterion("AC_ZipCode not in", values, "acZipcode");
            return (Criteria) this;
        }

        public Criteria andAcZipcodeBetween(String value1, String value2) {
            addCriterion("AC_ZipCode between", value1, value2, "acZipcode");
            return (Criteria) this;
        }

        public Criteria andAcZipcodeNotBetween(String value1, String value2) {
            addCriterion("AC_ZipCode not between", value1, value2, "acZipcode");
            return (Criteria) this;
        }

        public Criteria andAcFaxIsNull() {
            addCriterion("AC_Fax is null");
            return (Criteria) this;
        }

        public Criteria andAcFaxIsNotNull() {
            addCriterion("AC_Fax is not null");
            return (Criteria) this;
        }

        public Criteria andAcFaxEqualTo(String value) {
            addCriterion("AC_Fax =", value, "acFax");
            return (Criteria) this;
        }

        public Criteria andAcFaxNotEqualTo(String value) {
            addCriterion("AC_Fax <>", value, "acFax");
            return (Criteria) this;
        }

        public Criteria andAcFaxGreaterThan(String value) {
            addCriterion("AC_Fax >", value, "acFax");
            return (Criteria) this;
        }

        public Criteria andAcFaxGreaterThanOrEqualTo(String value) {
            addCriterion("AC_Fax >=", value, "acFax");
            return (Criteria) this;
        }

        public Criteria andAcFaxLessThan(String value) {
            addCriterion("AC_Fax <", value, "acFax");
            return (Criteria) this;
        }

        public Criteria andAcFaxLessThanOrEqualTo(String value) {
            addCriterion("AC_Fax <=", value, "acFax");
            return (Criteria) this;
        }

        public Criteria andAcFaxLike(String value) {
            addCriterion("AC_Fax like", value, "acFax");
            return (Criteria) this;
        }

        public Criteria andAcFaxNotLike(String value) {
            addCriterion("AC_Fax not like", value, "acFax");
            return (Criteria) this;
        }

        public Criteria andAcFaxIn(List<String> values) {
            addCriterion("AC_Fax in", values, "acFax");
            return (Criteria) this;
        }

        public Criteria andAcFaxNotIn(List<String> values) {
            addCriterion("AC_Fax not in", values, "acFax");
            return (Criteria) this;
        }

        public Criteria andAcFaxBetween(String value1, String value2) {
            addCriterion("AC_Fax between", value1, value2, "acFax");
            return (Criteria) this;
        }

        public Criteria andAcFaxNotBetween(String value1, String value2) {
            addCriterion("AC_Fax not between", value1, value2, "acFax");
            return (Criteria) this;
        }

        public Criteria andAcTelephoneIsNull() {
            addCriterion("AC_Telephone is null");
            return (Criteria) this;
        }

        public Criteria andAcTelephoneIsNotNull() {
            addCriterion("AC_Telephone is not null");
            return (Criteria) this;
        }

        public Criteria andAcTelephoneEqualTo(String value) {
            addCriterion("AC_Telephone =", value, "acTelephone");
            return (Criteria) this;
        }

        public Criteria andAcTelephoneNotEqualTo(String value) {
            addCriterion("AC_Telephone <>", value, "acTelephone");
            return (Criteria) this;
        }

        public Criteria andAcTelephoneGreaterThan(String value) {
            addCriterion("AC_Telephone >", value, "acTelephone");
            return (Criteria) this;
        }

        public Criteria andAcTelephoneGreaterThanOrEqualTo(String value) {
            addCriterion("AC_Telephone >=", value, "acTelephone");
            return (Criteria) this;
        }

        public Criteria andAcTelephoneLessThan(String value) {
            addCriterion("AC_Telephone <", value, "acTelephone");
            return (Criteria) this;
        }

        public Criteria andAcTelephoneLessThanOrEqualTo(String value) {
            addCriterion("AC_Telephone <=", value, "acTelephone");
            return (Criteria) this;
        }

        public Criteria andAcTelephoneLike(String value) {
            addCriterion("AC_Telephone like", value, "acTelephone");
            return (Criteria) this;
        }

        public Criteria andAcTelephoneNotLike(String value) {
            addCriterion("AC_Telephone not like", value, "acTelephone");
            return (Criteria) this;
        }

        public Criteria andAcTelephoneIn(List<String> values) {
            addCriterion("AC_Telephone in", values, "acTelephone");
            return (Criteria) this;
        }

        public Criteria andAcTelephoneNotIn(List<String> values) {
            addCriterion("AC_Telephone not in", values, "acTelephone");
            return (Criteria) this;
        }

        public Criteria andAcTelephoneBetween(String value1, String value2) {
            addCriterion("AC_Telephone between", value1, value2, "acTelephone");
            return (Criteria) this;
        }

        public Criteria andAcTelephoneNotBetween(String value1, String value2) {
            addCriterion("AC_Telephone not between", value1, value2, "acTelephone");
            return (Criteria) this;
        }

        public Criteria andAcIagencyrateIsNull() {
            addCriterion("AC_IAgencyRate is null");
            return (Criteria) this;
        }

        public Criteria andAcIagencyrateIsNotNull() {
            addCriterion("AC_IAgencyRate is not null");
            return (Criteria) this;
        }

        public Criteria andAcIagencyrateEqualTo(Float value) {
            addCriterion("AC_IAgencyRate =", value, "acIagencyrate");
            return (Criteria) this;
        }

        public Criteria andAcIagencyrateNotEqualTo(Float value) {
            addCriterion("AC_IAgencyRate <>", value, "acIagencyrate");
            return (Criteria) this;
        }

        public Criteria andAcIagencyrateGreaterThan(Float value) {
            addCriterion("AC_IAgencyRate >", value, "acIagencyrate");
            return (Criteria) this;
        }

        public Criteria andAcIagencyrateGreaterThanOrEqualTo(Float value) {
            addCriterion("AC_IAgencyRate >=", value, "acIagencyrate");
            return (Criteria) this;
        }

        public Criteria andAcIagencyrateLessThan(Float value) {
            addCriterion("AC_IAgencyRate <", value, "acIagencyrate");
            return (Criteria) this;
        }

        public Criteria andAcIagencyrateLessThanOrEqualTo(Float value) {
            addCriterion("AC_IAgencyRate <=", value, "acIagencyrate");
            return (Criteria) this;
        }

        public Criteria andAcIagencyrateIn(List<Float> values) {
            addCriterion("AC_IAgencyRate in", values, "acIagencyrate");
            return (Criteria) this;
        }

        public Criteria andAcIagencyrateNotIn(List<Float> values) {
            addCriterion("AC_IAgencyRate not in", values, "acIagencyrate");
            return (Criteria) this;
        }

        public Criteria andAcIagencyrateBetween(Float value1, Float value2) {
            addCriterion("AC_IAgencyRate between", value1, value2, "acIagencyrate");
            return (Criteria) this;
        }

        public Criteria andAcIagencyrateNotBetween(Float value1, Float value2) {
            addCriterion("AC_IAgencyRate not between", value1, value2, "acIagencyrate");
            return (Criteria) this;
        }

        public Criteria andAcDagencyrateIsNull() {
            addCriterion("AC_DAgencyRate is null");
            return (Criteria) this;
        }

        public Criteria andAcDagencyrateIsNotNull() {
            addCriterion("AC_DAgencyRate is not null");
            return (Criteria) this;
        }

        public Criteria andAcDagencyrateEqualTo(Float value) {
            addCriterion("AC_DAgencyRate =", value, "acDagencyrate");
            return (Criteria) this;
        }

        public Criteria andAcDagencyrateNotEqualTo(Float value) {
            addCriterion("AC_DAgencyRate <>", value, "acDagencyrate");
            return (Criteria) this;
        }

        public Criteria andAcDagencyrateGreaterThan(Float value) {
            addCriterion("AC_DAgencyRate >", value, "acDagencyrate");
            return (Criteria) this;
        }

        public Criteria andAcDagencyrateGreaterThanOrEqualTo(Float value) {
            addCriterion("AC_DAgencyRate >=", value, "acDagencyrate");
            return (Criteria) this;
        }

        public Criteria andAcDagencyrateLessThan(Float value) {
            addCriterion("AC_DAgencyRate <", value, "acDagencyrate");
            return (Criteria) this;
        }

        public Criteria andAcDagencyrateLessThanOrEqualTo(Float value) {
            addCriterion("AC_DAgencyRate <=", value, "acDagencyrate");
            return (Criteria) this;
        }

        public Criteria andAcDagencyrateIn(List<Float> values) {
            addCriterion("AC_DAgencyRate in", values, "acDagencyrate");
            return (Criteria) this;
        }

        public Criteria andAcDagencyrateNotIn(List<Float> values) {
            addCriterion("AC_DAgencyRate not in", values, "acDagencyrate");
            return (Criteria) this;
        }

        public Criteria andAcDagencyrateBetween(Float value1, Float value2) {
            addCriterion("AC_DAgencyRate between", value1, value2, "acDagencyrate");
            return (Criteria) this;
        }

        public Criteria andAcDagencyrateNotBetween(Float value1, Float value2) {
            addCriterion("AC_DAgencyRate not between", value1, value2, "acDagencyrate");
            return (Criteria) this;
        }

        public Criteria andAcAgencytypeIsNull() {
            addCriterion("AC_AgencyType is null");
            return (Criteria) this;
        }

        public Criteria andAcAgencytypeIsNotNull() {
            addCriterion("AC_AgencyType is not null");
            return (Criteria) this;
        }

        public Criteria andAcAgencytypeEqualTo(Integer value) {
            addCriterion("AC_AgencyType =", value, "acAgencytype");
            return (Criteria) this;
        }

        public Criteria andAcAgencytypeNotEqualTo(Integer value) {
            addCriterion("AC_AgencyType <>", value, "acAgencytype");
            return (Criteria) this;
        }

        public Criteria andAcAgencytypeGreaterThan(Integer value) {
            addCriterion("AC_AgencyType >", value, "acAgencytype");
            return (Criteria) this;
        }

        public Criteria andAcAgencytypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("AC_AgencyType >=", value, "acAgencytype");
            return (Criteria) this;
        }

        public Criteria andAcAgencytypeLessThan(Integer value) {
            addCriterion("AC_AgencyType <", value, "acAgencytype");
            return (Criteria) this;
        }

        public Criteria andAcAgencytypeLessThanOrEqualTo(Integer value) {
            addCriterion("AC_AgencyType <=", value, "acAgencytype");
            return (Criteria) this;
        }

        public Criteria andAcAgencytypeIn(List<Integer> values) {
            addCriterion("AC_AgencyType in", values, "acAgencytype");
            return (Criteria) this;
        }

        public Criteria andAcAgencytypeNotIn(List<Integer> values) {
            addCriterion("AC_AgencyType not in", values, "acAgencytype");
            return (Criteria) this;
        }

        public Criteria andAcAgencytypeBetween(Integer value1, Integer value2) {
            addCriterion("AC_AgencyType between", value1, value2, "acAgencytype");
            return (Criteria) this;
        }

        public Criteria andAcAgencytypeNotBetween(Integer value1, Integer value2) {
            addCriterion("AC_AgencyType not between", value1, value2, "acAgencytype");
            return (Criteria) this;
        }

        public Criteria andAcPermissionIsNull() {
            addCriterion("AC_Permission is null");
            return (Criteria) this;
        }

        public Criteria andAcPermissionIsNotNull() {
            addCriterion("AC_Permission is not null");
            return (Criteria) this;
        }

        public Criteria andAcPermissionEqualTo(String value) {
            addCriterion("AC_Permission =", value, "acPermission");
            return (Criteria) this;
        }

        public Criteria andAcPermissionNotEqualTo(String value) {
            addCriterion("AC_Permission <>", value, "acPermission");
            return (Criteria) this;
        }

        public Criteria andAcPermissionGreaterThan(String value) {
            addCriterion("AC_Permission >", value, "acPermission");
            return (Criteria) this;
        }

        public Criteria andAcPermissionGreaterThanOrEqualTo(String value) {
            addCriterion("AC_Permission >=", value, "acPermission");
            return (Criteria) this;
        }

        public Criteria andAcPermissionLessThan(String value) {
            addCriterion("AC_Permission <", value, "acPermission");
            return (Criteria) this;
        }

        public Criteria andAcPermissionLessThanOrEqualTo(String value) {
            addCriterion("AC_Permission <=", value, "acPermission");
            return (Criteria) this;
        }

        public Criteria andAcPermissionLike(String value) {
            addCriterion("AC_Permission like", value, "acPermission");
            return (Criteria) this;
        }

        public Criteria andAcPermissionNotLike(String value) {
            addCriterion("AC_Permission not like", value, "acPermission");
            return (Criteria) this;
        }

        public Criteria andAcPermissionIn(List<String> values) {
            addCriterion("AC_Permission in", values, "acPermission");
            return (Criteria) this;
        }

        public Criteria andAcPermissionNotIn(List<String> values) {
            addCriterion("AC_Permission not in", values, "acPermission");
            return (Criteria) this;
        }

        public Criteria andAcPermissionBetween(String value1, String value2) {
            addCriterion("AC_Permission between", value1, value2, "acPermission");
            return (Criteria) this;
        }

        public Criteria andAcPermissionNotBetween(String value1, String value2) {
            addCriterion("AC_Permission not between", value1, value2, "acPermission");
            return (Criteria) this;
        }

        public Criteria andAcAdministratorIsNull() {
            addCriterion("AC_Administrator is null");
            return (Criteria) this;
        }

        public Criteria andAcAdministratorIsNotNull() {
            addCriterion("AC_Administrator is not null");
            return (Criteria) this;
        }

        public Criteria andAcAdministratorEqualTo(String value) {
            addCriterion("AC_Administrator =", value, "acAdministrator");
            return (Criteria) this;
        }

        public Criteria andAcAdministratorNotEqualTo(String value) {
            addCriterion("AC_Administrator <>", value, "acAdministrator");
            return (Criteria) this;
        }

        public Criteria andAcAdministratorGreaterThan(String value) {
            addCriterion("AC_Administrator >", value, "acAdministrator");
            return (Criteria) this;
        }

        public Criteria andAcAdministratorGreaterThanOrEqualTo(String value) {
            addCriterion("AC_Administrator >=", value, "acAdministrator");
            return (Criteria) this;
        }

        public Criteria andAcAdministratorLessThan(String value) {
            addCriterion("AC_Administrator <", value, "acAdministrator");
            return (Criteria) this;
        }

        public Criteria andAcAdministratorLessThanOrEqualTo(String value) {
            addCriterion("AC_Administrator <=", value, "acAdministrator");
            return (Criteria) this;
        }

        public Criteria andAcAdministratorLike(String value) {
            addCriterion("AC_Administrator like", value, "acAdministrator");
            return (Criteria) this;
        }

        public Criteria andAcAdministratorNotLike(String value) {
            addCriterion("AC_Administrator not like", value, "acAdministrator");
            return (Criteria) this;
        }

        public Criteria andAcAdministratorIn(List<String> values) {
            addCriterion("AC_Administrator in", values, "acAdministrator");
            return (Criteria) this;
        }

        public Criteria andAcAdministratorNotIn(List<String> values) {
            addCriterion("AC_Administrator not in", values, "acAdministrator");
            return (Criteria) this;
        }

        public Criteria andAcAdministratorBetween(String value1, String value2) {
            addCriterion("AC_Administrator between", value1, value2, "acAdministrator");
            return (Criteria) this;
        }

        public Criteria andAcAdministratorNotBetween(String value1, String value2) {
            addCriterion("AC_Administrator not between", value1, value2, "acAdministrator");
            return (Criteria) this;
        }

        public Criteria andOaTeamcodeIsNull() {
            addCriterion("OA_TeamCode is null");
            return (Criteria) this;
        }

        public Criteria andOaTeamcodeIsNotNull() {
            addCriterion("OA_TeamCode is not null");
            return (Criteria) this;
        }

        public Criteria andOaTeamcodeEqualTo(String value) {
            addCriterion("OA_TeamCode =", value, "oaTeamcode");
            return (Criteria) this;
        }

        public Criteria andOaTeamcodeNotEqualTo(String value) {
            addCriterion("OA_TeamCode <>", value, "oaTeamcode");
            return (Criteria) this;
        }

        public Criteria andOaTeamcodeGreaterThan(String value) {
            addCriterion("OA_TeamCode >", value, "oaTeamcode");
            return (Criteria) this;
        }

        public Criteria andOaTeamcodeGreaterThanOrEqualTo(String value) {
            addCriterion("OA_TeamCode >=", value, "oaTeamcode");
            return (Criteria) this;
        }

        public Criteria andOaTeamcodeLessThan(String value) {
            addCriterion("OA_TeamCode <", value, "oaTeamcode");
            return (Criteria) this;
        }

        public Criteria andOaTeamcodeLessThanOrEqualTo(String value) {
            addCriterion("OA_TeamCode <=", value, "oaTeamcode");
            return (Criteria) this;
        }

        public Criteria andOaTeamcodeLike(String value) {
            addCriterion("OA_TeamCode like", value, "oaTeamcode");
            return (Criteria) this;
        }

        public Criteria andOaTeamcodeNotLike(String value) {
            addCriterion("OA_TeamCode not like", value, "oaTeamcode");
            return (Criteria) this;
        }

        public Criteria andOaTeamcodeIn(List<String> values) {
            addCriterion("OA_TeamCode in", values, "oaTeamcode");
            return (Criteria) this;
        }

        public Criteria andOaTeamcodeNotIn(List<String> values) {
            addCriterion("OA_TeamCode not in", values, "oaTeamcode");
            return (Criteria) this;
        }

        public Criteria andOaTeamcodeBetween(String value1, String value2) {
            addCriterion("OA_TeamCode between", value1, value2, "oaTeamcode");
            return (Criteria) this;
        }

        public Criteria andOaTeamcodeNotBetween(String value1, String value2) {
            addCriterion("OA_TeamCode not between", value1, value2, "oaTeamcode");
            return (Criteria) this;
        }

        public Criteria andAcRegistrationtimeIsNull() {
            addCriterion("AC_RegistrationTime is null");
            return (Criteria) this;
        }

        public Criteria andAcRegistrationtimeIsNotNull() {
            addCriterion("AC_RegistrationTime is not null");
            return (Criteria) this;
        }

        public Criteria andAcRegistrationtimeEqualTo(Date value) {
            addCriterion("AC_RegistrationTime =", value, "acRegistrationtime");
            return (Criteria) this;
        }

        public Criteria andAcRegistrationtimeNotEqualTo(Date value) {
            addCriterion("AC_RegistrationTime <>", value, "acRegistrationtime");
            return (Criteria) this;
        }

        public Criteria andAcRegistrationtimeGreaterThan(Date value) {
            addCriterion("AC_RegistrationTime >", value, "acRegistrationtime");
            return (Criteria) this;
        }

        public Criteria andAcRegistrationtimeGreaterThanOrEqualTo(Date value) {
            addCriterion("AC_RegistrationTime >=", value, "acRegistrationtime");
            return (Criteria) this;
        }

        public Criteria andAcRegistrationtimeLessThan(Date value) {
            addCriterion("AC_RegistrationTime <", value, "acRegistrationtime");
            return (Criteria) this;
        }

        public Criteria andAcRegistrationtimeLessThanOrEqualTo(Date value) {
            addCriterion("AC_RegistrationTime <=", value, "acRegistrationtime");
            return (Criteria) this;
        }

        public Criteria andAcRegistrationtimeIn(List<Date> values) {
            addCriterion("AC_RegistrationTime in", values, "acRegistrationtime");
            return (Criteria) this;
        }

        public Criteria andAcRegistrationtimeNotIn(List<Date> values) {
            addCriterion("AC_RegistrationTime not in", values, "acRegistrationtime");
            return (Criteria) this;
        }

        public Criteria andAcRegistrationtimeBetween(Date value1, Date value2) {
            addCriterion("AC_RegistrationTime between", value1, value2, "acRegistrationtime");
            return (Criteria) this;
        }

        public Criteria andAcRegistrationtimeNotBetween(Date value1, Date value2) {
            addCriterion("AC_RegistrationTime not between", value1, value2, "acRegistrationtime");
            return (Criteria) this;
        }

        public Criteria andAcSettlementcodeIsNull() {
            addCriterion("AC_SettlementCode is null");
            return (Criteria) this;
        }

        public Criteria andAcSettlementcodeIsNotNull() {
            addCriterion("AC_SettlementCode is not null");
            return (Criteria) this;
        }

        public Criteria andAcSettlementcodeEqualTo(String value) {
            addCriterion("AC_SettlementCode =", value, "acSettlementcode");
            return (Criteria) this;
        }

        public Criteria andAcSettlementcodeNotEqualTo(String value) {
            addCriterion("AC_SettlementCode <>", value, "acSettlementcode");
            return (Criteria) this;
        }

        public Criteria andAcSettlementcodeGreaterThan(String value) {
            addCriterion("AC_SettlementCode >", value, "acSettlementcode");
            return (Criteria) this;
        }

        public Criteria andAcSettlementcodeGreaterThanOrEqualTo(String value) {
            addCriterion("AC_SettlementCode >=", value, "acSettlementcode");
            return (Criteria) this;
        }

        public Criteria andAcSettlementcodeLessThan(String value) {
            addCriterion("AC_SettlementCode <", value, "acSettlementcode");
            return (Criteria) this;
        }

        public Criteria andAcSettlementcodeLessThanOrEqualTo(String value) {
            addCriterion("AC_SettlementCode <=", value, "acSettlementcode");
            return (Criteria) this;
        }

        public Criteria andAcSettlementcodeLike(String value) {
            addCriterion("AC_SettlementCode like", value, "acSettlementcode");
            return (Criteria) this;
        }

        public Criteria andAcSettlementcodeNotLike(String value) {
            addCriterion("AC_SettlementCode not like", value, "acSettlementcode");
            return (Criteria) this;
        }

        public Criteria andAcSettlementcodeIn(List<String> values) {
            addCriterion("AC_SettlementCode in", values, "acSettlementcode");
            return (Criteria) this;
        }

        public Criteria andAcSettlementcodeNotIn(List<String> values) {
            addCriterion("AC_SettlementCode not in", values, "acSettlementcode");
            return (Criteria) this;
        }

        public Criteria andAcSettlementcodeBetween(String value1, String value2) {
            addCriterion("AC_SettlementCode between", value1, value2, "acSettlementcode");
            return (Criteria) this;
        }

        public Criteria andAcSettlementcodeNotBetween(String value1, String value2) {
            addCriterion("AC_SettlementCode not between", value1, value2, "acSettlementcode");
            return (Criteria) this;
        }

        public Criteria andAcLaorgtidIsNull() {
            addCriterion("AC_LaOrgTid is null");
            return (Criteria) this;
        }

        public Criteria andAcLaorgtidIsNotNull() {
            addCriterion("AC_LaOrgTid is not null");
            return (Criteria) this;
        }

        public Criteria andAcLaorgtidEqualTo(Long value) {
            addCriterion("AC_LaOrgTid =", value, "acLaorgtid");
            return (Criteria) this;
        }

        public Criteria andAcLaorgtidNotEqualTo(Long value) {
            addCriterion("AC_LaOrgTid <>", value, "acLaorgtid");
            return (Criteria) this;
        }

        public Criteria andAcLaorgtidGreaterThan(Long value) {
            addCriterion("AC_LaOrgTid >", value, "acLaorgtid");
            return (Criteria) this;
        }

        public Criteria andAcLaorgtidGreaterThanOrEqualTo(Long value) {
            addCriterion("AC_LaOrgTid >=", value, "acLaorgtid");
            return (Criteria) this;
        }

        public Criteria andAcLaorgtidLessThan(Long value) {
            addCriterion("AC_LaOrgTid <", value, "acLaorgtid");
            return (Criteria) this;
        }

        public Criteria andAcLaorgtidLessThanOrEqualTo(Long value) {
            addCriterion("AC_LaOrgTid <=", value, "acLaorgtid");
            return (Criteria) this;
        }

        public Criteria andAcLaorgtidIn(List<Long> values) {
            addCriterion("AC_LaOrgTid in", values, "acLaorgtid");
            return (Criteria) this;
        }

        public Criteria andAcLaorgtidNotIn(List<Long> values) {
            addCriterion("AC_LaOrgTid not in", values, "acLaorgtid");
            return (Criteria) this;
        }

        public Criteria andAcLaorgtidBetween(Long value1, Long value2) {
            addCriterion("AC_LaOrgTid between", value1, value2, "acLaorgtid");
            return (Criteria) this;
        }

        public Criteria andAcLaorgtidNotBetween(Long value1, Long value2) {
            addCriterion("AC_LaOrgTid not between", value1, value2, "acLaorgtid");
            return (Criteria) this;
        }

        public Criteria andAcAuditstatusIsNull() {
            addCriterion("AC_AuditStatus is null");
            return (Criteria) this;
        }

        public Criteria andAcAuditstatusIsNotNull() {
            addCriterion("AC_AuditStatus is not null");
            return (Criteria) this;
        }

        public Criteria andAcAuditstatusEqualTo(Integer value) {
            addCriterion("AC_AuditStatus =", value, "acAuditstatus");
            return (Criteria) this;
        }

        public Criteria andAcAuditstatusNotEqualTo(Integer value) {
            addCriterion("AC_AuditStatus <>", value, "acAuditstatus");
            return (Criteria) this;
        }

        public Criteria andAcAuditstatusGreaterThan(Integer value) {
            addCriterion("AC_AuditStatus >", value, "acAuditstatus");
            return (Criteria) this;
        }

        public Criteria andAcAuditstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("AC_AuditStatus >=", value, "acAuditstatus");
            return (Criteria) this;
        }

        public Criteria andAcAuditstatusLessThan(Integer value) {
            addCriterion("AC_AuditStatus <", value, "acAuditstatus");
            return (Criteria) this;
        }

        public Criteria andAcAuditstatusLessThanOrEqualTo(Integer value) {
            addCriterion("AC_AuditStatus <=", value, "acAuditstatus");
            return (Criteria) this;
        }

        public Criteria andAcAuditstatusIn(List<Integer> values) {
            addCriterion("AC_AuditStatus in", values, "acAuditstatus");
            return (Criteria) this;
        }

        public Criteria andAcAuditstatusNotIn(List<Integer> values) {
            addCriterion("AC_AuditStatus not in", values, "acAuditstatus");
            return (Criteria) this;
        }

        public Criteria andAcAuditstatusBetween(Integer value1, Integer value2) {
            addCriterion("AC_AuditStatus between", value1, value2, "acAuditstatus");
            return (Criteria) this;
        }

        public Criteria andAcAuditstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("AC_AuditStatus not between", value1, value2, "acAuditstatus");
            return (Criteria) this;
        }

        public Criteria andAcTeamcodeIsNull() {
            addCriterion("AC_TeamCode is null");
            return (Criteria) this;
        }

        public Criteria andAcTeamcodeIsNotNull() {
            addCriterion("AC_TeamCode is not null");
            return (Criteria) this;
        }

        public Criteria andAcTeamcodeEqualTo(String value) {
            addCriterion("AC_TeamCode =", value, "acTeamcode");
            return (Criteria) this;
        }

        public Criteria andAcTeamcodeNotEqualTo(String value) {
            addCriterion("AC_TeamCode <>", value, "acTeamcode");
            return (Criteria) this;
        }

        public Criteria andAcTeamcodeGreaterThan(String value) {
            addCriterion("AC_TeamCode >", value, "acTeamcode");
            return (Criteria) this;
        }

        public Criteria andAcTeamcodeGreaterThanOrEqualTo(String value) {
            addCriterion("AC_TeamCode >=", value, "acTeamcode");
            return (Criteria) this;
        }

        public Criteria andAcTeamcodeLessThan(String value) {
            addCriterion("AC_TeamCode <", value, "acTeamcode");
            return (Criteria) this;
        }

        public Criteria andAcTeamcodeLessThanOrEqualTo(String value) {
            addCriterion("AC_TeamCode <=", value, "acTeamcode");
            return (Criteria) this;
        }

        public Criteria andAcTeamcodeLike(String value) {
            addCriterion("AC_TeamCode like", value, "acTeamcode");
            return (Criteria) this;
        }

        public Criteria andAcTeamcodeNotLike(String value) {
            addCriterion("AC_TeamCode not like", value, "acTeamcode");
            return (Criteria) this;
        }

        public Criteria andAcTeamcodeIn(List<String> values) {
            addCriterion("AC_TeamCode in", values, "acTeamcode");
            return (Criteria) this;
        }

        public Criteria andAcTeamcodeNotIn(List<String> values) {
            addCriterion("AC_TeamCode not in", values, "acTeamcode");
            return (Criteria) this;
        }

        public Criteria andAcTeamcodeBetween(String value1, String value2) {
            addCriterion("AC_TeamCode between", value1, value2, "acTeamcode");
            return (Criteria) this;
        }

        public Criteria andAcTeamcodeNotBetween(String value1, String value2) {
            addCriterion("AC_TeamCode not between", value1, value2, "acTeamcode");
            return (Criteria) this;
        }

        public Criteria andAcIsbspIsNull() {
            addCriterion("AC_IsBSP is null");
            return (Criteria) this;
        }

        public Criteria andAcIsbspIsNotNull() {
            addCriterion("AC_IsBSP is not null");
            return (Criteria) this;
        }

        public Criteria andAcIsbspEqualTo(Integer value) {
            addCriterion("AC_IsBSP =", value, "acIsbsp");
            return (Criteria) this;
        }

        public Criteria andAcIsbspNotEqualTo(Integer value) {
            addCriterion("AC_IsBSP <>", value, "acIsbsp");
            return (Criteria) this;
        }

        public Criteria andAcIsbspGreaterThan(Integer value) {
            addCriterion("AC_IsBSP >", value, "acIsbsp");
            return (Criteria) this;
        }

        public Criteria andAcIsbspGreaterThanOrEqualTo(Integer value) {
            addCriterion("AC_IsBSP >=", value, "acIsbsp");
            return (Criteria) this;
        }

        public Criteria andAcIsbspLessThan(Integer value) {
            addCriterion("AC_IsBSP <", value, "acIsbsp");
            return (Criteria) this;
        }

        public Criteria andAcIsbspLessThanOrEqualTo(Integer value) {
            addCriterion("AC_IsBSP <=", value, "acIsbsp");
            return (Criteria) this;
        }

        public Criteria andAcIsbspIn(List<Integer> values) {
            addCriterion("AC_IsBSP in", values, "acIsbsp");
            return (Criteria) this;
        }

        public Criteria andAcIsbspNotIn(List<Integer> values) {
            addCriterion("AC_IsBSP not in", values, "acIsbsp");
            return (Criteria) this;
        }

        public Criteria andAcIsbspBetween(Integer value1, Integer value2) {
            addCriterion("AC_IsBSP between", value1, value2, "acIsbsp");
            return (Criteria) this;
        }

        public Criteria andAcIsbspNotBetween(Integer value1, Integer value2) {
            addCriterion("AC_IsBSP not between", value1, value2, "acIsbsp");
            return (Criteria) this;
        }

        public Criteria andAcOperatornumIsNull() {
            addCriterion("AC_OPeratorNum is null");
            return (Criteria) this;
        }

        public Criteria andAcOperatornumIsNotNull() {
            addCriterion("AC_OPeratorNum is not null");
            return (Criteria) this;
        }

        public Criteria andAcOperatornumEqualTo(Integer value) {
            addCriterion("AC_OPeratorNum =", value, "acOperatornum");
            return (Criteria) this;
        }

        public Criteria andAcOperatornumNotEqualTo(Integer value) {
            addCriterion("AC_OPeratorNum <>", value, "acOperatornum");
            return (Criteria) this;
        }

        public Criteria andAcOperatornumGreaterThan(Integer value) {
            addCriterion("AC_OPeratorNum >", value, "acOperatornum");
            return (Criteria) this;
        }

        public Criteria andAcOperatornumGreaterThanOrEqualTo(Integer value) {
            addCriterion("AC_OPeratorNum >=", value, "acOperatornum");
            return (Criteria) this;
        }

        public Criteria andAcOperatornumLessThan(Integer value) {
            addCriterion("AC_OPeratorNum <", value, "acOperatornum");
            return (Criteria) this;
        }

        public Criteria andAcOperatornumLessThanOrEqualTo(Integer value) {
            addCriterion("AC_OPeratorNum <=", value, "acOperatornum");
            return (Criteria) this;
        }

        public Criteria andAcOperatornumIn(List<Integer> values) {
            addCriterion("AC_OPeratorNum in", values, "acOperatornum");
            return (Criteria) this;
        }

        public Criteria andAcOperatornumNotIn(List<Integer> values) {
            addCriterion("AC_OPeratorNum not in", values, "acOperatornum");
            return (Criteria) this;
        }

        public Criteria andAcOperatornumBetween(Integer value1, Integer value2) {
            addCriterion("AC_OPeratorNum between", value1, value2, "acOperatornum");
            return (Criteria) this;
        }

        public Criteria andAcOperatornumNotBetween(Integer value1, Integer value2) {
            addCriterion("AC_OPeratorNum not between", value1, value2, "acOperatornum");
            return (Criteria) this;
        }

        public Criteria andAcIdaddressIsNull() {
            addCriterion("AC_IDAddress is null");
            return (Criteria) this;
        }

        public Criteria andAcIdaddressIsNotNull() {
            addCriterion("AC_IDAddress is not null");
            return (Criteria) this;
        }

        public Criteria andAcIdaddressEqualTo(String value) {
            addCriterion("AC_IDAddress =", value, "acIdaddress");
            return (Criteria) this;
        }

        public Criteria andAcIdaddressNotEqualTo(String value) {
            addCriterion("AC_IDAddress <>", value, "acIdaddress");
            return (Criteria) this;
        }

        public Criteria andAcIdaddressGreaterThan(String value) {
            addCriterion("AC_IDAddress >", value, "acIdaddress");
            return (Criteria) this;
        }

        public Criteria andAcIdaddressGreaterThanOrEqualTo(String value) {
            addCriterion("AC_IDAddress >=", value, "acIdaddress");
            return (Criteria) this;
        }

        public Criteria andAcIdaddressLessThan(String value) {
            addCriterion("AC_IDAddress <", value, "acIdaddress");
            return (Criteria) this;
        }

        public Criteria andAcIdaddressLessThanOrEqualTo(String value) {
            addCriterion("AC_IDAddress <=", value, "acIdaddress");
            return (Criteria) this;
        }

        public Criteria andAcIdaddressLike(String value) {
            addCriterion("AC_IDAddress like", value, "acIdaddress");
            return (Criteria) this;
        }

        public Criteria andAcIdaddressNotLike(String value) {
            addCriterion("AC_IDAddress not like", value, "acIdaddress");
            return (Criteria) this;
        }

        public Criteria andAcIdaddressIn(List<String> values) {
            addCriterion("AC_IDAddress in", values, "acIdaddress");
            return (Criteria) this;
        }

        public Criteria andAcIdaddressNotIn(List<String> values) {
            addCriterion("AC_IDAddress not in", values, "acIdaddress");
            return (Criteria) this;
        }

        public Criteria andAcIdaddressBetween(String value1, String value2) {
            addCriterion("AC_IDAddress between", value1, value2, "acIdaddress");
            return (Criteria) this;
        }

        public Criteria andAcIdaddressNotBetween(String value1, String value2) {
            addCriterion("AC_IDAddress not between", value1, value2, "acIdaddress");
            return (Criteria) this;
        }

        public Criteria andAcCancelstatusIsNull() {
            addCriterion("AC_CancelStatus is null");
            return (Criteria) this;
        }

        public Criteria andAcCancelstatusIsNotNull() {
            addCriterion("AC_CancelStatus is not null");
            return (Criteria) this;
        }

        public Criteria andAcCancelstatusEqualTo(Integer value) {
            addCriterion("AC_CancelStatus =", value, "acCancelstatus");
            return (Criteria) this;
        }

        public Criteria andAcCancelstatusNotEqualTo(Integer value) {
            addCriterion("AC_CancelStatus <>", value, "acCancelstatus");
            return (Criteria) this;
        }

        public Criteria andAcCancelstatusGreaterThan(Integer value) {
            addCriterion("AC_CancelStatus >", value, "acCancelstatus");
            return (Criteria) this;
        }

        public Criteria andAcCancelstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("AC_CancelStatus >=", value, "acCancelstatus");
            return (Criteria) this;
        }

        public Criteria andAcCancelstatusLessThan(Integer value) {
            addCriterion("AC_CancelStatus <", value, "acCancelstatus");
            return (Criteria) this;
        }

        public Criteria andAcCancelstatusLessThanOrEqualTo(Integer value) {
            addCriterion("AC_CancelStatus <=", value, "acCancelstatus");
            return (Criteria) this;
        }

        public Criteria andAcCancelstatusIn(List<Integer> values) {
            addCriterion("AC_CancelStatus in", values, "acCancelstatus");
            return (Criteria) this;
        }

        public Criteria andAcCancelstatusNotIn(List<Integer> values) {
            addCriterion("AC_CancelStatus not in", values, "acCancelstatus");
            return (Criteria) this;
        }

        public Criteria andAcCancelstatusBetween(Integer value1, Integer value2) {
            addCriterion("AC_CancelStatus between", value1, value2, "acCancelstatus");
            return (Criteria) this;
        }

        public Criteria andAcCancelstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("AC_CancelStatus not between", value1, value2, "acCancelstatus");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}