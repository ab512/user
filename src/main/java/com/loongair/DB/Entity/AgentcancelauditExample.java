package com.loongair.DB.Entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AgentcancelauditExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public AgentcancelauditExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTidIsNull() {
            addCriterion("Tid is null");
            return (Criteria) this;
        }

        public Criteria andTidIsNotNull() {
            addCriterion("Tid is not null");
            return (Criteria) this;
        }

        public Criteria andTidEqualTo(Long value) {
            addCriterion("Tid =", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotEqualTo(Long value) {
            addCriterion("Tid <>", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThan(Long value) {
            addCriterion("Tid >", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThanOrEqualTo(Long value) {
            addCriterion("Tid >=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThan(Long value) {
            addCriterion("Tid <", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThanOrEqualTo(Long value) {
            addCriterion("Tid <=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidIn(List<Long> values) {
            addCriterion("Tid in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotIn(List<Long> values) {
            addCriterion("Tid not in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidBetween(Long value1, Long value2) {
            addCriterion("Tid between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotBetween(Long value1, Long value2) {
            addCriterion("Tid not between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNull() {
            addCriterion("CreateId is null");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNotNull() {
            addCriterion("CreateId is not null");
            return (Criteria) this;
        }

        public Criteria andCreateidEqualTo(Long value) {
            addCriterion("CreateId =", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotEqualTo(Long value) {
            addCriterion("CreateId <>", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThan(Long value) {
            addCriterion("CreateId >", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThanOrEqualTo(Long value) {
            addCriterion("CreateId >=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThan(Long value) {
            addCriterion("CreateId <", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThanOrEqualTo(Long value) {
            addCriterion("CreateId <=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidIn(List<Long> values) {
            addCriterion("CreateId in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotIn(List<Long> values) {
            addCriterion("CreateId not in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidBetween(Long value1, Long value2) {
            addCriterion("CreateId between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotBetween(Long value1, Long value2) {
            addCriterion("CreateId not between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNull() {
            addCriterion("ProxyUserId is null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNotNull() {
            addCriterion("ProxyUserId is not null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridEqualTo(Long value) {
            addCriterion("ProxyUserId =", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotEqualTo(Long value) {
            addCriterion("ProxyUserId <>", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThan(Long value) {
            addCriterion("ProxyUserId >", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId >=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThan(Long value) {
            addCriterion("ProxyUserId <", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId <=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIn(List<Long> values) {
            addCriterion("ProxyUserId in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotIn(List<Long> values) {
            addCriterion("ProxyUserId not in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId not between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNull() {
            addCriterion("LastGuid is null");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNotNull() {
            addCriterion("LastGuid is not null");
            return (Criteria) this;
        }

        public Criteria andLastguidEqualTo(Long value) {
            addCriterion("LastGuid =", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotEqualTo(Long value) {
            addCriterion("LastGuid <>", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThan(Long value) {
            addCriterion("LastGuid >", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThanOrEqualTo(Long value) {
            addCriterion("LastGuid >=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThan(Long value) {
            addCriterion("LastGuid <", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThanOrEqualTo(Long value) {
            addCriterion("LastGuid <=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidIn(List<Long> values) {
            addCriterion("LastGuid in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotIn(List<Long> values) {
            addCriterion("LastGuid not in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidBetween(Long value1, Long value2) {
            addCriterion("LastGuid between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotBetween(Long value1, Long value2) {
            addCriterion("LastGuid not between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andCloneIsNull() {
            addCriterion("Clone is null");
            return (Criteria) this;
        }

        public Criteria andCloneIsNotNull() {
            addCriterion("Clone is not null");
            return (Criteria) this;
        }

        public Criteria andCloneEqualTo(Integer value) {
            addCriterion("Clone =", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotEqualTo(Integer value) {
            addCriterion("Clone <>", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThan(Integer value) {
            addCriterion("Clone >", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThanOrEqualTo(Integer value) {
            addCriterion("Clone >=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThan(Integer value) {
            addCriterion("Clone <", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThanOrEqualTo(Integer value) {
            addCriterion("Clone <=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneIn(List<Integer> values) {
            addCriterion("Clone in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotIn(List<Integer> values) {
            addCriterion("Clone not in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneBetween(Integer value1, Integer value2) {
            addCriterion("Clone between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotBetween(Integer value1, Integer value2) {
            addCriterion("Clone not between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNull() {
            addCriterion("UpdateId is null");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNotNull() {
            addCriterion("UpdateId is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateidEqualTo(Long value) {
            addCriterion("UpdateId =", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotEqualTo(Long value) {
            addCriterion("UpdateId <>", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThan(Long value) {
            addCriterion("UpdateId >", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThanOrEqualTo(Long value) {
            addCriterion("UpdateId >=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThan(Long value) {
            addCriterion("UpdateId <", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThanOrEqualTo(Long value) {
            addCriterion("UpdateId <=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidIn(List<Long> values) {
            addCriterion("UpdateId in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotIn(List<Long> values) {
            addCriterion("UpdateId not in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidBetween(Long value1, Long value2) {
            addCriterion("UpdateId between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotBetween(Long value1, Long value2) {
            addCriterion("UpdateId not between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("UpdateDate is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("UpdateDate is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("UpdateDate =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("UpdateDate <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("UpdateDate >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("UpdateDate >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("UpdateDate <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("UpdateDate <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("UpdateDate in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("UpdateDate not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("UpdateDate between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("UpdateDate not between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("Status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("Status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("Status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("Status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("Status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("Status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("Status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("Status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("Status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("Status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("Status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("Status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andAgcaAgentorgtidIsNull() {
            addCriterion("AGCA_AgentOrgTid is null");
            return (Criteria) this;
        }

        public Criteria andAgcaAgentorgtidIsNotNull() {
            addCriterion("AGCA_AgentOrgTid is not null");
            return (Criteria) this;
        }

        public Criteria andAgcaAgentorgtidEqualTo(Long value) {
            addCriterion("AGCA_AgentOrgTid =", value, "agcaAgentorgtid");
            return (Criteria) this;
        }

        public Criteria andAgcaAgentorgtidNotEqualTo(Long value) {
            addCriterion("AGCA_AgentOrgTid <>", value, "agcaAgentorgtid");
            return (Criteria) this;
        }

        public Criteria andAgcaAgentorgtidGreaterThan(Long value) {
            addCriterion("AGCA_AgentOrgTid >", value, "agcaAgentorgtid");
            return (Criteria) this;
        }

        public Criteria andAgcaAgentorgtidGreaterThanOrEqualTo(Long value) {
            addCriterion("AGCA_AgentOrgTid >=", value, "agcaAgentorgtid");
            return (Criteria) this;
        }

        public Criteria andAgcaAgentorgtidLessThan(Long value) {
            addCriterion("AGCA_AgentOrgTid <", value, "agcaAgentorgtid");
            return (Criteria) this;
        }

        public Criteria andAgcaAgentorgtidLessThanOrEqualTo(Long value) {
            addCriterion("AGCA_AgentOrgTid <=", value, "agcaAgentorgtid");
            return (Criteria) this;
        }

        public Criteria andAgcaAgentorgtidIn(List<Long> values) {
            addCriterion("AGCA_AgentOrgTid in", values, "agcaAgentorgtid");
            return (Criteria) this;
        }

        public Criteria andAgcaAgentorgtidNotIn(List<Long> values) {
            addCriterion("AGCA_AgentOrgTid not in", values, "agcaAgentorgtid");
            return (Criteria) this;
        }

        public Criteria andAgcaAgentorgtidBetween(Long value1, Long value2) {
            addCriterion("AGCA_AgentOrgTid between", value1, value2, "agcaAgentorgtid");
            return (Criteria) this;
        }

        public Criteria andAgcaAgentorgtidNotBetween(Long value1, Long value2) {
            addCriterion("AGCA_AgentOrgTid not between", value1, value2, "agcaAgentorgtid");
            return (Criteria) this;
        }

        public Criteria andAgcaIndexIsNull() {
            addCriterion("AGCA_Index is null");
            return (Criteria) this;
        }

        public Criteria andAgcaIndexIsNotNull() {
            addCriterion("AGCA_Index is not null");
            return (Criteria) this;
        }

        public Criteria andAgcaIndexEqualTo(Integer value) {
            addCriterion("AGCA_Index =", value, "agcaIndex");
            return (Criteria) this;
        }

        public Criteria andAgcaIndexNotEqualTo(Integer value) {
            addCriterion("AGCA_Index <>", value, "agcaIndex");
            return (Criteria) this;
        }

        public Criteria andAgcaIndexGreaterThan(Integer value) {
            addCriterion("AGCA_Index >", value, "agcaIndex");
            return (Criteria) this;
        }

        public Criteria andAgcaIndexGreaterThanOrEqualTo(Integer value) {
            addCriterion("AGCA_Index >=", value, "agcaIndex");
            return (Criteria) this;
        }

        public Criteria andAgcaIndexLessThan(Integer value) {
            addCriterion("AGCA_Index <", value, "agcaIndex");
            return (Criteria) this;
        }

        public Criteria andAgcaIndexLessThanOrEqualTo(Integer value) {
            addCriterion("AGCA_Index <=", value, "agcaIndex");
            return (Criteria) this;
        }

        public Criteria andAgcaIndexIn(List<Integer> values) {
            addCriterion("AGCA_Index in", values, "agcaIndex");
            return (Criteria) this;
        }

        public Criteria andAgcaIndexNotIn(List<Integer> values) {
            addCriterion("AGCA_Index not in", values, "agcaIndex");
            return (Criteria) this;
        }

        public Criteria andAgcaIndexBetween(Integer value1, Integer value2) {
            addCriterion("AGCA_Index between", value1, value2, "agcaIndex");
            return (Criteria) this;
        }

        public Criteria andAgcaIndexNotBetween(Integer value1, Integer value2) {
            addCriterion("AGCA_Index not between", value1, value2, "agcaIndex");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditortidIsNull() {
            addCriterion("AGCA_AuditorTid is null");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditortidIsNotNull() {
            addCriterion("AGCA_AuditorTid is not null");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditortidEqualTo(Long value) {
            addCriterion("AGCA_AuditorTid =", value, "agcaAuditortid");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditortidNotEqualTo(Long value) {
            addCriterion("AGCA_AuditorTid <>", value, "agcaAuditortid");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditortidGreaterThan(Long value) {
            addCriterion("AGCA_AuditorTid >", value, "agcaAuditortid");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditortidGreaterThanOrEqualTo(Long value) {
            addCriterion("AGCA_AuditorTid >=", value, "agcaAuditortid");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditortidLessThan(Long value) {
            addCriterion("AGCA_AuditorTid <", value, "agcaAuditortid");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditortidLessThanOrEqualTo(Long value) {
            addCriterion("AGCA_AuditorTid <=", value, "agcaAuditortid");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditortidIn(List<Long> values) {
            addCriterion("AGCA_AuditorTid in", values, "agcaAuditortid");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditortidNotIn(List<Long> values) {
            addCriterion("AGCA_AuditorTid not in", values, "agcaAuditortid");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditortidBetween(Long value1, Long value2) {
            addCriterion("AGCA_AuditorTid between", value1, value2, "agcaAuditortid");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditortidNotBetween(Long value1, Long value2) {
            addCriterion("AGCA_AuditorTid not between", value1, value2, "agcaAuditortid");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditorIsNull() {
            addCriterion("AGCA_Auditor is null");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditorIsNotNull() {
            addCriterion("AGCA_Auditor is not null");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditorEqualTo(String value) {
            addCriterion("AGCA_Auditor =", value, "agcaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditorNotEqualTo(String value) {
            addCriterion("AGCA_Auditor <>", value, "agcaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditorGreaterThan(String value) {
            addCriterion("AGCA_Auditor >", value, "agcaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditorGreaterThanOrEqualTo(String value) {
            addCriterion("AGCA_Auditor >=", value, "agcaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditorLessThan(String value) {
            addCriterion("AGCA_Auditor <", value, "agcaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditorLessThanOrEqualTo(String value) {
            addCriterion("AGCA_Auditor <=", value, "agcaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditorLike(String value) {
            addCriterion("AGCA_Auditor like", value, "agcaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditorNotLike(String value) {
            addCriterion("AGCA_Auditor not like", value, "agcaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditorIn(List<String> values) {
            addCriterion("AGCA_Auditor in", values, "agcaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditorNotIn(List<String> values) {
            addCriterion("AGCA_Auditor not in", values, "agcaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditorBetween(String value1, String value2) {
            addCriterion("AGCA_Auditor between", value1, value2, "agcaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditorNotBetween(String value1, String value2) {
            addCriterion("AGCA_Auditor not between", value1, value2, "agcaAuditor");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditretIsNull() {
            addCriterion("AGCA_AuditRet is null");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditretIsNotNull() {
            addCriterion("AGCA_AuditRet is not null");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditretEqualTo(Integer value) {
            addCriterion("AGCA_AuditRet =", value, "agcaAuditret");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditretNotEqualTo(Integer value) {
            addCriterion("AGCA_AuditRet <>", value, "agcaAuditret");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditretGreaterThan(Integer value) {
            addCriterion("AGCA_AuditRet >", value, "agcaAuditret");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditretGreaterThanOrEqualTo(Integer value) {
            addCriterion("AGCA_AuditRet >=", value, "agcaAuditret");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditretLessThan(Integer value) {
            addCriterion("AGCA_AuditRet <", value, "agcaAuditret");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditretLessThanOrEqualTo(Integer value) {
            addCriterion("AGCA_AuditRet <=", value, "agcaAuditret");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditretIn(List<Integer> values) {
            addCriterion("AGCA_AuditRet in", values, "agcaAuditret");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditretNotIn(List<Integer> values) {
            addCriterion("AGCA_AuditRet not in", values, "agcaAuditret");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditretBetween(Integer value1, Integer value2) {
            addCriterion("AGCA_AuditRet between", value1, value2, "agcaAuditret");
            return (Criteria) this;
        }

        public Criteria andAgcaAuditretNotBetween(Integer value1, Integer value2) {
            addCriterion("AGCA_AuditRet not between", value1, value2, "agcaAuditret");
            return (Criteria) this;
        }

        public Criteria andAgcaNoteIsNull() {
            addCriterion("AGCA_Note is null");
            return (Criteria) this;
        }

        public Criteria andAgcaNoteIsNotNull() {
            addCriterion("AGCA_Note is not null");
            return (Criteria) this;
        }

        public Criteria andAgcaNoteEqualTo(String value) {
            addCriterion("AGCA_Note =", value, "agcaNote");
            return (Criteria) this;
        }

        public Criteria andAgcaNoteNotEqualTo(String value) {
            addCriterion("AGCA_Note <>", value, "agcaNote");
            return (Criteria) this;
        }

        public Criteria andAgcaNoteGreaterThan(String value) {
            addCriterion("AGCA_Note >", value, "agcaNote");
            return (Criteria) this;
        }

        public Criteria andAgcaNoteGreaterThanOrEqualTo(String value) {
            addCriterion("AGCA_Note >=", value, "agcaNote");
            return (Criteria) this;
        }

        public Criteria andAgcaNoteLessThan(String value) {
            addCriterion("AGCA_Note <", value, "agcaNote");
            return (Criteria) this;
        }

        public Criteria andAgcaNoteLessThanOrEqualTo(String value) {
            addCriterion("AGCA_Note <=", value, "agcaNote");
            return (Criteria) this;
        }

        public Criteria andAgcaNoteLike(String value) {
            addCriterion("AGCA_Note like", value, "agcaNote");
            return (Criteria) this;
        }

        public Criteria andAgcaNoteNotLike(String value) {
            addCriterion("AGCA_Note not like", value, "agcaNote");
            return (Criteria) this;
        }

        public Criteria andAgcaNoteIn(List<String> values) {
            addCriterion("AGCA_Note in", values, "agcaNote");
            return (Criteria) this;
        }

        public Criteria andAgcaNoteNotIn(List<String> values) {
            addCriterion("AGCA_Note not in", values, "agcaNote");
            return (Criteria) this;
        }

        public Criteria andAgcaNoteBetween(String value1, String value2) {
            addCriterion("AGCA_Note between", value1, value2, "agcaNote");
            return (Criteria) this;
        }

        public Criteria andAgcaNoteNotBetween(String value1, String value2) {
            addCriterion("AGCA_Note not between", value1, value2, "agcaNote");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}