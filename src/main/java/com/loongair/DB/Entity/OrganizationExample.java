package com.loongair.DB.Entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrganizationExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public OrganizationExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTidIsNull() {
            addCriterion("Tid is null");
            return (Criteria) this;
        }

        public Criteria andTidIsNotNull() {
            addCriterion("Tid is not null");
            return (Criteria) this;
        }

        public Criteria andTidEqualTo(Long value) {
            addCriterion("Tid =", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotEqualTo(Long value) {
            addCriterion("Tid <>", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThan(Long value) {
            addCriterion("Tid >", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThanOrEqualTo(Long value) {
            addCriterion("Tid >=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThan(Long value) {
            addCriterion("Tid <", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThanOrEqualTo(Long value) {
            addCriterion("Tid <=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidIn(List<Long> values) {
            addCriterion("Tid in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotIn(List<Long> values) {
            addCriterion("Tid not in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidBetween(Long value1, Long value2) {
            addCriterion("Tid between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotBetween(Long value1, Long value2) {
            addCriterion("Tid not between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNull() {
            addCriterion("CreateId is null");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNotNull() {
            addCriterion("CreateId is not null");
            return (Criteria) this;
        }

        public Criteria andCreateidEqualTo(Long value) {
            addCriterion("CreateId =", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotEqualTo(Long value) {
            addCriterion("CreateId <>", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThan(Long value) {
            addCriterion("CreateId >", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThanOrEqualTo(Long value) {
            addCriterion("CreateId >=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThan(Long value) {
            addCriterion("CreateId <", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThanOrEqualTo(Long value) {
            addCriterion("CreateId <=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidIn(List<Long> values) {
            addCriterion("CreateId in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotIn(List<Long> values) {
            addCriterion("CreateId not in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidBetween(Long value1, Long value2) {
            addCriterion("CreateId between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotBetween(Long value1, Long value2) {
            addCriterion("CreateId not between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNull() {
            addCriterion("ProxyUserId is null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNotNull() {
            addCriterion("ProxyUserId is not null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridEqualTo(Long value) {
            addCriterion("ProxyUserId =", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotEqualTo(Long value) {
            addCriterion("ProxyUserId <>", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThan(Long value) {
            addCriterion("ProxyUserId >", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId >=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThan(Long value) {
            addCriterion("ProxyUserId <", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId <=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIn(List<Long> values) {
            addCriterion("ProxyUserId in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotIn(List<Long> values) {
            addCriterion("ProxyUserId not in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId not between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNull() {
            addCriterion("LastGuid is null");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNotNull() {
            addCriterion("LastGuid is not null");
            return (Criteria) this;
        }

        public Criteria andLastguidEqualTo(Long value) {
            addCriterion("LastGuid =", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotEqualTo(Long value) {
            addCriterion("LastGuid <>", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThan(Long value) {
            addCriterion("LastGuid >", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThanOrEqualTo(Long value) {
            addCriterion("LastGuid >=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThan(Long value) {
            addCriterion("LastGuid <", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThanOrEqualTo(Long value) {
            addCriterion("LastGuid <=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidIn(List<Long> values) {
            addCriterion("LastGuid in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotIn(List<Long> values) {
            addCriterion("LastGuid not in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidBetween(Long value1, Long value2) {
            addCriterion("LastGuid between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotBetween(Long value1, Long value2) {
            addCriterion("LastGuid not between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andCloneIsNull() {
            addCriterion("Clone is null");
            return (Criteria) this;
        }

        public Criteria andCloneIsNotNull() {
            addCriterion("Clone is not null");
            return (Criteria) this;
        }

        public Criteria andCloneEqualTo(Integer value) {
            addCriterion("Clone =", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotEqualTo(Integer value) {
            addCriterion("Clone <>", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThan(Integer value) {
            addCriterion("Clone >", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThanOrEqualTo(Integer value) {
            addCriterion("Clone >=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThan(Integer value) {
            addCriterion("Clone <", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThanOrEqualTo(Integer value) {
            addCriterion("Clone <=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneIn(List<Integer> values) {
            addCriterion("Clone in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotIn(List<Integer> values) {
            addCriterion("Clone not in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneBetween(Integer value1, Integer value2) {
            addCriterion("Clone between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotBetween(Integer value1, Integer value2) {
            addCriterion("Clone not between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNull() {
            addCriterion("UpdateId is null");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNotNull() {
            addCriterion("UpdateId is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateidEqualTo(Long value) {
            addCriterion("UpdateId =", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotEqualTo(Long value) {
            addCriterion("UpdateId <>", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThan(Long value) {
            addCriterion("UpdateId >", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThanOrEqualTo(Long value) {
            addCriterion("UpdateId >=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThan(Long value) {
            addCriterion("UpdateId <", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThanOrEqualTo(Long value) {
            addCriterion("UpdateId <=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidIn(List<Long> values) {
            addCriterion("UpdateId in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotIn(List<Long> values) {
            addCriterion("UpdateId not in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidBetween(Long value1, Long value2) {
            addCriterion("UpdateId between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotBetween(Long value1, Long value2) {
            addCriterion("UpdateId not between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("UpdateDate is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("UpdateDate is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("UpdateDate =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("UpdateDate <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("UpdateDate >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("UpdateDate >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("UpdateDate <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("UpdateDate <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("UpdateDate in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("UpdateDate not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("UpdateDate between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("UpdateDate not between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("Status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("Status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("Status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("Status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("Status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("Status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("Status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("Status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("Status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("Status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("Status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("Status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andCNameIsNull() {
            addCriterion("C_Name is null");
            return (Criteria) this;
        }

        public Criteria andCNameIsNotNull() {
            addCriterion("C_Name is not null");
            return (Criteria) this;
        }

        public Criteria andCNameEqualTo(String value) {
            addCriterion("C_Name =", value, "cName");
            return (Criteria) this;
        }

        public Criteria andCNameNotEqualTo(String value) {
            addCriterion("C_Name <>", value, "cName");
            return (Criteria) this;
        }

        public Criteria andCNameGreaterThan(String value) {
            addCriterion("C_Name >", value, "cName");
            return (Criteria) this;
        }

        public Criteria andCNameGreaterThanOrEqualTo(String value) {
            addCriterion("C_Name >=", value, "cName");
            return (Criteria) this;
        }

        public Criteria andCNameLessThan(String value) {
            addCriterion("C_Name <", value, "cName");
            return (Criteria) this;
        }

        public Criteria andCNameLessThanOrEqualTo(String value) {
            addCriterion("C_Name <=", value, "cName");
            return (Criteria) this;
        }

        public Criteria andCNameLike(String value) {
            addCriterion("C_Name like", value, "cName");
            return (Criteria) this;
        }

        public Criteria andCNameNotLike(String value) {
            addCriterion("C_Name not like", value, "cName");
            return (Criteria) this;
        }

        public Criteria andCNameIn(List<String> values) {
            addCriterion("C_Name in", values, "cName");
            return (Criteria) this;
        }

        public Criteria andCNameNotIn(List<String> values) {
            addCriterion("C_Name not in", values, "cName");
            return (Criteria) this;
        }

        public Criteria andCNameBetween(String value1, String value2) {
            addCriterion("C_Name between", value1, value2, "cName");
            return (Criteria) this;
        }

        public Criteria andCNameNotBetween(String value1, String value2) {
            addCriterion("C_Name not between", value1, value2, "cName");
            return (Criteria) this;
        }

        public Criteria andCIndexIsNull() {
            addCriterion("C_Index is null");
            return (Criteria) this;
        }

        public Criteria andCIndexIsNotNull() {
            addCriterion("C_Index is not null");
            return (Criteria) this;
        }

        public Criteria andCIndexEqualTo(Integer value) {
            addCriterion("C_Index =", value, "cIndex");
            return (Criteria) this;
        }

        public Criteria andCIndexNotEqualTo(Integer value) {
            addCriterion("C_Index <>", value, "cIndex");
            return (Criteria) this;
        }

        public Criteria andCIndexGreaterThan(Integer value) {
            addCriterion("C_Index >", value, "cIndex");
            return (Criteria) this;
        }

        public Criteria andCIndexGreaterThanOrEqualTo(Integer value) {
            addCriterion("C_Index >=", value, "cIndex");
            return (Criteria) this;
        }

        public Criteria andCIndexLessThan(Integer value) {
            addCriterion("C_Index <", value, "cIndex");
            return (Criteria) this;
        }

        public Criteria andCIndexLessThanOrEqualTo(Integer value) {
            addCriterion("C_Index <=", value, "cIndex");
            return (Criteria) this;
        }

        public Criteria andCIndexIn(List<Integer> values) {
            addCriterion("C_Index in", values, "cIndex");
            return (Criteria) this;
        }

        public Criteria andCIndexNotIn(List<Integer> values) {
            addCriterion("C_Index not in", values, "cIndex");
            return (Criteria) this;
        }

        public Criteria andCIndexBetween(Integer value1, Integer value2) {
            addCriterion("C_Index between", value1, value2, "cIndex");
            return (Criteria) this;
        }

        public Criteria andCIndexNotBetween(Integer value1, Integer value2) {
            addCriterion("C_Index not between", value1, value2, "cIndex");
            return (Criteria) this;
        }

        public Criteria andCTreecodeIsNull() {
            addCriterion("C_TreeCode is null");
            return (Criteria) this;
        }

        public Criteria andCTreecodeIsNotNull() {
            addCriterion("C_TreeCode is not null");
            return (Criteria) this;
        }

        public Criteria andCTreecodeEqualTo(String value) {
            addCriterion("C_TreeCode =", value, "cTreecode");
            return (Criteria) this;
        }

        public Criteria andCTreecodeNotEqualTo(String value) {
            addCriterion("C_TreeCode <>", value, "cTreecode");
            return (Criteria) this;
        }

        public Criteria andCTreecodeGreaterThan(String value) {
            addCriterion("C_TreeCode >", value, "cTreecode");
            return (Criteria) this;
        }

        public Criteria andCTreecodeGreaterThanOrEqualTo(String value) {
            addCriterion("C_TreeCode >=", value, "cTreecode");
            return (Criteria) this;
        }

        public Criteria andCTreecodeLessThan(String value) {
            addCriterion("C_TreeCode <", value, "cTreecode");
            return (Criteria) this;
        }

        public Criteria andCTreecodeLessThanOrEqualTo(String value) {
            addCriterion("C_TreeCode <=", value, "cTreecode");
            return (Criteria) this;
        }

        public Criteria andCTreecodeLike(String value) {
            addCriterion("C_TreeCode like", value, "cTreecode");
            return (Criteria) this;
        }

        public Criteria andCTreecodeNotLike(String value) {
            addCriterion("C_TreeCode not like", value, "cTreecode");
            return (Criteria) this;
        }

        public Criteria andCTreecodeIn(List<String> values) {
            addCriterion("C_TreeCode in", values, "cTreecode");
            return (Criteria) this;
        }

        public Criteria andCTreecodeNotIn(List<String> values) {
            addCriterion("C_TreeCode not in", values, "cTreecode");
            return (Criteria) this;
        }

        public Criteria andCTreecodeBetween(String value1, String value2) {
            addCriterion("C_TreeCode between", value1, value2, "cTreecode");
            return (Criteria) this;
        }

        public Criteria andCTreecodeNotBetween(String value1, String value2) {
            addCriterion("C_TreeCode not between", value1, value2, "cTreecode");
            return (Criteria) this;
        }

        public Criteria andCCompanyidIsNull() {
            addCriterion("C_CompanyId is null");
            return (Criteria) this;
        }

        public Criteria andCCompanyidIsNotNull() {
            addCriterion("C_CompanyId is not null");
            return (Criteria) this;
        }

        public Criteria andCCompanyidEqualTo(Long value) {
            addCriterion("C_CompanyId =", value, "cCompanyid");
            return (Criteria) this;
        }

        public Criteria andCCompanyidNotEqualTo(Long value) {
            addCriterion("C_CompanyId <>", value, "cCompanyid");
            return (Criteria) this;
        }

        public Criteria andCCompanyidGreaterThan(Long value) {
            addCriterion("C_CompanyId >", value, "cCompanyid");
            return (Criteria) this;
        }

        public Criteria andCCompanyidGreaterThanOrEqualTo(Long value) {
            addCriterion("C_CompanyId >=", value, "cCompanyid");
            return (Criteria) this;
        }

        public Criteria andCCompanyidLessThan(Long value) {
            addCriterion("C_CompanyId <", value, "cCompanyid");
            return (Criteria) this;
        }

        public Criteria andCCompanyidLessThanOrEqualTo(Long value) {
            addCriterion("C_CompanyId <=", value, "cCompanyid");
            return (Criteria) this;
        }

        public Criteria andCCompanyidIn(List<Long> values) {
            addCriterion("C_CompanyId in", values, "cCompanyid");
            return (Criteria) this;
        }

        public Criteria andCCompanyidNotIn(List<Long> values) {
            addCriterion("C_CompanyId not in", values, "cCompanyid");
            return (Criteria) this;
        }

        public Criteria andCCompanyidBetween(Long value1, Long value2) {
            addCriterion("C_CompanyId between", value1, value2, "cCompanyid");
            return (Criteria) this;
        }

        public Criteria andCCompanyidNotBetween(Long value1, Long value2) {
            addCriterion("C_CompanyId not between", value1, value2, "cCompanyid");
            return (Criteria) this;
        }

        public Criteria andOAddressIsNull() {
            addCriterion("O_Address is null");
            return (Criteria) this;
        }

        public Criteria andOAddressIsNotNull() {
            addCriterion("O_Address is not null");
            return (Criteria) this;
        }

        public Criteria andOAddressEqualTo(String value) {
            addCriterion("O_Address =", value, "oAddress");
            return (Criteria) this;
        }

        public Criteria andOAddressNotEqualTo(String value) {
            addCriterion("O_Address <>", value, "oAddress");
            return (Criteria) this;
        }

        public Criteria andOAddressGreaterThan(String value) {
            addCriterion("O_Address >", value, "oAddress");
            return (Criteria) this;
        }

        public Criteria andOAddressGreaterThanOrEqualTo(String value) {
            addCriterion("O_Address >=", value, "oAddress");
            return (Criteria) this;
        }

        public Criteria andOAddressLessThan(String value) {
            addCriterion("O_Address <", value, "oAddress");
            return (Criteria) this;
        }

        public Criteria andOAddressLessThanOrEqualTo(String value) {
            addCriterion("O_Address <=", value, "oAddress");
            return (Criteria) this;
        }

        public Criteria andOAddressLike(String value) {
            addCriterion("O_Address like", value, "oAddress");
            return (Criteria) this;
        }

        public Criteria andOAddressNotLike(String value) {
            addCriterion("O_Address not like", value, "oAddress");
            return (Criteria) this;
        }

        public Criteria andOAddressIn(List<String> values) {
            addCriterion("O_Address in", values, "oAddress");
            return (Criteria) this;
        }

        public Criteria andOAddressNotIn(List<String> values) {
            addCriterion("O_Address not in", values, "oAddress");
            return (Criteria) this;
        }

        public Criteria andOAddressBetween(String value1, String value2) {
            addCriterion("O_Address between", value1, value2, "oAddress");
            return (Criteria) this;
        }

        public Criteria andOAddressNotBetween(String value1, String value2) {
            addCriterion("O_Address not between", value1, value2, "oAddress");
            return (Criteria) this;
        }

        public Criteria andOZipIsNull() {
            addCriterion("O_Zip is null");
            return (Criteria) this;
        }

        public Criteria andOZipIsNotNull() {
            addCriterion("O_Zip is not null");
            return (Criteria) this;
        }

        public Criteria andOZipEqualTo(String value) {
            addCriterion("O_Zip =", value, "oZip");
            return (Criteria) this;
        }

        public Criteria andOZipNotEqualTo(String value) {
            addCriterion("O_Zip <>", value, "oZip");
            return (Criteria) this;
        }

        public Criteria andOZipGreaterThan(String value) {
            addCriterion("O_Zip >", value, "oZip");
            return (Criteria) this;
        }

        public Criteria andOZipGreaterThanOrEqualTo(String value) {
            addCriterion("O_Zip >=", value, "oZip");
            return (Criteria) this;
        }

        public Criteria andOZipLessThan(String value) {
            addCriterion("O_Zip <", value, "oZip");
            return (Criteria) this;
        }

        public Criteria andOZipLessThanOrEqualTo(String value) {
            addCriterion("O_Zip <=", value, "oZip");
            return (Criteria) this;
        }

        public Criteria andOZipLike(String value) {
            addCriterion("O_Zip like", value, "oZip");
            return (Criteria) this;
        }

        public Criteria andOZipNotLike(String value) {
            addCriterion("O_Zip not like", value, "oZip");
            return (Criteria) this;
        }

        public Criteria andOZipIn(List<String> values) {
            addCriterion("O_Zip in", values, "oZip");
            return (Criteria) this;
        }

        public Criteria andOZipNotIn(List<String> values) {
            addCriterion("O_Zip not in", values, "oZip");
            return (Criteria) this;
        }

        public Criteria andOZipBetween(String value1, String value2) {
            addCriterion("O_Zip between", value1, value2, "oZip");
            return (Criteria) this;
        }

        public Criteria andOZipNotBetween(String value1, String value2) {
            addCriterion("O_Zip not between", value1, value2, "oZip");
            return (Criteria) this;
        }

        public Criteria andOLeadernameIsNull() {
            addCriterion("O_LeaderName is null");
            return (Criteria) this;
        }

        public Criteria andOLeadernameIsNotNull() {
            addCriterion("O_LeaderName is not null");
            return (Criteria) this;
        }

        public Criteria andOLeadernameEqualTo(String value) {
            addCriterion("O_LeaderName =", value, "oLeadername");
            return (Criteria) this;
        }

        public Criteria andOLeadernameNotEqualTo(String value) {
            addCriterion("O_LeaderName <>", value, "oLeadername");
            return (Criteria) this;
        }

        public Criteria andOLeadernameGreaterThan(String value) {
            addCriterion("O_LeaderName >", value, "oLeadername");
            return (Criteria) this;
        }

        public Criteria andOLeadernameGreaterThanOrEqualTo(String value) {
            addCriterion("O_LeaderName >=", value, "oLeadername");
            return (Criteria) this;
        }

        public Criteria andOLeadernameLessThan(String value) {
            addCriterion("O_LeaderName <", value, "oLeadername");
            return (Criteria) this;
        }

        public Criteria andOLeadernameLessThanOrEqualTo(String value) {
            addCriterion("O_LeaderName <=", value, "oLeadername");
            return (Criteria) this;
        }

        public Criteria andOLeadernameLike(String value) {
            addCriterion("O_LeaderName like", value, "oLeadername");
            return (Criteria) this;
        }

        public Criteria andOLeadernameNotLike(String value) {
            addCriterion("O_LeaderName not like", value, "oLeadername");
            return (Criteria) this;
        }

        public Criteria andOLeadernameIn(List<String> values) {
            addCriterion("O_LeaderName in", values, "oLeadername");
            return (Criteria) this;
        }

        public Criteria andOLeadernameNotIn(List<String> values) {
            addCriterion("O_LeaderName not in", values, "oLeadername");
            return (Criteria) this;
        }

        public Criteria andOLeadernameBetween(String value1, String value2) {
            addCriterion("O_LeaderName between", value1, value2, "oLeadername");
            return (Criteria) this;
        }

        public Criteria andOLeadernameNotBetween(String value1, String value2) {
            addCriterion("O_LeaderName not between", value1, value2, "oLeadername");
            return (Criteria) this;
        }

        public Criteria andOLeaderphoneIsNull() {
            addCriterion("O_LeaderPhone is null");
            return (Criteria) this;
        }

        public Criteria andOLeaderphoneIsNotNull() {
            addCriterion("O_LeaderPhone is not null");
            return (Criteria) this;
        }

        public Criteria andOLeaderphoneEqualTo(String value) {
            addCriterion("O_LeaderPhone =", value, "oLeaderphone");
            return (Criteria) this;
        }

        public Criteria andOLeaderphoneNotEqualTo(String value) {
            addCriterion("O_LeaderPhone <>", value, "oLeaderphone");
            return (Criteria) this;
        }

        public Criteria andOLeaderphoneGreaterThan(String value) {
            addCriterion("O_LeaderPhone >", value, "oLeaderphone");
            return (Criteria) this;
        }

        public Criteria andOLeaderphoneGreaterThanOrEqualTo(String value) {
            addCriterion("O_LeaderPhone >=", value, "oLeaderphone");
            return (Criteria) this;
        }

        public Criteria andOLeaderphoneLessThan(String value) {
            addCriterion("O_LeaderPhone <", value, "oLeaderphone");
            return (Criteria) this;
        }

        public Criteria andOLeaderphoneLessThanOrEqualTo(String value) {
            addCriterion("O_LeaderPhone <=", value, "oLeaderphone");
            return (Criteria) this;
        }

        public Criteria andOLeaderphoneLike(String value) {
            addCriterion("O_LeaderPhone like", value, "oLeaderphone");
            return (Criteria) this;
        }

        public Criteria andOLeaderphoneNotLike(String value) {
            addCriterion("O_LeaderPhone not like", value, "oLeaderphone");
            return (Criteria) this;
        }

        public Criteria andOLeaderphoneIn(List<String> values) {
            addCriterion("O_LeaderPhone in", values, "oLeaderphone");
            return (Criteria) this;
        }

        public Criteria andOLeaderphoneNotIn(List<String> values) {
            addCriterion("O_LeaderPhone not in", values, "oLeaderphone");
            return (Criteria) this;
        }

        public Criteria andOLeaderphoneBetween(String value1, String value2) {
            addCriterion("O_LeaderPhone between", value1, value2, "oLeaderphone");
            return (Criteria) this;
        }

        public Criteria andOLeaderphoneNotBetween(String value1, String value2) {
            addCriterion("O_LeaderPhone not between", value1, value2, "oLeaderphone");
            return (Criteria) this;
        }

        public Criteria andOQqIsNull() {
            addCriterion("O_QQ is null");
            return (Criteria) this;
        }

        public Criteria andOQqIsNotNull() {
            addCriterion("O_QQ is not null");
            return (Criteria) this;
        }

        public Criteria andOQqEqualTo(String value) {
            addCriterion("O_QQ =", value, "oQq");
            return (Criteria) this;
        }

        public Criteria andOQqNotEqualTo(String value) {
            addCriterion("O_QQ <>", value, "oQq");
            return (Criteria) this;
        }

        public Criteria andOQqGreaterThan(String value) {
            addCriterion("O_QQ >", value, "oQq");
            return (Criteria) this;
        }

        public Criteria andOQqGreaterThanOrEqualTo(String value) {
            addCriterion("O_QQ >=", value, "oQq");
            return (Criteria) this;
        }

        public Criteria andOQqLessThan(String value) {
            addCriterion("O_QQ <", value, "oQq");
            return (Criteria) this;
        }

        public Criteria andOQqLessThanOrEqualTo(String value) {
            addCriterion("O_QQ <=", value, "oQq");
            return (Criteria) this;
        }

        public Criteria andOQqLike(String value) {
            addCriterion("O_QQ like", value, "oQq");
            return (Criteria) this;
        }

        public Criteria andOQqNotLike(String value) {
            addCriterion("O_QQ not like", value, "oQq");
            return (Criteria) this;
        }

        public Criteria andOQqIn(List<String> values) {
            addCriterion("O_QQ in", values, "oQq");
            return (Criteria) this;
        }

        public Criteria andOQqNotIn(List<String> values) {
            addCriterion("O_QQ not in", values, "oQq");
            return (Criteria) this;
        }

        public Criteria andOQqBetween(String value1, String value2) {
            addCriterion("O_QQ between", value1, value2, "oQq");
            return (Criteria) this;
        }

        public Criteria andOQqNotBetween(String value1, String value2) {
            addCriterion("O_QQ not between", value1, value2, "oQq");
            return (Criteria) this;
        }

        public Criteria andOSortIsNull() {
            addCriterion("O_Sort is null");
            return (Criteria) this;
        }

        public Criteria andOSortIsNotNull() {
            addCriterion("O_Sort is not null");
            return (Criteria) this;
        }

        public Criteria andOSortEqualTo(Integer value) {
            addCriterion("O_Sort =", value, "oSort");
            return (Criteria) this;
        }

        public Criteria andOSortNotEqualTo(Integer value) {
            addCriterion("O_Sort <>", value, "oSort");
            return (Criteria) this;
        }

        public Criteria andOSortGreaterThan(Integer value) {
            addCriterion("O_Sort >", value, "oSort");
            return (Criteria) this;
        }

        public Criteria andOSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("O_Sort >=", value, "oSort");
            return (Criteria) this;
        }

        public Criteria andOSortLessThan(Integer value) {
            addCriterion("O_Sort <", value, "oSort");
            return (Criteria) this;
        }

        public Criteria andOSortLessThanOrEqualTo(Integer value) {
            addCriterion("O_Sort <=", value, "oSort");
            return (Criteria) this;
        }

        public Criteria andOSortIn(List<Integer> values) {
            addCriterion("O_Sort in", values, "oSort");
            return (Criteria) this;
        }

        public Criteria andOSortNotIn(List<Integer> values) {
            addCriterion("O_Sort not in", values, "oSort");
            return (Criteria) this;
        }

        public Criteria andOSortBetween(Integer value1, Integer value2) {
            addCriterion("O_Sort between", value1, value2, "oSort");
            return (Criteria) this;
        }

        public Criteria andOSortNotBetween(Integer value1, Integer value2) {
            addCriterion("O_Sort not between", value1, value2, "oSort");
            return (Criteria) this;
        }

        public Criteria andOUsersystemidIsNull() {
            addCriterion("O_UserSystemId is null");
            return (Criteria) this;
        }

        public Criteria andOUsersystemidIsNotNull() {
            addCriterion("O_UserSystemId is not null");
            return (Criteria) this;
        }

        public Criteria andOUsersystemidEqualTo(String value) {
            addCriterion("O_UserSystemId =", value, "oUsersystemid");
            return (Criteria) this;
        }

        public Criteria andOUsersystemidNotEqualTo(String value) {
            addCriterion("O_UserSystemId <>", value, "oUsersystemid");
            return (Criteria) this;
        }

        public Criteria andOUsersystemidGreaterThan(String value) {
            addCriterion("O_UserSystemId >", value, "oUsersystemid");
            return (Criteria) this;
        }

        public Criteria andOUsersystemidGreaterThanOrEqualTo(String value) {
            addCriterion("O_UserSystemId >=", value, "oUsersystemid");
            return (Criteria) this;
        }

        public Criteria andOUsersystemidLessThan(String value) {
            addCriterion("O_UserSystemId <", value, "oUsersystemid");
            return (Criteria) this;
        }

        public Criteria andOUsersystemidLessThanOrEqualTo(String value) {
            addCriterion("O_UserSystemId <=", value, "oUsersystemid");
            return (Criteria) this;
        }

        public Criteria andOUsersystemidLike(String value) {
            addCriterion("O_UserSystemId like", value, "oUsersystemid");
            return (Criteria) this;
        }

        public Criteria andOUsersystemidNotLike(String value) {
            addCriterion("O_UserSystemId not like", value, "oUsersystemid");
            return (Criteria) this;
        }

        public Criteria andOUsersystemidIn(List<String> values) {
            addCriterion("O_UserSystemId in", values, "oUsersystemid");
            return (Criteria) this;
        }

        public Criteria andOUsersystemidNotIn(List<String> values) {
            addCriterion("O_UserSystemId not in", values, "oUsersystemid");
            return (Criteria) this;
        }

        public Criteria andOUsersystemidBetween(String value1, String value2) {
            addCriterion("O_UserSystemId between", value1, value2, "oUsersystemid");
            return (Criteria) this;
        }

        public Criteria andOUsersystemidNotBetween(String value1, String value2) {
            addCriterion("O_UserSystemId not between", value1, value2, "oUsersystemid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}