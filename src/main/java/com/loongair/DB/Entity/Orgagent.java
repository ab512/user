package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Orgagent extends BaseEntity {


    private Long oaLaorgtid;

    private Integer oaAuditstatus;

    private String acAgentcompanyname;

    private String acOfficecode;

    private String acIatano;

    private String acBelongdepartment;

    private String acCountry;

    private String acCity;

    private Integer acAgentusertype;

    private Integer acMoneytype;

    private String acContact;

    private String acContactphone;

    private String acAddress;

    private String acEmail;

    private String acZipcode;

    private String acFax;

    private String acTelephone;

    private Float acIagencyrate;

    private Float acDagencyrate;

    private Integer acAgencytype;

    private String acPermission;

    private String acAdministrator;

    private String oaTeamcode;

    private Date acRegistrationtime;

    private String acSettlementcode;

    private Long acLaorgtid;

    private Integer acAuditstatus;

    private String acTeamcode;

    private Integer acIsbsp;

    private Integer acOperatornum;

    private String acIdaddress;

    private Integer acCancelstatus;

    

    public Long getOaLaorgtid() {
        return oaLaorgtid;
    }

    public void setOaLaorgtid(Long oaLaorgtid) {
        this.oaLaorgtid = oaLaorgtid;
    }

    public Integer getOaAuditstatus() {
        return oaAuditstatus;
    }

    public void setOaAuditstatus(Integer oaAuditstatus) {
        this.oaAuditstatus = oaAuditstatus;
    }

    public String getAcAgentcompanyname() {
        return acAgentcompanyname;
    }

    public void setAcAgentcompanyname(String acAgentcompanyname) {
        this.acAgentcompanyname = acAgentcompanyname == null ? null : acAgentcompanyname.trim();
    }

    public String getAcOfficecode() {
        return acOfficecode;
    }

    public void setAcOfficecode(String acOfficecode) {
        this.acOfficecode = acOfficecode == null ? null : acOfficecode.trim();
    }

    public String getAcIatano() {
        return acIatano;
    }

    public void setAcIatano(String acIatano) {
        this.acIatano = acIatano == null ? null : acIatano.trim();
    }

    public String getAcBelongdepartment() {
        return acBelongdepartment;
    }

    public void setAcBelongdepartment(String acBelongdepartment) {
        this.acBelongdepartment = acBelongdepartment == null ? null : acBelongdepartment.trim();
    }

    public String getAcCountry() {
        return acCountry;
    }

    public void setAcCountry(String acCountry) {
        this.acCountry = acCountry == null ? null : acCountry.trim();
    }

    public String getAcCity() {
        return acCity;
    }

    public void setAcCity(String acCity) {
        this.acCity = acCity == null ? null : acCity.trim();
    }

    public Integer getAcAgentusertype() {
        return acAgentusertype;
    }

    public void setAcAgentusertype(Integer acAgentusertype) {
        this.acAgentusertype = acAgentusertype;
    }

    public Integer getAcMoneytype() {
        return acMoneytype;
    }

    public void setAcMoneytype(Integer acMoneytype) {
        this.acMoneytype = acMoneytype;
    }

    public String getAcContact() {
        return acContact;
    }

    public void setAcContact(String acContact) {
        this.acContact = acContact == null ? null : acContact.trim();
    }

    public String getAcContactphone() {
        return acContactphone;
    }

    public void setAcContactphone(String acContactphone) {
        this.acContactphone = acContactphone == null ? null : acContactphone.trim();
    }

    public String getAcAddress() {
        return acAddress;
    }

    public void setAcAddress(String acAddress) {
        this.acAddress = acAddress == null ? null : acAddress.trim();
    }

    public String getAcEmail() {
        return acEmail;
    }

    public void setAcEmail(String acEmail) {
        this.acEmail = acEmail == null ? null : acEmail.trim();
    }

    public String getAcZipcode() {
        return acZipcode;
    }

    public void setAcZipcode(String acZipcode) {
        this.acZipcode = acZipcode == null ? null : acZipcode.trim();
    }

    public String getAcFax() {
        return acFax;
    }

    public void setAcFax(String acFax) {
        this.acFax = acFax == null ? null : acFax.trim();
    }

    public String getAcTelephone() {
        return acTelephone;
    }

    public void setAcTelephone(String acTelephone) {
        this.acTelephone = acTelephone == null ? null : acTelephone.trim();
    }

    public Float getAcIagencyrate() {
        return acIagencyrate;
    }

    public void setAcIagencyrate(Float acIagencyrate) {
        this.acIagencyrate = acIagencyrate;
    }

    public Float getAcDagencyrate() {
        return acDagencyrate;
    }

    public void setAcDagencyrate(Float acDagencyrate) {
        this.acDagencyrate = acDagencyrate;
    }

    public Integer getAcAgencytype() {
        return acAgencytype;
    }

    public void setAcAgencytype(Integer acAgencytype) {
        this.acAgencytype = acAgencytype;
    }

    public String getAcPermission() {
        return acPermission;
    }

    public void setAcPermission(String acPermission) {
        this.acPermission = acPermission == null ? null : acPermission.trim();
    }

    public String getAcAdministrator() {
        return acAdministrator;
    }

    public void setAcAdministrator(String acAdministrator) {
        this.acAdministrator = acAdministrator == null ? null : acAdministrator.trim();
    }

    public String getOaTeamcode() {
        return oaTeamcode;
    }

    public void setOaTeamcode(String oaTeamcode) {
        this.oaTeamcode = oaTeamcode == null ? null : oaTeamcode.trim();
    }

    public Date getAcRegistrationtime() {
        return acRegistrationtime;
    }

    public void setAcRegistrationtime(Date acRegistrationtime) {
        this.acRegistrationtime = acRegistrationtime;
    }

    public String getAcSettlementcode() {
        return acSettlementcode;
    }

    public void setAcSettlementcode(String acSettlementcode) {
        this.acSettlementcode = acSettlementcode == null ? null : acSettlementcode.trim();
    }

    public Long getAcLaorgtid() {
        return acLaorgtid;
    }

    public void setAcLaorgtid(Long acLaorgtid) {
        this.acLaorgtid = acLaorgtid;
    }

    public Integer getAcAuditstatus() {
        return acAuditstatus;
    }

    public void setAcAuditstatus(Integer acAuditstatus) {
        this.acAuditstatus = acAuditstatus;
    }

    public String getAcTeamcode() {
        return acTeamcode;
    }

    public void setAcTeamcode(String acTeamcode) {
        this.acTeamcode = acTeamcode == null ? null : acTeamcode.trim();
    }

    public Integer getAcIsbsp() {
        return acIsbsp;
    }

    public void setAcIsbsp(Integer acIsbsp) {
        this.acIsbsp = acIsbsp;
    }

    public Integer getAcOperatornum() {
        return acOperatornum;
    }

    public void setAcOperatornum(Integer acOperatornum) {
        this.acOperatornum = acOperatornum;
    }

    public String getAcIdaddress() {
        return acIdaddress;
    }

    public void setAcIdaddress(String acIdaddress) {
        this.acIdaddress = acIdaddress == null ? null : acIdaddress.trim();
    }

    public Integer getAcCancelstatus() {
        return acCancelstatus;
    }

    public void setAcCancelstatus(Integer acCancelstatus) {
        this.acCancelstatus = acCancelstatus;
    }
}