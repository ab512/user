package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Lapower extends BaseEntity {


    private String lpCode;

    private String lpDescription;

    private Integer lpPlat;



    public String getLpCode() {
        return lpCode;
    }

    public void setLpCode(String lpCode) {
        this.lpCode = lpCode == null ? null : lpCode.trim();
    }

    public String getLpDescription() {
        return lpDescription;
    }

    public void setLpDescription(String lpDescription) {
        this.lpDescription = lpDescription == null ? null : lpDescription.trim();
    }

    public Integer getLpPlat() {
        return lpPlat;
    }

    public void setLpPlat(Integer lpPlat) {
        this.lpPlat = lpPlat;
    }
}