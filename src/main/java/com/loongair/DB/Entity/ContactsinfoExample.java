package com.loongair.DB.Entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ContactsinfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ContactsinfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTidIsNull() {
            addCriterion("Tid is null");
            return (Criteria) this;
        }

        public Criteria andTidIsNotNull() {
            addCriterion("Tid is not null");
            return (Criteria) this;
        }

        public Criteria andTidEqualTo(Long value) {
            addCriterion("Tid =", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotEqualTo(Long value) {
            addCriterion("Tid <>", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThan(Long value) {
            addCriterion("Tid >", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThanOrEqualTo(Long value) {
            addCriterion("Tid >=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThan(Long value) {
            addCriterion("Tid <", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThanOrEqualTo(Long value) {
            addCriterion("Tid <=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidIn(List<Long> values) {
            addCriterion("Tid in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotIn(List<Long> values) {
            addCriterion("Tid not in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidBetween(Long value1, Long value2) {
            addCriterion("Tid between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotBetween(Long value1, Long value2) {
            addCriterion("Tid not between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNull() {
            addCriterion("CreateId is null");
            return (Criteria) this;
        }

        public Criteria andCreateidIsNotNull() {
            addCriterion("CreateId is not null");
            return (Criteria) this;
        }

        public Criteria andCreateidEqualTo(Long value) {
            addCriterion("CreateId =", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotEqualTo(Long value) {
            addCriterion("CreateId <>", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThan(Long value) {
            addCriterion("CreateId >", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidGreaterThanOrEqualTo(Long value) {
            addCriterion("CreateId >=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThan(Long value) {
            addCriterion("CreateId <", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidLessThanOrEqualTo(Long value) {
            addCriterion("CreateId <=", value, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidIn(List<Long> values) {
            addCriterion("CreateId in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotIn(List<Long> values) {
            addCriterion("CreateId not in", values, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidBetween(Long value1, Long value2) {
            addCriterion("CreateId between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andCreateidNotBetween(Long value1, Long value2) {
            addCriterion("CreateId not between", value1, value2, "createid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNull() {
            addCriterion("ProxyUserId is null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIsNotNull() {
            addCriterion("ProxyUserId is not null");
            return (Criteria) this;
        }

        public Criteria andProxyuseridEqualTo(Long value) {
            addCriterion("ProxyUserId =", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotEqualTo(Long value) {
            addCriterion("ProxyUserId <>", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThan(Long value) {
            addCriterion("ProxyUserId >", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridGreaterThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId >=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThan(Long value) {
            addCriterion("ProxyUserId <", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridLessThanOrEqualTo(Long value) {
            addCriterion("ProxyUserId <=", value, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridIn(List<Long> values) {
            addCriterion("ProxyUserId in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotIn(List<Long> values) {
            addCriterion("ProxyUserId not in", values, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andProxyuseridNotBetween(Long value1, Long value2) {
            addCriterion("ProxyUserId not between", value1, value2, "proxyuserid");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNull() {
            addCriterion("LastGuid is null");
            return (Criteria) this;
        }

        public Criteria andLastguidIsNotNull() {
            addCriterion("LastGuid is not null");
            return (Criteria) this;
        }

        public Criteria andLastguidEqualTo(Long value) {
            addCriterion("LastGuid =", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotEqualTo(Long value) {
            addCriterion("LastGuid <>", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThan(Long value) {
            addCriterion("LastGuid >", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidGreaterThanOrEqualTo(Long value) {
            addCriterion("LastGuid >=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThan(Long value) {
            addCriterion("LastGuid <", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidLessThanOrEqualTo(Long value) {
            addCriterion("LastGuid <=", value, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidIn(List<Long> values) {
            addCriterion("LastGuid in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotIn(List<Long> values) {
            addCriterion("LastGuid not in", values, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidBetween(Long value1, Long value2) {
            addCriterion("LastGuid between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andLastguidNotBetween(Long value1, Long value2) {
            addCriterion("LastGuid not between", value1, value2, "lastguid");
            return (Criteria) this;
        }

        public Criteria andCloneIsNull() {
            addCriterion("Clone is null");
            return (Criteria) this;
        }

        public Criteria andCloneIsNotNull() {
            addCriterion("Clone is not null");
            return (Criteria) this;
        }

        public Criteria andCloneEqualTo(Integer value) {
            addCriterion("Clone =", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotEqualTo(Integer value) {
            addCriterion("Clone <>", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThan(Integer value) {
            addCriterion("Clone >", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneGreaterThanOrEqualTo(Integer value) {
            addCriterion("Clone >=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThan(Integer value) {
            addCriterion("Clone <", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneLessThanOrEqualTo(Integer value) {
            addCriterion("Clone <=", value, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneIn(List<Integer> values) {
            addCriterion("Clone in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotIn(List<Integer> values) {
            addCriterion("Clone not in", values, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneBetween(Integer value1, Integer value2) {
            addCriterion("Clone between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andCloneNotBetween(Integer value1, Integer value2) {
            addCriterion("Clone not between", value1, value2, "clone");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNull() {
            addCriterion("UpdateId is null");
            return (Criteria) this;
        }

        public Criteria andUpdateidIsNotNull() {
            addCriterion("UpdateId is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateidEqualTo(Long value) {
            addCriterion("UpdateId =", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotEqualTo(Long value) {
            addCriterion("UpdateId <>", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThan(Long value) {
            addCriterion("UpdateId >", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidGreaterThanOrEqualTo(Long value) {
            addCriterion("UpdateId >=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThan(Long value) {
            addCriterion("UpdateId <", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidLessThanOrEqualTo(Long value) {
            addCriterion("UpdateId <=", value, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidIn(List<Long> values) {
            addCriterion("UpdateId in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotIn(List<Long> values) {
            addCriterion("UpdateId not in", values, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidBetween(Long value1, Long value2) {
            addCriterion("UpdateId between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdateidNotBetween(Long value1, Long value2) {
            addCriterion("UpdateId not between", value1, value2, "updateid");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("UpdateDate is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("UpdateDate is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("UpdateDate =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("UpdateDate <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("UpdateDate >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("UpdateDate >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("UpdateDate <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("UpdateDate <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("UpdateDate in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("UpdateDate not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("UpdateDate between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("UpdateDate not between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("Status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("Status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("Status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("Status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("Status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("Status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("Status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("Status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("Status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("Status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("Status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("Status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andCiLausertidIsNull() {
            addCriterion("CI_LaUserTid is null");
            return (Criteria) this;
        }

        public Criteria andCiLausertidIsNotNull() {
            addCriterion("CI_LaUserTid is not null");
            return (Criteria) this;
        }

        public Criteria andCiLausertidEqualTo(Long value) {
            addCriterion("CI_LaUserTid =", value, "ciLausertid");
            return (Criteria) this;
        }

        public Criteria andCiLausertidNotEqualTo(Long value) {
            addCriterion("CI_LaUserTid <>", value, "ciLausertid");
            return (Criteria) this;
        }

        public Criteria andCiLausertidGreaterThan(Long value) {
            addCriterion("CI_LaUserTid >", value, "ciLausertid");
            return (Criteria) this;
        }

        public Criteria andCiLausertidGreaterThanOrEqualTo(Long value) {
            addCriterion("CI_LaUserTid >=", value, "ciLausertid");
            return (Criteria) this;
        }

        public Criteria andCiLausertidLessThan(Long value) {
            addCriterion("CI_LaUserTid <", value, "ciLausertid");
            return (Criteria) this;
        }

        public Criteria andCiLausertidLessThanOrEqualTo(Long value) {
            addCriterion("CI_LaUserTid <=", value, "ciLausertid");
            return (Criteria) this;
        }

        public Criteria andCiLausertidIn(List<Long> values) {
            addCriterion("CI_LaUserTid in", values, "ciLausertid");
            return (Criteria) this;
        }

        public Criteria andCiLausertidNotIn(List<Long> values) {
            addCriterion("CI_LaUserTid not in", values, "ciLausertid");
            return (Criteria) this;
        }

        public Criteria andCiLausertidBetween(Long value1, Long value2) {
            addCriterion("CI_LaUserTid between", value1, value2, "ciLausertid");
            return (Criteria) this;
        }

        public Criteria andCiLausertidNotBetween(Long value1, Long value2) {
            addCriterion("CI_LaUserTid not between", value1, value2, "ciLausertid");
            return (Criteria) this;
        }

        public Criteria andCiNameIsNull() {
            addCriterion("CI_Name is null");
            return (Criteria) this;
        }

        public Criteria andCiNameIsNotNull() {
            addCriterion("CI_Name is not null");
            return (Criteria) this;
        }

        public Criteria andCiNameEqualTo(String value) {
            addCriterion("CI_Name =", value, "ciName");
            return (Criteria) this;
        }

        public Criteria andCiNameNotEqualTo(String value) {
            addCriterion("CI_Name <>", value, "ciName");
            return (Criteria) this;
        }

        public Criteria andCiNameGreaterThan(String value) {
            addCriterion("CI_Name >", value, "ciName");
            return (Criteria) this;
        }

        public Criteria andCiNameGreaterThanOrEqualTo(String value) {
            addCriterion("CI_Name >=", value, "ciName");
            return (Criteria) this;
        }

        public Criteria andCiNameLessThan(String value) {
            addCriterion("CI_Name <", value, "ciName");
            return (Criteria) this;
        }

        public Criteria andCiNameLessThanOrEqualTo(String value) {
            addCriterion("CI_Name <=", value, "ciName");
            return (Criteria) this;
        }

        public Criteria andCiNameLike(String value) {
            addCriterion("CI_Name like", value, "ciName");
            return (Criteria) this;
        }

        public Criteria andCiNameNotLike(String value) {
            addCriterion("CI_Name not like", value, "ciName");
            return (Criteria) this;
        }

        public Criteria andCiNameIn(List<String> values) {
            addCriterion("CI_Name in", values, "ciName");
            return (Criteria) this;
        }

        public Criteria andCiNameNotIn(List<String> values) {
            addCriterion("CI_Name not in", values, "ciName");
            return (Criteria) this;
        }

        public Criteria andCiNameBetween(String value1, String value2) {
            addCriterion("CI_Name between", value1, value2, "ciName");
            return (Criteria) this;
        }

        public Criteria andCiNameNotBetween(String value1, String value2) {
            addCriterion("CI_Name not between", value1, value2, "ciName");
            return (Criteria) this;
        }

        public Criteria andCiMobileIsNull() {
            addCriterion("CI_Mobile is null");
            return (Criteria) this;
        }

        public Criteria andCiMobileIsNotNull() {
            addCriterion("CI_Mobile is not null");
            return (Criteria) this;
        }

        public Criteria andCiMobileEqualTo(String value) {
            addCriterion("CI_Mobile =", value, "ciMobile");
            return (Criteria) this;
        }

        public Criteria andCiMobileNotEqualTo(String value) {
            addCriterion("CI_Mobile <>", value, "ciMobile");
            return (Criteria) this;
        }

        public Criteria andCiMobileGreaterThan(String value) {
            addCriterion("CI_Mobile >", value, "ciMobile");
            return (Criteria) this;
        }

        public Criteria andCiMobileGreaterThanOrEqualTo(String value) {
            addCriterion("CI_Mobile >=", value, "ciMobile");
            return (Criteria) this;
        }

        public Criteria andCiMobileLessThan(String value) {
            addCriterion("CI_Mobile <", value, "ciMobile");
            return (Criteria) this;
        }

        public Criteria andCiMobileLessThanOrEqualTo(String value) {
            addCriterion("CI_Mobile <=", value, "ciMobile");
            return (Criteria) this;
        }

        public Criteria andCiMobileLike(String value) {
            addCriterion("CI_Mobile like", value, "ciMobile");
            return (Criteria) this;
        }

        public Criteria andCiMobileNotLike(String value) {
            addCriterion("CI_Mobile not like", value, "ciMobile");
            return (Criteria) this;
        }

        public Criteria andCiMobileIn(List<String> values) {
            addCriterion("CI_Mobile in", values, "ciMobile");
            return (Criteria) this;
        }

        public Criteria andCiMobileNotIn(List<String> values) {
            addCriterion("CI_Mobile not in", values, "ciMobile");
            return (Criteria) this;
        }

        public Criteria andCiMobileBetween(String value1, String value2) {
            addCriterion("CI_Mobile between", value1, value2, "ciMobile");
            return (Criteria) this;
        }

        public Criteria andCiMobileNotBetween(String value1, String value2) {
            addCriterion("CI_Mobile not between", value1, value2, "ciMobile");
            return (Criteria) this;
        }

        public Criteria andCiEmailIsNull() {
            addCriterion("CI_EMail is null");
            return (Criteria) this;
        }

        public Criteria andCiEmailIsNotNull() {
            addCriterion("CI_EMail is not null");
            return (Criteria) this;
        }

        public Criteria andCiEmailEqualTo(String value) {
            addCriterion("CI_EMail =", value, "ciEmail");
            return (Criteria) this;
        }

        public Criteria andCiEmailNotEqualTo(String value) {
            addCriterion("CI_EMail <>", value, "ciEmail");
            return (Criteria) this;
        }

        public Criteria andCiEmailGreaterThan(String value) {
            addCriterion("CI_EMail >", value, "ciEmail");
            return (Criteria) this;
        }

        public Criteria andCiEmailGreaterThanOrEqualTo(String value) {
            addCriterion("CI_EMail >=", value, "ciEmail");
            return (Criteria) this;
        }

        public Criteria andCiEmailLessThan(String value) {
            addCriterion("CI_EMail <", value, "ciEmail");
            return (Criteria) this;
        }

        public Criteria andCiEmailLessThanOrEqualTo(String value) {
            addCriterion("CI_EMail <=", value, "ciEmail");
            return (Criteria) this;
        }

        public Criteria andCiEmailLike(String value) {
            addCriterion("CI_EMail like", value, "ciEmail");
            return (Criteria) this;
        }

        public Criteria andCiEmailNotLike(String value) {
            addCriterion("CI_EMail not like", value, "ciEmail");
            return (Criteria) this;
        }

        public Criteria andCiEmailIn(List<String> values) {
            addCriterion("CI_EMail in", values, "ciEmail");
            return (Criteria) this;
        }

        public Criteria andCiEmailNotIn(List<String> values) {
            addCriterion("CI_EMail not in", values, "ciEmail");
            return (Criteria) this;
        }

        public Criteria andCiEmailBetween(String value1, String value2) {
            addCriterion("CI_EMail between", value1, value2, "ciEmail");
            return (Criteria) this;
        }

        public Criteria andCiEmailNotBetween(String value1, String value2) {
            addCriterion("CI_EMail not between", value1, value2, "ciEmail");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}