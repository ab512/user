package com.loongair.DB.Entity;

import com.loongair.DB.Entity.base.BaseEntity;

import java.util.Date;

public class Rolepowerrelation extends BaseEntity {


    private Long rprRoletid;

    private Long rprPowertid;



    public Long getRprRoletid() {
        return rprRoletid;
    }

    public void setRprRoletid(Long rprRoletid) {
        this.rprRoletid = rprRoletid;
    }

    public Long getRprPowertid() {
        return rprPowertid;
    }

    public void setRprPowertid(Long rprPowertid) {
        this.rprPowertid = rprPowertid;
    }
}