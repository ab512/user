package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Prefer;
import com.loongair.DB.Entity.PreferExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PreferMapper {
    int countByExample(PreferExample example);

    int deleteByExample(PreferExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Prefer record);

    int insertSelective(Prefer record);

    List<Prefer> selectByExample(PreferExample example);

    Prefer selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Prefer record, @Param("example") PreferExample example);

    int updateByExample(@Param("record") Prefer record, @Param("example") PreferExample example);

    int updateByPrimaryKeySelective(Prefer record);

    int updateByPrimaryKey(Prefer record);
}