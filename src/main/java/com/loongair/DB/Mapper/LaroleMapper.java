package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Larole;
import com.loongair.DB.Entity.LaroleExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface LaroleMapper {
    int countByExample(LaroleExample example);

    int deleteByExample(LaroleExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Larole record);

    int insertSelective(Larole record);

    List<Larole> selectByExample(LaroleExample example);

    Larole selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Larole record, @Param("example") LaroleExample example);

    int updateByExample(@Param("record") Larole record, @Param("example") LaroleExample example);

    int updateByPrimaryKeySelective(Larole record);

    int updateByPrimaryKey(Larole record);
}