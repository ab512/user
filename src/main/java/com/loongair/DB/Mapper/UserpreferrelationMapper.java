package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Userpreferrelation;
import com.loongair.DB.Entity.UserpreferrelationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserpreferrelationMapper {
    int countByExample(UserpreferrelationExample example);

    int deleteByExample(UserpreferrelationExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Userpreferrelation record);

    int insertSelective(Userpreferrelation record);

    List<Userpreferrelation> selectByExample(UserpreferrelationExample example);

    Userpreferrelation selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Userpreferrelation record, @Param("example") UserpreferrelationExample example);

    int updateByExample(@Param("record") Userpreferrelation record, @Param("example") UserpreferrelationExample example);

    int updateByPrimaryKeySelective(Userpreferrelation record);

    int updateByPrimaryKey(Userpreferrelation record);
}