package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Grouporgrelation;
import com.loongair.DB.Entity.GrouporgrelationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GrouporgrelationMapper {
    int countByExample(GrouporgrelationExample example);

    int deleteByExample(GrouporgrelationExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Grouporgrelation record);

    int insertSelective(Grouporgrelation record);

    List<Grouporgrelation> selectByExample(GrouporgrelationExample example);

    Grouporgrelation selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Grouporgrelation record, @Param("example") GrouporgrelationExample example);

    int updateByExample(@Param("record") Grouporgrelation record, @Param("example") GrouporgrelationExample example);

    int updateByPrimaryKeySelective(Grouporgrelation record);

    int updateByPrimaryKey(Grouporgrelation record);
}