package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Orguserrelation;
import com.loongair.DB.Entity.OrguserrelationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrguserrelationMapper {
    int countByExample(OrguserrelationExample example);

    int deleteByExample(OrguserrelationExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Orguserrelation record);

    int insertSelective(Orguserrelation record);

    List<Orguserrelation> selectByExample(OrguserrelationExample example);

    Orguserrelation selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Orguserrelation record, @Param("example") OrguserrelationExample example);

    int updateByExample(@Param("record") Orguserrelation record, @Param("example") OrguserrelationExample example);

    int updateByPrimaryKeySelective(Orguserrelation record);

    int updateByPrimaryKey(Orguserrelation record);
}