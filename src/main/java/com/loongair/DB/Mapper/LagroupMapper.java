package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Lagroup;
import com.loongair.DB.Entity.LagroupExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface LagroupMapper {
    int countByExample(LagroupExample example);

    int deleteByExample(LagroupExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Lagroup record);

    int insertSelective(Lagroup record);

    List<Lagroup> selectByExample(LagroupExample example);

    Lagroup selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Lagroup record, @Param("example") LagroupExample example);

    int updateByExample(@Param("record") Lagroup record, @Param("example") LagroupExample example);

    int updateByPrimaryKeySelective(Lagroup record);

    int updateByPrimaryKey(Lagroup record);
}