package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Rolepowerrelation;
import com.loongair.DB.Entity.RolepowerrelationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RolepowerrelationMapper {
    int countByExample(RolepowerrelationExample example);

    int deleteByExample(RolepowerrelationExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Rolepowerrelation record);

    int insertSelective(Rolepowerrelation record);

    List<Rolepowerrelation> selectByExample(RolepowerrelationExample example);

    Rolepowerrelation selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Rolepowerrelation record, @Param("example") RolepowerrelationExample example);

    int updateByExample(@Param("record") Rolepowerrelation record, @Param("example") RolepowerrelationExample example);

    int updateByPrimaryKeySelective(Rolepowerrelation record);

    int updateByPrimaryKey(Rolepowerrelation record);
}