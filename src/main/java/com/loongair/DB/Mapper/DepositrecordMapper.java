package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Depositrecord;
import com.loongair.DB.Entity.DepositrecordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DepositrecordMapper {
    int countByExample(DepositrecordExample example);

    int deleteByExample(DepositrecordExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Depositrecord record);

    int insertSelective(Depositrecord record);

    List<Depositrecord> selectByExample(DepositrecordExample example);

    Depositrecord selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Depositrecord record, @Param("example") DepositrecordExample example);

    int updateByExample(@Param("record") Depositrecord record, @Param("example") DepositrecordExample example);

    int updateByPrimaryKeySelective(Depositrecord record);

    int updateByPrimaryKey(Depositrecord record);
}