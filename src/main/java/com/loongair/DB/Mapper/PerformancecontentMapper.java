package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Performancecontent;
import com.loongair.DB.Entity.PerformancecontentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PerformancecontentMapper {
    int countByExample(PerformancecontentExample example);

    int deleteByExample(PerformancecontentExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Performancecontent record);

    int insertSelective(Performancecontent record);

    List<Performancecontent> selectByExample(PerformancecontentExample example);

    Performancecontent selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Performancecontent record, @Param("example") PerformancecontentExample example);

    int updateByExample(@Param("record") Performancecontent record, @Param("example") PerformancecontentExample example);

    int updateByPrimaryKeySelective(Performancecontent record);

    int updateByPrimaryKey(Performancecontent record);
}