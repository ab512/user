package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Contactsinfo;
import com.loongair.DB.Entity.ContactsinfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ContactsinfoMapper {
    int countByExample(ContactsinfoExample example);

    int deleteByExample(ContactsinfoExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Contactsinfo record);

    int insertSelective(Contactsinfo record);

    List<Contactsinfo> selectByExample(ContactsinfoExample example);

    Contactsinfo selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Contactsinfo record, @Param("example") ContactsinfoExample example);

    int updateByExample(@Param("record") Contactsinfo record, @Param("example") ContactsinfoExample example);

    int updateByPrimaryKeySelective(Contactsinfo record);

    int updateByPrimaryKey(Contactsinfo record);
}