package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Laorg;
import com.loongair.DB.Entity.LaorgExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface LaorgMapper {
    int countByExample(LaorgExample example);

    int deleteByExample(LaorgExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Laorg record);

    int insertSelective(Laorg record);

    List<Laorg> selectByExample(LaorgExample example);

    Laorg selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Laorg record, @Param("example") LaorgExample example);

    int updateByExample(@Param("record") Laorg record, @Param("example") LaorgExample example);

    int updateByPrimaryKeySelective(Laorg record);

    int updateByPrimaryKey(Laorg record);
}