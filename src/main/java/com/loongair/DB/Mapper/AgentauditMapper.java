package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Agentaudit;
import com.loongair.DB.Entity.AgentauditExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AgentauditMapper {
    int countByExample(AgentauditExample example);

    int deleteByExample(AgentauditExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Agentaudit record);

    int insertSelective(Agentaudit record);

    List<Agentaudit> selectByExample(AgentauditExample example);

    Agentaudit selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Agentaudit record, @Param("example") AgentauditExample example);

    int updateByExample(@Param("record") Agentaudit record, @Param("example") AgentauditExample example);

    int updateByPrimaryKeySelective(Agentaudit record);

    int updateByPrimaryKey(Agentaudit record);
}