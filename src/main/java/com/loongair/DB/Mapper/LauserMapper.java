package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Lauser;
import com.loongair.DB.Entity.LauserExample;
import java.util.List;

import com.loongair.DB.ModelEx.UserAddress;
import org.apache.ibatis.annotations.Param;

public interface LauserMapper {
    int countByExample(LauserExample example);

    int deleteByExample(LauserExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Lauser record);

    int insertSelective(Lauser record);

    List<Lauser> selectByExample(LauserExample example);

    Lauser selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Lauser record, @Param("example") LauserExample example);

    int updateByExample(@Param("record") Lauser record, @Param("example") LauserExample example);

    int updateByPrimaryKeySelective(Lauser record);

    int updateByPrimaryKey(Lauser record);
    UserAddress selectUserAddress(long Tid);
}