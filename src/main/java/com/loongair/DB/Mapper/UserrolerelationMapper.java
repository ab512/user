package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Userrolerelation;
import com.loongair.DB.Entity.UserrolerelationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserrolerelationMapper {
    int countByExample(UserrolerelationExample example);

    int deleteByExample(UserrolerelationExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Userrolerelation record);

    int insertSelective(Userrolerelation record);

    List<Userrolerelation> selectByExample(UserrolerelationExample example);

    Userrolerelation selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Userrolerelation record, @Param("example") UserrolerelationExample example);

    int updateByExample(@Param("record") Userrolerelation record, @Param("example") UserrolerelationExample example);

    int updateByPrimaryKeySelective(Userrolerelation record);

    int updateByPrimaryKey(Userrolerelation record);
}