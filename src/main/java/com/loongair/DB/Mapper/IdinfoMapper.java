package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Idinfo;
import com.loongair.DB.Entity.IdinfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface IdinfoMapper {
    int countByExample(IdinfoExample example);

    int deleteByExample(IdinfoExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Idinfo record);

    int insertSelective(Idinfo record);

    List<Idinfo> selectByExample(IdinfoExample example);

    Idinfo selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Idinfo record, @Param("example") IdinfoExample example);

    int updateByExample(@Param("record") Idinfo record, @Param("example") IdinfoExample example);

    int updateByPrimaryKeySelective(Idinfo record);

    int updateByPrimaryKey(Idinfo record);
}