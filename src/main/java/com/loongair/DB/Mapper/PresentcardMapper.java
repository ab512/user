package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Presentcard;
import com.loongair.DB.Entity.PresentcardExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PresentcardMapper {
    int countByExample(PresentcardExample example);

    int deleteByExample(PresentcardExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Presentcard record);

    int insertSelective(Presentcard record);

    List<Presentcard> selectByExample(PresentcardExample example);

    Presentcard selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Presentcard record, @Param("example") PresentcardExample example);

    int updateByExample(@Param("record") Presentcard record, @Param("example") PresentcardExample example);

    int updateByPrimaryKeySelective(Presentcard record);

    int updateByPrimaryKey(Presentcard record);
}