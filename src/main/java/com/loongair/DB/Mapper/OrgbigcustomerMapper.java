package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Orgbigcustomer;
import com.loongair.DB.Entity.OrgbigcustomerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrgbigcustomerMapper {
    int countByExample(OrgbigcustomerExample example);

    int deleteByExample(OrgbigcustomerExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Orgbigcustomer record);

    int insertSelective(Orgbigcustomer record);

    List<Orgbigcustomer> selectByExampleWithBLOBs(OrgbigcustomerExample example);

    List<Orgbigcustomer> selectByExample(OrgbigcustomerExample example);

    Orgbigcustomer selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Orgbigcustomer record, @Param("example") OrgbigcustomerExample example);

    int updateByExampleWithBLOBs(@Param("record") Orgbigcustomer record, @Param("example") OrgbigcustomerExample example);

    int updateByExample(@Param("record") Orgbigcustomer record, @Param("example") OrgbigcustomerExample example);

    int updateByPrimaryKeySelective(Orgbigcustomer record);

    int updateByPrimaryKeyWithBLOBs(Orgbigcustomer record);

    int updateByPrimaryKey(Orgbigcustomer record);
}