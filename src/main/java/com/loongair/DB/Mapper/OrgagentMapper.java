package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Orgagent;
import com.loongair.DB.Entity.OrgagentExample;
import com.loongair.DB.Entity.OrgagentWithBLOBs;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrgagentMapper {
    int countByExample(OrgagentExample example);

    int deleteByExample(OrgagentExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(OrgagentWithBLOBs record);

    int insertSelective(OrgagentWithBLOBs record);

    List<OrgagentWithBLOBs> selectByExampleWithBLOBs(OrgagentExample example);

    List<Orgagent> selectByExample(OrgagentExample example);

    OrgagentWithBLOBs selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") OrgagentWithBLOBs record, @Param("example") OrgagentExample example);

    int updateByExampleWithBLOBs(@Param("record") OrgagentWithBLOBs record, @Param("example") OrgagentExample example);

    int updateByExample(@Param("record") Orgagent record, @Param("example") OrgagentExample example);

    int updateByPrimaryKeySelective(OrgagentWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(OrgagentWithBLOBs record);

    int updateByPrimaryKey(Orgagent record);
}