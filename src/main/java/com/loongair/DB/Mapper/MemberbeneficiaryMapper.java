package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Memberbeneficiary;
import com.loongair.DB.Entity.MemberbeneficiaryExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MemberbeneficiaryMapper {
    int countByExample(MemberbeneficiaryExample example);

    int deleteByExample(MemberbeneficiaryExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Memberbeneficiary record);

    int insertSelective(Memberbeneficiary record);

    List<Memberbeneficiary> selectByExample(MemberbeneficiaryExample example);

    Memberbeneficiary selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Memberbeneficiary record, @Param("example") MemberbeneficiaryExample example);

    int updateByExample(@Param("record") Memberbeneficiary record, @Param("example") MemberbeneficiaryExample example);

    int updateByPrimaryKeySelective(Memberbeneficiary record);

    int updateByPrimaryKey(Memberbeneficiary record);
}