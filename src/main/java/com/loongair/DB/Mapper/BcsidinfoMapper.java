package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Bcsidinfo;
import com.loongair.DB.Entity.BcsidinfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BcsidinfoMapper {
    int countByExample(BcsidinfoExample example);

    int deleteByExample(BcsidinfoExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Bcsidinfo record);

    int insertSelective(Bcsidinfo record);

    List<Bcsidinfo> selectByExample(BcsidinfoExample example);

    Bcsidinfo selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Bcsidinfo record, @Param("example") BcsidinfoExample example);

    int updateByExample(@Param("record") Bcsidinfo record, @Param("example") BcsidinfoExample example);

    int updateByPrimaryKeySelective(Bcsidinfo record);

    int updateByPrimaryKey(Bcsidinfo record);
}