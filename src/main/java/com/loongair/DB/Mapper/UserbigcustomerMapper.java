package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Userbigcustomer;
import com.loongair.DB.Entity.UserbigcustomerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserbigcustomerMapper {
    int countByExample(UserbigcustomerExample example);

    int deleteByExample(UserbigcustomerExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Userbigcustomer record);

    int insertSelective(Userbigcustomer record);

    List<Userbigcustomer> selectByExample(UserbigcustomerExample example);

    Userbigcustomer selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Userbigcustomer record, @Param("example") UserbigcustomerExample example);

    int updateByExample(@Param("record") Userbigcustomer record, @Param("example") UserbigcustomerExample example);

    int updateByPrimaryKeySelective(Userbigcustomer record);

    int updateByPrimaryKey(Userbigcustomer record);
}