package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Wechatinfo;
import com.loongair.DB.Entity.WechatinfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WechatinfoMapper {
    int countByExample(WechatinfoExample example);

    int deleteByExample(WechatinfoExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Wechatinfo record);

    int insertSelective(Wechatinfo record);

    List<Wechatinfo> selectByExample(WechatinfoExample example);

    Wechatinfo selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Wechatinfo record, @Param("example") WechatinfoExample example);

    int updateByExample(@Param("record") Wechatinfo record, @Param("example") WechatinfoExample example);

    int updateByPrimaryKeySelective(Wechatinfo record);

    int updateByPrimaryKey(Wechatinfo record);
}