package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Salechannel;
import com.loongair.DB.Entity.SalechannelExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SalechannelMapper {
    int countByExample(SalechannelExample example);

    int deleteByExample(SalechannelExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Salechannel record);

    int insertSelective(Salechannel record);

    List<Salechannel> selectByExample(SalechannelExample example);

    Salechannel selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Salechannel record, @Param("example") SalechannelExample example);

    int updateByExample(@Param("record") Salechannel record, @Param("example") SalechannelExample example);

    int updateByPrimaryKeySelective(Salechannel record);

    int updateByPrimaryKey(Salechannel record);
}