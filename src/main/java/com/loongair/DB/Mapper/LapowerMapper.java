package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Lapower;
import com.loongair.DB.Entity.LapowerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface LapowerMapper {
    int countByExample(LapowerExample example);

    int deleteByExample(LapowerExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Lapower record);

    int insertSelective(Lapower record);

    List<Lapower> selectByExample(LapowerExample example);

    Lapower selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Lapower record, @Param("example") LapowerExample example);

    int updateByExample(@Param("record") Lapower record, @Param("example") LapowerExample example);

    int updateByPrimaryKeySelective(Lapower record);

    int updateByPrimaryKey(Lapower record);
}