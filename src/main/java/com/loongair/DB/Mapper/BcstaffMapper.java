package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Bcstaff;
import com.loongair.DB.Entity.BcstaffExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BcstaffMapper {
    int countByExample(BcstaffExample example);

    int deleteByExample(BcstaffExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Bcstaff record);

    int insertSelective(Bcstaff record);

    List<Bcstaff> selectByExample(BcstaffExample example);

    Bcstaff selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Bcstaff record, @Param("example") BcstaffExample example);

    int updateByExample(@Param("record") Bcstaff record, @Param("example") BcstaffExample example);

    int updateByPrimaryKeySelective(Bcstaff record);

    int updateByPrimaryKey(Bcstaff record);
}