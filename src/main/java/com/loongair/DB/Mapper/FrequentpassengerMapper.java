package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Frequentpassenger;
import com.loongair.DB.Entity.FrequentpassengerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface FrequentpassengerMapper {
    int countByExample(FrequentpassengerExample example);

    int deleteByExample(FrequentpassengerExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Frequentpassenger record);

    int insertSelective(Frequentpassenger record);

    List<Frequentpassenger> selectByExample(FrequentpassengerExample example);

    Frequentpassenger selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Frequentpassenger record, @Param("example") FrequentpassengerExample example);

    int updateByExample(@Param("record") Frequentpassenger record, @Param("example") FrequentpassengerExample example);

    int updateByPrimaryKeySelective(Frequentpassenger record);

    int updateByPrimaryKey(Frequentpassenger record);
}