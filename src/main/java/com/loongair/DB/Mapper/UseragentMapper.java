package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Useragent;
import com.loongair.DB.Entity.UseragentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UseragentMapper {
    int countByExample(UseragentExample example);

    int deleteByExample(UseragentExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Useragent record);

    int insertSelective(Useragent record);

    List<Useragent> selectByExample(UseragentExample example);

    Useragent selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Useragent record, @Param("example") UseragentExample example);

    int updateByExample(@Param("record") Useragent record, @Param("example") UseragentExample example);

    int updateByPrimaryKeySelective(Useragent record);

    int updateByPrimaryKey(Useragent record);
}