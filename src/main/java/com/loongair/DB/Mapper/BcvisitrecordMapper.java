package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Bcvisitrecord;
import com.loongair.DB.Entity.BcvisitrecordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BcvisitrecordMapper {
    int countByExample(BcvisitrecordExample example);

    int deleteByExample(BcvisitrecordExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Bcvisitrecord record);

    int insertSelective(Bcvisitrecord record);

    List<Bcvisitrecord> selectByExample(BcvisitrecordExample example);

    Bcvisitrecord selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Bcvisitrecord record, @Param("example") BcvisitrecordExample example);

    int updateByExample(@Param("record") Bcvisitrecord record, @Param("example") BcvisitrecordExample example);

    int updateByPrimaryKeySelective(Bcvisitrecord record);

    int updateByPrimaryKey(Bcvisitrecord record);
}