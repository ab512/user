package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Agentcancelaudit;
import com.loongair.DB.Entity.AgentcancelauditExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AgentcancelauditMapper {
    int countByExample(AgentcancelauditExample example);

    int deleteByExample(AgentcancelauditExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Agentcancelaudit record);

    int insertSelective(Agentcancelaudit record);

    List<Agentcancelaudit> selectByExample(AgentcancelauditExample example);

    Agentcancelaudit selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Agentcancelaudit record, @Param("example") AgentcancelauditExample example);

    int updateByExample(@Param("record") Agentcancelaudit record, @Param("example") AgentcancelauditExample example);

    int updateByPrimaryKeySelective(Agentcancelaudit record);

    int updateByPrimaryKey(Agentcancelaudit record);
}