package com.loongair.DB.Mapper;

import com.loongair.DB.Entity.Userfrepassenger;
import com.loongair.DB.Entity.UserfrepassengerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserfrepassengerMapper {
    int countByExample(UserfrepassengerExample example);

    int deleteByExample(UserfrepassengerExample example);

    int deleteByPrimaryKey(Long tid);

    int insert(Userfrepassenger record);

    int insertSelective(Userfrepassenger record);

    List<Userfrepassenger> selectByExample(UserfrepassengerExample example);

    Userfrepassenger selectByPrimaryKey(Long tid);

    int updateByExampleSelective(@Param("record") Userfrepassenger record, @Param("example") UserfrepassengerExample example);

    int updateByExample(@Param("record") Userfrepassenger record, @Param("example") UserfrepassengerExample example);

    int updateByPrimaryKeySelective(Userfrepassenger record);

    int updateByPrimaryKey(Userfrepassenger record);
}