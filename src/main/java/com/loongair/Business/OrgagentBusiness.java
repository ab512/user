package com.loongair.Business;

import com.loongair.DB.Entity.Orgagent;
import com.loongair.Model.AddAgentPara;
import com.loongair.Model.Enum;
import com.loongair.Service.OrgagentService;
import com.loongair.Util.IdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import java.util.Date;

/**
 * Create By Hong Cui
 * 2017/10/30
 */
@Component
public class OrgagentBusiness{
    @Autowired
    private OrgagentService orgagentService;

    /**
     * 添加代理人机构
     * @param para
     */
    public void AddOrgAgent(AddAgentPara para) {
        Orgagent model = new Orgagent();
        String s=para.AC_OfficeCode;
        System.out.println(s);
        model.setTid(IdGenerator.getInstance().nextId());
        model.setCreateid(para.LoginUser.UserId);
        model.setUpdateid(para.LoginUser.UserId);
        model.setUpdatedate(new Date());
        model.setStatus(Enum.ActiveStatus.Enable);
        model.setAcOfficecode(para.AC_OfficeCode);
        model.setAcAgentcompanyname(para.AC_AgentCompanyName);
        model.setAcIatano(para.AC_IATANO);
        model.setAcBelongdepartment(para.AC_BelongDepartment+"");
        model.setAcAgentusertype(para.AC_AgentUserType);
        model.setStatus(para.Status);
        model.setAcMoneytype(para.AC_MoneyType);
        model.setAcCountry(para.AC_Country+"");
        model.setAcCity(para.AC_City);
        model.setAcPermission(para.AC_Permission);
        model.setAcIagencyrate(para.AC_IAgencyRate);
        model.setAcDagencyrate(para.AC_DAgencyRate);
        model.setAcTeamcode(para.AC_TeamReservationCode);
        model.setAcContact(para.AC_Contact);
        model.setAcTelephone(para.AC_Telephone);
        model.setAcContactphone(para.AC_ContactPhone);
        model.setAcEmail(para.AC_Email);
        model.setAcAddress(para.AC_Address);
        model.setAcZipcode(para.AC_ZipCode);
        model.setAcFax(para.AC_Fax);
//        orgagentService.insert(model);
    }
}