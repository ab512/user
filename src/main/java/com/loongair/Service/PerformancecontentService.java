package com.loongair.Service;

import com.loongair.DB.Entity.Performancecontent;
import com.loongair.DB.Mapper.PerformancecontentMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PerformancecontentService extends BaseService<Performancecontent> {

    @Autowired
    private PerformancecontentMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}
