package com.loongair.Service;

import com.loongair.DB.Entity.Contactsinfo;
import com.loongair.DB.Mapper.ContactsinfoMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Create By Hong Cui
 * 2017/10/30
 */
@Component
public class ContactsinfoService extends BaseService<Contactsinfo> {
    @Autowired
    private ContactsinfoMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}