package com.loongair.Service;

import com.loongair.DB.Entity.*;
import com.loongair.DB.Mapper.AddressMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Create By Hong Cui
 * 2017/10/30
 */
@Component
public class AddressService extends BaseService<Address> {
    @Autowired
    private AddressMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}
