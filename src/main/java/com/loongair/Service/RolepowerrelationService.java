package com.loongair.Service;

import com.loongair.DB.Entity.Rolepowerrelation;
import com.loongair.DB.Mapper.RolepowerrelationMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RolepowerrelationService extends BaseService<Rolepowerrelation> {

    @Autowired
    private RolepowerrelationMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}
