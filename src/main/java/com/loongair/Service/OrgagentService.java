package com.loongair.Service;

import com.loongair.DB.Entity.Orgagent;
import com.loongair.DB.Mapper.OrgagentMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrgagentService extends BaseService<Orgagent> {

    @Autowired
    private OrgagentMapper modelMapper;


    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}
