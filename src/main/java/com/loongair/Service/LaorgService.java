package com.loongair.Service;

import com.loongair.DB.Entity.Laorg;
import com.loongair.DB.Mapper.LaorgMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Create By Hong Cui
 * 2017/10/30
 */
@Component
public class LaorgService extends BaseService<Laorg> {
    @Autowired
    private LaorgMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}