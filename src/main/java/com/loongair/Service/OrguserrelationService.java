package com.loongair.Service;

import com.loongair.DB.Entity.Orguserrelation;
import com.loongair.DB.Mapper.OrguserrelationMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrguserrelationService extends BaseService<Orguserrelation> {

    @Autowired
    private OrguserrelationMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}
