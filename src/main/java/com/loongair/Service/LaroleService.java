package com.loongair.Service;

import com.loongair.DB.Entity.Larole;
import com.loongair.DB.Mapper.LaroleMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Create By Hong Cui
 * 2017/10/30
 */
@Component
public class LaroleService extends BaseService<Larole> {
    @Autowired
    private LaroleMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}