package com.loongair.Service;

import com.loongair.DB.Entity.Lauser;
import com.loongair.DB.Entity.LauserExample;
import com.loongair.DB.Mapper.LauserMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LauserService extends BaseService<Lauser> {
    @Autowired
    LauserMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }


}
