package com.loongair.Service.base;

import com.loongair.DB.Entity.base.BaseEntity;
import com.loongair.Model.base.LoginUser;
import com.loongair.Model.base.UISearchPages;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Create By Hong Cui
 * 2017-11-8
 */
public abstract class BaseService<T> {
    protected Object mapper;
    private Map<String,Method> methodMap;
    protected abstract Object setMapper();

    protected void getMapper() {
        if(mapper==null)
        {
            mapper= setMapper();
        }
    }

    public List<T> getList(Object example, UISearchPages pages){
        getMapper();
        int start=(pages.NowPage-1)*pages.OnePageCount;
        int end=start+pages.OnePageCount;
        try {
            Method select=mapper.getClass().getDeclaredMethod("selectByExample",example.getClass());
            Method limit=example.getClass().getDeclaredMethod("setOrderByClause",String.class);
            Method count=mapper.getClass().getDeclaredMethod("countByExample",example.getClass());
            pages.DataCount= (int) count.invoke(mapper,example);
            pages.PageCount=pages.DataCount/pages.OnePageCount;
            limit.invoke(example,"Tid ASC limit "+start+","+end);
            return (List<T>) select.invoke(mapper,example);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }
    public void insert(T obj, Long userId)
    {
        getMapper();
        try {
            Method insert=mapper.getClass().getDeclaredMethod("insertSelective");
            BaseEntity entity = (BaseEntity)obj;
            entity.setCreateid(userId);
            entity.setUpdatedate(new Date());
            entity.setUpdateid(userId);
            insert.invoke(mapper,obj);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
    public void update(T obj,Object example,Long userId)
    {
        getMapper();
        try {
            Method update=mapper.getClass().getDeclaredMethod("updateByExampleSelective",obj.getClass(),example.getClass());
            BaseEntity entity = (BaseEntity)obj;
            entity.setUpdatedate(new Date());
            entity.setUpdateid(userId);
            update.invoke(mapper,obj,example);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
    public void updateByTid(T obj,long userId)
    {
        getMapper();
        try {
            Method update=mapper.getClass().getDeclaredMethod("updateByPrimaryKeySelective",obj.getClass());
            BaseEntity entity = (BaseEntity)obj;
            entity.setUpdatedate(new Date());
            entity.setUpdateid(userId);
            update.invoke(mapper,obj);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
    public List<T> select(Object example){
        getMapper();
        try {
            Method select=mapper.getClass().getDeclaredMethod("selectByExample",example.getClass());
            return (List<T>) select.invoke(mapper,example);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }
}
