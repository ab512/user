package com.loongair.Service;

import com.loongair.DB.Entity.Presentcard;
import com.loongair.DB.Mapper.PresentcardMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PresentcardService extends BaseService<Presentcard> {

    @Autowired
    private PresentcardMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}
