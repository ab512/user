package com.loongair.Service;

import com.loongair.DB.Entity.Orgbigcustomer;
import com.loongair.DB.Mapper.OrgbigcustomerMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrgbigcustomerService extends BaseService<Orgbigcustomer> {

    @Autowired
    private OrgbigcustomerMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}
