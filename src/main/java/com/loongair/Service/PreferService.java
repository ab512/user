package com.loongair.Service;

import com.loongair.DB.Entity.Prefer;
import com.loongair.DB.Mapper.PreferMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PreferService extends BaseService<Prefer> {

    @Autowired
    private PreferMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}
