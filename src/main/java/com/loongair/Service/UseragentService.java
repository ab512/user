package com.loongair.Service;

import com.loongair.DB.Entity.Useragent;
import com.loongair.DB.Mapper.UseragentMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UseragentService extends BaseService<Useragent> {

    @Autowired
    private UseragentMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}
