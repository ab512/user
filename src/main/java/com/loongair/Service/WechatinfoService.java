package com.loongair.Service;

import com.loongair.DB.Entity.Wechatinfo;
import com.loongair.DB.Mapper.WechatinfoMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WechatinfoService extends BaseService<Wechatinfo> {

    @Autowired
    private WechatinfoMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}
