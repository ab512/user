package com.loongair.Service;

import com.loongair.DB.Entity.Depositrecord;
import com.loongair.DB.Mapper.DepositrecordMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Create By Hong Cui
 * 2017/10/30
 */
@Component
public class DepositrecordService extends BaseService<Depositrecord> {
    @Autowired
    private DepositrecordMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}