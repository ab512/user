package com.loongair.Service;

import com.loongair.DB.Entity.Lagroup;
import com.loongair.DB.Mapper.LagroupMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Create By Hong Cui
 * 2017/10/30
 */
@Component
public class LagroupService extends BaseService<Lagroup> {
    @Autowired
    private LagroupMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}