package com.loongair.Service;

import com.loongair.DB.Entity.Bcvisitrecord;
import com.loongair.DB.Mapper.BcvisitrecordMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Create By Hong Cui
 * 2017/10/30
 */
@Component
public class BcvisitrecordService extends BaseService<Bcvisitrecord> {
    @Autowired
    private BcvisitrecordMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}