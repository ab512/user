package com.loongair.Service;

import com.loongair.DB.Entity.Performance;
import com.loongair.DB.Mapper.PerformanceMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PerformanceService extends BaseService<Performance> {

    @Autowired
    private PerformanceMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}
