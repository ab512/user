package com.loongair.Service;

import com.loongair.DB.Entity.Userbigcustomer;
import com.loongair.DB.Mapper.UserbigcustomerMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserbigcustomerService extends BaseService<Userbigcustomer> {

    @Autowired
    private UserbigcustomerMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}
