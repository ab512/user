package com.loongair.Service;

import com.loongair.DB.Entity.Agentcancelaudit;
import com.loongair.DB.Mapper.AgentcancelauditMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Create By Hong Cui
 * 2017/10/30
 */
@Component
public class AgentcancelauditService extends BaseService<Agentcancelaudit> {
    @Autowired
    private AgentcancelauditMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}