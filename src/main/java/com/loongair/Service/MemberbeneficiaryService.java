package com.loongair.Service;

import com.loongair.DB.Entity.Memberbeneficiary;
import com.loongair.DB.Mapper.MemberbeneficiaryMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MemberbeneficiaryService extends BaseService<Memberbeneficiary> {
    @Autowired
    private MemberbeneficiaryMapper modelMapper;


    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}
