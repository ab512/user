package com.loongair.Service;

import com.loongair.DB.Entity.Grouporgrelation;
import com.loongair.DB.Mapper.GrouporgrelationMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Create By Hong Cui
 * 2017/10/30
 */
@Component
public class GrouporgrelationService extends BaseService<Grouporgrelation> {
    @Autowired
    private GrouporgrelationMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}