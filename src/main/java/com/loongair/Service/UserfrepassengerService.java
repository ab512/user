package com.loongair.Service;

import com.loongair.DB.Entity.Userfrepassenger;
import com.loongair.DB.Mapper.UserfrepassengerMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserfrepassengerService extends BaseService<Userfrepassenger> {

    @Autowired
    private UserfrepassengerMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}
