package com.loongair.Service;

import com.loongair.DB.Entity.Frequentpassenger;
import com.loongair.DB.Mapper.FrequentpassengerMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Create By Hong Cui
 * 2017/10/30
 */
@Component
public class FrequentpassengerService extends BaseService<Frequentpassenger> {
    @Autowired
    private FrequentpassengerMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}