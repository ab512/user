package com.loongair.Service;

import com.loongair.DB.Entity.Organization;
import com.loongair.DB.Mapper.OrganizationMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrganizationService extends BaseService<Organization> {

    @Autowired
    private OrganizationMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}
