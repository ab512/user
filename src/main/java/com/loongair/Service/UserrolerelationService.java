package com.loongair.Service;

import com.loongair.DB.Entity.Userrolerelation;
import com.loongair.DB.Mapper.UserrolerelationMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserrolerelationService extends BaseService<Userrolerelation> {

    @Autowired
    private UserrolerelationMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}
