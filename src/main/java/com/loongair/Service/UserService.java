package com.loongair.Service;

import com.loongair.DB.Entity.Lauser;
import com.loongair.DB.Entity.LauserExample;
import com.loongair.DB.Mapper.LauserMapper;
import com.loongair.DB.ModelEx.UserAddress;
import com.loongair.Model.GetUserAddressPara;
import com.loongair.Model.GetUserByLoginNameAndPasswordPara;
import com.loongair.Service.base.BaseService;
import com.loongair.Util.StreamUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Create By Hong Cui
 * 2017-10-31
 */
@Component
public class UserService extends BaseService<Lauser> {
    @Autowired
    private LauserMapper modelMapper;

    //    通过用户名和密码获取用户
    public Lauser getUserByLoginNameAndPassword(GetUserByLoginNameAndPasswordPara para)
    {
        LauserExample le=new LauserExample();
        le.createCriteria().andULoginnameEqualTo(para.loginName);
        List<Lauser> users=modelMapper.selectByExample(le);
        Lauser user=users.get(0);
        String password=para.password+user.getTid().toString()+user.getTid()%190121;
        password=password.toUpperCase();
        password= StreamUtil.ToDBC(password);
        password=DigestUtils.md5Hex(password);
        if(password.toUpperCase().equals(user.getuPassword()))
            return user;
        return null;
    }
//    获得用户地址视图
    public UserAddress getUserAddress(GetUserAddressPara para)
    {
        return modelMapper.selectUserAddress(para.userTid);
    }

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}
