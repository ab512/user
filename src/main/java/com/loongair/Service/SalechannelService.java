package com.loongair.Service;

import com.loongair.DB.Entity.Salechannel;
import com.loongair.DB.Mapper.SalechannelMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SalechannelService extends BaseService<Salechannel> {

    @Autowired
    private SalechannelMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}
