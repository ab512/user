package com.loongair.Service;

import com.loongair.DB.Entity.Bcsidinfo;
import com.loongair.DB.Mapper.BcsidinfoMapper;
import com.loongair.Service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Create By Hong Cui
 * 2017/10/30
 */
@Component
public class BcsidinfoService extends BaseService<Bcsidinfo> {
    @Autowired
    private BcsidinfoMapper modelMapper;

    @Override
    protected Object setMapper() {
        return modelMapper;
    }
}